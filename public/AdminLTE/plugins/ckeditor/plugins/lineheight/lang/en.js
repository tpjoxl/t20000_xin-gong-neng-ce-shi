﻿/**
 * 支持中文的语言包(文件名称zh-cn.js)，第一个参数是插件名称
 * 作者：中成网站建设 www.csccd.net
 */
CKEDITOR.plugins.setLang('lineheight', 'en', {
lineHeight: {
		label: 'Line height',
		voiceLabel: 'Line height',
		panelTitle: 'Line height'
	},
    label: 'Line height',
    panelTitle: 'Line height',
    voiceLabel: 'Line height'
});
