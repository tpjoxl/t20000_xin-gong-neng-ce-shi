﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addTemplates('default', {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates') + 'templates/images/'),
    templates: [{
            title: '表格範本1',
            image: 'tp1.png',
            description: '條列式表格，只有水平框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '表格範本2',
            image: 'tp2.png',
            description: '條列式表格，欄位四周皆有框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-bordered table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '表格範本3',
            image: 'tp3.png',
            description: '條列式表格，欄位間隔以灰階背景區分，只有水平框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-striped table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '表格範本4',
            image: 'tp4.png',
            description: '條列式表格，欄位間隔以灰階背景區分，欄位四周皆有框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-bordered table-striped table-hover"> <thead> <tr> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '表格範本5',
            image: 'tp5.png',
            description: '雙標題條列式表格，欄位間隔以灰階背景區分，只有水平框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-striped table-hover"> <thead> <tr> <th>#</th> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">2</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">3</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '表格範本6',
            image: 'tp6.png',
            description: '雙標題條列式表格，欄位間隔以灰階背景區分，欄位四周皆有框線。',
            html: '<div class="table-responsive"> <table class="editor-table table-striped table-bordered table-hover"> <thead> <tr> <th>#</th> <th>表格標題</th> <th>表格標題</th> <th>表格標題</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">2</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> <tr> <th scope="row">3</th> <td>文字</td> <td>文字</td> <td>文字</td> </tr> </tbody> </table> </div>'
        },
        {
            title: '段落範本1',
            image: 'tp7.png',
            description: 'RWD 2欄排版，768px以下為1欄排版',
            html: '<div class="row editor-row"> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
        },
        {
            title: '段落範本2',
            image: 'tp8.png',
            description: 'RWD 3欄排版，768px以下為1欄排版',
            html: '<div class="row editor-row"> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-4"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
        },
        {
            title: '段落範本3',
            image: 'tp9.png',
            description: 'RWD 4欄排版，992px以下為2欄排版，768px以下為1欄排版',
            html: '<div class="row editor-row"> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-md-3 col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
        },
        {
            title: '段落範本4',
            image: 'tp10.png',
            description: 'RWD 6欄排版，1200px以下為3欄排版，992px以下為2欄排版，768px以下為1欄排版',
            html: '<div class="row editor-row"> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
        },
        {
            title: '段落範本5',
            image: 'tp5.gif',
            description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
            html: '<div class="row editor-row"> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div></div> <div class="col-sm-6"> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> </div>'
        },
        {
            title: '段落範本6',
            image: 'tp6.gif',
            description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
            html: '<div class="row editor-row"> <div class="col-sm-6"> <h3 class="title">標題文字</h3> <p>內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字</p> </div> <div class="col-sm-6"> <div style="text-align:center;"><img src="http://fakeimg.pl/600x400/?text=NO IMAGE" alt=""></div> </div> </div>'
        },
        {
            title: '段落範本7',
            image: 'tp11.png',
            description: '圖片置左，文字資訊跟隨圖片右側，有標題及內容',
            html: '<div class="editor-media-box"> <div class="media-left"><img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWI5ZjFhNWFlZCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1YjlmMWE1YWVkIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"></div> <div class="media-body"> <h4 class="media-heading">標題文字</h4> 內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字 </div> </div>'
        },
        {
            title: '段落範本8',
            image: 'tp12.png',
            description: '文字資訊置左，圖片跟隨右側，有標題及內容',
            html: '<div class="editor-media-box"> <div class="media-body"> <h4 class="media-heading">標題文字</h4> 內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字內容文字 </div> <div class="media-right"><img alt="64x64" class="media-object" data-src="holder.js/64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWI5ZjFhNWFlZCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1YjlmMWE1YWVkIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true"></div> </div>'
        },
        {
            title: '圖片展示範本1',
            image: 'img_tmp1.png',
            description: '圖片展示(1*4)',
            html: '<div class="editor-img-layout x4"><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div></div>'
        },
        {
            title: '圖片展示範本2',
            image: 'img_tmp2.png',
            description: '圖片展示(1*3)',
            html: '<div class="editor-img-layout x3"><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div></div>'
        },
        {
            title: '圖片展示範本3',
            image: 'img_tmp3.png',
            description: '圖片展示(1*2)',
            html: '<div class="editor-img-layout x2"><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div><div class="item"><img alt="" src="http://fakeimg.pl/600x400/?text=NO IMAGE" /></div></div>'
        },
    ]
});
