$(document).ready(function() {
  // 面板 .submission-form-body
  // 動態列 .dynamic-rows
  // 動態列區域 .num-holder
  // 新增按鈕 .add-dynamic
  // 刪除鍵 .remove
  $('.submission-form-body').on('click', '.add-dynamic', function(event) {
    event.preventDefault();
    var parent = $(this).parents('.submission-form-body');
    var dynamicLast = parent.find('.dynamic-rows').last();
    // fieldName
    var fieldName = dynamicLast.find('.form-control').last().attr('name'); // *
    var fieldNameMatch = fieldName.match(/\[(.*?)\]/g);
    var fieldIndexMatch = fieldNameMatch[0].match(/\[(.*)\]/);
    var fieldIndex = parseInt(fieldIndexMatch?fieldIndexMatch[1]:0) + 1;
    var fieldNamePrefix = fieldName.substring(0, fieldName.indexOf('['));
    var $clone = dynamicLast.clone();
    // 欄位客製調整
    $clone.find('select.form-control').val('');
    $clone.find('.bootstrap-select').each(function () {
      $(this).parent().html($(this).find('select.form-control'));
    });
    $clone.find('.nos').val(fieldIndex);
    // $clone.find('input[type=checkbox]').prop('checked', false);
    $.each($clone.find('[name^='+fieldNamePrefix+']'), function( index, value ) {
      var field_name = $(this).attr('name');
      var field_name_match = field_name.match(/\[(.*?)\]/g);
      field_name_match[0] = '[' + fieldIndex + ']';
      var field_name_full = fieldNamePrefix + field_name_match.join('');
      $(this).attr('name', field_name_full);
      $(this).selectpicker();
    });
    parent.find('.num-holder').append($clone);
    $clone.find('select.form-control').trigger('change');
    parent.find('.dynamic-total').text(parent.find('.dynamic-rows').length);
  });

  $(document).on('click', '.remove', function(event) {
    var parent = $(this).parents('.submission-form-body');
    if(parent.find('.dynamic-rows').length>1) {
      $(this).parents('.dynamic-rows').remove();
    } else {
      alert('資料至少一筆')
    }
    parent.find('.dynamic-total').text($('.dynamic-rows').length);
    parent.find('select.notrepeat').trigger('change');
    return false;
  });

});
