$(document).ready(function() {
    $('.item-root input:checkbox').change(function(event) {
        if ($(this).is(':checked'))
            $(this).parents('.item-group').find('.item-child input:checkbox').prop('checked', true);
        else
            $(this).parents('.item-group').find('.item-child input:checkbox').prop('checked', false);
    });
    $('.item-child input:checkbox').change(function(event) {
        if ($(this).parents('.item-group').find('.item-child input:checkbox:checked').length > 0)
            $(this).parents('.item-group').find('.item-root input:checkbox').prop('checked', true);
        else
            $(this).parents('.item-group').find('.item-root input:checkbox').prop('checked', false);
    });
});
