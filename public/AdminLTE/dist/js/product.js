$(document).ready(function() {
  $('.material-field-holder select').selectpicker('destroy');
  var filed_init_name = $('.material-field:last-child').find('input.form-control').attr('name');
  // var field_index = parseInt(filed_init_name.slice(parseInt(filed_init_name.indexOf("["))+1, parseInt(filed_init_name.indexOf("]"))));
  var field_index = parseInt(filed_init_name.match(/\[(.*?)\]/)[1]);

  console.log(filed_init_name, field_index);
  $('.btn-add-material').click(function(event) {
    field_index += 1;
    var clone = $(this).parents('.form-group').find('.material-field').eq(0).clone();
    clone.find('[name^=tag]').val('');
    $.each(clone.find('[name^=tag]'), function( index, value ) {
      var filed_name = $(this).attr('name');
      var filed_name_before = filed_name.slice(0, (filed_name.indexOf("[")+1));
      var filed_name_after = filed_name.slice(filed_name.indexOf("]"), filed_name.length);
      var filed_name_full = filed_name_before + field_index + filed_name_after;
      $(this).attr('name', filed_name_full);
    });
    $('.material-field-holder').append(clone);

    // var index = $(this).parents('.form-group').find('.material-field').length + 1;
    // var clone = $(this).parents('.form-group').find('.material-field').eq(0).clone();
    // clone.find('.form-control').val('');
    // $('.material-field-holder').append(clone);
    return false;
  });
  $(document).on('click', '.btn-del-material', function(event) {
    if($('.material-field').length>1) {
      $(this).parents('.material-field').remove();
    } else {
      alert("使用食材至少需填入一項")
    }
    return false;
  })
});
