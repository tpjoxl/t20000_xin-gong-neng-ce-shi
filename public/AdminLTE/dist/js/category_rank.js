$(document).ready(function() {
  var updateOutput = function(e) {
      var list   = e.length ? e : $(e.target),
          output = $('#sortNum');
      if (window.JSON && list.nestable('serialize')) {
          output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
          // console.log(output.val());
      }
  };
  $('#nestable').nestable().on('change', updateOutput);
  // $('#nestable').change();
  var sortNum = window.JSON.stringify($('#nestable').nestable('serialize'));
  $('#sortNum').val(window.JSON.stringify($('#nestable').nestable('serialize')));
  $('.dd-control').on('click', function(e) {
      var action = $(this).data('action');
      if (action === 'expand-all') {
          $('#nestable').nestable('expandAll');
      }
      if (action === 'collapse-all') {
          $('#nestable').nestable('collapseAll');
      }
  });
  $('.save-rank').click(function(event) {
    if ($('#sortNum').val() != sortNum) {
      $("form#rankUpdate").submit();
    } else {
      alert('排序或階層尚無變動，不需儲存');
    }
  });
  $('.dd-item input:checkbox').change(function(event) {
    if ($(this).is(':checked') && $(this).parents('.dd-item').eq(0)) {
      $(this).parents('.dd-item').eq(0).find('.dd-item input:checkbox').prop('checked', true);
    } else {
      $(this).parents('.dd-item').eq(0).find('.dd-item input:checkbox').prop('checked', false);
    }
    if (!$(this).parents('.dd-list').eq(0).parent().hasClass('dd')) {
      checkChildrenCount($(this));
    }
  });
  function checkChildrenCount(t) {
    var parentCheckbox = t.parents('.dd-list').eq(0).siblings('.dd-content').find('input:checkbox');
    if (parentCheckbox && t.parents('.dd-list').eq(0).children('.dd-item').children('.dd-content').find('input:checkbox:checked').length == t.parents('.dd-list').eq(0).children('.dd-item').length) {
    // if (parentCheckbox && t.parents('.dd-list').eq(0).children('.dd-item').children('.dd-content').find('input:checkbox:checked').length >0) {
      parentCheckbox.prop('checked', true);
    }
    // else {
    //   parentCheckbox.prop('checked', false);
    // }
    if (!t.parents('.dd-list').eq(0).parent().hasClass('dd')) {
      checkChildrenCount(parentCheckbox);
    }
  }
});
