<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// LaravelFilemanager
Route::group(['prefix' => config('lfm.url_prefix'), 'middleware' => ['backend','auth:admin']], function () {
    // \UniSharp\LaravelFilemanager\Lfm::routes();
    \App\LaravelFilemanager\Lfm::routes();
});

Route::get('/', 'HomeController@index')->name('home');
// sitemap
Route::get('sitemap.xml', 'GeneratedController@siteMap');

// 排程
Route::group(['prefix' => 'cron'], function () {
    Route::get('order_overtime', 'CronController@orderOvertime')->name('cron.order_overtime');
    Route::get('order_complete', 'CronController@orderComplete')->name('cron.order_complete');
    Route::get('coupon_expired', 'CronController@couponExpired')->name('cron.coupon_expired');
});

// Facebook
Route::get('/facebook', 'Auth\FacebookController@redirectToFacebook')->name('auth.facebook');
Route::get('/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

// Google
Route::get('/google', 'Auth\GoogleController@redirectToGoogle')->name('auth.google');
Route::get('/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::group(['prefix' => (count(app('frontendLocales'))>1)?strtolower(app()->getLocale()):''], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::post('/login', 'LoginController@login')->name('login.submit');
    Route::get('/logout', 'LoginController@logout')->name('logout');

    Route::group(['prefix' => _routes('user')], function () {
        Route::any(_routes('login'), 'UserController@login')->name('user.login');
        Route::any(_routes('logout'), 'UserController@logout')->name('user.logout');
        Route::any(_routes('register'), 'UserController@register')->name('user.register');
        Route::get('register-confirmed', 'UserController@registerConfirmed')->name('user.register.confirmed');
        Route::any(_routes('forgot'), 'UserController@forgot')->name('user.forgot');
        Route::any(_routes('password'), 'UserController@password')->name('user.password');
        Route::any(_routes('edit'), 'UserController@edit')->name('user.edit');
        Route::any(_routes('coupon'), 'UserController@coupon')->name('user.coupon');
        Route::any(_routes('bonus'), 'UserController@bonus')->name('user.bonus');

        Route::group(['prefix' => _routes('order')], function () {
            Route::get('/', 'UserOrderController@index')->name('user.order.index');
            Route::get('/detail/{num}', 'UserOrderController@detail')->name('user.order.detail');
        });
    });

    Route::group(['prefix' => _routes('order')], function () {
        Route::any('/pay/{num}', 'OrderController@pay')->name('order.pay');
        Route::patch('/cancel', 'OrderController@cancel')->name('order.cancel');
        Route::any('/ecpay/return', 'OrderController@ecpayReturn')->name('order.ecpay.return');
        Route::any('/ecpay/payment_info', 'OrderController@ecpayPaymentInfo')->name('order.ecpay.payment_info');
        Route::any('/ecpay/order_result', 'OrderController@ecpayOrderResult')->name('order.ecpay.order_result');
        Route::any('/ecpay/client_redirect', 'OrderController@ecpayClientRedirect')->name('order.ecpay.client_redirect');
    });

    Route::group(['prefix' => _routes('order_query')], function () {
        Route::get('/', 'OrderQueryController@index')->name('order_query.index');
        Route::post('/', 'OrderQueryController@postIndex')->name('order_query.index');
        Route::get('/result', 'OrderQueryController@result')->name('order_query.result');
    });

    Route::group(['prefix' => _routes('about')], function () {
        Route::get('/', 'AboutController@index')->name('about.index');
    });

    Route::group(['prefix' => _routes('blog')], function () {
        Route::get('/', 'BlogController@index')->name('blog.index');
        Route::get(_routes('search'), 'BlogController@search')->name('blog.search');
        Route::get(_routes('category').'/{category}/{category2?}', 'BlogController@category')->name('blog.category');
        Route::get('/{slug}', 'BlogController@detail')->name('blog.detail');
    });

    Route::group(['prefix' => _routes('faq')], function () {
        Route::get('/', 'FaqController@index')->name('faq.index');
    });

    Route::group(['prefix' => _routes('product')], function () {
        Route::get('/', 'ProductController@index')->name('product.index');
        Route::get(_routes('search'), 'ProductController@search')->name('product.search');
        Route::get(_routes('category').'/{category}/{category2?}', 'ProductController@category')->name('product.category');
        Route::get('/{slug}', 'ProductController@detail')->name('product.detail');
    });

    Route::group(['prefix' => _routes('contact')], function () {
        Route::get('/', 'ContactController@index')->name('contact.index');
        Route::post('/', 'ContactController@formSubmit')->name('contact.index');
    });

    Route::group(['prefix' => _routes('subscriber')], function () {
        Route::post('/subscribe', 'SubscriberController@subscribe')->name('subscriber.subscribe');
        Route::post('/unsubscribe', 'SubscriberController@unsubscribe')->name('subscriber.unsubscribe');
    });

    Route::group(['prefix' => _routes('terms')], function () {
        Route::get('/', 'TermsController@index')->name('terms.index');
    });

    Route::group(['prefix' => _routes('privacy')], function () {
        Route::get('/', 'PrivacyController@index')->name('privacy.index');
    });

    Route::group(['prefix' => _routes('cart')], function () {
        Route::get('/', 'CartController@index')->name('cart.index');
        Route::post('/', 'CartController@postIndex')->name('cart.index');
        Route::get('/checkout', 'CartController@checkout')->name('cart.checkout');
        Route::post('/checkout', 'CartController@postCheckout')->name('cart.checkout');
        Route::get('/result', 'CartController@result')->name('cart.result');
    });

    Route::group(['prefix' => 'api'], function () {
        Route::get('order/payinfo', 'ApiController@getOrderPayinfo')->name('api.order.payinfo');
        Route::get('order/detail', 'ApiController@getOrderDetail')->name('api.order.detail');

        Route::group(['prefix' => 'cart'], function () {
            Route::post('/add', 'CartApiController@add');
            Route::post('/add-plus', 'CartApiController@addAddone');
            Route::post('/update_checked', 'CartApiController@updateChecked')->name('cart.updateChecked');
            Route::post('/update_qty', 'CartApiController@updateQty')->name('cart.updateQty');
            Route::post('/update_item_checked', 'CartApiController@updateItemChecked')->name('cart.updateItemChecked');
            Route::post('/update_coupon', 'CartApiController@updateCoupon')->name('cart.updateCoupon');
            Route::post('/update_bonus', 'CartApiController@updateBonus')->name('cart.updateBonus');
            Route::post('/update_shipping', 'CartApiController@updateShipping')->name('cart.updateShipping');
            Route::post('/update_shipping_type', 'CartApiController@updateShippingType')->name('cart.updateShippingType');
            Route::post('/update_remote', 'CartApiController@updateRemote')->name('cart.updateRemote');
            Route::post('/update_payment', 'CartApiController@updatePayment')->name('cart.updatePayment');
            Route::post('/update_tax', 'CartApiController@updateTax')->name('cart.updateTax');
            Route::post('/change_sku', 'CartApiController@changeSku')->name('cart.changeSku');
            Route::post('/delete', 'CartApiController@delete')->name('cart.delete');
            Route::get('/clear', 'CartApiController@clearCart')->name('cart.clear');
        });
    });
});
