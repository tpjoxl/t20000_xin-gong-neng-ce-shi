<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/login', 'LoginController@showLoginForm')->name('login');
Route::post('/login', 'LoginController@login')->name('login.submit');
Route::get('/logout', 'LoginController@logout')->name('logout');

// 權限功能管理
Route::group(['prefix' => 'power'], function () {
    Route::get('/', 'PowerController@index')->name('power.index');
    Route::get('/{id}/edit', 'PowerController@edit')->where('id', '[0-9]+')->name('power.edit');
    Route::patch('/{id}/edit', 'PowerController@update')->where('id', '[0-9]+')->name('power.update');
    Route::any('/modify', 'PowerController@modify')->name('power.modify');
    Route::any('/rank/update', 'PowerController@rankUpdate')->name('power.rank.update');
});

// 網站設定
Route::group(['prefix' => 'site'], function () {
    Route::any('/info', 'SiteSettingController@page')->name('site.info');
    Route::any('/setting', 'SiteSettingController@page')->name('site.setting');
    Route::any('/smtp', 'SiteSettingController@page')->name('site.smtp');
    Route::any('/editor', 'SiteSettingController@page')->name('site.editor');
});

// 管理員管理
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::any('/create', 'AdminController@create')->name('admin.create');
    Route::post('/create', 'AdminController@store')->name('admin.store');
    Route::get('/{id}/edit', 'AdminController@edit')->where('id', '[0-9]+')->name('admin.edit');
    Route::patch('/{id}/edit', 'AdminController@update')->where('id', '[0-9]+')->name('admin.update');
    Route::post('/{id}/edit/password', 'AdminController@password')->where('id', '[0-9]+')->name('admin.password');
    Route::any('/destroy', 'AdminController@destroy')->name('admin.destroy');
    Route::any('/modify', 'AdminController@modify')->name('admin.modify');

    //管理員權限群組分類
    Route::group(['prefix' => 'group'], function () {
        Route::get('', 'AdminGroupController@index')->name('admin.group.index');
        Route::get('create', 'AdminGroupController@create')->name('admin.group.create');
        Route::post('create', 'AdminGroupController@store')->name('admin.group.store');
        Route::get('{id}/edit', 'AdminGroupController@edit')->where('id', '[0-9]+')->name('admin.group.edit');
        Route::patch('{id}/edit', 'AdminGroupController@update')->where('id', '[0-9]+')->name('admin.group.update');
        Route::any('destroy', 'AdminGroupController@destroy')->name('admin.group.destroy');
        Route::any('/modify', 'AdminGroupController@modify')->name('admin.group.modify');
        Route::any('/rank/update', 'AdminGroupController@rankUpdate')->name('admin.group.rank.update');
    });
});

Route::group(['prefix' => 'about'], function () {
    Route::any('/page', 'AboutController@page')->name('about.page');
});

Route::group(['prefix' => 'user'], function () {
    Route::any('/', 'UserController@index')->name('user.index');
    Route::get('/create', 'UserController@create')->name('user.create');
    Route::post('/create', 'UserController@store')->name('user.store');
    Route::get('/{id}/edit', 'UserController@edit')->where('id', '[0-9]+')->name('user.edit');
    Route::patch('/{id}/edit', 'UserController@update')->where('id', '[0-9]+')->name('user.update');
    Route::post('/{id}/edit/password', 'UserController@password')->where('id', '[0-9]+')->name('user.password');
    Route::any('/destroy', 'UserController@destroy')->name('user.destroy');
    Route::get('/rank', 'UserController@rank')->name('user.rank.index');
    Route::post('/rank', 'UserController@rankUpdate')->name('user.rank.update');
    Route::any('/rank/reset', 'UserController@rankReset')->name('user.rank.reset');
    Route::any('/modify', 'UserController@modify')->name('user.modify');
    Route::any('/export', 'UserController@export')->name('user.export');
});

Route::group(['prefix' => 'banner'], function () {
    Route::any('/', 'BannerController@index')->name('banner.index');
    Route::get('/create', 'BannerController@create')->name('banner.create');
    Route::post('/create', 'BannerController@store')->name('banner.store');
    Route::get('/{id}/copy', 'BannerController@copy')->where('id', '[0-9]+')->name('banner.copy');
    Route::post('/{id}/copy', 'BannerController@store')->where('id', '[0-9]+')->name('banner.copystore');
    Route::get('/{id}/edit', 'BannerController@edit')->where('id', '[0-9]+')->name('banner.edit');
    Route::patch('/{id}/edit', 'BannerController@update')->where('id', '[0-9]+')->name('banner.update');
    Route::any('/destroy', 'BannerController@destroy')->name('banner.destroy');
    Route::get('/rank', 'BannerController@rank')->name('banner.rank.index');
    Route::post('/rank', 'BannerController@rankUpdate')->name('banner.rank.update');
    Route::any('/rank/reset', 'BannerController@rankReset')->name('banner.rank.reset');
    Route::any('/modify', 'BannerController@modify')->name('banner.modify');
});

Route::group(['prefix' => 'product'], function () {
    Route::any('/', 'ProductController@index')->name('product.index');
    Route::get('/create', 'ProductController@create')->name('product.create');
    Route::post('/create', 'ProductController@store')->name('product.store');
    Route::get('/{id}/copy', 'ProductController@copy')->where('id', '[0-9]+')->name('product.copy');
    Route::post('/{id}/copy', 'ProductController@store')->where('id', '[0-9]+')->name('product.copystore');
    Route::get('/{id}/edit', 'ProductController@edit')->where('id', '[0-9]+')->name('product.edit');
    Route::patch('/{id}/edit', 'ProductController@update')->where('id', '[0-9]+')->name('product.update');
    Route::any('/destroy', 'ProductController@destroy')->name('product.destroy');
    Route::get('/rank', 'ProductController@rank')->name('product.rank.index');
    Route::post('/rank', 'ProductController@rankUpdate')->name('product.rank.update');
    Route::any('/rank/reset', 'ProductController@rankReset')->name('product.rank.reset');
    Route::any('/modify', 'ProductController@modify')->where('val', '[0-9]+')->name('product.modify');
    Route::any('/page', 'ProductController@page')->name('product.page');

    // 分類
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'ProductCategoryController@index')->name('product.category.index');
        Route::get('/create', 'ProductCategoryController@create')->name('product.category.create');
        Route::post('/create', 'ProductCategoryController@store')->name('product.category.store');
        Route::get('/{id}/edit', 'ProductCategoryController@edit')->where('id', '[0-9]+')->name('product.category.edit');
        Route::patch('/{id}/edit', 'ProductCategoryController@update')->where('id', '[0-9]+')->name('product.category.update');
        Route::any('/destroy', 'ProductCategoryController@destroy')->name('product.category.destroy');
        Route::any('/modify', 'ProductCategoryController@modify')->where('val', '[0-9]+')->name('product.category.modify');
        Route::any('/rank/update', 'ProductCategoryController@rankUpdate')->name('product.category.rank.update');
        Route::any('/rank/reset', 'ProductCategoryController@rankReset')->name('product.category.rank.reset');
    });
    // 圖片
    Route::group(['prefix' => '{product_id}/image'], function () {
        Route::get('/', 'ProductImageController@index')->name('product.image.index');
        Route::post('/store', 'ProductImageController@store')->name('product.image.store');
        Route::post('/update', 'ProductImageController@update')->name('product.image.update');
        Route::any('/destroy', 'ProductImageController@destroy')->name('product.image.destroy');
        Route::any('/rank/update', 'ProductImageController@rankUpdate')->name('product.image.rank.update');
        Route::any('/rank/reset', 'ProductImageController@rankReset')->name('product.image.rank.reset');
        Route::any('/cover', 'ProductImageController@cover')->name('product.image.cover');
    });
    Route::group(['prefix' => 'sku'], function () {
        Route::any('/', 'ProductSkuController@index')->name('product.sku.index');
        Route::get('/create', 'ProductSkuController@create')->name('product.sku.create');
        Route::post('/create', 'ProductSkuController@store')->name('product.sku.store');
        Route::get('/{id}/copy', 'ProductSkuController@copy')->where('id', '[0-9]+')->name('product.sku.copy');
        Route::post('/{id}/copy', 'ProductSkuController@store')->where('id', '[0-9]+')->name('product.sku.copystore');
        Route::get('/{id}/edit', 'ProductSkuController@edit')->where('id', '[0-9]+')->name('product.sku.edit');
        Route::patch('/{id}/edit', 'ProductSkuController@update')->where('id', '[0-9]+')->name('product.sku.update');
        Route::any('/destroy', 'ProductSkuController@destroy')->name('product.sku.destroy');
        // Route::get('/rank', 'ProductSkuController@rank')->name('product.sku.rank.index');
        // Route::post('/rank', 'ProductSkuController@rankUpdate')->name('product.sku.rank.update');
        // Route::any('/rank/reset', 'ProductSkuController@rankReset')->name('product.sku.rank.reset');
        Route::any('/modify', 'ProductSkuController@modify')->name('product.sku.modify');
        Route::any('/import', 'ProductSkuController@import')->name('product.sku.import');
        Route::any('/export', 'ProductSkuController@export')->name('product.sku.export');
    });
});

Route::group(['prefix' => 'blog'], function () {
    Route::any('/', 'BlogController@index')->name('blog.index');
    Route::get('/create', 'BlogController@create')->name('blog.create');
    Route::post('/create', 'BlogController@store')->name('blog.store');
    Route::get('/{id}/copy', 'BlogController@copy')->where('id', '[0-9]+')->name('blog.copy');
    Route::post('/{id}/copy', 'BlogController@store')->where('id', '[0-9]+')->name('blog.copystore');
    Route::get('/{id}/edit', 'BlogController@edit')->where('id', '[0-9]+')->name('blog.edit');
    Route::patch('/{id}/edit', 'BlogController@update')->where('id', '[0-9]+')->name('blog.update');
    Route::any('/destroy', 'BlogController@destroy')->name('blog.destroy');
    Route::get('/rank', 'BlogController@rank')->name('blog.rank.index');
    Route::post('/rank', 'BlogController@rankUpdate')->name('blog.rank.update');
    Route::any('/rank/reset', 'BlogController@rankReset')->name('blog.rank.reset');
    Route::any('/modify', 'BlogController@modify')->where('val', '[0-9]+')->name('blog.modify');
    Route::any('/page', 'BlogController@page')->name('blog.page');

    // 分類
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'BlogCategoryController@index')->name('blog.category.index');
        Route::get('/create', 'BlogCategoryController@create')->name('blog.category.create');
        Route::post('/create', 'BlogCategoryController@store')->name('blog.category.store');
        Route::get('/{id}/copy', 'BlogCategoryController@copy')->where('id', '[0-9]+')->name('blog.category.copy');
        Route::post('/{id}/copy', 'BlogCategoryController@store')->where('id', '[0-9]+')->name('blog.category.copystore');
        Route::get('/{id}/edit', 'BlogCategoryController@edit')->where('id', '[0-9]+')->name('blog.category.edit');
        Route::patch('/{id}/edit', 'BlogCategoryController@update')->where('id', '[0-9]+')->name('blog.category.update');
        Route::any('/destroy', 'BlogCategoryController@destroy')->name('blog.category.destroy');
        Route::any('/modify', 'BlogCategoryController@modify')->where('val', '[0-9]+')->name('blog.category.modify');
        Route::any('/rank/update', 'BlogCategoryController@rankUpdate')->name('blog.category.rank.update');
        Route::any('/rank/reset', 'BlogCategoryController@rankReset')->name('blog.category.rank.reset');
    });

    // 標籤
    Route::group(['prefix' => 'tag'], function () {
        Route::any('/', 'BlogTagController@index')->name('blog.tag.index');
        Route::get('/create', 'BlogTagController@create')->name('blog.tag.create');
        Route::post('/create', 'BlogTagController@store')->name('blog.tag.store');
        Route::get('/{id}/copy', 'BlogTagController@copy')->where('id', '[0-9]+')->name('blog.tag.copy');
        Route::post('/{id}/copy', 'BlogTagController@store')->where('id', '[0-9]+')->name('blog.tag.copystore');
        Route::get('/{id}/edit', 'BlogTagController@edit')->where('id', '[0-9]+')->name('blog.tag.edit');
        Route::patch('/{id}/edit', 'BlogTagController@update')->where('id', '[0-9]+')->name('blog.tag.update');
        Route::any('/destroy', 'BlogTagController@destroy')->name('blog.tag.destroy');
        Route::get('/rank', 'BlogTagController@rank')->name('blog.tag.rank.index');
        Route::post('/rank', 'BlogTagController@rankUpdate')->name('blog.tag.rank.update');
        Route::any('/rank/reset', 'BlogTagController@rankReset')->name('blog.tag.rank.reset');
        Route::any('/modify', 'BlogTagController@modify')->where('val', '[0-9]+')->name('blog.tag.modify');
    });
});

Route::group(['prefix' => 'faq'], function () {
    Route::any('/', 'FaqController@index')->name('faq.index');
    Route::get('/create', 'FaqController@create')->name('faq.create');
    Route::post('/create', 'FaqController@store')->name('faq.store');
    Route::get('/{id}/copy', 'FaqController@copy')->where('id', '[0-9]+')->name('faq.copy');
    Route::post('/{id}/copy', 'FaqController@store')->where('id', '[0-9]+')->name('faq.copystore');
    Route::get('/{id}/edit', 'FaqController@edit')->where('id', '[0-9]+')->name('faq.edit');
    Route::patch('/{id}/edit', 'FaqController@update')->where('id', '[0-9]+')->name('faq.update');
    Route::any('/destroy', 'FaqController@destroy')->name('faq.destroy');
    Route::get('/rank', 'FaqController@rank')->name('faq.rank.index');
    Route::post('/rank', 'FaqController@rankUpdate')->name('faq.rank.update');
    Route::any('/rank/reset', 'FaqController@rankReset')->name('faq.rank.reset');
    Route::any('/modify', 'FaqController@modify')->where('val', '[0-9]+')->name('faq.modify');
    Route::any('/page', 'FaqController@page')->name('faq.page');

    // 分類
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'FaqCategoryController@index')->name('faq.category.index');
        Route::get('/create', 'FaqCategoryController@create')->name('faq.category.create');
        Route::post('/create', 'FaqCategoryController@store')->name('faq.category.store');
        Route::get('/{id}/edit', 'FaqCategoryController@edit')->where('id', '[0-9]+')->name('faq.category.edit');
        Route::patch('/{id}/edit', 'FaqCategoryController@update')->where('id', '[0-9]+')->name('faq.category.update');
        Route::any('/destroy', 'FaqCategoryController@destroy')->name('faq.category.destroy');
        Route::any('/modify', 'FaqCategoryController@modify')->where('val', '[0-9]+')->name('faq.category.modify');
        Route::any('/rank/update', 'FaqCategoryController@rankUpdate')->name('faq.category.rank.update');
        Route::any('/rank/reset', 'FaqCategoryController@rankReset')->name('faq.category.rank.reset');
    });
});

Route::group(['prefix' => 'question'], function () {
    Route::any('/', 'QuestionController@index')->name('question.index');
    Route::get('/create', 'QuestionController@create')->name('question.create');
    Route::post('/create', 'QuestionController@store')->name('question.store');
    Route::get('/{id}/edit', 'QuestionController@edit')->where('id', '[0-9]+')->name('question.edit');
    Route::patch('/{id}/edit', 'QuestionController@update')->where('id', '[0-9]+')->name('question.update');
    Route::any('/destroy', 'QuestionController@destroy')->name('question.destroy');
    Route::get('/rank', 'QuestionController@rank')->name('question.rank.index');
    Route::post('/rank', 'QuestionController@rankUpdate')->name('question.rank.update');
    Route::any('/rank/reset', 'QuestionController@rankReset')->name('question.rank.reset');

    // 分類
    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'QuestionCategoryController@index')->name('question.category.index');
        Route::get('/create', 'QuestionCategoryController@create')->name('question.category.create');
        Route::post('/create', 'QuestionCategoryController@store')->name('question.category.store');
        Route::get('/{id}/edit', 'QuestionCategoryController@edit')->where('id', '[0-9]+')->name('question.category.edit');
        Route::patch('/{id}/edit', 'QuestionCategoryController@update')->where('id', '[0-9]+')->name('question.category.update');
        Route::any('/destroy', 'QuestionCategoryController@destroy')->name('question.category.destroy');
        Route::any('/rank/update', 'QuestionCategoryController@rankUpdate')->name('question.category.rank.update');
        Route::any('/rank/reset', 'QuestionCategoryController@rankReset')->name('question.category.rank.reset');
    });
});
Route::group(['prefix' => 'record'], function () {
    Route::any('/', 'RecordController@index')->name('record.index');
    Route::get('/create', 'RecordController@create')->name('record.create');
    Route::post('/create', 'RecordController@store')->name('record.store');
    Route::get('/{id}/edit', 'RecordController@edit')->where('id', '[0-9]+')->name('record.edit');
    Route::patch('/{id}/edit', 'RecordController@update')->where('id', '[0-9]+')->name('record.update');
    Route::any('/destroy', 'RecordController@destroy')->name('record.destroy');
    Route::get('/rank', 'RecordController@rank')->name('record.rank.index');
    Route::post('/rank', 'RecordController@rankUpdate')->name('record.rank.update');
    Route::any('/rank/reset', 'RecordController@rankReset')->name('record.rank.reset');
});
Route::group(['prefix' => 'contact'], function () {
    Route::get('/', 'ContactController@index')->name('contact.index');
    Route::get('/create', 'ContactController@create')->name('contact.create');
    Route::post('/create', 'ContactController@store')->name('contact.store');
    Route::get('/{id}/edit', 'ContactController@edit')->where('id', '[0-9]+')->name('contact.edit');
    Route::patch('/{id}/edit', 'ContactController@update')->where('id', '[0-9]+')->name('contact.update');
    Route::any('/destroy', 'ContactController@destroy')->name('contact.destroy');
    Route::any('/modify', 'ContactController@modify')->name('contact.modify');
    Route::any('/page', 'ContactController@page')->name('contact.page');
});

Route::group(['prefix' => 'subscriber'], function () {
    Route::get('/', 'SubscriberController@index')->name('subscriber.index');
    Route::get('/{id}/edit', 'SubscriberController@edit')->where('id', '[0-9]+')->name('subscriber.edit');
    Route::patch('/{id}/edit', 'SubscriberController@update')->where('id', '[0-9]+')->name('subscriber.update');
    Route::any('/destroy', 'SubscriberController@destroy')->name('subscriber.destroy');
    Route::any('/modify', 'SubscriberController@modify')->name('subscriber.modify');
    Route::any('/export', 'SubscriberController@export')->name('subscriber.export');
});


// 使用條款
Route::group(['prefix' => 'terms'], function () {
    Route::any('/page', 'TermsController@page')->name('terms.page');
});

// 隱私權政策
Route::group(['prefix' => 'privacy'], function () {
    Route::any('/page', 'PrivacyController@page')->name('privacy.page');
});

// 信件內容管理
// Route::group(['prefix' => 'mail'], function () {
//     Route::group(['prefix' => 'user'], function () {
//         Route::any('/register', 'MailController@page')->name('mail.user.register');
//         Route::any('/forgot', 'MailController@page')->name('mail.user.forgot');
//     });
//     Route::group(['prefix' => 'senior'], function () {
//         Route::any('/register', 'MailController@page')->name('mail.senior.register');
//         Route::any('/forgot', 'MailController@page')->name('mail.senior.forgot');
//     });
// });

// 語系文字管理
Route::group(['prefix' => 'locale'], function () {
    Route::any('/', 'LocaleController@index')->name('locale.index');
    Route::get('/create', 'LocaleController@create')->name('locale.create');
    Route::post('/create', 'LocaleController@store')->name('locale.store');
    Route::get('/{id}/copy', 'LocaleController@copy')->where('id', '[0-9]+')->name('locale.copy');
    Route::post('/{id}/copy', 'LocaleController@store')->where('id', '[0-9]+')->name('locale.copystore');
    Route::get('/{id}/edit', 'LocaleController@edit')->where('id', '[0-9]+')->name('locale.edit');
    Route::patch('/{id}/edit', 'LocaleController@update')->where('id', '[0-9]+')->name('locale.update');
    Route::any('/destroy', 'LocaleController@destroy')->name('locale.destroy');
    Route::get('/rank', 'LocaleController@rank')->name('locale.rank.index');
    Route::post('/rank', 'LocaleController@rankUpdate')->name('locale.rank.update');
    Route::any('/rank/reset', 'LocaleController@rankReset')->name('locale.rank.reset');
    Route::any('/modify', 'LocaleController@modify')->where('val', '[0-9]+')->name('locale.modify');

    Route::group(['prefix' => 'text'], function () {
        Route::any('/index', 'LocaleTextController@index')->name('locale.text.index');
    });
});


Route::group(['prefix' => 'order'], function () {
    Route::any('/', 'OrderController@index')->name('order.index');
    // Route::get('/create', 'OrderController@create')->name('order.create');
    // Route::post('/create', 'OrderController@store')->name('order.store');
    // Route::get('/{id}/copy', 'OrderController@copy')->where('id', '[0-9]+')->name('order.copy');
    // Route::post('/{id}/copy', 'OrderController@store')->where('id', '[0-9]+')->name('order.copystore');
    Route::get('/{id}/edit', 'OrderController@edit')->where('id', '[0-9]+')->name('order.edit');
    Route::patch('/{id}/edit', 'OrderController@update')->where('id', '[0-9]+')->name('order.update');
    Route::any('/destroy', 'OrderController@destroy')->name('order.destroy');
    Route::get('/rank', 'OrderController@rank')->name('order.rank.index');
    Route::post('/rank', 'OrderController@rankUpdate')->name('order.rank.update');
    Route::any('/rank/reset', 'OrderController@rankReset')->name('order.rank.reset');
    Route::any('/modify', 'OrderController@modify')->name('order.modify');
    // Route::any('/import', 'OrderController@import')->name('order.import');
    Route::any('/export', 'OrderController@export')->name('order.export');

    Route::group(['prefix' => 'log'], function () {
        Route::get('/', 'OrderLogController@index')->name('order.log.index');
    });
});

Route::group(['prefix' => 'payment'], function () {
    Route::any('/', 'PaymentController@index')->name('payment.index');
    // Route::get('/create', 'PaymentController@create')->name('payment.create');
    // Route::post('/create', 'PaymentController@store')->name('payment.store');
    // Route::get('/{id}/copy', 'PaymentController@copy')->where('id', '[0-9]+')->name('payment.copy');
    // Route::post('/{id}/copy', 'PaymentController@store')->where('id', '[0-9]+')->name('payment.copystore');
    Route::get('/{id}/edit', 'PaymentController@edit')->where('id', '[0-9]+')->name('payment.edit');
    Route::patch('/{id}/edit', 'PaymentController@update')->where('id', '[0-9]+')->name('payment.update');
    // Route::any('/destroy', 'PaymentController@destroy')->name('payment.destroy');
    Route::get('/rank', 'PaymentController@rank')->name('payment.rank.index');
    Route::post('/rank', 'PaymentController@rankUpdate')->name('payment.rank.update');
    Route::any('/rank/reset', 'PaymentController@rankReset')->name('payment.rank.reset');
    Route::any('/modify', 'PaymentController@modify')->name('payment.modify');
});

Route::group(['prefix' => 'shipping'], function () {
    Route::any('/', 'ShippingController@index')->name('shipping.index');
    Route::get('/create', 'ShippingController@create')->name('shipping.create');
    Route::post('/create', 'ShippingController@store')->name('shipping.store');
    Route::get('/{id}/copy', 'ShippingController@copy')->where('id', '[0-9]+')->name('shipping.copy');
    Route::post('/{id}/copy', 'ShippingController@store')->where('id', '[0-9]+')->name('shipping.copystore');
    Route::get('/{id}/edit', 'ShippingController@edit')->where('id', '[0-9]+')->name('shipping.edit');
    Route::patch('/{id}/edit', 'ShippingController@update')->where('id', '[0-9]+')->name('shipping.update');
    Route::any('/destroy', 'ShippingController@destroy')->name('shipping.destroy');
    Route::get('/rank', 'ShippingController@rank')->name('shipping.rank.index');
    Route::post('/rank', 'ShippingController@rankUpdate')->name('shipping.rank.update');
    Route::any('/rank/reset', 'ShippingController@rankReset')->name('shipping.rank.reset');
    Route::any('/modify', 'ShippingController@modify')->name('shipping.modify');
    Route::any('/page', 'ShippingController@page')->name('shipping.page');
});

Route::group(['prefix' => 'coupon'], function () {
    Route::any('/', 'CouponController@index')->name('coupon.index');
    Route::get('/create', 'CouponController@create')->name('coupon.create');
    Route::post('/create', 'CouponController@store')->name('coupon.store');
    Route::get('/{id}/copy', 'CouponController@copy')->where('id', '[0-9]+')->name('coupon.copy');
    Route::post('/{id}/copy', 'CouponController@store')->where('id', '[0-9]+')->name('coupon.copystore');
    Route::get('/{id}/edit', 'CouponController@edit')->where('id', '[0-9]+')->name('coupon.edit');
    Route::patch('/{id}/edit', 'CouponController@update')->where('id', '[0-9]+')->name('coupon.update');
    Route::any('/destroy', 'CouponController@destroy')->name('coupon.destroy');
    Route::get('/rank', 'CouponController@rank')->name('coupon.rank.index');
    Route::post('/rank', 'CouponController@rankUpdate')->name('coupon.rank.update');
    Route::any('/rank/reset', 'CouponController@rankReset')->name('coupon.rank.reset');
    Route::any('/modify', 'CouponController@modify')->name('coupon.modify');
    Route::any('/page', 'CouponController@page')->name('coupon.page');
});

Route::group(['prefix' => 'bonus'], function () {
    Route::any('/', 'BonusController@index')->name('bonus.index');
    Route::get('/create', 'BonusController@create')->name('bonus.create');
    Route::post('/create', 'BonusController@store')->name('bonus.store');
    Route::get('/{id}/copy', 'BonusController@copy')->where('id', '[0-9]+')->name('bonus.copy');
    Route::post('/{id}/copy', 'BonusController@store')->where('id', '[0-9]+')->name('bonus.copystore');
    Route::get('/{id}/edit', 'BonusController@edit')->where('id', '[0-9]+')->name('bonus.edit');
    Route::patch('/{id}/edit', 'BonusController@update')->where('id', '[0-9]+')->name('bonus.update');
    Route::any('/destroy', 'BonusController@destroy')->name('bonus.destroy');
    Route::get('/rank', 'BonusController@rank')->name('bonus.rank.index');
    Route::post('/rank', 'BonusController@rankUpdate')->name('bonus.rank.update');
    Route::any('/rank/reset', 'BonusController@rankReset')->name('bonus.rank.reset');
    Route::any('/modify', 'BonusController@modify')->name('bonus.modify');
    Route::any('/page', 'BonusController@page')->name('bonus.page');
});

Route::group(['prefix' => 'cart'], function () {
    Route::any('/page', 'CartController@page')->name('cart.page');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('user', 'ApiController@user')->name('api.user');
    Route::get('blog', 'ApiController@blog')->name('api.blog');
    Route::get('tw_region', 'ApiController@twRegion')->name('api.tw_region');
    Route::get('product', 'ApiController@product')->name('api.product');
    Route::get('product/sku', 'ApiController@productSku')->name('api.product.sku');
    Route::get('order', 'ApiController@order')->name('api.order');
    Route::get('sku', 'ApiController@sku')->name('api.sku');
});
