<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Setting;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\NewebpayRecord;
use App\Payment\Newebpay;
use App\Mail\AdminOrderPaid;
use App\Mail\SeniorOrderPaid;

trait NewebpayTrait
{
    public function newebpayCheckout($order)
    {
        try {
            $obj = new Newebpay;
            $obj->Send['MerchantOrderNo'] = $order->epaynumber;
            $obj->Send['Amt'] = (int)$order->sum;
            $obj->Send['ItemDesc'] = '訂單' . $order->num;
            $obj->Send['Email'] = $order->user->email;
            $obj->Send['NotifyURL'] = route($this->newebpayNotifyRoute); // 支付完成 通知網址
            $obj->Send['ClientBackURL'] = route($this->newebpayClientBackRoute, ['num' => $order->num]); // 支付取消 返回商店網址
            if (in_array($order->payment_info['type'], ['CREDIT', 'WEBATM', 'P2G', 'ANDROIDPAY', 'SAMSUNGPAY', 'LINEPAY'])) {
                $obj->Send['ReturnURL'] = route($this->newebpayReturnRoute); // 支付完成 返回商店網址
            }
            if (in_array($order->payment_info['type'], ['ATM', 'CVS', 'BARCODE', 'CVSCOM'])) {
                $obj->Send['CustomerURL'] = route($this->newebpayCustomerRoute); // ATM 轉帳(VACC)、超商代碼繳費(CVS) 、超商條碼繳費(BARCODE)、超商取貨付款(CVSCOM)完成取號後的導回頁面
            }
            if ($order->payment_info['type'] == 'CREDIT') { // 信用卡
                $obj->Send['CREDIT'] = 1;
            } elseif ($order->payment_info['type'] == 'CVS') { // 超商代碼
                $obj->Send['CVS'] = 1;
            }
            Log::channel('payment')->info('user order pay', [
                'provider' => $order->payment_info['provider'],
                'type' => $order->payment_info['type'],
                'Send' => array_filter($obj->Send),
            ]);
            $obj->CheckOut();
        } catch (Exception $e) {
            Log::channel('payment')->error('user order newebpay checkout error', ['num' => $order->num, 'message' => $e->getMessage()]);
            return redirect()->route($this->newebpayClientBackRoute, ['num' => $order->num])
                ->with('error', '付款失敗')
                ->with('errorText', $e->getMessage());
        }
    }
    // 即時交易 支付完成返回商店網址
    // 即時交易支付方式：信用卡(CREDIT)、WebATM(WEBATM) 、ezPay 電子錢包(P2G)、Google Pay(ANDROIDPAY)、Samsung Pay(SAMSUNGPAY) 、LINE Pay(LINEPAY)
    public function newebpayReturn(Request $request)
    {
        $requestData = $request->all();
        $data = $this->newebpay->json_response_decrypt($requestData);
        Log::channel('payment')->info('user order newebpay return', ['response' => $data]);
        // 儲存付款資訊
        $recordData = $data;
        $record = new NewebpayRecord($recordData);
        $record->save();

        $order = $this->model->where('epaynumber', $data['MerchantOrderNo'])->first();
        if ($data['Status'] == "SUCCESS") {
            return redirect()->route($this->newebpayResultRoute, ['num' => $order->num])
                    ->with('result', '付款成功');
        } else {
            return redirect()->route($this->newebpayResultRoute, ['num' => $order->num])
                ->with('result', '付款失敗')
                ->with('resultText', '您可由會員訂單管理頁面內點選「付款」按鈕重新付款，謝謝您！');
        }
    }
    // 付款完成背景回傳資訊(包含ATM...)
    public function newebpayNotify(Request $request)
    {
        $requestData = $request->all();
        $data = $this->newebpay->json_response_decrypt($requestData);
        Log::channel('payment')->info('user order newebpay notify', ['response' => $data]);
        // 儲存回傳資訊
        $recordData = $data;
        $record = new NewebpayRecord($recordData);
        $record->save();

        $order = $this->model->where('epaynumber', $data['MerchantOrderNo'])->first();
        if ($data['Status'] == "SUCCESS") {
            // 變更狀態為已繳費
            $order->update(['status' => 5, 'payment_status' => 4]);

            // 變更優惠券狀態
            if (!is_null($order->coupon_id)) {
                $order->coupon->update(['status' => 2]);
            }

            // 寄通知信
            $this->SendMailToAdmin(new AdminOrderPaid($order));
            $this->SendMailTo($data, new SeniorOrderPaid($order));
        }
    }
    // 非即時交易 完成取號後的導回頁面
    // 非即時交易支付方式：ATM 轉帳(VACC)、超商代碼繳費(CVS) 、超商條碼繳費(BARCODE)、超商取貨付款(CVSCOM)
    public function newebpayCustomer(Request $request)
    {
        $requestData = $request->all();
        $data = $this->newebpay->json_response_decrypt($requestData);
        Log::channel('payment')->info('user order newebpay customer', ['response' => $data]);
        // 儲存付款資訊
        $recordData = $data;
        $record = new NewebpayRecord($recordData);
        $record->save();

        $order = $this->model->where('epaynumber', $data['MerchantOrderNo'])->first();
        if ($data['Status'] == "SUCCESS") {
            // 更新回傳的資訊
            if ($data['PaymentType'] == 'CVS') {
                $order->update([
                    'payment_return' => '繳費代碼：' . $data['CodeNo'] . '<br>繳費截止日期：' . $data['ExpireDate'],
                    'payment_deadline' => $data['ExpireDate'] . ' ' . $data['ExpireTime'],
                ]);
            } elseif ($data['PaymentType'] == 'VACC') {
                $order->update([
                    'payment_return' => '金融機構代碼：' . $data['BankCode'] . '<br>繳費帳號：' . $data['CodeNo'] . '<br>繳費截止日期：' . $data['ExpireDate'],
                    'payment_deadline' => $data['ExpireDate'] . ' ' . $data['ExpireTime'],
                ]);
            } elseif ($data['PaymentType'] == 'BARCODE') {
                $order->update([
                    'payment_return' => '第一段條碼：' . $data['Barcode_1'] . '<br>第二段條碼：' . $data['Barcode_2'] . '<br>第三段條碼：' . $data['Barcode_3'] . '<br>繳費截止日期：' . $data['ExpireDate'],
                    'payment_deadline' => $data['ExpireDate'] . ' ' . $data['ExpireTime'],
                ]);
            } elseif ($data['PaymentType'] == 'CVSCOM') {
                $order->update([
                    'payment_return' => '取貨門市編號：' . $data['StoreCode'] . '<br>取貨門市名稱：' . $data['StoreName'] . '<br>超商類別：' . $data['StoreType'] . '<br>取貨門市地址：' . $data['StoreAddr'],
                    'payment_deadline' => $data['ExpireDate'] . ' ' . $data['ExpireTime'],
                ]);
            }

            // 變更優惠券狀態
            if (!is_null($order->coupon_id)) {
                $order->coupon->update(['status' => 2]);
            }
            return redirect()->route($this->newebpayResultRoute, ['num' => $order->num])
                    ->with('result', '取號成功')
                    ->with('resultText', $order->payment_return);
        } else {
            return redirect()->route($this->newebpayResultRoute, ['num' => $order->num])
                ->with('result', '取號失敗')
                ->with('resultText', '您可由會員訂單管理頁面內點選「付款」按鈕重新付款，謝謝您！');
        }
    }
}
