<?php
namespace App\Traits;

trait SluggableTrait
{

    public static function bootSluggableTrait()
	{
		static::saving(function ($model) {
            $model->slug = $model->generateSlug();
        });
    }

    public function getRouteKeyName() {
        return 'slug';
    }

    public function generateSlug()
	{
        return $this->convertToUrlEncode($this->slug?:$this->{$this->slugFrom});
    }

    public function convertToUrlEncode($str)
	{
        // 把字串內的符號半形轉全形
        $nft = array(
            "(", ")", "[", "]", "{", "}", ",", ";", ":",
            "?", "!", "@", "#", "$", "%", "&", "|", "\\",
            "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
            "^"
        );
        $wft = array(
            "（", "）", "〔", "〕", "｛", "｝", "，", "；", "：",
            "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
            "／", "＋", "＝", "＊", "～", "、", "、", "＂", "＜", "＞",
            "︿"
        );
        $str = str_replace($nft, $wft, $str);

        // 把字串內的空格轉成「-」
        $str = preg_replace('/\s+/', '-', $str);
        // 把字串內的「_」轉成「-」
        $str = preg_replace('/_+/', '-', $str);
        // 把字串內多個「-」變成一個
        $str = preg_replace('/-+/', '-', $str);
        // 把字串內的大寫轉小寫
        return trim(strtolower($str));
	}
}
