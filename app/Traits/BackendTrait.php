<?php

namespace App\Traits;

use App\Helpers\ArrHelper as Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;


trait BackendTrait
{
    /* 表單欄位 */
    protected function getTemplateFormKeys()
    {
        if ($this->template == 'default') {
            $keys = ['status', 'home_status', 'is_top', 'title', 'slug', 'url', 'description', 'text', 'date_range', 'pic', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'];
            if (method_exists($this->model, 'categories')) {
                $keys = Arr::prepend($keys, 'categories');
            }
            if (method_exists($this->model, 'recommends')) {
                $keys = Arr::insertAfter($keys, ['recommends'], 'date_range');
            }
            if (method_exists($this->model, 'tags')) {
                $keys = Arr::insertAfter($keys, ['tags'], 'date_range');
            }
        } elseif ($this->template == 'category') {
            $keys = ['status', 'title', 'slug', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'];
            if ($this->model->getLimitLevel() > 1) {
                $keys = Arr::prepend($keys, 'parent_id');
            }
        } else {
            $keys = [];
        }
        return $keys;
    }
    protected function getTemplateFormFields($keys = [])
    {
        $templateFormFields = [
            'categories' => [
                'type' => 'multi_select',
                'label' => $this->getValidAttrs('categories'),
                'name' => 'categories',
                'required' => true,
                'options' => !empty($this->category) ? $this->category->tree()->get() : [],
                'add_first' => [
                    'url' => Route::has('backend.' . $this->prefix . '.category.create') ? route('backend.' . $this->prefix . '.category.create') : '',
                    'title' => $this->getValidAttrs('category'),
                ],
            ],
            'parent_id' => [
                'type' => 'select',
                'label' => $this->getValidAttrs('parent_id'),
                'name' => 'parent_id',
                'required' => true,
                'options' => method_exists($this->model, 'getTree') ? $this->model->tree(null, -1)->get() : [],
                'placeholder' => __('backend.top_level', [], env('BACKEND_LOCALE')),
                'ignore' => !empty($this->data->id)?$this->data->id:null,
            ],
            'active' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('active'),
                'name' => 'active',
                'required' => true,
                'options' => $this->model->present()->active(),
                'default' => 1,
            ],
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
            'home_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('home_status'),
                'name' => 'home_status',
                'required' => false,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
            'is_top' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('is_top'),
                'name' => 'is_top',
                'required' => false,
                'options' => $this->model->present()->yes_or_no(),
                'default' => 0,
            ],
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'required' => true,
            ],
            'slug' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('slug'),
                'name' => 'slug',
                'required' => false,
            ],
            'url' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('url'),
                'name' => 'url',
                'required' => false,
            ],
            'target' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('target'),
                'name' => 'target',
                'label' => '',
                'required' => false,
                'options' => $this->model->present()->target(),
                'default' => '',
                'hint' => __('backend.url_hint', [], env('BACKEND_LOCALE'))
            ],
            'description' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('description'),
                'name' => 'description',
                'required' => false,
                'rows' => 3,
            ],
            'text' => [
                'type' => 'text_editor',
                'label' => $this->getValidAttrs('text'),
                'name' => 'text',
                'required' => false,
            ],
            'created_at' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('created_at'),
                'name' => 'created_at',
                'required' => false,
                'hint' => __('backend.created_at_hint', [], env('BACKEND_LOCALE')),
            ],
            'updated_at' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('updated_at'),
                'name' => 'updated_at',
                'required' => false,
            ],
            'date_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('date_range'),
                'name' => ['date_on', 'date_off'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'required' => false,
            ],
            'tags' => [
                'type' => 'multi_select',
                'label' => $this->getValidAttrs('tags'),
                'name' => 'tags',
                'required' => false,
                'options' => method_exists($this->model, 'tags') ? $this->model->tags()->getRelated()->ordered()->get() : [],
                'live_search' => true,
                'add_first' => [
                    'url' => Route::has('backend.' . $this->prefix . '.tag.create') ? route('backend.' . $this->prefix . '.tag.create') : '',
                ]
            ],
            'recommends' => [
                'type' => 'multi_select',
                'label' => $this->getValidAttrs('recommends'),
                'name' => 'recommends',
                'required' => false,
                'options' => $this->model->ordered()->get(),
                'actions' => true,
                'live_search' => true,
                'ignore' => !empty($this->data->id)?$this->data->id:null,
            ],
            'banner' => [
                'type' => 'img',
                'label' => $this->getValidAttrs('banner'),
                'name' => 'banner',
                'required' => false,
                'w' => null,
                'h' => null,
                'folder' => str_replace('.', '_', $this->prefix),
            ],
            'pic' => [
                'type' => 'img',
                'label' => $this->getValidAttrs('pic'),
                'name' => 'pic',
                'required' => false,
                'w' => null,
                'h' => null,
                'folder' => str_replace('.', '_', $this->prefix),
            ],
            'pic2' => [
                'type' => 'img',
                'label' => $this->getValidAttrs('pic2'),
                'name' => 'pic2',
                'required' => false,
                'w' => null,
                'h' => null,
                'folder' => str_replace('.', '_', $this->prefix),
            ],
            'seo_title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('seo_title'),
                'name' => 'seo_title',
                'required' => false,
            ],
            'seo_description' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('seo_description'),
                'name' => 'seo_description',
                'required' => false,
                'rows' => 3,
            ],
            'seo_keyword' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('seo_keyword'),
                'name' => 'seo_keyword',
                'required' => false,
            ],
            'og_title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('og_title'),
                'name' => 'og_title',
                'required' => false,
            ],
            'og_description' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('og_description'),
                'name' => 'og_description',
                'required' => false,
                'rows' => 3,
            ],
            'og_image' => [
                'type' => 'img',
                'label' => $this->getValidAttrs('og_image'),
                'name' => 'og_image',
                'required' => false,
                'w' => null,
                'h' => null,
                'folder' => str_replace('.', '_', $this->prefix),
            ],
            'meta_robots' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('meta_robots'),
                'name' => 'meta_robots',
                'required' => false,
                'default' => 'index, follow',
            ],
            'tag_title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('tag_title'),
                'name' => 'tag_title',
                'required' => false,
            ],
            'tag_alt' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('tag_alt'),
                'name' => 'tag_alt',
                'required' => false,
            ],
            'group_id' => [
                'type' => 'select',
                'label' => $this->getValidAttrs('group_id'),
                'name' => 'group_id',
                'required' => true,
                'options' => method_exists($this->model, 'group') ? $this->model->group()->getRelated()->tree()->get() : [],
                'key' => 'title',
            ],
            'account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('account'),
                'name' => 'account',
                'required' => true,
            ],
            'admin_account' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('admin_account'),
                'name' => 'admin_account',
            ],
            'password' => [
                'type' => 'password_input',
                'label' => $this->getValidAttrs('password'),
                'name' => 'password',
                'required' => true,
            ],
            'password_confirmation' => [
                'type' => 'password_input',
                'label' => $this->getValidAttrs('password_confirmation'),
                'name' => 'password_confirmation',
                'required' => true,
                'error_name' => 'password',
            ],
            'name' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('name'),
                'name' => 'name',
                'required' => true,
            ],
            'phone' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('phone'),
                'name' => 'phone',
                'required' => false,
            ],
            'phone2' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('phone2'),
                'name' => 'phone2',
                'required' => false,
            ],
            'fax' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('fax'),
                'name' => 'fax',
                'required' => false,
            ],
            'email' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('email'),
                'name' => 'email',
                'required' => false,
            ],
            'address' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('address'),
                'name' => 'address',
                'required' => false,
            ],
            'google_map' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('google_map'),
                'name' => 'google_map',
                'required' => false,
            ],
            'business_hours' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('business_hours'),
                'name' => 'business_hours',
                'required' => false,
                'rows' => 3,
            ],
            'copyright' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('copyright'),
                'name' => 'copyright',
                'required' => false,
            ],
            'facebook' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('facebook'),
                'name' => 'facebook',
                'required' => false,
            ],
            'youtube' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('youtube'),
                'name' => 'youtube',
                'required' => false,
            ],
            'line' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('line'),
                'name' => 'line',
                'required' => false,
            ],
            'backend_name' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('backend_name'),
                'name' => 'backend_name',
                'required' => false,
            ],
            'site_mail' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('site_mail'),
                'name' => 'site_mail',
                'required' => false,
                'hint' => __('backend.multi_mail_hint'),
            ],
            'site_url' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('site_url'),
                'name' => 'site_url',
                'required' => false,
            ],
            'head_code' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('head_code'),
                'name' => 'head_code',
                'required' => false,
                'rows' => 3,
            ],
            'body_code' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('body_code'),
                'name' => 'body_code',
                'required' => false,
                'rows' => 3,
            ],
            'smtp' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('smtp'),
                'name' => 'smtp',
                'required' => false,
            ],
            'smtp_port' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('smtp_port'),
                'name' => 'smtp_port',
                'required' => false,
            ],
            'smtp_account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('smtp_account'),
                'name' => 'smtp_account',
                'required' => false,
            ],
            'smtp_password' => [
                'type' => 'password_input',
                'label' => $this->getValidAttrs('smtp_password'),
                'name' => 'smtp_password',
                'required' => false,
            ],
            'smtp_encryption' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('smtp_encryption'),
                'name' => 'smtp_encryption',
                'required' => false,
            ],
            'sex' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('sex'),
                'name' => 'sex',
                'show' => 'option',
                'required' => false,
                'options' => $this->model->present()->sex(),
            ],
            'sex_title' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('sex_title'),
                'name' => 'sex',
                'show' => 'option',
                'required' => false,
                'options' => $this->model->present()->sex_title(),
            ],
            'message' => [
                'type' => 'text',
                'show' => 'nl2br',
                'label' => $this->getValidAttrs('message'),
                'name' => 'message',
                'required' => false,
            ],
            'admin_note' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('admin_note'),
                'name' => 'admin_note',
                'required' => false,
            ],
        ];

        if (count($keys) == 0) {
            $keys = $this->getTemplateFormKeys();
        }

        if (count($keys)) {
            $fields = array_merge(
                array_flip((array) $keys),
                Arr::only($templateFormFields, $keys)
            );
            if ($this->template == 'default' && method_exists($this->model, 'categories') && in_array('categories', $keys)) {
                if (!empty($this->category) && $this->category->getLimitLevel() == 1) {
                    $fields['categories']['max'] = 1;
                }
            }
            return $fields;
        } else {
            return $templateFormFields;
        }
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => $this->getTemplateFormFields([
                'text', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getFormFields()
    {
        return $this->getTemplateFormFields();
    }
    protected function getCreateFormFields()
    {
        return $this->getFormFields();
    }
    protected function getEditFormFields()
    {
        if ($this->template == 'default' && in_array('created_at', $this->model->getFillable())) {
            $fields = Arr::insertAfter($this->getFormFields(), $this->getTemplateFormFields(['created_at']), 'text');
            return $fields;
        } else {
            return $this->getFormFields();
        }
    }

    /* 搜尋 */
    protected function getTemplateSearchKeys()
    {
        if ($this->template == 'default') {
            $keys = ['status', 'title', 'date_range', 'created_range'];
            if (method_exists($this->model, 'categories')) {
                $keys = Arr::prepend($keys, 'categories');
            }
        } else {
            $keys = [];
        }
        return $keys;
    }
    protected function getTemplateSearchFields($keys = [])
    {
        $templateSearchFields = [
            'categories' => [
                'type' => 'select',
                'label' => $this->getValidAttrs('categories'),
                'name' => 'categories',
                'options' => !empty($this->category) ? $this->category->tree()->get() : [],
                'search' => 'category',
                // 'search' => 'relation',
                // 'compare_fields' => 'id',
            ],
            'hidden_categories' => [
                'type' => 'hidden',
                'label' => $this->getValidAttrs('categories'),
                'name' => 'categories',
                'search' => 'category',
                // 'search' => 'relation',
                // 'compare_fields' => 'id',
            ],
            'group_id' => [
                'type' => 'select',
                'label' => $this->getValidAttrs('group_id'),
                'name' => 'group_id',
                'options' => method_exists($this->model, 'group') ? $this->model->group()->getRelated()->tree()->get() : [],
                'key' => 'title',
                'search' => 'equal',
            ],
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'options' => $this->model->present()->status(),
                'has_all' => true,
                'search' => 'equal',
            ],
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'search' => 'like',
            ],
            'account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('account'),
                'name' => 'account',
                'search' => 'like',
            ],
            'admin_account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('admin_account'),
                'name' => 'admin_account',
                'search' => 'like',
            ],
            'name' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('name'),
                'name' => 'name',
                'search' => 'like',
            ],
            'phone' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('phone'),
                'name' => 'phone',
                'search' => 'like',
            ],
            'email' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('email'),
                'name' => 'email',
                'search' => 'like',
            ],
            'date_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('date_range'),
                'name' => ['date_on', 'date_off'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search' => 'range',
                'compare_fields' => ['date_on', 'date_off']
            ],
            'created_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('created_range'),
                'name' => ['created_at1', 'created_at2'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search' => 'range',
                'compare_fields' => ['created_at', 'created_at']
            ],
             'updated_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('updated_range'),
                'name' => ['updated_at1', 'updated_at2'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search' => 'range',
                'compare_fields' => ['updated_at', 'updated_at']
            ],
            'login_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('login_range'),
                'name' => ['login_at1', 'login_at2'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search' => 'range',
                'compare_fields' => ['login_at', 'login_at']
            ],
        ];

        if (count($keys) == 0) {
            $keys = $this->getTemplateSearchKeys();
        }

        if (count($keys)) {
            $fields = array_merge(
                array_flip((array) $keys),
                Arr::only($templateSearchFields, $keys)
            );
            if ($this->template == 'default') {
                if (method_exists($this->model, 'categories') && $this->rank_all == false) {
                    $fields['categories'] = $templateSearchFields['hidden_categories'];
                }
            }
            return $fields;
        } else {
            return $templateSearchFields;
        }
    }
    protected function getSearchFields()
    {
        return $this->getTemplateSearchFields();
    }
    protected function getSearchBasicDatas()
    {
        if (request()->has('categories') && method_exists($this->model, 'categories') && $this->getSearchFields()['categories']['search'] == 'category') {
            $datas = $this->category
                ->find(request()->input('categories'))
                ->{$this->model->getTable()}()
                ->ordered('pivot_rank');
        } elseif(!is_null($this->parent) && !is_null($this->parent_relation)) {
            $datas = $this->parent
                ->{$this->parent_relation}()
                ->ordered();
        } else {
            $datas = $this->model
                ->ordered();
        }
        return $datas;
    }
    public function search()
    {
        $request = request();
        $datas = $this->getSearchBasicDatas();

        foreach ($this->getSearchFields() as $search_field) {
            if ($search_field['search'] == 'equal') {
                if ($request->filled($search_field['name'])) {
                    if (method_exists($this->model, 'translations') && in_array($search_field['name'], $this->model->translatedAttributes)) {
                        $datas->whereTranslation($search_field['name'], $request->input($search_field['name']));
                    } else {
                        $datas->where($search_field['name'], $request->input($search_field['name']));
                    }
                }
            } elseif ($search_field['search'] == 'find_in_set') {
                if ($request->filled($search_field['name'])) {
                    if (method_exists($this->model, 'translations') && in_array($search_field['name'], $this->model->translatedAttributes)) {
                        $datas->whereHas('translations', function($q) use ($search_field, $request) {
                            $q->whereRaw("FIND_IN_SET('".$request->input($search_field['name'])."',`".$search_field['name']."`)>0");
                        });
                    } else {
                        $datas->whereRaw("FIND_IN_SET('".$request->input($search_field['name'])."',`".$search_field['name']."`)>0");
                    }
                }
            } elseif ($search_field['search'] == 'like') {
                if ($request->filled($search_field['name'])) {
                    if (method_exists($this->model, 'translations') && in_array($search_field['name'], $this->model->translatedAttributes)) {
                        $datas->whereTranslationLike($search_field['name'], '%' . $request->input($search_field['name']) . '%');
                    } else {
                        $datas->where($search_field['name'], 'like', '%' . $request->input($search_field['name']) . '%');
                    }
                }
            } elseif ($search_field['search'] == 'range') {
                if ($search_field['compare_fields'][0] == $search_field['compare_fields'][1]) {
                    if ($request->filled($search_field['name'][0])) {
                        $datas->where($search_field['compare_fields'][0], '>=', $request->input($search_field['name'][0]));
                    }
                    if ($request->filled($search_field['name'][1])) {
                        $datas->where($search_field['compare_fields'][1], '<=', (new Carbon($request->input($search_field['name'][1])))->addDay());
                    }
                } else {
                    if ($request->filled($search_field['name'][0])) {
                        $datas->where(function ($q) use ($search_field, $request) {
                            $q->whereNull($search_field['compare_fields'][0])
                                ->orWhere($search_field['compare_fields'][0], '>=', $request->input($search_field['name'][0]));
                        })->where(function ($q) use ($search_field, $request) {
                            $q->whereNull($search_field['compare_fields'][1])
                                ->orWhere($search_field['compare_fields'][1], '>=', $request->input($search_field['name'][0]));
                        });
                    }
                    if ($request->filled($search_field['name'][1])) {
                        $datas->where(function ($q) use ($search_field, $request) {
                            $q->whereNull($search_field['compare_fields'][0])
                                ->orWhere($search_field['compare_fields'][0], '<=', $request->input($search_field['name'][1]));
                        })->where(function ($q) use ($search_field, $request) {
                            $q->whereNull($search_field['compare_fields'][1])
                                ->orWhere($search_field['compare_fields'][1], '<=', $request->input($search_field['name'][1]));
                        });
                    }
                }
            } elseif ($search_field['search'] == 'relation') {
                if ($request->filled($search_field['name'])) {
                    $datas->whereHas($search_field['relation']??$search_field['name'], function ($query) use ($search_field, $request) {
                        $query->where($search_field['compare_fields'], $request->input($search_field['name']));
                    });
                }
            } elseif ($search_field['search'] == 'relation_like') {
                if ($request->filled($search_field['name'])) {
                    $datas->whereHas($search_field['relation']??$search_field['name'], function ($query) use ($search_field, $request) {
                        $query->where($search_field['compare_fields'], 'like', '%' . $request->input($search_field['name']) . '%');
                    });
                }
            } elseif ($search_field['search'] == 'custom') {
                $datas = $this->searchCustom($request, $datas, $search_field);
            }
        }
        return $datas;
    }
    protected function searchCustom($request, $datas, $search_field)
    {
        return $datas;
    }

    /* 表單驗證 */
    protected function getTemplateValidRuleKeys()
    {
        if ($this->template == 'default') {
            $keys = ['title', 'slug', 'url', 'description', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'];
            if (method_exists($this->model, 'categories')) {
                $keys = Arr::insertAfter($keys, ['categories', 'categories.*']);
            }
        } elseif ($this->template == 'category') {
            $keys = ['title', 'slug', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'];
        } else {
            $keys = [];
        }
        return $keys;
    }
    protected function getTemplateValidRules($keys = [])
    {
        $templateValidRules = [
            'categories' => 'required|array',
            'categories.*' => 'required',
            'title' => ['required', Rule::unique($this->model->getTable(), 'title'), 'string', 'max:191'],
            'slug' => ['nullable', Rule::unique($this->model->getTable(), 'slug'), 'string', 'max:191'],
            'url' => 'nullable|url|max:255',
            'description' => 'nullable|string|max:255',
            'seo_title' => 'nullable|string|max:191',
            'seo_description' => 'nullable|string|max:255',
            'seo_keyword' => 'nullable|string|max:255',
            'og_title' => 'nullable|string|max:191',
            'og_description' => 'nullable|string|max:255',
        ];

        if (count($keys) == 0) {
            $keys = $this->getTemplateValidRuleKeys();
        }

        if (count($keys)) {
            $fields = array_merge(
                array_flip((array) $keys),
                Arr::only($templateValidRules, $keys)
            );
            return $fields;
        } else {
            return $templateValidRules;
        }
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
    protected function getValidRules()
    {
        return $this->getTemplateValidRules();
    }
    protected function getCreateValidRules()
    {
        return $this->getValidRules();
    }
    protected function getEditValidRules()
    {
        return $this->getValidRules();
    }

    protected function getValidAttrs(...$keys)
    {
        if (count($keys) == 1 && is_array($keys[0])) {
            $keys = $keys[0];
        }
        $attrs = array_merge(
            array_combine($keys, $keys),
            __('validation.attributes', [], env('BACKEND_LOCALE')),
            is_array(__('validation.' . str_replace('.', '_', $this->prefix), [], env('BACKEND_LOCALE'))) ? __('validation.' . str_replace('.', '_', $this->prefix), [], env('BACKEND_LOCALE')) : [],
            $this->valid_attrs
        );
        if (count($keys) == 1 && Arr::exists($attrs, head($keys))) {
            return $attrs[head($keys)];
        } elseif (count($keys) > 1) {
            return Arr::only($attrs, $keys);
        }
        return $attrs;
    }

    public function valid($requestData, $rules, $msgs, $id = null)
    {
        if (method_exists($this->model, 'translations') || $this->locales->count() > 1) {
            $locales = $this->locales;
        } else {
            $locales = $this->locales->where('code', env('BACKEND_LOCALE'));
        }
        $valid_rules = [];
        $valid_msgs = $msgs;
        $valid_attrs = [];
        foreach ($rules as $name => $rule) {
            if (count($locales)>1 && ($this->model->getTable()=='settings' || method_exists($this->model, 'translations')) && in_array($name, $this->data->translatedAttributes)) {
                foreach ($locales as $locale) {
                    $locale_code = $locale->code;
                    $myRule[$locale->code][$name] = $rule;
                    if (is_array($rule)) {
                        foreach ($rule as $k => $r) {
                            if (is_a($r, 'Illuminate\Validation\Rules\Unique')) {
                                $myRule[$locale->code][$name][$k] = Rule::unique($this->data->translations()->getRelated()->getTable(), $name)->where(function ($query) use ($locale_code) {
                                    return $query->where('locale', $locale_code);
                                })->ignore($id, Str::singular($this->data->getTable()) . '_id');
                                // $myRule[$locale->code][$name][$k] = $myRule[$locale->code][$name][$k]->ignore($id, Str::singular($this->data->getTable()).'_id');
                            } else {
                                $myRule[$locale->code][$name][$k] = $r;
                            }
                        }
                    }
                    $valid_rules[$locale->code . '.' . $name] = $myRule[$locale->code][$name];
                    $valid_attrs[$locale->code . '.' . $name] = $this->getValidAttrs($name) . " [ $locale->title ]";
                }
            } else {
                $myRule[$name] = $rule;
                if (is_array($rule)) {
                    foreach ($rule as $k => $r) {
                        if (is_a($r, 'Illuminate\Validation\Rules\Unique')) {
                            if (method_exists($this->model, 'translations') && in_array($name, $this->data->translatedAttributes)) {
                                $locale_code = $locales->first()->code;
                                $myRule[$name][$k] = Rule::unique($this->data->translations()->getRelated()->getTable(), $name)->where(function ($query) use ($locale_code) {
                                    return $query->where('locale', $locale_code);
                                })->ignore($id, Str::singular($this->data->getTable()) . '_id');
                            } else {
                                $myRule[$name][$k] = $myRule[$name][$k]->ignore($id);
                            }
                        } else {
                            $myRule[$name][$k] = $r;
                        }
                    }
                }
                $valid_rules[$name] = $myRule[$name];
                $valid_attrs[$name] = $this->getValidAttrs($name);
            }
        }
        return Validator::make($requestData, $valid_rules, $valid_msgs, $valid_attrs);
    }

    /* 列表資料顯示 */
    protected function getDropdowns()
    {
        $dropdowns = [
            'status' => [
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'options' => $this->model->present()->status(),
            ],
            // 'is_top' => [
            //     'label' => $this->getValidAttrs('is_top'),
            //     'name' => 'is_top',
            //     'options' => $this->model->present()->yes_or_no(),
            // ],
        ];
        return $dropdowns;
    }
    protected function getTemplateListKeys()
    {
        $keys = ['title', 'date_range', 'created_at', 'status'];
        if (method_exists($this->model, 'categories')) {
            $keys = Arr::insertAfter($keys, ['categories'], 'title');
        }
        if (isset($this->getFormFields()['is_top'])) {
            $keys = Arr::insertAfter($keys, ['is_top'], 'created_at');
        }
        // $keys = $this->checkTemplateListKeysPower($keys);
        return $keys;
    }
    protected function checkTemplateListKeysPower($keys)
    {
        $auth_admin = auth()->guard('admin')->user();
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.copy") && Route::has("backend.$this->prefix.copy")) {
            $keys[] = 'copy';
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.edit") && Route::has("backend.$this->prefix.edit")) {
            $keys[] = 'edit';
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.destroy") && Route::has("backend.$this->prefix.destroy")) {
            $keys[] = 'delete';
        }
        return $keys;
    }
    protected function checkListPower($data)
    {
        $fields = [];
        $auth_admin = auth()->guard('admin')->user();
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.copy") && Route::has("backend.$this->prefix.copy")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['copy']));
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.edit") && Route::has("backend.$this->prefix.edit")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['edit']));
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.destroy") && Route::has("backend.$this->prefix.destroy")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['delete']));
        }
        return $fields;
    }
    protected function getTemplateList($data, $keys = [])
    {
        $templateList = [
            'pic' => [
                'label' => $this->getValidAttrs('pic'),
                'align' => 'left',
                'type' => 'link',
                'data' => '<img src="'.imgsrc($data->pic_path).'"  class="list-pic">',
                'url' => Route::has("backend.$this->prefix.edit")?route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
            ],
            'title' => [
                'label' => $this->getValidAttrs('title'),
                'align' => 'left',
                'type' => 'link',
                'data' => $data->title,
                'url' => Route::has("backend.$this->prefix.edit")?route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
            ],
            'account' => [
                'label' => $this->getValidAttrs('account'),
                'align' => 'left',
                'type' => 'link',
                'data' => $data->account,
                'url' => Route::has("backend.$this->prefix.edit")?route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
            ],
            'group_id' => [
                'label' => $this->getValidAttrs('group_id'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->group_id?$data->group->title:'',
            ],
            'admin_account' => [
                'label' => $this->getValidAttrs('admin_account'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->admin_account,
            ],
            'name' => [
                'label' => $this->getValidAttrs('name'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->name,
            ],
            'sex' => [
                'label' => $this->getValidAttrs('sex'),
                'align' => 'left',
                'type' => 'text',
                'data' => !is_null($data->sex)?$data->present()->sex($data->sex):'',
            ],
            'sex_title' => [
                'label' => $this->getValidAttrs('sex_title'),
                'align' => 'left',
                'type' => 'text',
                'data' => !is_null($data->sex)?$data->present()->sex_title($data->sex):'',
            ],
            'phone' => [
                'label' => $this->getValidAttrs('phone'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->phone,
            ],
            'email' => [
                'label' => $this->getValidAttrs('email'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->email,
            ],
            'admin_note' => [
                'label' => $this->getValidAttrs('admin_note'),
                'align' => 'left',
                'type' => 'html',
                'data' => nl2br($data->admin_note),
            ],
            'login_at' => [
                'label' => $this->getValidAttrs('login_at'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->login_at,
            ],
            'categories' => [
                'label' => $this->getValidAttrs('categories'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->categories ? $data->categories->implode('title', ', ') : '',
            ],
            'date_range' => [
                'label' => $this->getValidAttrs('date_range'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->present()->date_range($data->date_on, $data->date_off),
            ],
            'updated_at' => [
                'label' => $this->getValidAttrs('updated_at'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->updated_at,
            ],
            'created_at' => [
                'label' => $this->getValidAttrs('created_at'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->created_at,
            ],
            'is_top' => [
                'label' => $this->getValidAttrs('is_top'),
                'align' => 'center',
                'type' => 'html',
                'data' => $data->present()->checkSetTop(),
            ],
            'status' => [
                'label' => $this->getValidAttrs('status'),
                'align' => 'center',
                'type' => 'html',
                'options' => $this->model->present()->status(),
                'data' => !is_null($data->status)?$this->model->present()->status($data->status):'',
            ],
            'copy' => [
                'label' => __('backend.copy', [], env('BACKEND_LOCALE')),
                'align' => 'center',
                'type' => 'btn',
                'data' => '<span class="fa fa-copy"></span>',
                'url' => Route::has("backend.$this->prefix.copy")?route('backend.' . $this->prefix . '.copy', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
            ],
            'edit' => [
                'label' => __('backend.edit', [], env('BACKEND_LOCALE')),
                'align' => 'center',
                'type' => 'btn',
                'data' => '<span class="glyphicon glyphicon-pencil"></span>',
                'url' => Route::has("backend.$this->prefix.edit")?route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
            ],
            'delete' => [
                'label' => __('backend.delete', [], env('BACKEND_LOCALE')),
                'align' => 'center',
                'type' => 'btn',
                'data' => '<span class="'.($data->deletable?'glyphicon glyphicon-trash':'fa fa-ban').'"></span>',
                'url' => $data->deletable?(Route::has("backend.$this->prefix.destroy")?route('backend.' . $this->prefix . '.destroy', array_merge(request()->route()->parameters, ['id' => $data->id])):''):'',
                'class' => 'btn-default '.($data->deletable?'btn-delete-row':'delete-ban'),
                'tooltip' => $data->deletable?'':__('backend.delete_ban', [], env('BACKEND_LOCALE')),
            ],
        ];
        if (count($keys) == 0) {
            $keys = $this->getTemplateListKeys();
        }

        if (count($keys)) {
            $fields = array_merge(
                array_flip((array) $keys),
                Arr::only($templateList, $keys)
            );
            return $fields;
        } else {
            return $templateList;
        }
    }
    protected function getIndexList($data)
    {
        return $this->getTemplateList($data);
    }
    public function generateListData($datas)
    {
        $list_datas = [];
        foreach ($datas as $data) {
            $list_datas[$data->id] = array_merge($this->getIndexList($data), $this->checkListPower($data));
        }
        return $list_datas;
    }

    public function fillAndSave($data, $requestData)
    {
        $result = [];
        foreach ($requestData as $locale => $saveDatas) {
            if ($data->has($locale)) {
                $result[$locale] = $data[$locale]->fillAndSave($saveDatas);
            }
        }
        return $result;
    }

    // 排序頁顯示的欄位資料
    protected function getRankFields()
    {
        $fields = [
            // 'pic' => [
            //   'type' => 'img',
            //   'name' => 'pic',
            // ],
            'title' => [
                'type' => 'text',
                'name' => 'title',
            ],
        ];
        return $fields;
    }
    protected function getRankDropdowns()
    {
        $dropdowns = [];
        if (method_exists($this->model, 'categories')) {
            $dropdowns['categories'] = [
                'label' => $this->getValidAttrs('categories'),
                'name' => 'categories',
                'options' => $this->category->tree()->get(),
            ];
        }
        return $dropdowns;
    }
    protected function rank_flatten($array)
    {
        if (!is_array($array)) {
            return false;
        }
        $result = array();
        foreach ($array as $value) {
            array_push($result, $value->id);
            if (property_exists($value, 'children')) {
                $result = array_merge($result, $this->rank_flatten($value->children));
            }
        }
        return $result;
    }
    protected function update_level($array, $lv = 0, $parent = 0)
    {
        $level = $lv;
        if (!is_array($array)) {
            return false;
        }
        $result = array();
        foreach ($array as $value) {
            array_push($result, $value->id);
            if (property_exists($value, 'children')) {
                $level = $lv + 1;
                if ($level >= $this->model->getLimitLevel()) {
                    $parent_id = $parent;
                    $this->rank_msg = __('backend.level_max_error', ['limit' => $this->model->getLimitLevel()], env('BACKEND_LOCALE'));
                    $result = array_merge($result, $this->update_level($value->children, $level, $parent_id));
                } else {
                    $parent_id = $value->id;
                    $this->levelUpdate += $this->model->whereIn('id', $this->update_level($value->children, $level))->update(['parent_id' => $parent_id]);
                }
            }
        }
        return $result;
    }
}
