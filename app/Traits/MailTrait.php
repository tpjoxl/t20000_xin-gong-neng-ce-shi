<?php

namespace App\Traits;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\Setting;
use Exception;
use Illuminate\Support\Facades\Log;

trait MailTrait
{
    public function SendMailToAdmin($mail)
    {
        $setting = app('siteSettings');
        if (!empty($setting->site_mail)) {
            $emails = explode(',', preg_replace('/\s(?=)/', '', $setting->site_mail));
            $bccEmails = array_slice($emails, 1);
            try {
                Mail::to($emails[0])->bcc($bccEmails)->queue($mail);

                return !Mail::failures();
            } catch (Exception $e) {
                Log::channel('mail')->error('mail error', ['message' => $e->getMessage()]);
                return false;
            }
        } else {
            // 後台沒有填管理員信箱則直接回傳
            return true;
        }
    }
    public function SendMailTo($to, $mail)
    {
        try {
            Mail::to($to)->queue($mail);
            return !Mail::failures();
        } catch (Exception $e) {
            Log::channel('mail')->error('mail error', ['message' => $e->getMessage()]);
            return false;
        }
    }
}
