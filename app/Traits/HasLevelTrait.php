<?php

namespace App\Traits;

trait HasLevelTrait
{
    public function initializeHasLevelTrait()
    {
        // $this->with[] = 'children';
        $this->fillable[] = 'parent_id';

        $this->appends[] = 'level';
        $this->appends[] = 'full_title';
        $this->appends[] = 'full_parents_id';
        $this->appends[] = 'full_children_id';

        $this->visible[] = 'id';
        $this->visible[] = 'level';
        $this->visible[] = 'full_title';
        $this->visible[] = 'full_parents_id';
        $this->visible[] = 'full_children_id';
        $this->visible[] = 'children';
    }
    public function parent()
    {
        return $this->belongsTo(get_class($this), 'parent_id');
    }
    public function children()
    {
        return $this->hasMany(get_class($this), 'parent_id')->ordered();
    }
    public function getLimitLevel()
    {
        return $this->limit_level;
    }
    public function getLevelAttribute()
    {
        return $this->getLevel();
    }
    public function getFullTitleAttribute()
    {
        return $this->getFullParents()->reverse()->push($this)->implode('title', ' / ');
    }
    public function getFullParentsIdAttribute()
    {
        return $this->getFullParents()->reverse()->push($this)->implode('id', ',');
    }
    public function getFullChildrenIdAttribute()
    {
        return $this->getFullChildren()->prepend($this)->implode('id', ',');
    }
    public function getLevel($lv = 1)
    {
        $level = $lv;
        if (!is_null($this->parent_id)) {
            $level += $this->parent->getLevel($level);
        }
        return $level;
    }
    public function getFullParents($parents = null)
    {
        if (is_null($parents)) {
            $parents = collect();
        }

        if (!is_null($this->parent_id)) {
            $parents->push($this->parent);
            $parents = $this->parent->getFullParents($parents);
        }
        return $parents;
    }
    public function getFullChildren($children = null)
    {
        if (is_null($children)) {
            $children = collect();
        }

        if ($this->children->count()) {
            foreach ($this->children as $child) {
                $children->push($child);
                $children = $child->getFullChildren($children);
            }
        }
        return $children;
    }
    public function getTree($parent_id = null)
    {
        return $this->where('parent_id', $parent_id)->ordered()->get();
    }
    public function scopeTree($query, $parent_id = null, $limit_level = null)
    {
        if (is_null($limit_level)) {
            $limit_level = $this->limit_level;
        } elseif ($limit_level < 0) {
            $limit_level += $this->limit_level;
        }
        $childrenWith = [];
        for ($i = 1; $i <= $limit_level - 1; $i++) {
            $arr = array_fill(0, $i, 'children');
            $childrenWith[] = implode('.', $arr);
        }
        return $query
            ->when(count($childrenWith), function ($q) use ($childrenWith) {
                $q->with($childrenWith);
            })
            ->where('parent_id', $parent_id)->ordered();
    }
    public function scopeDisplayTree($query, $parent_id = null, $limit_level = null)
    {
        if (is_null($limit_level)) {
            $limit_level = $this->limit_level;
        } elseif ($limit_level < 0) {
            $limit_level += $this->limit_level;
        }
        $childrenWith = [];
        for ($i = 1; $i <= $limit_level - 1; $i++) {
            $arr = array_fill(0, $i, 'children');
            $childrenWith[implode('.', $arr)] = function($q) {
                $q->display();
            };
        }
        return $query
            ->when(count($childrenWith), function ($q) use ($childrenWith) {
                $q->with($childrenWith);
            })
            ->where('parent_id', $parent_id)->ordered();
    }
}
