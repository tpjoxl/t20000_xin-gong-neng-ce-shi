<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Setting;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\EcpayRecord;
use App\Payment\Ecpay;
use ECPaySdk\ECPay_PaymentMethod;

trait EcpayTrait
{
    public function ecpayCheckout($order)
    {
        try {
            $obj = new Ecpay();

            //基本參數(請依系統規劃自行調整)
            $obj->Send['ReturnURL']         = route($this->ecpayReturnRoute);           //付款完成通知回傳的網址
            $obj->Send['MerchantTradeNo']   = $order->epaynumber;                       //訂單編號
            $obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                      //交易時間
            $obj->Send['TotalAmount']       = (int)$order->ecpayData['TotalAmount'];    //交易金額
            $obj->Send['TradeDesc']         = $order->num;                              //交易描述

            if (($order->payment_info['type']??$order->payment['type']) == 'CREDIT') { // 信用卡
                $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::Credit ;         //付款方式:Credit
                $obj->Send['IgnorePayment']     = ECPay_PaymentMethod::GooglePay ;      //不使用付款方式:GooglePay
                $obj->Send['OrderResultURL']    = route($this->ecpayOrderResultRoute) ;      //刷卡完成轉回來的網址

                //Credit信用卡分期付款延伸參數(可依系統需求選擇是否代入)
                //以下參數不可以跟信用卡定期定額參數一起設定
                // $obj->SendExtend['CreditInstallment'] = '' ;    //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
                // $obj->SendExtend['InstallmentAmount'] = 0 ;    //使用刷卡分期的付款金額，預設0(不分期)
                // $obj->SendExtend['Redeem'] = false ;           //是否使用紅利折抵，預設false
                // $obj->SendExtend['UnionPay'] = false;          //是否為聯營卡，預設false;

                //Credit信用卡定期定額付款延伸參數(可依系統需求選擇是否代入)
                //以下參數不可以跟信用卡分期付款參數一起設定
                // $obj->SendExtend['PeriodAmount'] = '' ;    //每次授權金額，預設空字串
                // $obj->SendExtend['PeriodType']   = '' ;    //週期種類，預設空字串
                // $obj->SendExtend['Frequency']    = '' ;    //執行頻率，預設空字串
                // $obj->SendExtend['ExecTimes']    = '' ;    //執行次數，預設空字串
            } elseif (($order->payment_info['type']??$order->payment['type']) == 'ATM') { // ATM虛擬帳戶轉帳
                $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::ATM ;            //付款方式:ATM

                //ATM 延伸參數(可依系統需求選擇是否代入)
                $obj->SendExtend['ExpireDate'] = 3 ;     //繳費期限 (預設3天，最長60天，最短1天)
                $obj->SendExtend['PaymentInfoURL']    = route($this->ecpayPaymentInfoRoute);     //取號成功或失敗 背景回傳付款相關資訊。
                $obj->SendExtend['ClientRedirectURL'] = route($this->ecpayClientRedirectRoute);  //回來顯示操作結果的網址
            } elseif (($order->payment_info['type']??$order->payment['type']) == 'CVS') { // 超商代碼
                $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::CVS ;            //付款方式:CVS超商代碼

                //CVS超商代碼延伸參數(可依系統需求選擇是否代入)
                $obj->SendExtend['PaymentInfoURL']    = route($this->ecpayPaymentInfoRoute);     //代碼產生成功或失敗 背景回傳付款相關資訊。
                $obj->SendExtend['ClientRedirectURL'] = route($this->ecpayClientRedirectRoute);  //回來顯示操作結果的網址
            } elseif (($order->payment_info['type']??$order->payment['type']) == 'BARCODE') { // 超商條碼
                $obj->Send['ChoosePayment']     = ECPay_PaymentMethod::BARCODE ;        //付款方式:BARCODE超商代碼

                //BARCODE超商條碼延伸參數(可依系統需求選擇是否代入)
                $obj->SendExtend['PaymentInfoURL']    = route($this->ecpayPaymentInfoRoute);     //代碼產生成功或失敗 背景回傳付款相關資訊。
                $obj->SendExtend['ClientRedirectURL'] = route($this->ecpayClientRedirectRoute);  //回來顯示操作結果的網址
            }

            //訂單的商品資料
            $obj->Send['Items'] = $order->ecpayData['Items'];

            # 電子發票參數
            /*
            $obj->Send['InvoiceMark'] = ECPay_InvoiceState::Yes;
            $obj->SendExtend['RelateNumber'] = "Test".time();
            $obj->SendExtend['CustomerEmail'] = 'test@ecpay.com.tw';
            $obj->SendExtend['CustomerPhone'] = '0911222333';
            $obj->SendExtend['TaxType'] = ECPay_TaxType::Dutiable;
            $obj->SendExtend['CustomerAddr'] = '台北市南港區三重路19-2號5樓D棟';
            $obj->SendExtend['InvoiceItems'] = array();
            // 將商品加入電子發票商品列表陣列
            foreach ($obj->Send['Items'] as $info)
            {
                array_push($obj->SendExtend['InvoiceItems'],array('Name' => $info['Name'],'Count' =>
                    $info['Quantity'],'Word' => '個','Price' => $info['Price'],'TaxType' => ECPay_TaxType::Dutiable));
            }
            $obj->SendExtend['InvoiceRemark'] = '測試發票備註';
            $obj->SendExtend['DelayDay'] = '0';
            $obj->SendExtend['InvType'] = ECPay_InvType::General;
            */

            Log::channel('payment')->info('ecpay checkout', [
                'provider' => $order->payment_info['provider']??$order->payment['provider'],
                'type' => $order->payment_info['type']??$order->payment['type'],
                'Send' => array_filter($obj->Send),
            ]);

            //產生訂單(auto submit至ECPay)
            $obj->CheckOut();
        } catch (Exception $e) {
            Log::channel('payment')->error('ecpay checkout error', ['num' => $order->num, 'message' => $e->getMessage()]);
            return redirect()->route($this->ecpayResultRoute)
                ->with('num', $order->num)
                ->with('result', '付款失敗')
                ->with('resultText', $e->getMessage());
        }
    }
    // 付款完成背景回傳資訊與檢查碼處理(包含ATM與線上刷卡)
    public function ecpayReturn(Request $request)
    {
        $requestData = $request->all();
        try {
            // 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
            $obj = new Ecpay();
            $feedback = $obj->CheckOutFeedback();
            Log::channel('payment')->info('ecpay return', ['feedback' => $feedback, 'response' => $requestData]);

            // 儲存回傳訊息
            $recordData = $requestData;
            $recordData['route_name'] = Route::currentRouteName();
            $record = new EcpayRecord($recordData);
            $record->save();

            // 在網頁端回應 1|OK
            echo '1|OK';

            $this->afterPaid($record);
        } catch(Exception $e) {
            echo '0|' . $e->getMessage();
        }
    }
    // 完成取號後的導回頁面(ATM虛擬帳戶轉帳、超商代碼、超商條碼)
    public function ecpayPaymentInfo(Request $request)
    {
        $requestData = $request->all();
        // 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
        $obj = new Ecpay();
        $feedback = $obj->CheckOutFeedback();
        Log::channel('payment')->info('ecpay return payment info', ['feedback' => $feedback, 'response' => $requestData]);

        // 儲存回傳訊息
        $recordData = $requestData;
        $recordData['route_name'] = Route::currentRouteName();
        $record = new EcpayRecord($recordData);
        $record->save();

        // 在網頁端回應 1|OK
        echo '1|OK';

        $this->afterPayinfo($record);
    }
    // 即時交易回來的頁面
    public function ecpayOrderResult(Request $request)
    {
        $requestData = $request->all();
        // 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
        $obj = new Ecpay();
        $feedback = $obj->CheckOutFeedback();
        Log::channel('payment')->info('ecpay order result', ['feedback' => $feedback, 'response' => $requestData]);

        // 儲存回傳訊息
        $recordData = $requestData;
        $recordData['route_name'] = Route::currentRouteName();
        $record = new EcpayRecord($recordData);
        $record->save();

        $order = $this->model->where('epaynumber', $requestData['MerchantTradeNo'])->first();
        if ($request->RtnCode==1){
            return redirect()->route($this->ecpayResultRoute)
                ->with('num', $order->num)
                ->with('result', '付款成功');
        } else {
            return redirect()->route($this->ecpayResultRoute)
                ->with('num', $order->num)
                ->with('result', '付款失敗')
                ->with('resultText', '您可由會員訂單管理頁面內點選「付款」按鈕重新付款，謝謝您！');
        }
    }
    // 非即時交易回來的頁面
    public function ecpayClientRedirect(Request $request)
    {
        $requestData = $request->all();
        // 收到綠界科技的付款結果訊息，並判斷檢查碼是否相符
        $obj = new Ecpay();
        $feedback = $obj->CheckOutFeedback();
        Log::channel('payment')->info('ecpay client redirect', ['feedback' => $feedback, 'response' => $requestData]);

        // 儲存回傳訊息
        $recordData = $requestData;
        $recordData['route_name'] = Route::currentRouteName();
        $record = new EcpayRecord($recordData);
        $record->save();

        $order = $this->model->where('epaynumber', $requestData['MerchantTradeNo'])->first();
        if ($requestData['RtnCode'] == '2' || $requestData['RtnCode'] == '10100073'){
            return redirect()->route($this->ecpayResultRoute)
                ->with('num', $order->num)
                ->with('result', '取號成功');
        } else {
            return redirect()->route($this->ecpayResultRoute)
                ->with('num', $order->num)
                ->with('result', '取號失敗')
                ->with('resultText', '您可由會員訂單管理頁面內點選「付款」按鈕重新付款，謝謝您！');
        }
    }
}
