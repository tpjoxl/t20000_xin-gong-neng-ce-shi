<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use App\Helpers\ArrHelper as Arr;

trait UploadableTrait
{
    public static function bootUploadableTrait()
	{
		static::deleting(function ($model) {
            if (count($model->uploadable)) {
                Storage::disk(env('FILESYSTEM_DRIVER'))->deleteDirectory(($model->uploadFoleder??$model->getTable()).'/'.$model->{$model->uploadFolederKey??'id'});
            }
        });
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->uploadable)) {
            parent::setAttribute($key, serialize(is_null($value)?null:$value));
        } else {
            parent::setAttribute($key, $value);
        }
    }
    public function getAttribute($key)
    {
        if (in_array($key, $this->uploadable)) {
            return is_null(parent::getAttribute($key))?null:unserialize(parent::getAttribute($key));
        } else {
            return parent::getAttribute($key);
        }
    }
    public function handleFiles($datas)
    {
        if (count(Arr::only($datas, $this->uploadable))) {
            $this->fill($this->fileUpload(Arr::only($datas, $this->uploadable)))->save();
        }
        if (Arr::has($datas, 'delete_files')) {
            $deleteFiles = [];
            $saveFileData = [];
            foreach (Arr::get($datas, 'delete_files') as $key => $fileIndex) {
                $deleteFiles = array_merge($deleteFiles, $fileIndex);
                $saveFileData[$key] = array_diff($this->$key, $fileIndex);
            }
            $this->fill($saveFileData)->save();
            $this->fileDelete($deleteFiles);
        }
    }
    public function fileUpload($datas)
    {
        $uploadFileArr = [];
        $deleteFiles = [];
        if (count($datas)) {
            foreach ($this->uploadable as $key) {
                if (Arr::has($datas, $key)) {
                    $files = Arr::get($datas, $key);
                    $uploadFileArr[$key] = $this->$key;
                    if (is_array($files)) {
                        foreach ($files as $i => $file) {
                            if (is_array($this->$key) && array_key_exists($i, $this->$key)) {
                                $deleteFiles[] = $this->$key[$i];
                            }

                            $uploadFileArr[$key][$i] = $this->fileSave($file);
                        }
                    } else {
                        $deleteFiles[] = $this->$key;
                        $uploadFileArr[$key] = $this->fileSave($files);
                    }
                }
            }
            if (count($deleteFiles)) {
                $this->fileDelete($deleteFiles);
            }
        }
        return $uploadFileArr;
    }
    public function fileSave($file)
    {
        $name = $file->getClientOriginalName();
        $ext = $file->extension();
        $fileName = pathinfo($name, PATHINFO_FILENAME)."_".date('YmdHis').".$ext";
        $title = str_replace('.'.$ext,'',$name);
        $folder = ($this->uploadFoleder??$this->getTable()).'/'.$this->{$this->uploadFolederKey??'id'};
        $path = $file->storeAs($folder, $fileName);
        return '/'.env('FILESYSTEM_DRIVER').'/'.$path;
    }
    public function fileDelete($deleteFiles)
    {
        $deleteFiles = array_filter($deleteFiles, function ($path) {
            return strpos($path, 'http')!==0;
        });
        Storage::disk(env('FILESYSTEM_DRIVER'))->delete(array_map(function($path) {
            return str_replace("/uploads/","",$path);
        }, $deleteFiles));
    }
}
