<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Models\EzpayInvoiceRecord;
use App\Payment\EzpayInvoice;
use App\Mail\UserOrderInvoice;
use App\Traits\MailTrait;

trait EzpayInvoiceTrait
{
    use MailTrait;
    public function ezpayInvoice()
    {
        try {
            $invoiceSum = $this->sum - $this->payment_fee;
            $obj = new EzpayInvoice;
            $obj->Send['MerchantOrderNo'] = $this->num;
            $obj->Send['Status'] = 1; // *開立發票方式(1=即時開立發票；0=等待觸發開立發票；3=預約自動開立發票)
            $obj->Send['Category'] = 'B2C'; // *發票種類(B2B=買受人為營業人；B2C=買受人為個人)
            $obj->Send["BuyerName"] = $this->user->name; // *買受人名稱(B2B=買方營業人名稱，長度限制 60 字元，若長度不足使用則帶入買方統一編號。；B2C=個人姓名或商店通知消費者之識別碼，如：會員編號，長度限制 30 字元。)
            $obj->Send["BuyerEmail"] = $this->user->email; // 買受人電子信箱(CarrierType=2 時，則此參數為必填。)
            $obj->Send["PrintFlag"] = 'N'; // *索取紙本發票(Y=索取；N=不索取)(Category=B2B 時，則此參數必填 Y。Category=B2C 時，若 CarrierType、LoveCode 參數皆為空值，則此參數必填Y。)
            $obj->Send["TaxType"] = '1'; // *課稅別(1=應稅；2=零稅率；3=免稅；9=混合應稅與免稅或零稅率(限Category=B2C時使用))
            $obj->Send["TaxRate"] = 5; // *稅率(TaxType=1時，一般稅率請帶 5，特種稅率請帶入規定的課稅稅率(不含%，例稅率18%則帶入 18)。TaxType=2或3時，稅率請帶入 0)
            $obj->Send["Amt"] = round($invoiceSum / 1.05); // *銷售額合計(未稅)(TaxType =9 混合應稅與免稅或零稅率時，此欄位為 AmtSales+ AmtZero + AmtFree 欄位之合計金額。)
            $obj->Send["TaxAmt"] = $invoiceSum - round($invoiceSum / 1.05); // *稅額
            $obj->Send["TotalAmt"] = $invoiceSum; // *發票總金額(含稅)(TotalAmt=Amt+TaxAmt)
            $obj->Send["ItemName"] = '平台服務費'; // *商品名稱(多項商品時，商品名稱以 | 分隔。例：ItemName="商品一|商品二")
            $obj->Send["ItemCount"] = 1; // *商品數量(多項商品時，商品數量以 |分隔。例：ItemCount ="1|2")
            $obj->Send["ItemUnit"] = '次'; // *商品單位(限中文 2 字或英數 6 字。多項商品時，商品單位以 | 分隔。例：ItemUnit ="個|本")
            $obj->Send["ItemPrice"] = $invoiceSum; // *商品單價(Category=B2B 時，此參數金額為未稅金額。Category=B2C 時，此參數金額為含稅金額。多項商品時，商品單價以 | 分隔。例：ItemPrice ="200|100")
            $obj->Send["ItemAmt"] = $invoiceSum; // *商品小計(ItemAmt = ItemCount * ItemPrice)(Category=B2B 時，此參數金額為未稅金額。Category=B2C 時，此參數金額為含稅金額。多項商品時，商品單價以 | 分隔。例：ItemPrice ="200|100")
            $obj->Send["Comment"] = $this->payment_id == 11 && $this->records->whereNotNull('Card4No')->first() ? ('信用卡末四碼：' . $this->records->whereNotNull('Card4No')->first()->Card4No) : ''; // 備註(信用卡繳費時需填卡號末四碼)
            if ($this->invoice == 1) {
                // 二聯式
                if (is_null($this->invoice_mobile) && is_null($this->invoice_certificate)) {
                    // 會員載具
                    $obj->Send["CarrierType"] = '2'; // 載具類別(Category=B2C 時，才適用此參數。0=手機條碼載具；1=自然人憑證條碼載具；2=ezPay 電子發票載具；null=捐贈)
                    $obj->Send["CarrierNum"] = rawurlencode($this->user->email); // 載具編號(當 CarrierType 為 ezPay 電子發票載具時，此參數請提供可識別買受人之代號(例：e-mail、手機號碼、會員編號…等)，由賣方自訂即可，同一個代號則視為同一個買受人。本平台將以賣方統編加上買受人代號做為該買受人的 ezPay 電子發票載具號碼。)
                } elseif (!is_null($this->invoice_mobile)) {
                    // 手機載具
                    $obj->Send["CarrierType"] = '0'; // 載具類別(Category=B2C 時，才適用此參數。0=手機條碼載具；1=自然人憑證條碼載具；2=ezPay 電子發票載具；null=捐贈)
                    $obj->Send["CarrierNum"] = rawurlencode($this->invoice_mobile); // 載具編號
                } elseif (!is_null($this->invoice_certificate)) {
                    // 自然人憑證
                    $obj->Send["CarrierType"] = '1'; // 載具類別(Category=B2C 時，才適用此參數。0=手機條碼載具；1=自然人憑證條碼載具；2=ezPay 電子發票載具；null=捐贈)
                    $obj->Send["CarrierNum"] = rawurlencode($this->invoice_certificate); // 載具編號
                }
            } elseif ($this->invoice == 2) {
                // 三聯式
                $obj->Send['Category'] = 'B2B'; // *發票種類(B2B=買受人為營業人；B2C=買受人為個人)
                $obj->Send["BuyerName"] = strlen($this->ein_title) > 60 ? $this->ein_num : $this->ein_title; // *買受人名稱(B2B=買方營業人名稱，長度限制 60 字元，若長度不足使用則帶入買方統一編號。；B2C=個人姓名或商店通知消費者之識別碼，如：會員編號，長度限制 30 字元。)
                $obj->Send["BuyerUBN"] = $this->ein_num; // 買受人統一編號(B2C時，則不須填寫。)
                $obj->Send["PrintFlag"] = 'Y'; // *索取紙本發票(Y=索取；N=不索取)(Category=B2B 時，則此參數必填 Y。Category=B2C 時，若 CarrierType、LoveCode 參數皆為空值，則此參數必填Y。)
                $obj->Send["ItemPrice"] = round($invoiceSum / 1.05); // *商品單價(Category=B2B 時，此參數金額為未稅金額。Category=B2C 時，此參數金額為含稅金額。多項商品時，商品單價以 | 分隔。例：ItemPrice ="200|100")
                $obj->Send["ItemAmt"] = round($invoiceSum / 1.05); // *商品小計(ItemAmt = ItemCount * ItemPrice)(Category=B2B 時，此參數金額為未稅金額。Category=B2C 時，此參數金額為含稅金額。多項商品時，商品單價以 | 分隔。例：ItemPrice ="200|100")
            } elseif ($this->invoice == 3) {
                // 捐贈
                $obj->Send["CarrierType"] = null; // 載具類別(Category=B2C 時，才適用此參數。0=手機條碼載具；1=自然人憑證條碼載具；2=ezPay 電子發票載具；null=捐贈)
                $obj->Send["LoveCode"] = $this->donate_code; // 捐贈碼(Category=B2C 時，才適用此參數。)
            }
            // dd($obj);
            Log::channel('payment')->info('ezpay invoice send', [
                'Send' => array_filter($obj->Send),
            ]);
            $result = $obj->proccess();
            $response = json_decode($result['web_info'], true);
            Log::channel('payment')->info('ezpay invoice send result', [
                'result' => $result,
                'response' => $response,
            ]);
            $recordData = json_decode($response["Result"], true);
            $recordData['Status'] = $response['Status'];
            $recordData['Message'] = $response['Message'];
            $record = new EzpayInvoiceRecord($recordData);
            $record->save();

            if ($response['Status'] == 'SUCCESS') {
                // 寄通知信
                // $this->SendMailTo($this->user->email, new UserOrderInvoice($record));
            }
        } catch (Exception $e) {
            Log::channel('payment')->error('ezpay invoice error', ['num' => $this->num, 'message' => $e->getMessage()]);
        }
    }
}
