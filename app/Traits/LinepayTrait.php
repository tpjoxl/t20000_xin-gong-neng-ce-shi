<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Setting;
use App\Models\Order;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\LinepayRecord;
use App\Payment\Linepay;

trait LinepayTrait
{
    public function linepayCheckout($order)
    {
        try {
            $obj = new Linepay();
            $ordernumber = $order->epaynumber;
            $order_item = $order->items()->first();
            $setting = app('siteSettings');

            // 發送 Reserve 請求
            $sum_price = $order->sum_price;
            $obj->RequestAction = 'Request';
            $obj->Params = [
                'amount' => (int)$sum_price,  // *Number 付款金額 = sum(packages[].amount) + sum(packages[].userFee) + options.shipping.feeAmount
                'orderId' => $ordernumber,  // *String(100) 商家訂單編號 商家管理的唯一ID
                'packages' => [
                    [
                        'amount' => (int)$sum_price,  // *Number 一個Package中的商品總價 =sum(products[].quantity * products[].price)
                        'name' => $setting->name, // *String(100) Package名稱 （or Shop Name）
                        'products' => [
                            [
                                'name' => !empty($order_item->title) ? $order_item->title : $ordernumber,  // *String(4000) 商品名
                                'imageUrl' => !empty($order_item->pic) ? imgsrc($order_item->pic) : '',  // String(500) 商品圖示的URL
                                'quantity' => 1,  // *Number 商品數量
                                'price' => (int)$sum_price,  // *Number 各商品付款金額
                            ]
                        ]
                    ]
                ],
                'redirectUrls' => [
                    'confirmUrl' => route($this->linepayConfirmUrlRoute),  // String(500) 使用者授權付款後，跳轉到該商家URL
                    'cancelUrl' => route($this->linepayCancelUrlRoute),  // String(500) 使用者通過LINE付款頁，取消付款後跳轉到該URL
                ],
            ];
            $result = $obj->process();

            // 儲存 Request API 交易回應
            // if (!empty($result['info']['transactionId'])) {
            //     $transactionId = (string)$result['info']['transactionId']; // 交易編號 (19 位數)
            //     $order->update(['epaynumber' => $transactionId]);
            // }
            $record = new LinepayRecord();
            if (!empty($result['info'])) {
                $record->fill($result['info']);
            }
            $record['route_name'] = Route::currentRouteName();
            $record['action'] = $obj->RequestAction;
            $record['returnCode'] = $result['returnCode'];
            $record['returnMessage'] = $result['returnMessage'];
            $record['orderId'] = $ordernumber;
            $record->save();

            // 請求成功
            if ($result['returnCode'] == '0000') {
                return redirect()->to($result['info']['paymentUrl']['web']);
            } else {
                return redirect()->route($this->linepayCancelUrlRoute)->with('customMsg', 'Line Pay 付款失敗!')->with('customMsgText', '錯誤代碼：'.$result['returnCode'].'<br>'.$result['returnMessage']);
            }
        } catch (Exception $e) {
            Log::channel('payment')->error('linepay checkout error', ['num' => $order->num, 'message' => $e->getMessage()]);
            return redirect()->route($this->linepayResultRoute)
                ->with('num', $order->num)
                ->with('result', '付款失敗')
                ->with('resultText', $e->getMessage());
        }
    }
    // 付款完成背景回傳資訊與檢查碼處理
    public function linepayReturn(Request $request)
    {
        $requestData = $request->all();
        Log::channel('payment')->info('linepay return', ['response' => $requestData]);

        $obj = new Linepay();
        $order = Order::where('epaynumber', $request->orderId)->first();
        if ($order && !empty($request->transactionId)) {
            // 發送 Confirm 請求
            try {
                $obj->RequestAction = 'Confirm';
                $obj->TransactionID = $request->transactionId;
                $obj->Params['amount'] = (int)$order->sum_price;
                $result = $obj->process();

                $record = new LinepayRecord();
                if (!empty($result['info'])) {
                    $record->fill($result['info']);
                }
                $record['route_name'] = Route::currentRouteName();
                $record['action'] = $obj->RequestAction;
                $record['returnCode'] = $result['returnCode'];
                $record['returnMessage'] = $result['returnMessage'];
                $record->save();

                $order = $this->afterPaid($record);

                return redirect()->route($this->linepayResultRoute)
                    ->with('num', $order->num)
                    ->with('result', '付款成功');
            } catch(Exception $e) {
                Log::channel('payment')->error('linepay confirm error', ['num' => $order->num, 'message' => $e->getMessage()]);
                return redirect()->route($this->linepayResultRoute)
                    ->with('num', $order->num)
                    ->with('result', '付款失敗')
                    ->with('resultText', $e->getMessage());
            }
        }
    }
}
