<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class Blog extends Model
{
    use Translatable;
    protected $with = ['translations'];

    public function categories() {
        return $this->belongsToMany(BlogCategory::class, 'blog_category', 'blog_id', 'blog_category_id');
    }
    public function tags() {
        return $this->belongsToMany(BlogTag::class, 'blog_tag', 'blog_id', 'blog_tag_id');
    }
    public function recommends() {
        return $this->belongsToMany(Blog::class, 'blog_recommend', 'blog_id', 'recommend_id');
    }
}
