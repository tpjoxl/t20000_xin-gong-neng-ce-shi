<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class Question extends Model
{
    

    public function categories() {
        return $this->belongsToMany(QuestionCategory::class, 'question_category', 'question_id', 'question_category_id');
    }
}
