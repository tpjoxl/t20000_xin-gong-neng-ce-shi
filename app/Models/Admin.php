<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laracasts\Presenter\PresentableTrait;

class Admin extends Authenticatable
{
    use PresentableTrait;

    protected $guard = 'admin';
    protected $presenter = 'App\Presenters\AdminPresenter';
    public $translatedAttributes = [];

    public function scopeOrdered($query, $rank = 'rank')
    {
      return $query->orderBy('created_at', 'asc');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public function group() {
        return $this->belongsTo(AdminGroup::class, 'group_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'account', 'password', 'status', 'login_at', 'ip', 'group_id', 'passable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function fillAndSave($data) {
        $result = $this->fill($data)->save();
        return $result;
    }
}
