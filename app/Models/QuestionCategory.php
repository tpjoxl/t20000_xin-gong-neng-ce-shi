<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class QuestionCategory extends ModelCategory
{

    protected $limit_level = 1;

    public function Questions() {
        return $this->belongsToMany(Question::class, 'question_category', 'question_category_id', 'question_id')->withPivot('rank');
    }
}
