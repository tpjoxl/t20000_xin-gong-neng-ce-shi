<?php

namespace App\Models;

use App\Helpers\ArrHelper as Arr;
use Laracasts\Presenter\PresentableTrait;

class Cart
{
    use PresentableTrait;
    protected $presenter = 'App\Presenters\Presenter';
    public $step = 1;
    public $items = [];
    public $itemsChecked = 0;
    public $coupon = null;
    public $couponPrice = 0;
    public $shippingSetting = null;
    public $allShippingTypes = [];
    public $shippingType = 1;
    public $allShippings = [];
    public $shipping = null;
    public $cities = null; // 全部縣市
    public $remote = 0; // 是否偏鄉
    public $payment = null;
    public $paymentPrice = 0;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $shippingPrice = 0;
    public $sumPrice = 0;
    public $bonusSetting = null;
    public $bonus = 0; // 使用者輸入想用紅利折抵的金額
    public $bonusPrice = 0; // 紅利折抵的金額
    public $usableBonus = 0; // 紅利最大可折抵的金額
    public $orderBonus = 0; // 訂單可獲得的紅利
    public $invoice = 0;
    public $tax = 0;
    public $user = null;
    public $formData = null;

    public function __construct($oldCart = null)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->coupon = $oldCart->coupon;
            $this->couponPrice = $oldCart->couponPrice;
            $this->shippingType = $oldCart->shippingType;
            $this->shipping = $oldCart->shipping;
            $this->shippingPrice = $oldCart->shippingPrice;
            $this->remote = $oldCart->remote;
            $this->cities = $oldCart->cities;
            $this->payment = $oldCart->payment;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->sumPrice = $oldCart->sumPrice;
            $this->bonus = $oldCart->bonus;
            $this->bonusPrice = $oldCart->bonusPrice;
            $this->orderBonus = $oldCart->orderBonus;
            $this->invoice = $oldCart->invoice;
            $this->tax = $oldCart->tax;
            $this->formData = $oldCart->formData;
        }
        // $this->updateItemPrice();
        $this->user = $user = auth()->guard('web')->user();
        $user_type = is_null($this->user) ? 0 : $user->type;
        $this->shippingSetting = Setting::getData('shipping');
        $this->bonusSetting = Setting::getData('bonus');

        if (is_null($this->shipping)) {
            $this->shipping = new Shipping();
        } else {
            $this->shipping = Shipping::find($this->shipping->id);
        }
        if (is_null($this->formData)) {
            $defaultData = [
                'user_id' => $user?$user->id:null,
                'name' => $user?$user->name:null,
                'sex' => $user?$user->sex:null,
                'email' => $user?$user->email:null,
                'phone' => $user?$user->phone:null,
                'receiver_name' => $user?$user->name:null,
                'receiver_sex' => $user?$user->sex:null,
                'receiver_email' => $user?$user->email:null,
                'receiver_phone' => $user?$user->phone:null,
                'receiver_city_id' => $user?$user->city_id:null,
                'receiver_region_id' => $user?$user->region_id:null,
                'receiver_address' => $user?$user->address:null,
            ];
            $this->formData = new Order($defaultData);
        }
        $this->cities = app('twCities');

        $this->allShippingTypes = $this->shippingSetting->oversea_status ? $this->shipping->present()->type() : Arr::only($this->shipping->present()->type(), [1]);
        $this->allShippings = $this->shipping
            ->when($user, function ($q) use ($user_type) {
                $q->whereRaw("FIND_IN_SET('$user_type',`user_type`)>0");
            })
            ->with([
                'payments' => function ($q) use ($user_type) {
                    $q->display()->ordered()->whereRaw("FIND_IN_SET('$user_type',`user_type`)>0");
                },
            ])
            ->display()
            ->ordered()
            ->get();
        $this->refresh();
    }
    // 加入品項
    public function add($sku_id, $qty, $type = 'sku')
    {
        if ($type == 'sku') {
            $props = "$type-$sku_id";
            $storedItem = [
                'type' => $type,
                'sku_id' => $sku_id,
                'qty' => 0,
                'checked' => 1,
            ];
            // 如果原本購物車內有商品 && 有同規格的商品
            if ($this->items && array_key_exists($props, $this->items)) {
                // 就先記成舊的資料
                $storedItem = $this->items[$props];
            }
        }

        // 數量累加
        $storedItem['qty'] += $qty;
        $this->items[$props] = $storedItem;
        $this->refresh();
    }
    // 檢查庫存
    public function checkQtyEnough($sku_id, $qty, $type = 'sku')
    {
        if ($type == 'sku') {
            $sku = Sku::find($sku_id);
            if (is_null($sku) || $qty > $sku->qty) {
                return [
                    'status' => false,
                    'qty' => $sku->qty,
                ];
            }
        }
        return [
            'status' => true,
        ];
    }
    // 刪除品項
    public function delete($sku_id, $type = 'sku')
    {
        if ($type == 'sku') {
            $props = "$type-$sku_id";
        }
        unset($this->items[$props]);
        $this->refresh();
    }
    // 更新選取狀態
    public function updateItemChecked($sku_id, $checked, $type = 'sku')
    {
        if ($sku_id == 'all') {
            foreach ($this->items as $props => $item) {
                $this->items[$props]['checked'] = (int)$checked;
            }
        } else {
            if ($type == 'sku') {
                $props = "$type-$sku_id";
            }
            $this->items[$props]['checked'] = (int)$checked;
        }
        $this->refresh();
    }
    // 更新數量
    public function updateQty($sku_id, $qty, $type = 'sku')
    {
        if ($type == 'sku') {
            $props = "$type-$sku_id";
        }
        $this->items[$props]['qty'] = $qty;
        $this->refresh();
    }
    // 更新運送地點
    public function updateShippingType($type)
    {
        $this->shippingType = is_null($type) ? 1 : $type;
        $this->shipping = null;
        $this->refresh();
    }
    // 更新運送方式
    public function updateShipping($shipping_id = null)
    {
        if (!is_null($shipping_id)) {
            $this->shipping = $this->allShippings->where('id', $shipping_id)->first();
        } elseif (is_null($this->shipping) || is_null($this->shipping->id)) {
            $this->shipping = $this->allShippings->where('type', $this->shippingType)->first();
        }
    }
    // 更新付款方式
    public function updatePayment($payment_id = null)
    {
        $this->paymentPrice = 0;
        if (is_null($this->shipping) || is_null($payment_id)) {
            $this->payment = null;
        } else {
            $this->payment = $this->shipping->payments->where('id', $payment_id)->first();
        }
    }
    // 檢查優惠代碼
    public function checkCoupon($coupon_code)
    {
        $user = $this->user;
        $user_type = $this->user ? $this->user->type : 0;
        $usableCoupons = Coupon::usable($user)
            ->with([
                'orders' => function ($q) {
                    // 排除已取消的訂單
                    $q->where('status', '!=', 2);
                }
            ])
            ->get()
            ->filter(function ($item, $key) use ($user) {
                if ($user) {
                    if ($item->orders->where('user_id', $user->id)->count() == 0) {
                        if ($item->use_type == 1) {
                            return !($item->count) || ($item->count > 0 && $item->count > $item->orders->count());
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if ($item->use_type == 1) {
                        return !($item->count) || ($item->count > 0 && $item->count > $item->orders->count());
                    } else {
                        return false;
                    }
                }
            });
        $coupon = $usableCoupons->where('code', $coupon_code)->first();
        return $coupon;
    }
    // 更新優惠代碼
    public function updateCoupon($coupon=null)
    {
        $this->coupon = $coupon;
    }
    // 刷新紅利點數
    public function refreshBonus()
    {
        if ($this->user && !empty($this->bonusSetting->limit_percentage) && !empty($this->bonusSetting->point_per_price) && $bonusSum = $this->user->bonuses()->display()->get()->sum('point')) {
            $limit = floor(($this->totalPrice - $this->couponPrice) * $this->bonusSetting->limit_percentage * 0.01);
            $user_bonus = $bonusSum?floor($bonusSum / $this->bonusSetting->point_per_price):0;
            // $this->bonusPrice = auth()->id();
            $this->usableBonus = $user_bonus > $limit ? $limit : $user_bonus;
        } else {
            $this->usableBonus = 0;
        }
    }
    // 更新紅利點數
    public function updateBonus($bonus=null)
    {
        if (!is_null($bonus) && $this->user) {
            $this->bonus = $bonus;
        } else {
            $this->bonus = 0;
        }
    }
    // 更新表單資訊
    public function updateFormData($data)
    {
        $this->formData = new Order($data);
    }
    // 更新偏鄉判斷
    public function updateRemote($region_id)
    {
        $city = $this->cities
        ->filter(function ($city, $key) use ($region_id) {
            return $city->regions->where('id', $region_id)->count();
        })->first();
        $region = $city?$city->regions->where('id', $region_id)->first():null;
        $this->formData->receiver_region_id = $region_id;
        $this->formData->receiver_city_id = $city?$city->id:null;

        $this->remote = $region?$region->remoted:0;

        $this->updateShipping();
    }
    // 刷新購物車
    public function refresh()
    {
        $this->updateItems();
        $this->updateShipping();
        $this->updateSum();
    }
    // 更新品項資訊
    protected function updateItems()
    {
        $skuIds = array_column(array_filter($this->items, function ($item) {
            return $item['type'] == 'sku';
        }), 'sku_id');
        $skus = Sku::whereIn('id', $skuIds)
            ->with([
                'product' => function ($q) {
                    $q->with([
                        'images' => function ($q) {
                            $q->where('is_cover', 1);
                        },
                    ]);
                },
            ])
            ->get()
            ->keyBy('id');
        foreach ($this->items as $props => $item) {
            if (!empty($sku = $skus[$item['sku_id']])) {
                $this->items[$props]['sku'] = $sku;
                $this->items[$props]['pic'] = $sku->product->pic_path;
                $this->items[$props]['title'] = $sku->product->title;
                $this->items[$props]['num'] = $sku->num;
                $this->items[$props]['options'] = $sku->description;
                $this->items[$props]['original_price'] = $sku->price;
                $this->items[$props]['price'] = $sku->current_price;
                $this->items[$props]['price_type'] = $sku->current_price_type;
                $this->items[$props]['total'] = $sku->current_price * $item['qty'];
                if ($sku->qty < $item['qty']) {
                    $this->items[$props]['checked'] = 0;
                }
            }
        }
        $checkedItems = array_filter($this->items, function($item) {
            return $item['checked'];
        });
        $this->totalQty = array_sum(array_column($checkedItems, 'qty'));
        $this->totalPrice = array_sum(array_column($checkedItems, 'total'));
        $this->itemsChecked = count($checkedItems);
    }
    // 更新總計
    public function updateSum()
    {
        // 運費
        if (!is_null($this->shipping) && !is_null($this->shipping->id)) {
            $this->shippingPrice = $this->totalPrice >= $this->shipping->limit_order_price ? $this->shipping->limit_price : $this->shipping->price;
        } else {
            $this->shippingPrice = 0;
        }
        if ($this->shippingSetting->remote_status && $this->remote) {
            $this->shippingPrice += $this->shippingSetting->remote_price;
        }
        // 金流手續費
        if (!is_null($this->payment) && $this->payment->fee_type && $this->payment->fee) {
            if ($this->payment->fee_type == 1) {
                // 百分比
                $this->paymentPrice = round($this->totalPrice * $this->payment->fee + 0.01);
            } elseif ($this->payment->fee_type == 2) {
                // 固定金額
                $this->paymentPrice = $this->payment->fee;
            }
        } else {
            $this->paymentPrice = 0;
        }
        // 優惠代碼折扣金額
        if (is_null($this->coupon)) {
            $this->couponPrice = 0;
        } else {
            if ($this->coupon->type==1) {
                $this->couponPrice = ($this->totalPrice >= $this->coupon->min_price) ? $this->coupon->number : 0;
            } elseif ($this->coupon->type==2) {
                $this->couponPrice = ($this->totalPrice >= $this->coupon->min_price) ? floor($this->totalPrice * $this->coupon->number / 100) : 0;
            }
        }
        // 紅利折抵的金額
        $this->refreshBonus();
        if (empty($this->bonus) || is_null($this->user)) {
            $this->bonusPrice = 0;
        } else {
            $this->bonusPrice = $this->bonus > $this->usableBonus ? $this->usableBonus : $this->bonus;
        }
        // 訂單獲得的紅利點數
        $this->orderBonus = $this->user && !empty($this->bonusSetting->price_per_point) ? floor($this->totalPrice / $this->bonusSetting->price_per_point) : 0;
        // 計算結帳金額
        $this->sumPrice = $this->totalPrice + $this->shippingPrice + $this->paymentPrice + $this->tax - $this->couponPrice - $this->bonusPrice;
    }
    // 清空結帳項目
    public function finishCheckout()
    {
        $this->items = array_filter($this->items, function($item) {
            return !$item['checked'];
        });
        $this->coupon = null;
        $this->shipping = null;
        $this->bonus = 0;
        $this->orderBonus = 0;
        $this->refresh();
    }
}
