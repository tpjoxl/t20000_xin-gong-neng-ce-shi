<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;

class Coupon extends Model
{
    use Translatable;
    protected $with = ['translations'];
    protected $presenter = 'App\Presenters\CouponPresenter';
    protected $appends = ['content'];
    protected $fillable = [
        'status',
        'home_status',
        'use_type',
        'user_type',
        'code',
        'type',
        'number',
        'min_price',
        'count',
        'pic',
        'date_on',
        'date_off',
        'user_visible',
        'rank',
        'is_top',
        'title',
        'text',
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_coupon', 'coupon_id', 'order_id');
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'coupon_user', 'coupon_id', 'user_id');
    }
    public function setUserTypeAttribute($user_type)
    {
        $this->attributes['user_type'] = is_null($user_type)?null:implode(',', $user_type);
    }
    public function getUserTypeAttribute($user_type)
    {
        return is_null($user_type)?null:explode(',', $user_type);
    }
    public function getContentAttribute()
    {
        if ($this->type == 1) {
            return "折扣 $this->number 元";
        } elseif ($this->type == 2) {
            return floor((100 - $this->number) / 10) . (floor((100 - $this->number) / 10) <= 0 ? '.' : '') . ((100 - $this->number) % 10 > 0 ? (100 - $this->number) % 10 : '') . " 折優惠";
        }
    }
    public function scopeDisplay($query)
    {
        return $query->where($this->getTable() . '.user_visible', 1);
    }
    public function scopeUsable($query, $user)
    {
        $user_type = $user?$user->type:0;
        return $query->where($this->getTable() . '.status', 1)
            ->where(function ($q) {
                $q->whereNull('date_on')
                    ->orWhere('date_on', '<=', Carbon::today()->toDateString());
            })
            ->where(function ($q) {
                $q->whereNull('date_off')
                    ->orWhere('date_off', '>=', Carbon::today()->toDateString());
            })
            ->where(function ($q) use ($user_type) {
                // 指定會員等級
                $q->where('use_type', 1)
                    ->whereRaw("FIND_IN_SET('$user_type',`user_type`)>0");
            })
            ->when($user, function($q) use ($user) {
                $q->orWhere(function ($q) use ($user) {
                    // 指定會員帳號
                    $q->where('use_type', 2)
                        ->whereHas('users', function($q) use ($user) {
                            $q->where('id', $user->id);
                        });
                });
            });
    }
    public function scopeExpired($query)
    {
        return $query->where('date_off', '<', Carbon::today()->toDateString());
    }
}
