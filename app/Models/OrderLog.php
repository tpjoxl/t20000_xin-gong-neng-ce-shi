<?php

namespace App\Models;

class OrderLog extends Model
{
    protected $presenter = 'App\Presenters\OrderLogPresenter';
    protected $fillable = [
        'order_id',
        'type',
        'status',
        'who',
        'note',
    ];
    protected $appends = ['type_status'];

    public function scopeOrdered($query, $rank = 'rank')
    {
        return $query
            ->orderBy('created_at', $this->sortBy)
            ->orderBy('id', $this->sortBy);
    }
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function getTypeStatusAttribute()
    {
        $type_status_arr = [
            1 => 'status',
            2 => 'payment_status',
            3 => 'shipping_status',
        ];
        if (array_key_exists($this->type, $type_status_arr)) {
            return $type_status_arr[$this->type];
        } else {
            return head($type_status_arr);
        }
    }
}
