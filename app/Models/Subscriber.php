<?php

namespace App\Models;

class Subscriber extends Model
{
    protected $presenter = 'App\Presenters\SubscriberPresenter';
    protected $fillable = [
        'name',
        'email',
        'status',
        'rank',
        'admin_note',
        'locale',
      ];
}
