<?php

namespace App\Models;

class Power extends ModelCategory
{
    public $timestamps = false;
    protected $limit_level = 2;
    public $translatedAttributes = [];
    protected $fillable = [
        'title',
        'name',
        'parent_id',
        'status',
        'rank',
        'icon',
        'route_prefix',
        'route_name',
        'url_param',
        'active',
        'level',
        'divider_after',
    ];
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable().".".$rank;
        }
        return $query->orderBy('is_top', 'desc')
            ->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc');
    }
    public function admin_groups()
    {
        return $this->belongsToMany(AdminGroup::class, 'admin_group_power', 'power_id', 'admin_group_id');
    }
}
