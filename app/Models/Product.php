<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;

class Product extends Model
{
    use Translatable;
    protected $with = ['translations'];
    protected $presenter = 'App\Presenters\ProductPresenter';

    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($data) {
            if ($data->images->count()) {
                $data->images->each->delete();
            }
            foreach ($data->images->unique('path')->pluck('path')->toArray() as $directory) {
                Storage::disk('uploads')->deleteDirectory($directory);
            }
        });
    }
    public function scopeDisplay($query)
    {
        if (method_exists($this, 'categories')) {
            $result = $query->whereHas('categories', function ($query) {
                $query->display();
            });
        } else {
            $result = $query;
        }

        return $result->where($this->getTable() . '.status', '!=', 0)
            ->where(function ($q) {
                $q->whereNull('date_on')
                    ->orWhere('date_on', '<=', Carbon::today()->toDateString());
            })
            ->where(function ($q) {
                $q->whereNull('date_off')
                    ->orWhere('date_off', '>=', Carbon::today()->toDateString());
            });
    }
    public function categories() {
        return $this->belongsToMany(ProductCategory::class, 'product_category', 'product_id', 'product_category_id');
    }
    public function images()
    {
        return $this->hasMany(ProductImage::class, 'parent_id');
    }
    public function skus()
    {
        return $this->hasMany(Sku::class, 'product_id');
    }
    public function recommends()
    {
        return $this->belongsToMany(Product::class, 'product_recommend', 'product_id', 'recommend_id');
    }
    protected function saveRelateions($data)
    {
        if (array_key_exists('categories', $data) && method_exists($this, 'categories')) {
            $this->categories()->sync(array_filter($data['categories']));
        }
        if (array_key_exists('recommends', $data) && method_exists($this, 'recommends')) {
            $this->recommends()->sync(array_filter($data['recommends']));
        }
        if (array_key_exists('skus', $data) && method_exists($this, 'skus')) {
            $old = $this->skus->pluck('id')->toArray();
            $new = array_filter($data['skus']);
            $remove = array_diff($old, $new);
            $this->skus()->getRelated()->whereIn('id', $new)->update(['product_id' => $this->id]);
            $this->skus()->whereIn('id', $remove)->update(['product_id' => null]);
        }
    }
    public function getPicPathAttribute()
    {
        if ($this->images->count()) {
            return $this->images->where('is_cover', 1)->first()->default_path;
        } else {
            return 'images/' . Str::singular($this->table) . '_default.png';
        }
    }
    public function getIsNewAttribute()
    {
        if ($this->created_at->toDateString() >= Carbon::today()->subMonth()) {
            return true;
        } else {
            return false;
        }
    }
}
