<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Support\Str;
use Laracasts\Presenter\PresentableTrait;
use App\Traits\UploadableTrait;
use Carbon\Carbon;

class Model extends Eloquent
{
    use PresentableTrait;
    use UploadableTrait;
    protected $presenter = 'App\Presenters\Presenter';
    protected $slugFrom = 'title';
    public $uploadable = [];
    public $uploadFolederKey = 'id';

    protected $fillable = [
        'status',
        'home_status',
        'date_on',
        'date_off',
        'pic',
        'og_image',
        'meta_robots',
        'rank',
        'is_top',
        'title',
        'slug',
        'url',
        'target',
        'description',
        'text',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'og_title',
        'og_description',
    ];
    public $translatedAttributes = [
        'title',
        'slug',
        'url',
        'target',
        'description',
        'text',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'og_title',
        'og_description',
    ];

    public $sortBy = 'desc'; // <- change this, asc OR desc
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable() . "." . $rank;
        }
        return $query->orderBy('is_top', 'desc')
            ->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc')
            ->orderBy('created_at', $this->sortBy)
            ->orderBy('id', $this->sortBy);
    }
    public function scopeDisplay($query)
    {
        if (method_exists($this, 'categories')) {
            $result = $query->whereHas('categories', function ($query) {
                $query->display();
            });
        } else {
            $result = $query;
        }

        return $result->where($this->getTable() . '.status', 1)
            ->where(function ($q) {
                $q->whereNull('date_on')
                    ->orWhere('date_on', '<=', Carbon::today()->toDateString());
            })
            ->where(function ($q) {
                $q->whereNull('date_off')
                    ->orWhere('date_off', '>=', Carbon::today()->toDateString());
            });
    }
    public function fillAndSave($data)
    {
        $dataToSave = Arr::except($data, $this->uploadable);

        if (method_exists($this, 'translations') && count(config('translatable.locales'))>1) {
            foreach (app('backendLocales') as $locale) {
                if (Arr::has($dataToSave, $locale->code . '.slug')) {
                    $dataToSave[$locale->code]['slug'] = string_slug($dataToSave[$locale->code]['slug'] ?: $dataToSave[$locale->code][$this->slugFrom]);
                }
            }
        } else {
            if (Arr::has($dataToSave, 'slug')) {
                $dataToSave['slug'] = string_slug($dataToSave['slug'] ?: $dataToSave[$this->slugFrom]);
            }
        }
        $result = $this->fill($dataToSave)->save();

        if (count($this->uploadable)) {
            $this->handleFiles($data);
        }
        $this->saveRelateions($data);

        return $result;
    }
    // 儲存關聯
    protected function saveRelateions($data)
    {
        if (array_key_exists('categories', $data) && method_exists($this, 'categories')) {
            $this->categories()->sync(array_filter($data['categories']));
        }
        if (array_key_exists('recommends', $data) && method_exists($this, 'recommends')) {
            $this->recommends()->sync(array_filter($data['recommends']));
        }
        if (array_key_exists('tags', $data) && method_exists($this, 'tags')) {
            $this->tags()->sync(array_filter($data['tags']));
        }
    }
    public function getPicPathAttribute()
    {
        if ($this->pic) {
            return $this->pic;
        } else {
            return 'images/' . Str::singular($this->getTable()) . '_default.png';
        }
    }
}
