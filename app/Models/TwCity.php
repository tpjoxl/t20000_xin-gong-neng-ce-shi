<?php

namespace App\Models;

class TwCity extends Model
{
    public $timestamps = false;
    public $sortBy = 'asc';
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable() . "." . $rank;
        }
        return $query->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc')
            ->orderBy('id', $this->sortBy);
    }
    public function regions() {
      return $this->hasMany(TwRegion::class, 'city_id');
    }
}
