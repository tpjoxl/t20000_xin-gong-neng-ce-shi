<?php

namespace App\Models;

class ShippingTranslation extends ModelTranslation
{
  protected $fillable = [
    'title',
    'text',
  ];
}
