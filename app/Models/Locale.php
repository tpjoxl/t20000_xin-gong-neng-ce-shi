<?php

namespace App\Models;

use Illuminate\Support\Str;

class Locale extends Model
{
    public $timestamps = false;
    protected $with = ['texts'];
    protected $fillable = [
        'code',
        'title',
        'abbr',
        'status',
        'frontend_status',
        'is_top',
        'rank',
        'backend_use',
    ];

    public function scopeDisplay($query)
    {
        return $query->where('status', 1);
    }
    public function scopeBackend($query)
    {
        return $query->where('status', 1)->ordered();
    }
    public function scopeFrontend($query)
    {
        return $query->where('status', 1)->where('frontend_status', 1)->ordered();
    }
    public $sortBy = 'asc';
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable().".".$rank;
        }
        return $query->orderBy('is_top', 'desc')
            ->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc')
            ->orderBy('id', $this->sortBy);
    }
    public function texts()
    {
        return $this->hasMany(LocaleText::class, 'code', 'code');
    }
    public function getText($key, $replace=[])
    {
        $textArr = $this->texts->pluck('text_value', 'text_key')->toArray();
        if (array_key_exists($key, $textArr)) {
            return $this->makeReplacements($textArr[$key], $replace);
        } elseif (__($key, $replace=[], $this->code) !== $key) {
            return __($key, $replace=[], $this->code);
        } else {
            return '#'.$key;
        }
    }
    protected function makeReplacements($string, array $replace)
    {
        if (empty($replace)) {
            return $string;
        }

        foreach ($replace as $key => $value) {
            $string = str_replace(
                [':'.$key, ':'.Str::upper($key), ':'.Str::ucfirst($key)],
                [$value, Str::upper($value), Str::ucfirst($value)],
                $string
            );
        }

        return $string;
    }
}
