<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;

class ModelImage extends Model
{
    public $maxImgSize = [ // 後台顯示的建議尺寸
        'w' => null,
        'h' => null,
    ];
    public $sizes = [
        'default' => [
            'w' => 300,
            'h' => 300,
        ],
        'thumb' => [
            'w' => 150,
            'h' => 150,
        ],
        'zoom' => [
            'w' => null,
            'h' => null,
        ],
    ];
    protected $resize = false; // 是否等比縮放後補白邊
    protected $fillContent = false; // 當圖小於範圍時，是否等比放大
    protected $bgColor = 'rgba(0, 0, 0, 0)'; // 補白邊的背景色
    public $maxUploadSize = 2048; // KB
    public $translatedAttributes = [
        'description',
    ];
    protected $appends = ['default_path', 'thumb_path', 'zoom_path'];
    public function getDefaultPathAttribute()
    {
        return 'uploads/'.$this->path.$this->filename.'.'.$this->ext;
    }
    public function getThumbPathAttribute()
    {
        return 'uploads/'.$this->path.$this->filename.'-thumb'.'.'.$this->ext;
    }
    public function getZoomPathAttribute()
    {
        return 'uploads/'.$this->path.$this->filename.'-zoom'.'.'.$this->ext;
    }

    protected $fillable = [
        'path',
        'filename',
        'ext',
        'parent_id',
        'is_cover',
        'rank',
        'description',
    ];

    public $sortBy = 'asc';
    public function scopeDisplay($query)
    {
        return $query->where('status', 1);
    }
    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($data) {
            if (!empty($data->path)) {
                $delete_file = [];
                foreach ($data->sizes as $key => $size) {
                    if ($key == 'default') {
                        $delete_file[] = $data->path.$data->filename.'.'.$data->ext;
                    } else {
                        $delete_file[] = $data->path.$data->filename.'-'.$key.'.'.$data->ext;
                    }
                }
                Storage::disk('uploads')->delete($delete_file);
            }
        });
    }
    public function uploadFile($request_files, $parent_id, $folder_name = null)
    {
        ini_set('memory_limit', '256M');
        $folder_name = is_null($folder_name) ? $this->table : $folder_name;
        $uploadFileArr = array();
        // $parent_id = request()->parent;
        foreach ($request_files as $image) {
            $originalName = $image->getClientOriginalName();
            $ext = $image->getClientOriginalExtension();
            // $type = $image->getClientMimeType();
            // $realPath = $image->getRealPath();
            $imgName = date('YmdHis') . '-' . uniqid();
            if (is_null($parent_id)) {
                $path = $folder_name . '/';
            } else {
                $path = $folder_name . '/' . $parent_id . '/';
            }
            // $path = $image->store('album', 'uploads');
            // return asset('uploads/'.$path);
            if (!Storage::disk('uploads')->exists($path)) {
                Storage::disk('uploads')->makeDirectory($path);
            }
            foreach ($this->sizes as $key => $size) {
                if ($key == 'default') {
                    $this->createThumbs($image, $path . $imgName . '.' . $ext, $size['w'], $size['h']);
                } else {
                    $this->createThumbs($image, $path . $imgName . '-' . $key . '.' . $ext, $size['w'], $size['h']);
                }
            }

            array_push($uploadFileArr, ['path' => $path, 'filename' => $imgName, 'ext' => $ext, 'description' => str_replace('.' . $ext, '', $originalName)]);
        }
        // $files = Storage::files('album/'.$parent_id);
        return $uploadFileArr;
    }

    // 另存縮圖
    public function createThumbs($image, $path, $coverW, $coverH)
    {
        $img = Image::make($image);
        $imgH = $img->height();
        $imgW = $img->width();
        if (is_null($coverW) && is_null($coverH)) {
            // 照原圖尺寸存檔
            $pic = Image::canvas($imgW, $imgH)->fill($this->bgColor);
            $pic->insert($img, 'center');
        } else if (is_null($coverH)) {
            // 等比縮小到不超過指定寬度
            $pic = $img->resize($coverW, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            if ($imgW < $coverW && $imgH < $coverH) {
                // 補白邊
                $pic = Image::canvas($coverW, $coverH)->fill($this->bgColor);
                if ($this->fillContent) {
                    if ($imgW - $coverW > $imgH - $coverH) {
                        $resizeImg = $img->resize($coverW, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $resizeImg = $img->resize(null, $coverH, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    if ($resizeImg->width() - $coverW > $resizeImg->height() - $coverH) {
                        $resizeImg = $resizeImg->resize($coverW, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $resizeImg = $resizeImg->resize(null, $coverH, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $pic->insert($resizeImg, 'center');
                } else {
                    $pic->insert($img, 'center');
                }
            } else {
                if ($this->resize) {
                    if ($imgW - $coverW > $imgH - $coverH) {
                        $resizeImg = $img->resize($coverW, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $resizeImg = $img->resize(null, $coverH, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    if ($resizeImg->width() - $coverW > $resizeImg->height() - $coverH) {
                        $resizeImg = $resizeImg->resize($coverW, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    } else {
                        $resizeImg = $resizeImg->resize(null, $coverH, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $pic = Image::canvas($coverW, $coverH)->fill($this->bgColor);
                    $pic->insert($resizeImg, 'center');
                } else {
                    // 裁切填滿範圍
                    $pic = $img->fit($coverW, $coverH);
                }
            }
        }
        $result = $pic->save('uploads/'.$path);
        $pic->destroy();

        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    public function deleteFile($ids)
    {
        $datas = $this->find($ids);
        $result = $datas->each->delete();

        return $result;
    }
    public function setCover($id, $parent_id)
    {
        $img = $this->find($id);
        // $parent_id = $img->getAlbum->id;
        $result = $img->update(['is_cover' => 1]);
        $this->where('parent_id', $parent_id)
            ->where('id', '!=', $id)
            ->update(['is_cover' => 0]);
        return $result;
    }
    public function updateCover($parent_id)
    {
        $imgs = $this->where('parent_id', $parent_id)->get();
        $cover = $imgs->groupBy('is_cover');

        if ($imgs->count() > 0 && $cover->has(0) && $cover[0]->count() == $imgs->count()) {
            $imgs->first()->update(['is_cover' => 1]);
        }
    }
}
