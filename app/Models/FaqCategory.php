<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class FaqCategory extends ModelCategory
{
    use Translatable;
    protected $with = ['translations'];
    protected $limit_level = 1;

    public function faqs() {
        return $this->belongsToMany(Faq::class, 'faq_category', 'faq_category_id', 'faq_id')->withPivot('rank');
    }
}
