<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class BlogCategory extends ModelCategory
{
    use Translatable;
    protected $with = ['translations'];
    protected $limit_level = 1;

    public function blogs() {
        return $this->belongsToMany(Blog::class, 'blog_category', 'blog_category_id', 'blog_id')->withPivot('rank');
    }
}
