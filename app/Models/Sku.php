<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Sku extends Model
{
    protected $presenter = 'App\Presenters\SkuPresenter';
    public $sortBy = 'desc';
    protected $fillable = [
        'belong',
        'product_id',
        'usein',
        'title',
        'description',
        'props',
        'num',
        'price',
        'price2',
        'price3',
        'price4',
        'price5',
        'qty',
        'qty2',
        'sales',
        'sales2',
        'pic',
        'status',
        'rank',
        'is_top',
    ];
    protected $appends = ['current_price', 'current_price_type'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function setUseinAttribute($usein)
    {
        $this->attributes['usein'] = is_null($usein) ? null : implode(',', $usein);
    }
    public function getUseinAttribute($usein)
    {
        return is_null($usein) ? null : explode(',', $usein);
    }
    public function getCurrentPriceAttribute()
    {
        return $this->price2 ?? $this->price;
    }
    public function getCurrentPriceTypeAttribute()
    {
        return $this->price2 ? 2 : 1;
    }
}
