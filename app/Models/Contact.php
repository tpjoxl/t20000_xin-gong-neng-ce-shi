<?php

namespace App\Models;

class Contact extends Model
{
    protected $presenter = 'App\Presenters\ContactPresenter';
    protected $fillable = [
        'name',
        'sex',
        'email',
        'phone',
        'type',
        'message',
        'status',
        'rank',
        'admin_note',
        'locale',
      ];
}
