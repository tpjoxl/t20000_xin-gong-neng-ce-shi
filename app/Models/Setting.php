<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Helpers\ArrHelper as Arr;
use App\Models\SettingSave;

class Setting extends Eloquent
{
    use PresentableTrait;
    protected $presenter = 'App\Presenters\Presenter';
    public $timestamps = false;
    protected $guarded = [];
    public $translatedAttributes = [
        'home_editor1',
        'home_editor2',
        'home_editor3',
        'home_editor4',
        'text',
        'text2',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'title',
        'og_title',
        'og_description',
        'name',
        'phone',
        'phone2',
        'fax',
        'address',
        'google_map',
        'copyright',
        'logo',
    ];
    public $pageExcept = [
        'backend.site.editor' => ['products'],
        'backend.shipping.page' => ['remotes'],
    ];

    public function scopeOrdered($query, $rank = 'rank')
    {
        return $query;
    }
    protected static function getByPage($page)
    {
        if (is_array($page)) {
            $query = static::query();
            foreach ($page as $key => $p) {
                $p = Str::replaceFirst('backend.', '', $p);
                if ($key == 0) {
                    $query->where('page', 'like', 'backend.' . $p . '%');
                } else {
                    $query->orWhere('page', 'like', 'backend.' . $p . '%');
                }
            }
        } else {
            $page = Str::replaceFirst('backend.', '', $page);
            $query = static::where('page', 'like', 'backend.' . $page . '%');
        }
        $datas = $query->get()->transform(function ($item, $key) {
            $item->setting_value = $item->convertShowDataByType($item->setting_value, $item->type);
            return $item;
        });
        return $datas;
    }
    // 取得當前語系資料
    public static function getData($page, $lang=null)
    {
        $locales = app('backendLocales');
        if (is_null($lang)) {
            $lang = app()->getLocale();
        }
        $datas = static::getByPage($page);
        $result = $datas->whereIn('locale', ['common', $lang])->pluck('setting_value', 'setting_name')->toArray();
        foreach ($locales as $locale) {
            $result[$locale->code] = static::make($datas->whereIn('locale', ['common', $locale->code])->pluck('setting_value', 'setting_name')->toArray());
        }
        return static::make($result);
    }
    public function translate($locale)
    {
        return $this->{$locale};
    }
    public function fillAndSave($data)
    {
        $dataToSave = [];
        $page = Route::currentRouteName();
        $formFields = $data;
        $requestExcept = ['_token'];
        if (array_key_exists($page, $this->pageExcept)) {
            $requestExcept = array_merge($requestExcept, $this->pageExcept[$page]);
            $formFields = Arr::except($formFields, $this->pageExcept[$page]);
        }
        $requestData = request()->except($requestExcept);
        $locales = config('translatable.locales');

        foreach (Arr::only($requestData, $locales) as $locale => $array) {
            foreach ($array as $key => $value) {
                $dataToSave[] = [
                    'setting_name' => $key,
                    'setting_value' => $this->convertSaveDataByType($value, $formFields[$key]['type']),
                    'locale' => $locale,
                    'page' => $page,
                    'type' => $formFields[$key]['type'],
                ];
            }
        }
        foreach (Arr::except($requestData, $locales) as $key => $value) {
            if (in_array($key, $this->translatedAttributes)) {
                $dataToSave[] = [
                    'setting_name' => $key,
                    'setting_value' => $this->convertSaveDataByType($value, $formFields[$key]['type']),
                    'locale' => head($locales),
                    'page' => $page,
                    'type' => $formFields[$key]['type'],
                ];
            } else {
                $dataToSave[] = [
                    'setting_name' => $key,
                    'setting_value' => $this->convertSaveDataByType($value, $formFields[$key]['type']),
                    'locale' => 'common',
                    'page' => $page,
                    'type' => $formFields[$key]['type'],
                ];
            }
        }
        $dataToSave[] = [
            'setting_name' => 'updated_at',
            'setting_value' => Carbon::now()->toDateTimeString(),
            'locale' => 'common',
            'page' => $page,
            'type' => 'datetime',
        ];
        $result = [];

        // 刪除多餘的欄位
        $settings = SettingSave::where('page', $page)->get();
        $formFieldsLimit = array_unique(Arr::pluck($dataToSave, 'setting_name'));
        $deleteField = array_unique(array_diff($settings->pluck('setting_name')->toArray(), $formFieldsLimit));
        $settings->whereIn('setting_name', $deleteField)->each->delete();

        foreach ($dataToSave as $save) {
            $setting = SettingSave::firstOrCreate(Arr::only($save, ['setting_name', 'locale', 'page']));
            $result[] = $setting->update($save);
        }
        return $result;
    }
    protected function convertShowDataByType($data, $type)
    {
        if ($type == 'password') {
            $val = decrypt($data);
        } elseif ($type == 'date') {
            $val = Carbon::parse($data);
        } elseif ($type == 'dateTime') {
            $val = Carbon::parse($data);
        } else {
            $val = $data;
        }
        return $val;
    }
    protected function convertSaveDataByType($data, $type)
    {
        if ($type == 'password') {
            $val = encrypt($data);
        } elseif ($type == 'date') {
            $val = Carbon::parse($data)->toDateString();
        } elseif ($type == 'dateTime') {
            $val = Carbon::parse($data)->toDateTimeString();
        } else {
            $val = $data;
        }
        return $val;
    }
}
