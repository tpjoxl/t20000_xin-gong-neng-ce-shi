<?php

namespace App\Models;

class LocaleText extends Model
{
    protected $fillable = [
        'code',
        'text_key',
        'text_value',
        'rank',
      ];

    public function scopeOrdered($query, $rank = 'rank')
    {
        return $query->orderBy($rank, 'desc')
            ->orderBy('created_at', 'asc');
    }
    public function locale() {
        return $this->belongsTo(Locale::class, 'code', 'code');
    }
}
