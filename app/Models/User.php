<?php

namespace App\Models;

use App\Helpers\ArrHelper as Arr;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laracasts\Presenter\PresentableTrait;

class User extends Authenticatable
{
    use PresentableTrait;
    use Notifiable;
    protected $presenter = 'App\Presenters\UserPresenter';
    public $uploadable = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'account', 'email', 'password',
        'name',
        'sex',
        'birthday',
        'phone',
        'city_id',
        'region_id',
        'address',
        'status',
        'email_verified',
        'email_verified_at',
        'register_confirmed',
        'login_at',
        'ip',
        'admin_note',
        'facebook_id',
        'is_facebook',
        'google_id',
        'is_google',
        'line_id',
        'is_line',
        'first_login',
        'is_top',
        'rank',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public $sortBy = 'desc'; // <- change this, asc OR desc
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable() . "." . $rank;
        }
        return $query->orderBy('is_top', 'desc')
            ->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc')
            ->orderBy('created_at', $this->sortBy);
    }
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }
    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupon_user', 'user_id', 'coupon_id')->withPivot('rank');
    }
    public function bonuses()
    {
        return $this->hasMany(Bonus::class, 'user_id');
    }
    public function city()
    {
        return $this->belongsTo(TwCity::class, 'city_id');
    }
    public function region()
    {
        return $this->belongsTo(TwRegion::class, 'region_id');
    }
    public function fillAndSave($data)
    {
        $dataToSave = Arr::except($data, $this->uploadable);

        if (method_exists($this, 'translations') && count(config('translatable.locales')) > 1) {
            foreach (app('backendLocales') as $locale) {
                if (Arr::has($dataToSave, $locale->code . '.slug')) {
                    $dataToSave[$locale->code]['slug'] = string_slug($dataToSave[$locale->code]['slug'] ?: $dataToSave[$locale->code][$this->slugFrom]);
                }
            }
        } else {
            if (Arr::has($dataToSave, 'slug')) {
                $dataToSave['slug'] = string_slug($dataToSave['slug'] ?: $dataToSave[$this->slugFrom]);
            }
        }
        $result = $this->fill($dataToSave)->save();

        if (count($this->uploadable)) {
            $this->handleFiles($data);
        }
        return $result;
    }
    public function getPicPathAttribute()
    {
        if ($this->pic) {
            return is_array($this->pic) ? $this->pic[0] : $this->pic;
        } else {
            return 'images/' . Str::singular($this->table) . '_default.png';
        }
    }
    public function random_password($length = 8)
    {
        // 隨機密碼可能包含的字符
        // $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+=_,!@$#*%<>[]{}";
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($str), 0, $length);
        return $password;
    }
    public function random_register($length = 60)
    {
        // 隨機密碼可能包含的字符
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr(str_shuffle($str), 0, $length);
        return $password;
    }
}
