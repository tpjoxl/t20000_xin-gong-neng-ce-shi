<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class ProductCategory extends ModelCategory
{
    use Translatable;
    protected $table = 'product_categories';
    public $translationForeignKey = 'product_category_id';
    protected $with = ['translations'];
    protected $limit_level = 2;
    protected $fillable = [
        'belong',
        'status',
        'home_status',
        'date_on',
        'date_off',
        'pic',
        'og_image',
        'meta_robots',
        'rank',
        'is_top',
        'title',
        'slug',
        'url',
        'target',
        'description',
        'text',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'og_title',
        'og_description',
        'note',
        'admin_note',
    ];

    public function products() {
        return $this->belongsToMany(Product::class, 'product_category', 'product_category_id', 'product_id')->withPivot('rank');
    }
}
