<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class Banner extends Model
{
    use Translatable;
    protected $with = ['translations'];
    public $sortBy = 'asc';
    protected $fillable = [
        'status',
        'home_status',
        'date_on',
        'date_off',
        'pic',
        'pic2',
        'rank',
        'is_top',
        'created_at',
        'title',
        'url',
        'btn_txt',
        'target',
        'url2',
        'btn_txt2',
        'target2',
        'tag_title',
        'tag_alt',
        'text',
    ];
    public $translatedAttributes = [
        'title',
        'url',
        'btn_txt',
        'target',
        'url2',
        'btn_txt2',
        'target2',
        'tag_title',
        'tag_alt',
        'text',
    ];
}
