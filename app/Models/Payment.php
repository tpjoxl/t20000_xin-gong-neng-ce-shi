<?php

namespace App\Models;

use App\Helpers\ArrHelper as Arr;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;

class Payment extends Model
{
    use Translatable;
    protected $with      = ['translations'];
    protected $presenter = 'App\Presenters\PaymentPresenter';
    public $sortBy       = 'asc';
    protected $fillable  = [
        'user_type',
        'fee',
        'pic',
        'status',
        'is_top',
        'rank',
        'title',
        'text',
    ];
    public $translatedAttributes = [
        'title',
        'text',
    ];

    protected static function booted()
    {
        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('active', 1);
        });
    }
    public function scopeDisplay($query)
    {
        return $query->where($this->getTable() . '.status', 1);
    }
    protected function saveRelateions($data)
    {
        if (array_key_exists('shippings', $data) && method_exists($this, 'shippings')) {
            $this->shippings()->sync(array_filter($data['shippings']));
        }
    }
    public function shippings()
    {
        return $this->belongsToMany(Shipping::class, 'shipping_payment', 'payment_id', 'shipping_id');
    }
    public function setUserTypeAttribute($user_type)
    {
        $this->attributes['user_type'] = is_null($user_type) ? null : implode(',', $user_type);
    }
    public function getUserTypeAttribute($user_type)
    {
        return is_null($user_type) ? null : explode(',', $user_type);
    }
}
