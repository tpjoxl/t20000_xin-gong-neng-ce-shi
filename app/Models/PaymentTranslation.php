<?php

namespace App\Models;

class PaymentTranslation extends ModelTranslation
{
  protected $fillable = [
    'title',
    'text',
  ];
}
