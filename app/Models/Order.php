<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use App\Traits\MailTrait;
use App\Mail\OrderShipping;

class Order extends Model
{
    use MailTrait;
    protected $presenter = 'App\Presenters\OrderPresenter';
    protected $fillable = [
        'user_id',
        'num',
        'epaynumber',
        'shippingnumber',
        'items_total_price',
        'coupon_price',
        'bonus_price',
        'shipping_price',
        'payment_price',
        'sum_price',
        'payment_info',
        'payment_id',
        'payment_return',
        'payment_deadline',
        'shipping_type',
        'shipping_info',
        'shipping_id',
        'coupon_info',
        'name',
        'sex',
        'email',
        'phone',
        'city_id',
        'region_id',
        'address',
        'receiver_name',
        'receiver_sex',
        'receiver_email',
        'receiver_phone',
        'receiver_city_id',
        'receiver_region_id',
        'receiver_address',
        'note',
        'hope_date',
        'hope_time',
        'invoice_type',
        'ein_title',
        'ein_num',
        'admin_note',
        'status',
        'payment_status',
        'shipping_status',
        'is_top',
        'rank',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->num = $model->generateNum();
        });
        static::created(function ($model) {
            $model->logs()->createMany([
                ['type' => 1, 'status' => 1, 'who' => 'system'],
                ['type' => 2, 'status' => 1, 'who' => 'system'],
                ['type' => 3, 'status' => 1, 'who' => 'system'],
            ]);
        });
        static::saving(function ($model) {
            if ($model->isDirty(["shippingnumber"]) && !is_null($model->shippingnumber)) {
                $model->status = 4;
                $model->shipping_status = 2;
            }
        });
        static::saved(function ($model) {
            if ($model->isDirty(["status"]) || $model->isDirty(["payment_status"]) || $model->isDirty(["shipping_status"])) {
                if (in_array(Route::currentRouteName(), ['backend.order.update'])) {
                    $auth_admin = auth()->guard('admin')->user();
                    $who = "admin-$auth_admin->id-$auth_admin->name";
                } elseif (in_array(Route::currentRouteName(), ['user.order.cancel'])) {
                    $auth_web = auth()->guard('web')->user();
                    $who = "user-$auth_web->id-$auth_web->name";
                } else {
                    $who = 'system';
                }
            }
            if ($model->isDirty(["status"])) {
                $model->logs()->create(['type' => 1, 'status' => $model->status, 'who' => $who]);
            }
            if ($model->isDirty(["payment_status"])) {
                $model->logs()->create(['type' => 2, 'status' => $model->payment_status, 'who' => $who]);
            }
            if ($model->isDirty(["shipping_status"])) {
                $model->logs()->create(['type' => 3, 'status' => $model->shipping_status, 'who' => $who]);
                if ($model->shipping_status == 2) {
                    // 寄出貨通知
                    $model->SendMailTo($model, new OrderShipping($model));
                }
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function logs()
    {
        return $this->hasMany(OrderLog::class, 'order_id', 'id');
    }
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }
    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }
    public function shipping()
    {
        return $this->belongsTo(Shipping::class, 'shipping_id');
    }
    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'order_coupon', 'order_id', 'coupon_id')->withPivot('rank');
    }
    public function bonuses()
    {
        return $this->hasMany(Bonus::class, 'order_id', 'id');
    }
    public function city()
    {
        return $this->belongsTo(TwCity::class, 'city_id');
    }
    public function region()
    {
        return $this->belongsTo(TwRegion::class, 'region_id');
    }
    public function receiver_city()
    {
        return $this->belongsTo(TwCity::class, 'receiver_city_id');
    }
    public function receiver_region()
    {
        return $this->belongsTo(TwRegion::class, 'receiver_region_id');
    }
    public function records()
    {
        $provider = $this->payment_info['provider']??$this->payment['provider'];
        if ($provider == 'ECPAY') {
            return $this->hasMany(EcpayRecord::class, 'MerchantTradeNo', 'epaynumber')->orderBy('created_at', 'desc');
        } elseif ($provider == 'NEWEBPAY') {
            return $this->hasMany(NewebpayRecord::class, 'MerchantOrderNo', 'epaynumber')->orderBy('created_at', 'desc');
        } elseif ($provider == 'LINEPAY') {
            return $this->hasMany(LinepayRecord::class, 'orderId', 'num')->orderBy('created_at', 'desc');
        } elseif ($provider == 'JKOPAY') {
            return $this->hasMany(JkopayRecord::class, 'platform_order_id', 'num')->orderBy('created_at', 'desc');
        }
    }
    // 產生訂單編號，格式：三碼隨機大寫字母 + 今日日期 + 流水號(n位數)
    public function generateNum($letters = 3, $n = 1)
    {
        $date = date('YmdHis');
        $maxnum_row = $this->select(DB::raw('max(substr(`num`, -' . $n . ')) as max_num'))
            ->where('num', 'like', '%' . $date . '%')
            ->first();

        $maxnum = $maxnum_row->max_num;
        $maxnum = str_pad($maxnum + 1, $n, '0', STR_PAD_LEFT);
        $str_rand = '';
        for ($i = 1; $i <= $letters; $i++) {
            //$c=rand(1,2); //隨機取得大小寫
            //if($c==1){$a=rand(97,122);$b=chr($a);} //小寫字母
            //if($c==2){
            $a = rand(65, 90);
            $b = chr($a);
            //} //大寫字母
            $str_rand .= $b;
        }

        $num = $str_rand . date('YmdHis') . $maxnum;
        return $num;
    }
    public function generateEpaynumber()
    {
        $number = !empty($this->epaynumber) ? $this->epaynumber : $this->num;
        $str_prefix = substr($number, 0, 18);
        $str_number = (int)substr($number, 18, 2);
        $str_number = str_pad($str_number + 1, 2, '0', STR_PAD_LEFT);
        $epaynumber = $str_prefix . $str_number;

        return $epaynumber;
    }
    public function setHopeExpAttribute($hope_exp)
    {
        $this->attributes['hope_exp'] = implode(', ', $hope_exp);
    }
    public function setPaymentInfoAttribute($payment_info)
    {
        $this->attributes['payment_info'] = serialize($payment_info);
    }
    public function getPaymentInfoAttribute($payment_info)
    {
        return is_null($payment_info) ? $payment_info : unserialize($payment_info);
    }
    public function setShippingInfoAttribute($shipping_info)
    {
        $this->attributes['shipping_info'] = serialize($shipping_info);
    }
    public function getShippingInfoAttribute($shipping_info)
    {
        return is_null($shipping_info) ? $shipping_info : unserialize($shipping_info);
    }
    public function setCouponInfoAttribute($coupon_info)
    {
        $this->attributes['coupon_info'] = serialize($coupon_info);
    }
    public function getCouponInfoAttribute($coupon_info)
    {
        return is_null($coupon_info) ? $coupon_info : unserialize($coupon_info);
    }
    public function getReceiverFullAddressAttribute()
    {
        if ($this->shipping_type == 1) {
            return ($this->receiver_region?$this->receiver_region->code.' ':'').($this->receiver_city?$this->receiver_city->name.' ':'').($this->receiver_region?$this->receiver_region->name.' ':'').$this->receiver_address;
        } else {
            return $this->receiver_address;
        }
    }
    public function calcCouponPrice($price = null, $coupon = null)
    {
        $coupon_price = 0;
        if (!is_null($this->coupon_id)) {
            $price = $price ?: $this->price;
            $coupon = $coupon ?: $this->coupon_info;
            if ($price >= $coupon['min_price']) {
                if ($coupon['type'] == 1) {
                    // 固定金額
                    $coupon_price = $coupon['number'];
                } elseif ($coupon['type'] == 2) {
                    // 折扣百分比
                    $coupon_price = floor($price * $coupon['number'] * 0.01);
                }
            }
        }
        return $coupon_price;
    }
    public function calcSum($price = null, $coupon_price = null)
    {
        $price = $price ?: $this->price;
        $coupon_price = $coupon_price ?: $this->coupon_price;
        return $price - $coupon_price;
    }
    public function getEcpayDataAttribute()
    {
        $data = [
            'TotalAmount' => $this->sum_price,
            'Items' => [],
        ];
        foreach ($this->items as $key => $item) {
            array_push($data['Items'],
                array('Name' => $obj->convertItemName($item->title.($item->options?'('.$item->options.')':''))
                    , 'Price' => (int)$item->price
                    , 'Currency' => "元"
                    , 'Quantity' => (int) $item->qty
                    , 'URL' => "dedwed"));
        }
        if ($this->shipping_price) {
            array_push($data['Items'],
            array('Name' => '運費'
                , 'Price' => (int)$this->shipping_price
                , 'Currency' => "元"
                , 'Quantity' => (int) 1
                , 'URL' => "dedwed"));
        }
        if ($this->payment_price) {
            array_push($data['Items'],
            array('Name' => '金流手續費'
                , 'Price' => (int)$this->payment_price
                , 'Currency' => "元"
                , 'Quantity' => (int) 1
                , 'URL' => "dedwed"));
        }
        if ($this->coupon_price) {
            array_push($data['Items'],
            array('Name' => '優惠券折扣'
                , 'Price' => -(int)$this->coupon_price
                , 'Currency' => "元"
                , 'Quantity' => (int) 1
                , 'URL' => "dedwed"));
        }
        if ($this->bonus_price) {
            array_push($data['Items'],
            array('Name' => '紅利折扣'
                , 'Price' => -(int)$this->bonus_price
                , 'Currency' => "元"
                , 'Quantity' => (int) 1
                , 'URL' => "dedwed"));
        }
        return $data;
    }
}
