<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use App\Helpers\ArrHelper as Arr;

class Shipping extends Model
{
    use Translatable;
    protected $with = ['translations'];
    protected $presenter = 'App\Presenters\ShippingPresenter';
    public $sortBy = 'asc';
    protected $fillable = [
        'type',
        'user_type',
        'price',
        'limit_order_price',
        'limit_price',
        'url',
        'pic',
        'status',
        'is_top',
        'rank',
        'title',
        'text',
    ];
    public $translatedAttributes = [
        'title',
        'text',
    ];

    public function scopeDisplay($query)
    {
        return $query->where($this->getTable() . '.status', 1);
    }
    protected function saveRelateions($data)
    {
        if (array_key_exists('payments', $data) && method_exists($this, 'payments')) {
            $this->payments()->sync(array_filter($data['payments']));
        }
    }
    public function payments() {
      return $this->belongsToMany(Payment::class, 'shipping_payment', 'shipping_id', 'payment_id');
    }
    public function setUserTypeAttribute($user_type)
    {
        $this->attributes['user_type'] = is_null($user_type)?null:implode(',', $user_type);
    }
    public function getUserTypeAttribute($user_type)
    {
        return is_null($user_type)?null:explode(',', $user_type);
    }
}
