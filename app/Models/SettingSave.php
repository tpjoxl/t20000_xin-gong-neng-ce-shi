<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SettingSave extends Eloquent
{
    public $timestamps = false;
    protected $table = 'settings';
    protected $fillable = [
        'setting_name',
        'setting_value',
        'locale',
        'page',
        'type',
    ];
}
