<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class BlogTag extends Model
{
    use Translatable;
    protected $with = ['translations'];
    public $sortBy = 'asc';

    public function blogs()
    {
        return $this->belongsToMany(Blog::class, 'blog_tag', 'blog_tag_id', 'blog_id');
    }
    public function scopeDisplay($query)
    {
        if (method_exists($this, 'categories')) {
            $result = $query->whereHas('categories', function ($query) {
                $query->display();
            });
        } else {
            $result = $query;
        }

        return $result->where($this->getTable() . '.status', 1);
    }
}
