<?php

namespace App\Models;

class Bonus extends Model
{
    protected $presenter = 'App\Presenters\BonusPresenter';
    public $sortBy = 'desc';
    protected $fillable = [
        'user_id',
        'order_id',
        'point',
        'date_on',
        'date_off',
        'note',
        'admin_note',
        'status',
        'is_top',
        'rank',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
