<?php

namespace App\Models;

class TwRegion extends Model
{
    public $timestamps = false;
    protected $fillable = [
      'name',
      'code',
      'city_id',
      'rank',
      'remoted',
    ];
    public $sortBy = 'asc';
    public function scopeJoinCity($query)
    {
      $query->selectRaw('tw_regions.*, tw_cities.name as city_name')
            ->join('tw_cities', 'tw_cities.id', '=', 'tw_regions.city_id');
    }
    public function scopeOrdered($query, $rank = 'rank')
    {
        if ($rank == 'rank') {
            $rank = $this->getTable() . "." . $rank;
        }
        return $query->orderByRaw("CASE
                WHEN $rank > 0 THEN 10
                WHEN $rank = 0 THEN 20
                END $this->sortBy")
            ->orderBy($rank, 'desc')
            ->orderBy('id', $this->sortBy);
    }
    public function city() {
      return $this->belongsTo(TwCity::class, 'city_id');
    }
}
