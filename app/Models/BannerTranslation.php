<?php

namespace App\Models;

class BannerTranslation extends ModelTranslation
{
    protected $fillable = [
        'title',
        'url',
        'btn_txt',
        'target',
        'url2',
        'btn_txt2',
        'target2',
        'tag_title',
        'tag_alt',
        'text',
    ];
}
