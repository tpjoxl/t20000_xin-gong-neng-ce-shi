<?php

namespace App\Models;

use App\Helpers\ArrHelper as Arr;

class AdminGroup extends ModelCategory
{
    protected $limit_level = 3;
    public $translatedAttributes = [];

    public function admins() {
        return $this->hasMany(Admin::class,'group_id','id');
    }
    public function powers() {
        return $this->belongsToMany(Power::class, 'admin_group_power', 'admin_group_id', 'power_id');
    }
    public function checkPower($id) {
        $power = $this->powers->pluck('id')->toArray();
        return in_array($id, $power);
    }
    public function checkPowerByName($name) {
        $power = $this->powers->pluck('name')->toArray();
        return in_array($name, $power);
    }
    public function checkPowerByRouteName($name) {
        $power = $this->powers->filter(function ($power, $key) use ($name) {
            $routeNames = explode(',', $power->route_name);
            return in_array($name, $routeNames);
        });
        return $power->count() > 0;
    }
    public function fillAndSave($data)
    {
        $dataToSave = $data;
        $result = $this->fill($dataToSave)->save();
        // 儲存關聯
        if (array_key_exists('powers', $data) && method_exists($this, 'powers')) {
            $this->powers()->sync(array_filter($data['powers']));
        }
        return $result;
    }
}
