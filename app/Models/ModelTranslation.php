<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ModelTranslation extends Eloquent
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'slug',
        'url',
        'target',
        'description',
        'text',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'og_title',
        'og_description',
    ];
}
