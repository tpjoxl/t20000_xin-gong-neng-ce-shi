<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

class ProductImage extends ModelImage
{
    use Translatable;
    protected $table = 'product_images';
    public $translationForeignKey = 'product_image_id';
    protected $with = ['translations'];
    public $maxImgSize = [
        'w' => 1200,
        'h' => 1200,
    ];
    public $sizes = [
        'default' => [
            'w' => 280,
            'h' => 280,
        ],
        'thumb' => [
            'w' => 128,
            'h' => 128,
        ],
        'detail' => [
            'w' => 700,
            'h' => 700,
        ],
        'zoom' => [
            'w' => 1200,
            'h' => 1200,
        ],
    ];
    protected $resize = false; // 是否等比縮放後補白邊
    protected $fillContent = false; // 當圖小於範圍時，是否等比放大
    protected $appends = ['default_path', 'thumb_path', 'zoom_path', 'detail_path'];

    public function getDetailPathAttribute()
    {
        return 'uploads/'.$this->path.$this->filename.'-detail'.'.'.$this->ext;
    }
    public function product() {
        return $this->belongsTo(Product::class, 'parent_id');
    }
}
