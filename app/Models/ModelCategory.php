<?php

namespace App\Models;

use App\Traits\HasLevelTrait;

class ModelCategory extends Model
{
    use HasLevelTrait;
    protected $limit_level = 1;

    public $sortBy = 'asc';
    public function scopeDisplay($query)
    {
      return $query->where('status', 1);
    }
}
