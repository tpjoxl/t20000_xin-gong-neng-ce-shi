<?php

namespace App\Models;

class LinepayRecord extends Model
{
    protected $fillable = [
        'route_name',
        'action',
        'returnCode',
        'returnMessage',
        'orderId',
        'transactionId',
        'transactionDate',
        'transactionType',
        'payStatus',
        'productName',
        'merchantName',
        'currency',
        'paymentAccessToken',
        'paymentUrl',
        'authorizationExpireDate',
        'regKey',
        'payInfo',
        'packages',
        'merchantReference',
        'shipping',
        'refundTransactionId',
        'refundTransactionDate',
        'refundList',
        'events',
        'errorDetails',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'orderId', 'epaynumber');
    }
    public function setPaymentUrlAttribute($paymentUrl)
    {
        $this->attributes['paymentUrl'] = serialize($paymentUrl);
    }
    public function getPaymentUrlAttribute($paymentUrl)
    {
        return unserialize($paymentUrl);
    }
    public function setPayInfoAttribute($payInfo)
    {
        $this->attributes['payInfo'] = serialize($payInfo);
    }
    public function getPayInfoAttribute($payInfo)
    {
        return unserialize($payInfo);
    }
    public function setPackagesAttribute($packages)
    {
        $this->attributes['packages'] = serialize($packages);
    }
    public function getPackagesAttribute($packages)
    {
        return unserialize($packages);
    }
    public function setMerchantReferenceAttribute($merchantReference)
    {
        $this->attributes['merchantReference'] = serialize($merchantReference);
    }
    public function getMerchantReferenceAttribute($merchantReference)
    {
        return unserialize($merchantReference);
    }
    public function setShippingAttribute($shipping)
    {
        $this->attributes['shipping'] = serialize($shipping);
    }
    public function getShippingAttribute($shipping)
    {
        return unserialize($shipping);
    }
    public function setRefundListAttribute($refundList)
    {
        $this->attributes['refundList'] = serialize($refundList);
    }
    public function getRefundListAttribute($refundList)
    {
        return unserialize($refundList);
    }
    public function setEventsAttribute($events)
    {
        $this->attributes['events'] = serialize($events);
    }
    public function getEventsAttribute($events)
    {
        return unserialize($events);
    }
    public function setErrorDetailsAttribute($errorDetails)
    {
        $this->attributes['errorDetails'] = serialize($errorDetails);
    }
    public function getErrorDetailsAttribute($errorDetails)
    {
        return unserialize($errorDetails);
    }
}
