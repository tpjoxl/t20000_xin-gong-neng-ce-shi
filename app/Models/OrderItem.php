<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'sku_id',
        'type',
        'pic',
        'title',
        'num',
        'options',
        'price_type',
        'original_price',
        'price',
        'discount',
        'qty',
        'total',
        'status',
        'is_top',
        'rank',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    public function sku()
    {
        if ($this->type == 'sku') {
            return $this->belongsTo(Sku::class, 'sku_id');
        }
    }
}
