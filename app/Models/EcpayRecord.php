<?php

namespace App\Models;

class EcpayRecord extends Model
{
    protected $fillable = [
        "type",
        "status",
        "CheckMacValue",
        "chkstr",
        "chkstr_original",
        "chkstr_original2",
        "MerchantID",
        "MerchantTradeNo",
        "RtnCode",
        "RtnMsg",
        "TradeNo",
        "TradeAmt",
        "PaymentDate",
        "PaymentType",
        "PaymentTypeChargeFee",
        "TradeDate",
        "SimulatePaid",
        "PaymentNo",
        "ExpireDate",
        "BankCode",
        "vAccount",
        "ReturnMsg",
        "StoreID",
        "createdate",
        "CustomField1",
        "CustomField2",
        "CustomField3",
        "CustomField4",
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'MerchantTradeNo', 'epaynumber');
    }
}
