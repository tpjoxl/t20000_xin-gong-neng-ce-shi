<?php

namespace App\Models;

class EzpayInvoiceRecord extends Model
{
    protected $fillable = [
        'Status',
        'Message',
        'MerchantID',
        'InvoiceTransNo',
        'MerchantOrderNo',
        'TotalAmt',
        'InvoiceNumber',
        'RandomNum',
        'CreateTime',
        'CheckCode',
        'BarCode',
        'QRcodeL',
        'QRcodeR',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'MerchantOrderNo', 'num');
    }
}
