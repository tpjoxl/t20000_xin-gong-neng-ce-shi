<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class Export implements FromCollection, WithHeadings, WithEvents, ShouldAutoSize, WithColumnFormatting
{
    public function __construct($headings, $result, $columnFormats=[])
    {
        $this->headings = $headings;
        $this->result = $result;
        $this->columnFormats = $columnFormats;
    }

    public function headings(): array
    {
        return $this->headings;
    }
    public function columnFormats(): array
    {
        return $this->columnFormats;
    }
    // freeze the first row with headings
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->freezePane('A' . (count($this->headings) + 1), 'A' . (count($this->headings) + 1));

                $event->sheet->styleCells(
                    count($this->headings) . ':' . count($this->headings),
                    [
                        'font' => [
                            'bold' => true,
                        ],
                        'fill' => [
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                            'color' => ['argb' => 'FFBBBBBB'],
                        ],
                        'borders' => [
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                                'color' => ['argb' => 'FF333333'],
                            ],
                        ]
                    ]
                );
            },
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->result;
    }
}
