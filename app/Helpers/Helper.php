<?php
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use App\Models\Locale;

if (!function_exists('array_insert_after')) {
    /**
     * Insert array after the key in the array.
     *
     * @param  array  $origin_array
     *   An array to insert in to.
     * @param  array  $insert_array
     *   An array to insert
     * @param  string  $key
     *   The key to insert after
     * @return array
     *   The new array
     */
    function array_insert_after($origin_array, $insert_array, $key=null)
    {
        $key_indices = array_flip(array_keys($origin_array));
        if (is_null($key) || empty($key)) {
            $pos = 0;
        } else {
            if (array_key_exists($key, $key_indices)) {
                $pos = $key_indices[$key]+1;
            } else {
                $pos = array_search($key, $origin_array)+1;
            }
        }
        $newArr = array_merge(
            array_slice($origin_array, 0, $pos, is_string($key)),
            $insert_array,
            array_slice($origin_array, $pos, NULL, is_string($key))
        );
        // array_splice($origin_array, $pos, 0, $insert_array);
        return $newArr;
    }
}
if (!function_exists('check_null')) {
    /**
     * 檢查是否為NULL直到最後一個值當作預設
     */
    function check_null(...$values)
    {
        foreach ($values as $key => $value) {
            if ($key>0 && $key == count($values)-1) {
                return $value;
            } else {
                if (!is_null($value)) {
                    return $value;
                }
            }
        }
    }
}
if (!function_exists('string_slug')) {
    function string_slug($str)
	{
        // 把字串內的符號半形轉全形
        $nft = array(
            "(", ")", "[", "]", "{", "}", ",", ";", ":",
            "?", "!", "@", "#", "$", "%", "&", "|", "\\",
            "/", "+", "=", "*", "~", "`", "'", "\"", "<", ">",
            "^"
        );
        $wft = array(
            "（", "）", "〔", "〕", "｛", "｝", "，", "；", "：",
            "？", "！", "＠", "＃", "＄", "％", "＆", "｜", "＼",
            "／", "＋", "＝", "＊", "～", "、", "、", "＂", "＜", "＞",
            "︿"
        );
        $str = str_replace($nft, $wft, $str);

        // 把字串內的空格轉成「-」
        $str = preg_replace('/\s+/', '-', $str);
        // 把字串內的「_」轉成「-」
        $str = preg_replace('/_+/', '-', $str);
        // 把字串內多個「-」變成一個
        $str = preg_replace('/-+/', '-', $str);
        // 把字串內的大寫轉小寫
        return trim(strtolower($str));
    }
}
if (!function_exists('paginate')) {
    function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $paginator = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
        $paginator->setPath(url()->current());
        return $paginator;
    }
}
if (!function_exists('replace_mail_content_params')) {
    /**
     * 替換信件內容文字的變數
     *
     * @param  string  $text
     *   信件內容文字
     * @param  array  $replaces
     *   要替換的變數陣列，雙井字號中間的英文代表變數名稱(陣列的key)
     *   例如：##site_name##就會變替換成陣列$replaces['site_name']的值
     */
    function replace_mail_content_params($text, $replaces)
    {
        $output = preg_replace_callback('/##(.+?)##/', function($m) use ($replaces){
            if(isset($replaces[$m[1]])){ // If it exists in our array
                return $replaces[$m[1]]; // Then replace it from our array
            }else{
                return $m[0]; // Otherwise return the whole match (basically we won't change it)
            }
        }, $text);

        return $output;
    }
}
if (!function_exists('imgsrc')) {
    /**
     * 產生img標籤src圖片路徑
     *
     * @param  string  $path
     *   圖片路徑
     */
    function imgsrc($path)
    {
        return strpos($path, 'http')===0?$path:asset($path);
    }
}
if (!function_exists('_lang')) {
    /**
     * 抓取後台LocaleText儲存的文字
     * 前台view裡面使用{{_lang('text.about')}}的形式顯示
     */
    function _lang($key, $replace = [], $code = null)
    {
        if (is_null($code)) {
            $code = app()->getLocale();
        }
        $localeDatas = app('frontendLocales');
        $localeData = $localeDatas->where('code', $code)->first();
        if (is_null($localeData)) {
            $localeData = new Locale([
                'code' => 'en',
                'title' => 'English',
            ]);
        }
        return $localeData->getText($key, $replace);
    }
}
if (!function_exists('_routes')) {
    /**
     * 抓取後台LocaleText儲存的route相關文字
     * routes\web.php裡面使用{{_route('about')}}的形式顯示
     */
    function _routes($key)
    {
        $code = app()->getLocale();
        $allRoutesKey = app('allRoutesKey');
        $routesKey =  $allRoutesKey&&$allRoutesKey->has($code)?$allRoutesKey->get($code):[];
        return ($key ? Arr::get($routesKey, 'routes.' . $key) : $routesKey) ?? __('routes.'.$key);
    }
}
