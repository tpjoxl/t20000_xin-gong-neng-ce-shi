<?php
namespace App\Helpers;

use Illuminate\Support\Arr;

class ArrHelper extends Arr
{
    /**
     * Insert array after the key in the array.
     *
     * @param  array  $origin_array
     *   An array to insert in to.
     * @param  array  $insert_array
     *   An array to insert
     * @param  string  $key
     *   The key to insert after
     * @return array
     *   The new array
     */
    public static function insertAfter($origin_array, $insert_array, $key=null)
    {
        $key_indices = array_flip(array_keys($origin_array));
        if (is_null($key) || empty($key)) {
            $pos = 0;
        } else {
            if (array_key_exists($key, $key_indices)) {
                $pos = $key_indices[$key]+1;
            } else {
                $pos = array_search($key, $origin_array)+1;
            }
        }
        $newArr = array_merge(
            array_slice($origin_array, 0, $pos, is_string($key)),
            $insert_array,
            array_slice($origin_array, $pos, NULL, is_string($key))
        );
        // array_splice($origin_array, $pos, 0, $insert_array);
        return $newArr;
    }
}
