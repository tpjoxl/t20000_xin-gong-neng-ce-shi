<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Power;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->lang_datas = app('backendLocales');
        $this->setting = app('siteSettings');

        config([
            'app.name' => $this->setting->name,
            'app.url' => $this->setting->site_url,
            'mail.host' => $this->setting->smtp,
            'mail.port' => $this->setting->smtp_port,
            'mail.username' => $this->setting->smtp_account,
            'mail.password' => $this->setting->smtp_password,
            'mail.encryption' => env('MAIL_ENCRYPTION', $this->setting->smtp_encryption),
            'mail.from.name' => env('MAIL_FROM_NAME', $this->setting->name),
            'translatable.locales' => $this->lang_datas->pluck('code')->toArray(),
        ]);
        if ($this->setting->site_mail) {
            $emails = explode(',', preg_replace('/\s(?=)/', '', $this->setting->site_mail));
            config([
                'mail.from.address' => env('MAIL_FROM_ADDRESS', $emails[0])
            ]);
        }

        $this->app->singleton('backendPowers', function(){
            $auth_admin = auth()->guard('admin')->user();
            $powers = Power::whereNull('parent_id')->where('status', 1)->whereHas('admin_groups', function($q) use ($auth_admin) {
                $q->where('id', $auth_admin->group_id);
              })
              ->with([
                'children' => function($q) use ($auth_admin) {
                  $q->whereHas('admin_groups', function($q) use ($auth_admin) {
                    $q->where('id', $auth_admin->group_id);
                  })->ordered();
                }
              ])
              ->ordered()->get();
            return $powers;
        });

        view()->composer('backend.*', function($view) {
            $auth_admin = auth()->guard('admin')->user();

            $view->with([
                'lang_datas' => $this->lang_datas,
                'setting' => $this->setting,
                'auth_admin' => $auth_admin,
            ]);
        });

        view()->composer('backend.layouts.master', function($view) {
            $page_power = Power::where('route_name', 'like','%'.request()->route()->getName().'%')->first();
            $view->with('page_power', $page_power);
        });
        view()->composer(['backend.layouts.sidebar', 'backend.home'], function($view) {
            $powers = app('backendPowers');
            $view->with('powers', $powers);
        });
        view()->composer(['backend.layouts.master', 'backend.common.form_fields.city_region'], function($view) {
            $cities = app('twCities');
            $view->with([
                'cities'=> $cities
            ]);
        });
    }
}
