<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\ProductCategory;
use App\Models\BlogCategory;
use App\Models\Blog;

class FrontendServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->lang_datas = app('frontendLocales');
        $this->setting = app('siteSettings');

        config([
            'app.name' => $this->setting->name,
            'app.url' => $this->setting->site_url,
            'mail.host' => $this->setting->smtp,
            'mail.port' => $this->setting->smtp_port,
            'mail.username' => $this->setting->smtp_account,
            'mail.password' => $this->setting->smtp_password,
            'mail.encryption' => env('MAIL_ENCRYPTION', $this->setting->smtp_encryption),
            'mail.from.name' => env('MAIL_FROM_NAME', $this->setting->name),
            'translatable.locales' => $this->lang_datas->pluck('code')->toArray(),
        ]);
        if ($this->setting->site_mail) {
            $emails = explode(',', preg_replace('/\s(?=)/', '', $this->setting->site_mail));
            config([
                'mail.from.address' => env('MAIL_FROM_ADDRESS', $emails[0])
            ]);
        }

        $this->menu_blog_categories = BlogCategory::with([
            'children' => function($q) {
              $q->display()->ordered();
            },
            'children.children' => function($q) {
                $q->display()->ordered();
            }
          ])->display()->tree()->get();
        $this->menu_product_categories = ProductCategory::with([
            'children' => function($q) {
              $q->withCount([
                  'products' => function($q) {
                    $q->display()->ordered();
                  }
              ])->display()->ordered();
            },
          ])
          ->withCount([
              'products' => function($q) {
                $q->display()->ordered();
              }
          ])
          ->display()->displayTree()->get();

        view()->composer('frontend.*', function($view) {
            $auth_web = auth()->guard('web')->user();

            $view->with([
                'lang_datas' => $this->lang_datas,
                'setting' => $this->setting,
                'auth_web' => $auth_web,
            ]);
        });
        view()->composer(['frontend.layouts.header', 'frontend.product.side'], function($view) {
            $view->with([
                'menu_product_categories' => $this->menu_product_categories,
            ]);
        });
        view()->composer(['frontend.blog.side'], function($view) {
            $blog_latests = Blog::display()->orderBy('created_at', 'desc')->limit(5)->get();

            $view->with([
                'menu_blog_categories' => $this->menu_blog_categories,
                'blog_latests' => $blog_latests,
            ]);
        });
    }
}
