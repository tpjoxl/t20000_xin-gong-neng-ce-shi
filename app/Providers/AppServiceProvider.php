<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Validation\Validator;
use Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Writer;
use App\Models\Setting;
use App\Models\TwCity;
use App\Models\Locale;
use App\Models\LocaleText;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (env('APP_ENV') != 'install') {
            if (request()->segment(1) == env('BACKEND_ROUTE_PREFIX')) {
                $this->app->register('App\Providers\BackendServiceProvider');
            } else {
                $this->app->register('App\Providers\FrontendServiceProvider');
            }
            // 載入自訂的validator
            $this->app['validator']->resolver(function (
                $translator,
                $data,
                $rules,
                $messages,
                $customAttributes
            ) {
                return new Validator($translator, $data, $rules, $messages, $customAttributes);
            });

            $this->app->singleton('allRoutesKey', function(){
                return LocaleText::where('text_key', 'like', 'routes.%')->get()->groupBy('code')->transform(function ($item, $key) {
                    return $item->pluck('text_value', 'text_key')->toArray();
                });
            });
            $this->app->singleton('backendLocales', function(){
                return Locale::backend()->get();
            });
            $this->app->singleton('frontendLocales', function(){
                return Locale::frontend()->get();
            });
            $this->app->singleton('siteSettings', function(){
                return Setting::getData('site');
            });
            $this->app->singleton('twCities', function(){
                return TwCity::with('regions')->ordered()->get()->keyBy('id');
            });
        } else {
            $this->app->singleton('frontendLocales', function(){
                $collection = collect();
                foreach (config('translatable.locales') as $localeCode) {
                    $collection->push(new Locale([
                        'code' => $localeCode,
                        'title' => $localeCode,
                        'status' => 1,
                        'frontend_status' => 1,
                    ]));
                }
                return $collection;
            });
            $this->app->singleton('allRoutesKey', function(){
                return [];
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Page format macros
         */
        Writer::macro('setCreator', function (Writer $writer, string $creator) {
            $writer->getDelegate()->getProperties()->setCreator($creator);
        });

        Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
            $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
        });

        /**
         * Cell macros
         */
        Writer::macro('setCellValue', function (Writer $writer, string $cell, string $data) {
            $writer->getDelegate()->setCellValue($cell, $data);
        });

        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        Sheet::macro('horizontalAlign', function (Sheet $sheet, string $cellRange, string $align) {
            $sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal($align);
        });

        Sheet::macro('verticalAlign', function (Sheet $sheet, string $cellRange, string $align) {
            $sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setVertical($align);
        });

        Sheet::macro('wrapText', function (Sheet $sheet, string $cellRange) {
            $sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setWrapText(true);
        });

        Sheet::macro('mergeCells', function (Sheet $sheet, string $cellRange) {
            $sheet->getDelegate()->mergeCells($cellRange);
        });

        Sheet::macro('columnWidth', function (Sheet $sheet, string $column, float $width) {
            $sheet->getDelegate()->getColumnDimension($column)->setWidth($width);
        });

        Sheet::macro('rowHeight', function (Sheet $sheet, string $row, float $height) {
            $sheet->getDelegate()->getRowDimension($row)->setRowHeight($height);
        });

        Sheet::macro('setFontFamily', function (Sheet $sheet, string $cellRange, string $font) {
            $sheet->getDelegate()->getStyle($cellRange)->getFont()->setName($font);
        });

        Sheet::macro('setFontSize', function (Sheet $sheet, string $cellRange, float $size) {
            $sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize($size);
        });

        Sheet::macro('textRotation', function (Sheet $sheet, string $cellRange, int $degrees) {
            $sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setTextRotation($degrees);
        });
    }
}
