<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapBackendRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $localeDatas = env('APP_ENV')!='install'?app('frontendLocales')->pluck('code')->toArray():config('translatable.locales');
        $locale = request()->segment(1);
        $locale_code_arr = explode('-', $locale);
        if (count($locale_code_arr)>1) {
            $locale_code_arr[count($locale_code_arr)-1] = strtoupper($locale_code_arr[count($locale_code_arr)-1]);
            $locale_code = implode('-', $locale_code_arr);
        } else {
            $locale_code = $locale;
        }
        if (in_array($locale_code, $localeDatas)) {
            app()->setLocale($locale_code);
        }
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "backend" routes for the application.
     *
     * @return void
     */
    protected function mapBackendRoutes()
    {
        if (request()->segment(1) == env('BACKEND_ROUTE_PREFIX')) {
            app()->setLocale(env('BACKEND_LOCALE'));
        }
        Route::prefix(env('BACKEND_ROUTE_PREFIX'))
            ->name('backend.')
            ->middleware('backend')
            ->namespace($this->namespace.'\Backend')
            ->group(base_path('routes/backend.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
