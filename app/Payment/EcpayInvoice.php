<?php

namespace App\Payment;

use ECPayInvoiceSdk\EcpayInvoice as EcpayInv;

class EcpayInvoice extends EcpayInv
{
    function __construct()
    {
        parent::__construct();

        $this->Invoice_Method = 'INVOICE';
        $this->Invoice_Url = config('payment.ecpay.InvoiceURL').'/Invoice/Issue';
        $this->MerchantID = config('payment.ecpay.InvoiceMerchantID');
        $this->HashKey = config('payment.ecpay.InvoiceHashKey');
        $this->HashIV = config('payment.ecpay.InvoiceHashIV');
    }
}
