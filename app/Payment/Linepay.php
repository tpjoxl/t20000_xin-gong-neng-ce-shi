<?php

namespace App\Payment;

use Exception;
use Illuminate\Support\Facades\Log;
use App\Helpers\ArrHelper as Arr;
/*
 * Line Pay API v3
 * 文件：https://pay.line.me/jp/developers/apis/onlineApis?locale=zh_TW
 * 參考教學：https://weiupward.com/2020/07/09/line-pay-api-v3-php-%e7%af%84%e4%be%8b%e4%b8%80/
 */ 

class Linepay
{
    public $ServiceURL, $ChannelID, $ChannelSecret, $RequestAction, $TransactionID, $RegKey, $Params;
    function __construct()
    {
        $this->ServiceURL = trim(config('payment.linepay.ServiceURL'), '/');
        $this->ChannelID = config('payment.linepay.ChannelID');
        $this->ChannelSecret = config('payment.linepay.ChannelSecret');
        $this->RequestAction = 'RequestAction';
        $this->TransactionID = 'TransactionID';
        $this->RegKey = 'RegKey';
        $this->Params = [];
    }
    public function process()
    {
        $action = $this->getRequestAction();
        $params = $action['params'];
        foreach (Arr::dot($this->Params) as $key => $value) {
            Arr::set($params, $key, $value);
        }
        $this->Params = $params;
        // dd(trim($this->ServiceURL, '/').$action['uri']);
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getRequestHeader());
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $action['connection_timeout']); 
            curl_setopt($curl, CURLOPT_TIMEOUT, $action['read_timeout']);
            if ($action['method'] == 'GET') {
                curl_setopt($curl, CURLOPT_URL, $this->ServiceURL.$action['uri'].(!empty($this->Params)||count($this->Params)?'?'.http_build_query($this->Params):''));
            } elseif ($action['method'] == 'POST') {
                curl_setopt($curl, CURLOPT_URL, $this->ServiceURL.$action['uri']);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->Params));
            }
            ob_start();
            $curl_exec = curl_exec($curl);
            $curl_errno = curl_errno($curl);
            $curl_error = curl_error($curl);
            ob_end_clean();
            curl_close($curl);
            // dd($curl_exec, $curl_errno, $curl_error);
            if ($curl_errno) {
                Log::channel('payment')->error('Line Pay error.', ['RequestAction' => $this->RequestAction, 'Params' => $this->Params, 'errno'=>$curl_errno, 'error'=>$curl_error]);
                return [
                    'returnCode' => 'error',
                    'returnMessage' => $curl_errno.' '.$curl_error,
                ];
            } else {
                $result = json_decode($curl_exec, true);
                Log::channel('payment')->info('Line Pay process', ['RequestAction' => $this->RequestAction, 'Headers' => $this->getRequestHeader(), 'Params' => $this->Params, 'Result' => $result]);
                return $result;
            }
        } catch (Exception $e) {
            Log::channel('payment')->error('Line Pay error.', ['RequestAction' => $this->RequestAction, 'Params' => $this->Params, 'errorDetail'=>$e->getMessage()]);
            return [
                'returnCode' => 'error',
                'returnMessage' => $e->getMessage(),
            ];
        }
    }
    protected function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }
    protected function getSignature($nonce)
    {
        $action = $this->getRequestAction();
        if ($action['method'] == 'GET') {
            // GET : Channel Secret + URI + Query String + nonce
            $signature = $this->ChannelSecret . $action['uri'] . (!empty($this->Params)||count($this->Params)?http_build_query($this->Params):null) . $nonce;
        } elseif ($action['method'] == 'POST') {
            // POST : Channel Secret + URI + Request Body + nonce
            $signature = $this->ChannelSecret . $action['uri'] . json_encode($this->Params) . $nonce;
        }
        return $this->signatureEncrypt($signature);
    }
    protected function signatureEncrypt($signature)
    {
        return base64_encode(hash_hmac('sha256', $signature, $this->ChannelSecret, true));
    }
    protected function getRequestHeader() {
        $nonce = $this->generateUuid();
        $header = array(
            'Content-Type: application/json',
            'X-LINE-ChannelId: ' . $this->ChannelID,
            'X-LINE-Authorization-Nonce: ' . $nonce,
            'X-LINE-Authorization: ' . $this->getSignature($nonce)
        );
        return $header;
    }
    protected function getRequestAction()
    {
        $requestAction = [
            'Request' => [
                'method' => 'POST',
                'uri' => '/v3/payments/request',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [
                    'amount' => 1,  // *Number 付款金額 = sum(packages[].amount) + sum(packages[].userFee) + options.shipping.feeAmount
                    'currency' => 'TWD',  // *String(3) 貨幣（ISO 4217） 支援貨幣：USD、JPY、TWD、THB
                    'orderId' => 'testorderID',  // *String(100) 商家訂單編號 商家管理的唯一ID
                    'packages' => [
                        [
                            'id' => '1',  // *String(50) Package list的唯一ID
                            'amount' => 1,  // *Number 一個Package中的商品總價 =sum(products[].quantity * products[].price)
                            // 'userFee' => 0,  // Number 手續費：在付款金額中含手續費時設定
                            'name' => '測試商家名稱', // *String(100) Package名稱 （or Shop Name）
                            'products' => [
                                [
                                    // 'id' => 'test001',  // String(50) 商家商品ID
                                    'name' => 'testgoods',  // *String(4000) 商品名
                                    // 'imageUrl' => '',  // String(500) 商品圖示的URL
                                    'quantity' => 1,  // *Number 商品數量
                                    'price' => 1,  // *Number 各商品付款金額
                                    // 'originalPrice' => 1,  // Number 各商品原金額
                                ]
                            ]
                        ]
                    ],
                    'redirectUrls' => [
                        'confirmUrl' => '',  // String(500) 使用者授權付款後，跳轉到該商家URL
                        'cancelUrl' => '',  // String(500) 使用者通過LINE付款頁，取消付款後跳轉到該URL
                    ],
                    'options' => [
                        'display' => [
                            'locale' => 'zh_TW', // 等待付款頁的語言程式碼，預設為英文（en） 支援語言：en、ja、ko、th、zh_TW、zh_CN
                        ]
                    ]
                ],
                'returnMessages' => [],
            ],
            'Confirm' => [
                'method' => 'POST',
                'uri' => '/v3/payments/'.$this->TransactionID.'/confirm',
                'connection_timeout' => 5,
                'read_timeout' => 40,
                'params' => [
                    'amount' => 1,  // *Number 付款金額
                    'currency' => 'TWD',  // *String(3) 貨幣（ISO 4217） 支援貨幣：USD、JPY、TWD、THB
                ],
                'returnMessages' => [
                    '1198' => 'API調用重覆', // 
                ],
            ],
            'Capture' => [
                'method' => 'POST',
                'uri' => '/v3/payments/authorizations/'.$this->TransactionID.'/capture',
                'connection_timeout' => 5,
                'read_timeout' => 60,
                'params' => [
                    'amount' => 1,  // *Number 付款金額
                    'currency' => 'TWD',  // *String(3) 貨幣（ISO 4217） 支援貨幣：USD、JPY、TWD、THB
                ],
                'returnMessages' => [],
            ],
            'Void' => [
                'method' => 'POST',
                'uri' => '/v3/payments/authorizations/'.$this->TransactionID.'/void',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [],
                'returnMessages' => [
                    '1165' => '該交易已經被取消授權且無效', //
                ],
            ],
            'Refund' => [
                'method' => 'POST',
                'uri' => '/v3/payments/'.$this->TransactionID.'/refund',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [
                    // 'refundAmount' => null, // 退款金額 返回空值的話，進行全部退款
                ],
                'returnMessages' => [
                    '1155' => '交易編號不符合退款資格',//
                    '1164' => '退款金額超過限制金額',//
                    '1198' => 'API呼叫重複', // 
                ],
            ],
            'Details' => [
                'method' => 'GET',
                'uri' => '/v3/payments',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [
                    // 'transactionId' => [], // 由LINE Pay建立的交易序號或退款序號
                    // 'orderId' => [], // 商家訂單編號
                    // 'fields' => '',  // 可以選擇查詢物件 transaction / order (預設為所有)
                ],
                'returnMessages' => [],
            ],
            'Status' => [
                'method' => 'GET',
                'uri' => '/v3/payments/requests/'.$this->TransactionID.'/check',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [],
                'returnMessages' => [
                    '0000' => '授權尚未完成',
                    '0110' => '授權完成 - 現在可以呼叫Confirm API',
                    '0121' => '該交易已被用戶取消，或者超時取消（20分鐘）- 交易已經結束了',
                    '0122' => '付款失敗 - 交易已經結束了',
                    '0123' => '付款成功 - 交易已經結束了',
                ],
            ],
            'RegKey' => [
                'method' => 'GET',
                'uri' => '/v3/payments/preapprovedPay/'.$this->RegKey.'/check',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [
                    // 'creditCardAuth' => '', // Boolean 使用RegKey的信用卡，是否完成預授權 true : 通過LINE Pay驗證和信用卡預授權，查詢RegKey狀態。請注意，這必須經過LINE Pay管理人員的稽核進行。 false：通過LINE Pay驗證查詢RegKey狀態。
                ],
                'returnMessages' => [],
            ],
            'Preapproved' => [
                'method' => 'POST',
                'uri' => '/v3/payments/preapprovedPay/'.$this->RegKey.'/payment',
                'connection_timeout' => 5,
                'read_timeout' => 40,
                'params' => [
                    'productName' => '',  // *String(4000) 商品名稱
                    'amount' => 1, // *Number 付款金額
                    'currency' => 'TWD',  // *String(3) 貨幣（ISO 4217） 支援貨幣：USD、JPY、TWD、THB
                    'orderId' => '', // *String(100) 商家唯一訂單編號
                ],
                'returnMessages' => [],
            ],
            'ExpireRegKey' => [
                'method' => 'POST',
                'uri' => '/v3/payments/preapprovedPay/'.$this->RegKey.'/expire',
                'connection_timeout' => 5,
                'read_timeout' => 20,
                'params' => [],
                'returnMessages' => [],
            ],
        ];
        return Arr::get($requestAction, $this->RequestAction);
    }
    public function getReturnMessage($code)
    {
        $msgs = [
            '0000' => '成功',
            '1101' => '買家不是LINE Pay的用戶',
            '1102' => '買方被停止交易',
            '1104' => '此商家不存在',
            '1105' => '此商家無法使用LINE Pay',
            '1106' => '標頭(Header)資訊錯誤',
            '1110' => '無法使用的信用卡',
            '1124' => '金額錯誤 (scale)',
            '1141' => '付款帳戶狀態錯誤',
            '1142' => 'Balance餘額不足',
            '1145' => '正在進行付款',
            '1150' => '交易記錄不存在',
            '1152' => '該transactionId的交易記錄已經存在',
            '1153' => '付款request時的金額與申請confirm的金額不一致',
            '1154' => '買家設定為自動付款的信用卡暫時無法使用',
            '1155' => '交易編號不符合退款資格',
            '1159' => '無付款申請資訊',
            '1163' => '可退款日期已過無法退款',
            '1164' => '超過退款額度',
            '1165' => '已經退款而關閉的交易',
            '1169' => '用來確認付款的資訊錯誤（請訪問LINE Pay設置付款方式與密碼認證）',
            '1170' => '使用者帳戶的餘額有變動',
            '1172' => '該訂單編號(orderId)的交易記錄已經存在',
            '1177' => '超過允許查詢的交易數目 (100筆)',
            '1178' => '商家不支援該貨幣',
            '1179' => '無法處理的狀態',
            '1180' => '付款時限已過',
            '1183' => '付款金額不能小於 0',
            '1184' => '付款金額比付款申請時候的金額還大',
            '1190' => 'regKey 不存在',
            '1193' => 'regKey 已過期',
            '1194' => '此商家無法使用自動付款',
            '1197' => '已在處理使用 regKey 進行的付款',
            '1198' => 'API重覆呼叫，或者授權更新過程中，呼叫了Capture API（請幾分鐘後重試一下）',
            '1199' => '內部請求錯誤',
            '1264' => 'LINE Pay Money相關錯誤',
            '1280' => '信用卡付款時候發生了臨時錯誤',
            '1281' => '信用卡付款錯誤',
            '1282' => '信用卡授權錯誤',
            '1283' => '因有異常交易疑慮暫停交易，請洽LINE Pay客服確認',
            '1284' => '暫時無法以信用卡付款',
            '1285' => '信用卡資訊不完整',
            '1286' => '信用卡付款資訊不正確',
            '1287' => '信用卡已過期',
            '1288' => '信用卡的額度不足',
            '1289' => '超過信用卡付款金額上限',
            '1290' => '超過一次性付款的額度',
            '1291' => '此信用卡已被掛失',
            '1292' => '此信用卡已被停卡',
            '1293' => '信用卡驗證碼 (CVN) 無效',
            '1294' => '此信用卡已被列入黑名單',
            '1295' => '信用卡號無效',
            '1296' => '無效的金額',
            '1298' => '信用卡付款遭拒絕',
            '1900' => '發生暫時錯誤，請稍後重試',
            '1902' => '發生暫時錯誤，請稍後重試',
            '1999' => '跟已發出的請求資訊不同',
            '2101' => '參數錯誤',
            '2102' => 'JSON 資料格式錯誤',
            '9000' => '內部錯誤',
        ];

        if ($action = $this->getRequestAction()) {
            $msgs = array_merge($msgs, $action['returnMessages']);
        }
        return Arr::get($msgs, $code);
    }
}
