<?php

namespace App\Payment;

use ECPaySdk\ECPay_AllInOne;

class Ecpay extends ECPay_AllInOne
{
    function __construct()
    {
        parent::__construct();

        $this->ServiceURL = config('payment.ecpay.ServiceURL');
        $this->MerchantID = config('payment.ecpay.MerchantID');
        $this->HashKey = config('payment.ecpay.HashKey');
        $this->HashIV = config('payment.ecpay.HashIV');
        $this->EncryptType = 1;
    }
}
