<?php

namespace App\Payment;

class Newebpay
{

  public $ServiceURL = 'ServiceURL';
  public $MerchantID = 'MerchantID'; // 商店代號
  public $HashKey = 'HashKey';
  public $HashIV = 'HashIV';
  public $Version = 'Version'; // 串接程式版本
  public $Send = 'Send';
  public $SendExtend = 'SendExtend';

  function __construct()
  {
    $this->ServiceURL = config('payment.newebpay.ServiceURL');
    $this->MerchantID = config('payment.newebpay.MerchantID'); // 商店代號
    $this->HashKey = config('payment.newebpay.HashKey');
    $this->HashIV = config('payment.newebpay.HashIV');
    $this->Version = config('payment.newebpay.Version');
    $this->PaymentType = 'aio';
    $this->Send = array(
      "MerchantID"        => $this->MerchantID, // *商店代號
      "RespondType"       => 'JSON', // *回傳格式(JSON / String)
      "TimeStamp"         => time(), // *時間戳記
      "Version"           => $this->Version, // *串接程式版本
      "LangType"          => 'zh-tw', // 語系(zh-tw / en / jp)
      "MerchantOrderNo"   => '', // *商店訂單編號(限英、數字、_格式) Varchar(30)
      "Amt"               => 0, // *訂單金額(純數字不含符號，幣別：新台幣)
      "ItemDesc"          => '', // *商品資訊 Utf-8 Varchar(50)
      "TradeLimit"        => 0, // 交易限制秒數(60-900)
      "ExpireDate"        => '', // 繳費有效期限(格式為 date('Ymd')，若為空值，系統預設為 7 天，最大180天)
      "ReturnURL"         => '', // 支付完成返回商店網址(若為空值，交易完成後，消費者將停留在藍新金流付款或取號完成頁面。只接受 80 與 443 Port)
      "NotifyURL"         => '', // 支付通知網址(以幕後方式回傳給商店相關支付結果資料。只接受 80 與 443 Port)
      "CustomerURL"       => '', // 商店取號網址(若為空值，則會顯示取號結果在藍新金流頁面。)
      "ClientBackURL"     => '', // 返回商店網址(若為空值，則會顯示取號結果在藍新金流頁面。)
      "Email"             => '', // *付款人電子信箱
      "EmailModify"       => 1, // 付款人電子信箱是否開放修改(1=可修改；0=不可修改)
      "LoginType"         => 0, // *藍新金流會員(1=須要登入藍新金流會員；0=不須登入藍新金流會員)
      "OrderComment"      => '', // 商店備註 Varchar(300)
      "CREDIT"            => 0, // 信用卡㇐次付清啟用(1=啟用；0 或者未有此參數=不啟用)
      "ANDROIDPAY"        => 0, // Google Pay 啟用(1=啟用；0 或者未有此參數=不啟用)
      "SAMSUNGPAY"        => 0, // Samsung Pay 啟用(1=啟用；0 或者未有此參數=不啟用)
      "LINEPAY"           => 0, // LINE Pay 啟用(1=啟用；0 或者未有此參數=不啟用)
      "InstFlag"          => 0, // 信用卡分期付款啟用(1=開啟所有分期期別，且不可帶入其他期別參數；3=分3期；6=分6期；12=分12期；18=分18期；24=分24期；30=分30期；0或無值=不開啟分期)
      "CreditRed"         => 0, // 信用卡紅利啟用(1=啟用；0 或者未有此參數=不啟用)
      "CREDITAE"          => 0, // 信用卡美國運通卡啟用(1=啟用；0 或者未有此參數=不啟用)
      "UNIONPAY"          => 0, // 信用卡銀聯卡啟用(1=啟用；0 或者未有此參數=不啟用)
      "WEBATM"            => 0, // WEBATM 啟用(1=啟用；0 或者未有此參數=不啟用)
      "VACC"              => 0, // ATM 轉帳啟用(1=啟用；0 或者未有此參數=不啟用)
      "CVS"               => 0, // 超商代碼繳費啟用(1=啟用；0 或者未有此參數=不啟用。訂單金額小於 30 元或超過 2 萬元時不會顯示此支付方式)
      "BARCODE"           => 0, // 超商條碼繳費啟用(1=啟用；0 或者未有此參數=不啟用。訂單金額小於 20 元或超過 4 萬元時不會顯示此支付方式)
      "ALIPAY"            => 0, // 支付寶啟用(1=啟用；0 或者未有此參數=不啟用)
      "P2G"               => 0, // ezPay 電子錢包(1=啟用；0 或者未有此參數=不啟用)
      "CVSCOM"            => 0, // 物流啟用(1=啟用超商取貨不付款；2=啟用超商取貨付款；3=啟用超商取貨不付款及超商取貨付款；0 或者未有此參數=不啟用。訂單金額小於 30 元或超過 2 萬元時不會顯示此支付方式)
    );

    $this->SendExtend = array();
  }

  /*HashKey AES 加解密 */
  function create_mpg_aes_encrypt($parameter = "", $key = "", $iv = "")
  {
    $return_str = '';
    if (!empty($parameter)) {
      //將參數經過 URL ENCODED QUERY STRING
      $return_str = http_build_query($parameter);
    }
    return trim(bin2hex(openssl_encrypt($this->addpadding($return_str), 'aes-256-cbc', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv)));
  }
  function addpadding($string, $blocksize = 32)
  {
    $len = strlen($string);
    $pad = $blocksize - ($len % $blocksize);
    $string .= str_repeat(chr($pad), $pad);

    return $string;
  }

  function strippadding($string)
  {
    $slast = ord(substr($string, -1));
    $slastc = chr($slast);
    $pcheck = substr($string, -$slast);
    if (preg_match("/$slastc{" . $slast . "}/", $string)) {
      $string = substr($string, 0, strlen($string) - $slast);
      return $string;
    } else {
      return false;
    }
  }

  /*HashKey AES 解密 */
  function create_aes_decrypt($parameter = "", $key = "", $iv = "")
  {
    return $this->strippadding(openssl_decrypt(hex2bin($parameter), 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv));
  }

  /*HashIV SHA256 加密*/
  function SHA256($key = "", $tradeinfo = "", $iv = "")
  {
    $HashIV_Key = "HashKey=" . $key . "&" . $tradeinfo . "&HashIV=" . $iv;

    return $HashIV_Key;
  }

  function json_response_decrypt($response = [])
  {
    $data = $this->create_aes_decrypt($response['TradeInfo'], $this->HashKey, $this->HashIV);
    $json = json_decode($data, true);
    // return $json['Result'];
    return array_merge(['TradeInfo'=>$response['TradeInfo'], 'TradeSha'=>$response['TradeSha'], 'Status'=>$json['Status'], 'Message'=>$json['Message']], $json['Result']);
  }

  function CheckOut()
  {
    $trade_info_arr = array_merge($this->Send, $this->SendExtend);
    $TradeInfo = $this->create_mpg_aes_encrypt($trade_info_arr, $this->HashKey, $this->HashIV);
    $SHA256 = strtoupper(hash("sha256", $this->SHA256($this->HashKey,$TradeInfo,$this->HashIV)));
    echo $this->getCheckOutHtml($this->ServiceURL,$this->MerchantID,$TradeInfo,$SHA256,$this->Version);
  }

  function getCheckOutHtml($ServiceURL = "", $MerchantID = "", $TradeInfo = "", $TradeSha = "", $Version = "")
  {
    $szHtml = '<!doctype html>';
    $szHtml .= '<html>';
    $szHtml .= '<head>';
    $szHtml .= '<meta charset="utf-8">';
    $szHtml .= '</head>';
    $szHtml .= '<body>';
    $szHtml .= '<form name="newebpay" id="newebpay" method="post" action="' . $ServiceURL . '" style="display:none;">';
    $szHtml .= '<input type="text" name="MerchantID" value="' . $MerchantID . '" type="hidden">';
    $szHtml .= '<input type="text" name="TradeInfo" value="' . $TradeInfo . '"   type="hidden">';
    $szHtml .= '<input type="text" name="TradeSha" value="' . $TradeSha . '" type="hidden">';
    $szHtml .= '<input type="text" name="Version"  value="' . $Version . '" type="hidden">';
    $szHtml .= '</form>';
    $szHtml .= '<script type="text/javascript">';
    $szHtml .= 'document.getElementById("newebpay").submit();';
    $szHtml .= '</script>';
    $szHtml .= '</body>';
    $szHtml .= '</html>';

    return $szHtml;
  }

  function getStatusErrorMessage($code) {
    $errorMsgs = [
      'MPG01001' => '會員參數 LoginType 不可空白/設定錯誤',
      'MPG01002' => '時間戳記 TimeStamp 不可空白',
      'MPG01005' => 'TokenTerm 不可空白/設定錯誤',
      'MPG01008' => '分期參數 InstFlag 設定錯誤',
      'MPG01009' => '商店代號 MerchantID 不可空白',
      'MPG01010' => '程式版本 Version 設定錯誤',
      'MPG01011' => '回傳規格 RespondType 設定錯誤',
      'MPG01012' => '商店訂單編號 MerchantOrderNo 不可空白/設定錯誤(限英數字、底線，⾧度 30 字)',
      'MPG01013' => '付款人電子信箱 Email 設定錯誤',
      'MPG01014' => '網址 ReturnURL、NotifyURL、CustomerURL、ClientBackURL 設定錯誤',
      'MPG01015' => '訂單金額 Amt 不可空白/設定錯誤',
      'MPG01016' => '檢查碼 CheckValue 不可空白',
      'MPG01017' => '商品資訊 ItemDesc 不可空白',
      'MPG01018' => '繳費有效期限 ExpireDate 設定錯誤',
      'MPG01023' => '交易加密資料 TradeInfo 不可空白',
      'MPG01024' => '交易加密 SHA 資料 TradeSha 不可空白',
      'MPG02001' => '檢查碼 CheckValue 錯誤',
      'MPG02002' => '查無商店開啟任何金流服務',
      'MPG02003' => '支付方式未啟用，請洽客服中心',
      'MPG02004' => '匯率時間戳記不存在，請確認；已過匯率鎖定時限，請重新交易',
      'MPG02005' => '送出後檢查，驗證資料錯誤',
      'MPG02006' => '信用卡收單機構系統發生異常，請洽客服中心',
      'MPG03001' => 'FormPost 加密失敗',
      'MPG03002' => '拒絕交易 IP',
      'MPG03003' => 'IP 交易次數限制(N 分鐘內不可交易達 M 次)',
      'MPG03004' => '商店狀態已被暫停或是關閉，無法進行交易',
      'MPG03005' => '回傳參數資料不存在/解密失敗',
      'MPG03006' => '回傳參數資料不存在/解密失敗',
      'MPG03007' => '該筆訂單交易資訊不存在，請重新交易(物流服務)；查無此商店代號；TradeInfo 裡的 MerchantID 參數與回傳 MerchantID參數不㇐致',
      'MPG03008' => '已存在相同的商店訂單編號',
      'MPG03009' => '交易資料 SHA 256 (TradeSha) 檢查不符合；交易資料 TradeInfo 解密失敗，請確認',
      'MPG05001' => '未有信用卡驗證資料；信用卡驗證資料解密失敗',
      'MPG05002' => '信用卡卡號⾧度不足；未有商店代號；本商店不支援此信用卡付款，請改用其它發卡行所核發之信用卡',
      'MPG05003' => '此信用卡不支援㇐次付清，請改用可㇐次付清之信用卡或改用其他支付方式；此信用卡不支援分期付款，請改用可分期付款之信用卡或改用其他支付方式；此信用卡不支援紅利付款，請改用可紅利折抵之信用卡或改用其他支付方式',
      'MPG05004' => '發卡行暫時無法提供信用卡分期付款服務',
    ];
    if (array_key_exists($code, $errorMsgs)) {
      return $errorMsgs[$code];
    } else {
      return null;
    }
  }
}
