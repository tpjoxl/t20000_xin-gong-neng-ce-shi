<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Power;

class AdminPowerCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $auth = Auth::guard('admin')->user();
        // 狀態判定
        $loginErrorMsg = '';
        if ($auth->status == 0) {
            $loginErrorMsg = __('backend.login_status_error');
        }
        if (!empty($loginErrorMsg)) {
            Auth::guard('admin')->logout();
            return redirect()->route('backend.login')
                ->with('error', $loginErrorMsg);
        }
        // 權限判定
        $currentRoute = request()->route()->getName();
        $exceptRoutes = [
            'backend.home',
        ];
        if (in_array($currentRoute, $exceptRoutes) || ($auth && $auth->group->checkPowerByRouteName($currentRoute))) {
            return $next($request);
        }

        return redirect()->route('backend.home')->withError(__('backend.no_power_auth', [], env('BACKEND_LOCALE')));
    }
}
