<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class FirstLoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authArray = [
            'web' => 'user',
        ];
        foreach ($authArray as $authGuard => $prefix) {
            $auth = Auth::guard($authGuard)->user();
            if ($auth) {
                // 狀態判定
                $loginErrorMsg = '';
                if ($auth->email_verified == 0) {
                    $loginErrorMsg = __('text.login_email_verified_error');
                } elseif ($auth->status == 2) {
                    $loginErrorMsg = __('text.login_status_error');
                }
                if (!empty($loginErrorMsg)) {
                    Auth::guard($authGuard)->logout();
                    return redirect()->route($prefix.'.login')
                        ->with('errorText', $loginErrorMsg);
                }
                // 社群登入
                if ($auth->first_login == 1 && Route::currentRouteName() != $prefix.'.edit') {
                    return redirect()->route($prefix.'.edit');
                }
            }
        }
        return $next($request);
    }
}
