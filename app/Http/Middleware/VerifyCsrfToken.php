<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // 藍新
        'member/order-list/newebpay/return',
        'member/order-list/newebpay/notify',
        'member/order-list/newebpay/customer',
    ];
}
