<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class Language {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locales = config('translatable.locales');
        if (count($locales)>1) {
            // Make sure current locale exists.
            $locale = $request->segment(1);
            $userLangs = preg_split('/,|;/', $request->server('HTTP_ACCEPT_LANGUAGE'));

            if (is_null($locale)) {
                $segments = $request->segments();
                $defaultLang = null;
                foreach ($userLangs as $userLang) {
                  if (array_key_exists($userLang, $locales)) {
                    $defaultLang = $userLang;
                  }
                }
                $segments[0] = strtolower($defaultLang?:config('app.fallback_locale'));
                return redirect()->to(implode('/', $segments));
            }
        }

        return $next($request);
    }

}
