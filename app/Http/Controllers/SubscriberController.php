<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Subscriber;
use App\Models\Setting;

class SubscriberController extends Controller
{
    protected $model, $category, $perPage;

    public function __construct(Subscriber $Subscriber)
    {
        $this->model = $Subscriber;
        $this->prefix = 'subscriber';
    }
    /**
     * 訂閱
     */
    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => ['required', 'email', 'max:150', Rule::unique($this->model->getTable())->where(function ($query) {
                return $query->where('status', 1);
            })],
        ], [
            'email.unique' => __('validation.custom.subscriber.email.unique')
        ], []);
        if ($validator->fails()) {
            // return dd($validator->getMessageBag());
            return back()->withErrors($validator)->withInput()->with('openModal', 'subscribeModal');
        }
        $dataSave = $request->all();
        $data = $this->model->where('email', $request->email)->first();
        if (is_null($data)) {
            $data = $this->model->make();
            $data->fill($dataSave);
        } else {
            $data->name = $request->name;
            $data->status = 1;
        }
        if ($data->save()) {
            return back()->with('success', __('text.subscribe_success'));
        } else {
            return back()->with('error', __('text.subscribe_error'))->withInput();
        }
    }
    /**
     * 取消訂閱
     */
    public function unsubscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:150|exists:' . $this->model->getTable(),
        ], [
            'email.exists' => __('validation.custom.subscriber.email.exists')
        ], []);
        if ($validator->fails()) {
            // return dd($validator->getMessageBag());
            return back()->withErrors($validator)->withInput()->with('openModal', 'unsubscribeModal');
        }
        $data = $this->model->where('email', $request->email)->first();
        $data->fill([
            'status' => 0
        ]);
        if ($data->save()) {
            return back()->with('success', __('text.unsubscribe_success'));
        } else {
            return back()->with('error', __('text.unsubscribe_error'))->withInput();
        }
    }
}
