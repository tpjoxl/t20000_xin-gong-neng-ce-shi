<?php

namespace App\Http\Controllers\Backend;

use App\Models\Payment;
use App\Helpers\ArrHelper as Arr;

class PaymentController extends BackendController
{
    public function __construct(Payment $Payment)
    {
        parent::__construct($Payment);
        $this->prefix = 'payment';
    }
    protected function getFormFields()
    {
        $fields = array_merge(
            $this->getTemplateFormFields(['status', 'title', 'text']),
            [
                'shippings' => [
                    'type' => 'multi_select',
                    'label' => $this->getValidAttrs('shippings'),
                    'name' => 'shippings',
                    'required' => false,
                    'options' => method_exists($this->model, 'shippings') ? $this->model->shippings()->getRelated()->ordered()->get() : [],
                    'add_first' => [
                        'url' => route('backend.shipping.create'),
                    ]
                ],
                'fee_type' => [
                    'type' => 'radio',
                    'label' => $this->getValidAttrs('fee_type'),
                    'name' => 'fee_type',
                    'required' => false,
                    'options' => $this->model->present()->fee_type(),
                ],
                'fee' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('fee'),
                    'name' => 'fee',
                    'required' => false,
                ],
            ]
        );
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = $this->getTemplateSearchFields(['status', 'title']);
        return $fields;
    }
    protected function getValidRules()
    {
        return [
            'title' => 'required|string|max:255',
        ];
    }
    protected function getIndexList($data)
    {
        $fields = array_insert_after($this->getTemplateList($data, ['title', 'status']), [
            'provider' => [
                'label' => $this->getValidAttrs('provider'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->provider,
            ],
        ], 'title');
        return $fields;
    }
}
