<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\LocaleText;

class LocaleTextController extends BackendController
{

    public function __construct(LocaleText $LocaleText)
    {
        parent::__construct($LocaleText);
        $this->prefix = 'locale.text';
    }
    public function index(Request $request)
    {
        $prefix = $this->prefix;
        $locales = $this->locales;
        $localeTextKeys = [];
        foreach (__('routes') as $key => $value) {
            $localeTextKeys[] = 'routes.'.$key;
        }
        foreach (__('text') as $key => $value) {
            $localeTextKeys[] = 'text.'.$key;
        }

        if ($request->isMethod('POST')) {
            // dd($request->all());
            foreach ($request->text as $code => $arr) {
                foreach ($arr as $key => $value) {
                    if (!is_null($value)) {
                        $data = $this->model->where('code', $code)->where('text_key', $key)->firstOrCreate([
                            'code' => $code,
                            'text_key' => $key,
                        ]);
                        $data->update(['text_value' => $value]);
                    }
                }
            }
            return back()->with('success', __('backend.save_success'));
        }

        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'locales', 'localeTextKeys'));
    }
}
