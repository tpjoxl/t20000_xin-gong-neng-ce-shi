<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banner;
use App\Helpers\ArrHelper as Arr;

class BannerController extends BackendController
{

    public function __construct(Banner $Banner)
    {
        parent::__construct($Banner);
        $this->prefix = 'banner';
    }
    protected function getValidRules()
    {
        return [
            'title' => 'required|max:191',
            'pic' => 'required',
            'url' => 'nullable|url|max:255',
        ];
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields(['status', 'title', 'date_range', 'pic', 'tag_title', 'tag_alt', 'url', 'target']);
        $fields['pic']['required'] = true;
        $fields['pic']['w'] = 1920;
        $fields['pic']['h'] = 700;
        $fields['title']['type'] = 'text_area';
        $fields = array_insert_after($fields, [
            'text' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('text'),
                'name' => 'text',
                'required' => false,
                'rows' => 3,
            ],
        ], 'title');
        $fields = array_insert_after($fields, [
            'btn_txt' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('btn_txt'),
                'name' => 'btn_txt',
                'required' => false,
            ],
        ], 'tag_alt');
        $fields = array_insert_after($fields, [
            'btn_txt2' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('btn_txt').'2',
                'name' => 'btn_txt2',
                'required' => false,
            ],
            'url2' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('url').'2',
                'name' => 'url2',
                'required' => false,
            ],
            'target2' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('target').'2',
                'name' => 'target2',
                'label' => '',
                'required' => false,
                'options' => $this->model->present()->target(),
                'default' => '',
                'hint' => __('backend.url_hint', [], env('BACKEND_LOCALE'))
            ],
        ], 'target');
        return $fields;
    }
    protected function getIndexList($data)
    {
        return $this->getTemplateList($data, ['pic', 'title', 'date_range', 'status']);
    }
}
