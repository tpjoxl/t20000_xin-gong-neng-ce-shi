<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ArrHelper as Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use App\Models\Setting;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Traits\BackendTrait;
use Illuminate\Support\Str;
use App\Models\AdminGroup;
use App\Models\Power;

class BackendController extends Controller
{
    use BackendTrait;

    protected $model, $data, $template, $locales, $rank_all, $category, $valid_attrs, $valid_msgs, $perPage,$admin_account;

    public function __construct($Model)
    {
        parent::__construct();
        $this->model = $Model;
        $this->data = $this->model;
        $this->template = 'default';
        $this->locales = app('backendLocales');
        $this->rank_all = true;  // 前台是否有全部項目顯示的頁面，有的話後台就需要全部項目(不分類)的排序
        if (method_exists($this->model, 'categories')) {
            $this->category = $this->model->categories()->getRelated();
        } else {
            $this->category = null;
        }
        $this->parent = null;
        $this->parent_prefix = null;
        $this->parent_relation = null;
        $this->valid_attrs = [];
        $this->valid_msgs = [];
        $this->perPage = 20;

    }
    /**
     * 列表
     */
    public function index(Request $request)
    {
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $parent = $this->parent;
        $parent_prefix = $this->parent_prefix;
        $model = $this->model;
        $template = $this->template;
        $view = 'backend.' . $this->prefix . '.index';

        if ($this->template == 'default') {
            $rank_dropdowns = $this->getRankDropdowns();
            if (!$this->rank_all) {
                $rank_key = head(array_keys($rank_dropdowns));
                if (is_array(head($rank_dropdowns)['options'])) {
                    $rank_val = head(array_keys(head($rank_dropdowns)['options']));
                } elseif (head($rank_dropdowns)['options']->first()) {
                    $rank_val = head($rank_dropdowns)['options']->first()->id;
                }
                if (isset($rank_key) && !$request->filled($rank_key) && count($rank_dropdowns[$rank_key]['options'])) {
                    return redirect()->route('backend.' . $this->prefix . '.index', array_merge(request()->route()->parameters, [$rank_key => $rank_val]));
                }
            }
            $datas = $this->search()->paginate($this->perPage);

            if (!is_null($this->category)) {
                $categories = $this->category->tree()->get();
            }

            $search_fields = $this->getSearchFields();
            $list_datas = $this->generateListData($datas);
            $list_pager = [
                'first' => $datas->firstItem(),
                'last' => $datas->lastItem(),
                'total' => $datas->total(),
                'render' => $datas->appends(request()->input())->links('vendor.pagination.default')
            ];
            $dropdowns = $this->getDropdowns();

            if (!view()->exists($view)) {
                $view = 'backend.common.index';
            }
            $viewDatas = compact('prefix', 'template', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'dropdowns', 'rank_dropdowns', 'parent', 'parent_prefix');
        } elseif ($this->template == 'category') {
            $datas = $this->model->tree()->get();
            $dropdowns = $this->getDropdowns();

            if (!view()->exists($view)) {
                $view = 'backend.common.category.index';
            }
            $viewDatas = compact('prefix', 'template', 'datas', 'model', 'dropdowns', 'parent', 'parent_prefix');
        } elseif ($this->template == 'image') {
            $datas = $this->search()->get();

            if (!view()->exists($view)) {
                $view = 'backend.common.image.index';
            }
            $viewDatas = compact('prefix', 'template', 'datas', 'model', 'dropdowns', 'rank_dropdowns', 'parent', 'parent_prefix');
        }

        return view($view, $viewDatas);
    }

    /**
     * 新增頁面
     */
    public function create(Request $request)
    {
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $parent = $this->parent;
        $parent_prefix = $this->parent_prefix;
        $model = $this->model;
        $template = $this->template;
        if (!is_null($this->parent)) {
            $this->data = $data = $this->parent->{$this->parent_relation}()->make();
        } else {
            $this->data = $data = $this->model->make();
        }
        $form_fields = $this->getCreateFormFields();

        $view = 'backend.' . $this->prefix . '.create';
        if (!view()->exists($view)) {
            $view = 'backend.common.create';
        }
        return view($view, compact('model', 'template', 'data', 'prefix', 'rank_all', 'form_fields', 'parent', 'parent_prefix'));
    }
    /**
     * 儲存
     */
    public function store(Request $request)
    {
        $requestData = $request->all();

        if ($this->template == 'image') {
            $parent_id = $request->parent;
            $validator = Validator::make($requestData, [
                'images.*' => 'nullable|mimes:png,gif,jpeg,jpg,bmp|max:' . $this->model->maxUploadSize,
            ], [], [
                'images.*' => __('validation.attributes.pic', [], env('BACKEND_LOCALE')),
            ]);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $upload_reault = $this->model->uploadFile($request->file('images'), $this->useParentFolder ? $parent_id : null);
            // return dd($upload_reault);
            $store_reault = array();
            foreach ($upload_reault as $uploadFile) {
                $dataToSave = $uploadFile;
                $dataToSave['parent_id'] = $parent_id;
                // dd($this->model->translatedAttributes);
                if (count(config('translatable.locales')) > 1 && method_exists($this->model, 'translations')) {
                    foreach (config('translatable.locales') as $lang_code) {
                        $dataToSave[$lang_code] = ['description' => $uploadFile['description']];
                    }
                }

                // dd($dataToSave);
                $data = $this->model->make();
                $result = $data->fillAndSave($dataToSave);
                array_push($store_reault, $result);
            }
            $this->model->updateCover($parent_id);

            if ($upload_reault && $store_reault) {
                return redirect()->back()->with('success', __('backend.create_success', [], env('BACKEND_LOCALE')));
            } else {
                return redirect()->back()->with('error', __('backend.create_error', [], env('BACKEND_LOCALE')));
            }
        } else {
            if (!is_null($this->parent)) {
                $this->data = $data = $this->parent->{$this->parent_relation}()->make();
            } else {
                $this->data = $data = $this->model->make();
            }

            $validator = $this->valid($requestData, $this->getCreateValidRules(), $this->valid_msgs);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            if($this->prefix == 'question' || $this->prefix == 'question.category' ||$this->prefix == 'record'){
                $this->data->admin_account = auth()->guard('admin')->user()->account;
            }
            $result1 = $data->fillAndSave($requestData);

            if ($result1) {
                if ($this->rank_all) {
                    return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('success', __('backend.create_success', [], env('BACKEND_LOCALE')));
                } else {
                    $rank_dropdowns = $this->getRankDropdowns();
                    $rank_key = head(array_keys($rank_dropdowns));
                    $rank_val = is_array(request($rank_key))?head(request($rank_key)):request($rank_key);
                    return redirect()->route('backend.' . $this->prefix . '.index', array_merge(request()->route()->parameters, [$rank_key => $rank_val]))->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                }
            } else {
                return redirect()->back()->with('error', __('backend.create_error', [], env('BACKEND_LOCALE')));
            }
        }
    }
    /**
     * 顯示複製表單
     */
    public function copy(Request $request)
    {
        $id = $request->id;
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $parent = $this->parent;
        $parent_prefix = $this->parent_prefix;
        $model = $this->model;
        $template = $this->template;

        if (!is_null($this->parent)) {
            $this->data = $data = $this->parent->{$this->parent_relation}()->find($id);
        } else {
            $this->data = $data = $this->model->find($id);
        }
        if (is_null($data)) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('error', __('backend.data_not_found', [], env('BACKEND_LOCALE')));
        }
        if (method_exists($this->model, 'translations')) {
            $data->translations->transform(function ($item, $key) {
                $item->title .= '_' . __('backend.copy', [], $item->locale);
                if ($item->slug) {
                    $item->slug = '';
                }
                return $item;
            });
        } else {
            $data->title .= '_' . __('backend.copy');
            if ($data->slug) {
                $data->slug = '';
            }
        }
        $this->data = $data;
        $form_fields = $this->getCreateFormFields();
        $view = 'backend.' . $this->prefix . '.copy';
        if (!view()->exists($view)) {
            if (!view()->exists($view)) {
                $view = 'backend.common.copy';
            }
        }
        return view($view, compact('model', 'template', 'data', 'prefix', 'rank_all', 'form_fields', 'parent', 'parent_prefix'));
    }
    /**
     * 顯示修改表單
     */
    public function edit(Request $request)
    {
        $id = $request->id;
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $parent = $this->parent;
        $parent_prefix = $this->parent_prefix;
        $model = $this->model;
        $template = $this->template;

        if (!is_null($this->parent)) {
            $this->data = $data = $this->parent->{$this->parent_relation}()->find($id);
        } else {
            $this->data = $data = $this->model->find($id);
        }
        if (is_null($data)) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('error', __('backend.data_not_found', [], env('BACKEND_LOCALE')));
        }

        $form_fields = $this->getEditFormFields();
        $view = 'backend.' . $this->prefix . '.edit';
        if (!view()->exists($view)) {
            if (!view()->exists($view)) {
                $view = 'backend.common.edit';
            }
        }
        return view($view, compact('model', 'template', 'data', 'prefix', 'rank_all', 'form_fields', 'parent', 'parent_prefix'));
    }
    /**
     * 更新
     */
    public function update(Request $request)
    {
        if ($this->template == 'image') {
            $data = $this->model->find($request->id);
            if (is_null($data)) {
                return Response::json([
                    'error' => true,
                    'message' => __('backend.img_desc_update_error', [], env('BACKEND_LOCALE')),
                    'code' => 400
                ], 400);
            }
            // $data->description = $request->description;
            $data->translateOrNew($request->lang)->fill(['description' => $request->description]);

            if ($data->save()) {
                return Response::json([
                    'datas' => $this->parent_prefix?$data->{$this->parent_prefix}->images()->ordered()->get():$this->model->ordered()->get(),
                    'lang_datas' => config('translatable.locales'),
                    'error' => false,
                    'message' => __('backend.img_desc_update_success', [], env('BACKEND_LOCALE')),
                    'code' => 200
                ], 200);
            } else {
                return Response::json([
                    'error' => true,
                    'message' => __('backend.img_desc_update_error', [], env('BACKEND_LOCALE')),
                    'code' => 400
                ], 400);
            }
        } else {
            $id = $request->id;
            $requestData = $request->all();
            if (!is_null($this->parent)) {
                $this->data = $data = $this->parent->{$this->parent_relation}()->find($id);
            } else {
                $this->data = $data = $this->model->find($id);
            }
            $this->data->updated_at = Carbon::now();
            $validator = $this->valid($requestData, $this->getEditValidRules(), $this->valid_msgs, $id);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $result1 = $data->fillAndSave($requestData);

            if ($result1) {
                if ($this->rank_all) {
                    return redirect()->back()->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                } else {
                    $rank_dropdowns = $this->getRankDropdowns();
                    $rank_key = head(array_keys($rank_dropdowns));
                    $rank_val = is_array(request($rank_key))?head(request($rank_key)):request($rank_key);
                    return redirect()->route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, ['id' => $id, $rank_key => $rank_val]))->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                }
            } else {
                return redirect()->back()->with('error', __('backend.save_error', [], env('BACKEND_LOCALE')));
            }
        }
    }
    /**
     * 刪除
     */
    public function destroy(Request $request)
    {
        $ids = explode(",", $request->id);
        if (!is_null($this->parent)) {
            $result = $this->parent->{$this->parent_relation}()->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
        } else {
            $result = $this->model->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
        }
        if ($this->template == 'image' && $this->parent) {
            $this->model->updateCover($this->parent->id);
        }

        if ($result) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('success', __('backend.delete_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->back()->with('error', __('backend.delete_error', [], env('BACKEND_LOCALE')));
        }
    }
    /**
     * 修改狀態
     */
    public function modify(Request $request)
    {
        $ids = explode(",", $request->id);
        $dropdowns = $this->getDropdowns();
        $modifyDatas = $request->only(array_keys($dropdowns));
        if (!is_null($this->parent)) {
            $result = $this->parent->{$this->parent_relation}()->whereIn('id', $ids)->get()->each->update($modifyDatas);
        } else {
            $result = $this->model->whereIn('id', $ids)->get()->each->update($modifyDatas);
        }
        if ($result) {
            return redirect()->back()->with('success', __('backend.modify_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->back()->with('error', __('backend.modify_error', [], env('BACKEND_LOCALE')));
        }
    }
    /**
     * 排序列表頁
     */
    public function rank(Request $request)
    {
        $category_id = $request->categories;
        $parent = $this->parent;
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $rank_fields = $this->getRankFields();
        $dropdowns = $this->getRankDropdowns();
        $view = 'backend.' . $this->prefix . '.rank';
        if (!view()->exists($view)) {
            $view = 'backend.common.rank';
        }
        if (!is_null($this->category)) {
            if (is_null($category_id)) {
                if (!$rank_all) {
                    $rank_key = head(array_keys($dropdowns));
                    if (count(head($dropdowns)['options'])) {
                        if (is_array(head($dropdowns)['options'])) {
                            $rank_val = head(array_keys(head($dropdowns)['options']));
                        } elseif (head($dropdowns)['options']->first()) {
                            $rank_val = head($dropdowns)['options']->first()->id;
                        }
                        return  redirect()->route('backend.' . $this->prefix . '.rank.index', array_merge(request()->route()->parameters, [$rank_key => $rank_val]));
                    } else {
                        $datas = null;
                        return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'dropdowns'));
                    }
                }
                $datas = $this->model->ordered()->get();
                return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'dropdowns'));
            } else {
                $category = $this->category->find($category_id);
                $datas = $category->{$this->model->getTable()}()->ordered('pivot_rank')->get();
                return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'dropdowns'));
            }
        } elseif(!is_null($this->parent)) {
            $datas = $this->parent->{$this->parent_relation}()->ordered()->get();
            return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'dropdowns', 'parent'));
        } else {
            $datas = $this->model->ordered()->get();
            return view($view, compact('datas', 'prefix', 'rank_all', 'rank_fields', 'dropdowns'));
        }
    }
    /**
     * 重置排序
     */
    public function rankReset(Request $request)
    {
        if (!is_null($this->category) && $request->filled('categories')) {
            $data = $this->category->find($request->categories);
            foreach ($data->{$this->model->getTable()} as $category) {
                $data->{$this->model->getTable()}()->updateExistingPivot($category->id, ['rank' => 0]);
            }
        } elseif(!is_null($this->parent)) {
            $this->parent->{$this->parent_relation}()->update(['rank' => 0]);
        } else {
            $this->model->query()->update(['rank' => 0]);
        }
        return back()->with('success', __('backend.rank_reset_success', [], env('BACKEND_LOCALE')));
    }
    /**
     * 排序更新
     */
    public function rankUpdate(Request $request)
    {
        $category_id = $request->categories;
        if ($this->template == 'default') {
            $ids = explode(",", $request->input('sortNum'));

            $result = 0;
            if (is_null($category_id)) {
                foreach ($ids as $key => $id) {
                    if (!is_null($this->parent)) {
                        $data = $this->parent->{$this->parent_relation}()->find($id);
                    } else {
                        $data = $this->model->find($id);
                    }
                    $result += $data->update(['rank' => count($ids) - $key]);
                }
            } elseif (!is_null($this->category)) {
                foreach ($ids as $key => $id) {
                    $data = $this->category->find($category_id);
                    $result += $data->{$this->model->getTable()}()->updateExistingPivot($id, ['rank' => count($ids) - $key]);
                }
            }

            if ($result) {
                return back()->with('success', __('backend.rank_success', [], env('BACKEND_LOCALE')));
            } else {
                return back()->with('error', __('backend.rank_error', [], env('BACKEND_LOCALE')));
            }
        } elseif ($this->template == 'category') {
            $msg = '';
            $this->rank_msg = '';
            $this->rankUpdate = 0;
            $this->levelUpdate = 0;

            $tree = json_decode($request->sortNum);
            $rankTree = $this->rank_flatten($tree);
            $levelTree = $this->update_level($tree);
            $this->levelUpdate += $this->model->whereIn('id', $levelTree)->update(['parent_id' => NULL]);

            foreach ($rankTree as $key => $id) {
                $this->rankUpdate += $this->model->findOrFail($id)->update(['rank' => count($rankTree) - $key]);
            }
            if ($this->rankUpdate && $this->levelUpdate) {
                $msg = __('backend.rank_level_success', [], env('BACKEND_LOCALE'));
            } else {
                if ($this->rankUpdate) {
                    $msg = __('backend.rank_success', [], env('BACKEND_LOCALE'));
                }
                if ($this->levelUpdate) {
                    $msg = __('backend.level_success', [], env('BACKEND_LOCALE'));
                }
            }

            if ($this->rankUpdate || $this->levelUpdate) {
                if ($this->rank_msg != '') {
                    return back()->with('success', $msg)->with('error', $this->rank_msg);
                } else {
                    return back()->with('success', $msg);
                }
            } else {
                if ($this->rank_msg != '') {
                    return back()->with('error', $this->rank_msg);
                } else {
                    return back()->with('error', __('backend.rank_level_error', [], env('BACKEND_LOCALE')));
                }
            }
        } elseif ($this->template == 'image') {
            $ids = explode(",", $request->input('sortNum'));

            $result = 0;
            foreach ($ids as $key => $id) {
                $data = $this->model->find($id);
                if ($data) {
                    $result += $data->update(['rank' => count($ids) - $key]);
                }
            }
            if ($result) {
                return back()->with('success', __('backend.rank_success', [], env('BACKEND_LOCALE')));
            } else {
                return back()->with('error', __('backend.rank_error', [], env('BACKEND_LOCALE')));
            }
        }
    }
    public function password(Request $request)
    {
        $id = $request->id;
        $requestData = $request->all();
        $this->data = $data = $this->model->find($id);

        $validator = Validator::make($request->all(), [
            'password' => 'required|user_password|confirmed',
        ], [], [
            'password' => __('validation.attributes.new_password', [], env('BACKEND_LOCALE')),
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $result1 = $data->fillAndSave($requestData);

        if ($result1) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('success', __('backend.password_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('error', __('backend.password_error', [], env('BACKEND_LOCALE')));
        }
    }
    public function page(Request $request)
    {
        $model = new Setting();
        $prefix = $this->prefix;
        $page = Route::currentRouteName();
        $form_fields = $this->getPageFormFields($page);
        $this->data = $data = $model->getData($page);
        // dd($page, $data);

        if ($request->isMethod('POST')) {
            $requestData = $request->all();

            $validator = $this->valid($requestData, $this->getPageValidRules($page), $this->valid_msgs);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $result = $data->fillAndSave($form_fields);

            if (array_key_exists($page, $data->pageExcept)) {
                $this->handlePageExcept();
            }

            if ($result) {
                return back()->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
            } else {
                return back()->with('error', __('backend.save_error', [], env('BACKEND_LOCALE')));
            }
        }

        $view = 'backend.' . $this->prefix . '.page';
        if (!view()->exists($view)) {
            $view = 'backend.common.page';
        }
        return view($view, compact('model', 'data', 'prefix', 'form_fields'));
    }
    protected function handlePageExcept()
    {
    }
    /**
     * 設為封面
     */
    public function cover(Request $request)
    {
        // dd($request->all());
        $parent_id = $this->model->find($request->id)->parent_id;
        $result = $this->model->setCover($request->id, $parent_id);

        if($result) {
        return redirect()->back()->with('success', __('backend.set_cover_success', [], env('BACKEND_LOCALE')));
        } else {
        return redirect()->back()->with('error', __('backend.set_cover_error', [], env('BACKEND_LOCALE')));
        }
    }
}
