<?php

namespace App\Http\Controllers\Backend;

use App\Models\Blog;
use App\Helpers\ArrHelper as Arr;

class BlogController extends BackendController
{

    public function __construct(Blog $Blog)
    {
        parent::__construct($Blog);
        $this->prefix = 'blog';
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields();
        $fields['pic']['w'] = 300;
        $fields['pic']['h'] = 200;
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => $this->getTemplateFormFields([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::get($validRules, $page) ?: Arr::get($validRules, "backend.$this->prefix.page");
    }
}
