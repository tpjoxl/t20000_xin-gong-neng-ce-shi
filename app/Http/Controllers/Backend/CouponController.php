<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Helpers\ArrHelper as Arr;
use App\Traits\MailTrait;
use App\Mail\UserCoupon;

class CouponController extends BackendController
{
    use MailTrait;
    public function __construct(Coupon $Coupon)
    {
        parent::__construct($Coupon);
        $this->prefix = 'coupon';
        $this->valid_msgs = [
            'required_if' => __('validation.required'),
        ];
    }
    protected function getValidRules()
    {
        return [
            'title' => 'required',
            'code' => 'required',
            'type' => 'required',
            'number' => 'required|integer',
            'min_price' => 'required|integer',
            'date_on' => 'required',
            'date_off' => 'required|after_or_equal:date_on',
            'use_type' => 'required',
            'user_type' => 'nullable|required_if:use_type,1',
            'users' => 'nullable|required_if:use_type,2',
        ];
    }
    protected function getEditFormFields()
    {
        $fields = Arr::insertAfter($this->getFormFields(), $this->getTemplateFormFields(['created_at']), 'title');
        $fields = Arr::insertAfter($fields, [
            'code' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('code'),
                'name' => 'code',
                'required' => false,
            ],
        ], 'title');

        return $fields;
    }
    protected function getFormFields()
    {
        $fields = [
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'required' => true,
            ],
            'code' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('code'),
                'name' => 'code',
                'required' => true,
            ],
            'date_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('date_range'),
                'name' => ['date_on', 'date_off'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'required' => true,
            ],
            'type' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('type'),
                'name' => 'type',
                'required' => false,
                'options' => $this->model->present()->type(),
                'default' => 1,
            ],
            'number' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('number'),
                'name' => 'number',
                'required' => true,
                'hint' => '折扣類型選擇百分比時：75折則輸入25，8折則輸入20，依此類推...'
            ],
            'min_price' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('min_price'),
                'name' => 'min_price',
                'required' => true,
                'default' => 0,
                'hint' => '無最低消費金額限制則填入0',
            ],
            'user_visible' => [
              'type' => 'radio',
              'label' => $this->getValidAttrs('user_visible'),
              'name' => 'user_visible',
              'required' => false,
              'default' => 1,
              'options' => $this->model->present()->yes_or_no(),
              'hint' => '勾選「否」時僅供前台輸入優惠碼',
            ],
            'use_type' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('use_type'),
                'name' => 'use_type',
                'required' => false,
                'options' => $this->model->present()->use_type(),
                'default' => 1,
            ],
            'user_type' => [
                'type' => 'checkbox',
                'label' => $this->getValidAttrs('user_type'),
                'name' => 'user_type',
                'required' => false,
                'options' => $this->model->present()->user_type(),
            ],
            'count' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('count'),
                'name' => 'count',
                'required' => false,
            ],
            'users' => [
                'type' => 'multi_select_ajax',
                'label' => $this->getValidAttrs('users'),
                'name' => 'users',
                'required' => true,
                'options' => old('users') ? $this->data->users()->getRelated()->whereIn('id', old('users'))->get() : $this->data->users,
                'key' => ['account', 'name', 'phone'],
                'ajax_attrs' => [
                    'url' => route('backend.api.user'),
                    'type' => 'GET',
                ],
                'hint' => '可輸入會員帳號、姓名、電話當作搜尋關鍵字',
            ],
            'use_type_js' => [
                'type' => 'include',
                'name' => 'use_type_js',
                'view' => 'backend.coupon._use_type_js',
            ],
        ];
        // dd($fields);
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = array_merge(
            [
                'status' => [
                    'type' => 'radio',
                    'label' => $this->getValidAttrs('status'),
                    'name' => 'status',
                    'has_all' => true,
                    'options' => $this->model->present()->status(),
                    'search' => 'equal',
                ],
                'title' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('title'),
                    'name' => 'title',
                    'search' => 'like',
                ],
                'code' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('code'),
                    'name' => 'code',
                    'search' => 'like',
                ],
                'user_type' => [
                    'type' => 'radio',
                    'label' => $this->getValidAttrs('user_type'),
                    'name' => 'user_type',
                    'has_all' => true,
                    'options' => $this->model->present()->user_type(),
                    'search' => 'find_in_set',
                ],
            ],
            $this->getTemplateSearchFields(['date_range', 'created_range'])
        );
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = array_merge([
            'title' => [
                'label' => $this->getValidAttrs('title'),
                'align' => 'left',
                'type' => 'link',
                'data' => $data->title,
                'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
            ],
            'user_type' => [
                'label' => $this->getValidAttrs('user_type'),
                'align' => 'left',
                'type' => 'html',
                'options' => $this->model->present()->user_type(),
                'data' => !is_null($data->user_type)?implode(', ', Arr::only($this->model->present()->user_type(), $data->user_type)):'',
            ],
            'code' => [
                'label' => $this->getValidAttrs('code'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->code,
            ],
            'content' => [
                'label' => $this->getValidAttrs('content'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->content,
            ],
        ], $this->getTemplateList($data, ['date_range', 'created_at', 'status']));
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => [
                'birthday_number_1' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('birthday_number_1'),
                    'name' => 'birthday_number_1',
                    'required' => true,
                    'common' => true,
                    'hint' => '75折則輸入25，8折則輸入20，依此類推...',
                ],
                'birthday_min_price_1' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('birthday_min_price_1'),
                    'name' => 'birthday_min_price_1',
                    'required' => true,
                    'common' => true,
                ],
                'hr' => [
                    'type' => 'hr',
                    'label' => '',
                    'name' => 'hr',
                ],
                'birthday_number_2' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('birthday_number_2'),
                    'name' => 'birthday_number_2',
                    'required' => true,
                    'common' => true,
                    'hint' => '75折則輸入25，8折則輸入20，依此類推...',
                ],
                'birthday_min_price_2' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('birthday_min_price_2'),
                    'name' => 'birthday_min_price_2',
                    'required' => true,
                    'common' => true,
                ],
            ],
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => [
                'birthday_number_1' => 'required|integer',
                'birthday_min_price_1' => 'required|integer',
                'birthday_number_2' => 'required|integer',
                'birthday_min_price_2' => 'required|integer',
            ],
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
}
