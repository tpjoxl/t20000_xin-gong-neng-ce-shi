<?php

namespace App\Http\Controllers\Backend;

use App\Models\AdminGroup;
use App\Models\Power;
use App\Models\Product;
use App\Models\Contact;
use App\Models\User;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if (auth()->guard('admin')->user()->group->title = env('MASTER_GROUP')) {
            AdminGroup::find(1)->powers()->sync(Power::all()->pluck('id')->toArray());
        }
        $listLimit = 10;
        $products_count = Product::count();
        $orders_count_status1 = Order::where('status', 1)->count();
        $orders_count_status3 = Order::where('status', 3)->count();
        $orders = Order::where('status', 1)->ordered()->take($listLimit)->get();
        $users_count_sex0 = User::where('sex', 0)->count();
        $users_count_sex1 = User::where('sex', 1)->count();
        $users_count_sex2 = User::where('sex', 2)->count();
        $contacts_count_status0 = Contact::where('status', 0)->count();
        $contacts = Contact::where('status', 0)->ordered()->take($listLimit)->get();

        $ordersThisYear = Order::where('created_at', '>=', Carbon::parse('first day of January'))
            ->selectRaw('year(created_at) year, month(created_at) month, count(*) count')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->keyBy('month')
            ->toArray();
        $orderMonthCount = array();
        if (count($ordersThisYear)) {
            for ($i = 1; $i <= 12; $i++) {
                if (array_key_exists($i, $ordersThisYear)) {
                    $orderMonthCount[$i] = $ordersThisYear[$i]['count'];
                } else {
                    $orderMonthCount[$i] = 0;
                }
            }
        }
        return view('backend.home', compact('listLimit', 'products_count', 'orders_count_status1', 'orders_count_status3', 'orders', 'users_count_sex0', 'users_count_sex1', 'users_count_sex2', 'contacts_count_status0', 'contacts', 'orderMonthCount'));
    }
}
