<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Helpers\ArrHelper as Arr;
use App\Models\Setting;
use App\Models\Product;

class SiteSettingController extends BackendController
{
    public function __construct(Setting $Setting, Product $Product)
    {
        parent::__construct($Setting);
        $this->prefix = 'site';
        $this->product = $Product;
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.info" => [
                'name' => 'required',
                'phone' => 'nullable|regex:/\d$/',
                // 'fax' => 'nullable|regex:/\d$/',
                // 'phone' => 'nullable|regex:/0\d{1,2}-?\d{3,4}-?\d{3,4}/',
                'email' => 'nullable|email',
                // 'address' => 'nullable|string',
                'business_hours' => 'nullable|string',
            ],
            "backend.$this->prefix.editor" => [],
            "backend.$this->prefix.setting" => [
                'site_mail' => 'nullable|emails',
                'site_mail2' => 'nullable|emails',
                'site_mail3' => 'nullable|emails',
                'site_url' => 'required',
                'head_code' => 'nullable',
                'body_code' => 'nullable',
            ],
            "backend.$this->prefix.smtp" => [],
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.info" => Arr::insertAfter($this->getTemplateFormFields([
                'name', 'email', 'business_hours', 'copyright', 'facebook', 'line'
            ]), [
                'linkedin' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('linkedin'),
                    'name' => 'linkedin',
                    'required' => false,
                ],
            ], 'facebook'),
            "backend.$this->prefix.editor" => array_merge(
                [
                    'home_editor1' => [
                        'type' => 'text_editor',
                        'label' => $this->getValidAttrs('home_editor1'),
                        'name' => 'home_editor1',
                        'required' => false,
                    ],
                    'home_editor2' => [
                        'type' => 'text_editor',
                        'label' => $this->getValidAttrs('home_editor2'),
                        'name' => 'home_editor2',
                        'required' => false,
                    ],
                    'products' => [
                        'type' => 'multi_select_ajax',
                        'label' => $this->getValidAttrs('products'),
                        'name' => 'products',
                        'required' => false,
                        'options' => old('products') ? $this->product->whereIn('products.id', old('products'))->display()->ordered()->get() : $this->product->where('products.home_status', 1)->display()->ordered()->get(),
                        'value' => $this->product->where('products.home_status', 1)->display()->ordered()->get()->pluck('id')->toArray(),
                        'key' => ['title'],
                        'max' => 20,
                        'ajax_attrs' => [
                            'url' => route('backend.api.product'),
                            'type' => 'GET',
                        ],
                        'hint' => '可輸入商品編號、商品標題當作搜尋關鍵字',
                    ],
                ],
                $this->getTemplateFormFields([
                    'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
                ])
            ),
            "backend.$this->prefix.setting" => $this->getTemplateFormFields([
                'backend_name', 'site_mail', 'site_url', 'head_code', 'body_code'
            ]),
            "backend.$this->prefix.smtp" => $this->getTemplateFormFields([
                'smtp', 'smtp_port', 'smtp_account', 'smtp_password', 'smtp_encryption'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function handlePageExcept()
    {
        if (request()->filled('products')) {
            $this->product->whereIn('id', array_filter(request('products')))->update(['home_status' => 1]);
            $this->product->whereNotIn('id', array_filter(request('products')))->update(['home_status' => 0]);
        }
    }
}
