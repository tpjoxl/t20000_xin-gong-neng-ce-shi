<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Product;
use App\Models\Blog;
use App\Models\TwRegion;
use App\Models\Order;
use App\Models\Sku;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
        $this->limit = 100;
    }
    public function user(Request $request)
    {
        if ($request->q == '*') {
            $datas = User::ordered()->limit($this->limit)->get();
        } else {
            $datas = User::where('account', 'like', "%$request->q%")
                ->orWhere('name', 'like', "%$request->q%")
                ->orWhere('phone', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->account.' - '.$data->name.' - '.$data->phone,
                'title' => $data->account.' - '.$data->name.' - '.$data->phone,
            ];
        }
        return $result;
    }
    public function blog(Request $request)
    {
        if ($request->q == '*') {
            $datas = Blog::when(request()->filled('ignore'), function($q){
                $q->where('id', '!=', request('ignore'));
            })
            ->ordered()->limit($this->limit)->get();
        } else {
            $datas = Blog::when(request()->filled('ignore'), function($q){
                $q->where('id', '!=', request('ignore'));
            })
            ->where('title', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->title,
                'title' => $data->title,
            ];
        }
        return $result;
    }
    public function twRegion(Request $request)
    {
        if ($request->q == '*') {
            $datas = TwRegion::joinCity()
            ->ordered()->limit($this->limit)->get();
        } else {
            $datas = TwRegion::joinCity()
            ->where('tw_cities.name', 'like', "%$request->q%")
            ->orWhere('tw_regions.name', 'like', "%$request->q%")
            ->orWhere('tw_regions.code', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->city_name.' - '.$data->name.' - '.$data->code,
                'title' => $data->city_name.' - '.$data->name.' - '.$data->code,
            ];
        }
        return $result;
    }
    public function product(Request $request)
    {
        if ($request->q == '*') {
            $datas = Product::ordered()->limit($this->limit)->get();
        } else {
            $datas = Product::whereHas('skus', function($q) use ($request) {
                    $q->where('num', 'like', "%$request->q%")
                        ->orWhere('title', 'like', "%$request->q%");
                })
                ->orWhereTranslationLike('title', "%$request->q%")
                ->ordered()->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->title,
                'title' => $data->title,
            ];
        }
        return $result;
    }
    public function productSku(Request $request)
    {
        if ($request->q == '*') {
            $datas = Sku::whereNull('product_id')
            ->when(request()->filled('ignore'), function($q){
                $q->orWhere('product_id', request('ignore'));
            })
            ->ordered()->limit($this->limit)->get();
        } else {
            $datas = Sku::whereNull('product_id')
            ->when(request()->filled('ignore'), function($q){
                $q->orWhere('product_id', request('ignore'));
            })
            ->where('title', 'like', "%$request->q%")
                ->orWhere('num', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->title.' - '.$data->num,
                'title' => $data->title.' - '.$data->num,
            ];
        }
        return $result;
    }
    public function sku(Request $request)
    {
        if ($request->q == '*') {
            $datas = Sku::ordered()->limit($this->limit)->get();
        } else {
            $datas = Sku::where('title', 'like', "%$request->q%")
                ->orWhere('num', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->title.' - '.$data->num,
                'title' => $data->title.' - '.$data->num,
            ];
        }
        return $result;
    }
    public function order(Request $request)
    {
        if ($request->q == '*') {
            $datas = Order::ordered()->limit($this->limit)->get();
        } else {
            $datas = Order::where('num', 'like', "%$request->q%")
                ->limit($this->limit)->get();
        }
        $result = [];
        foreach ($datas as $data) {
            $result[] = [
                'value' => $data->id,
                'text' => $data->num.' - '.$data->name,
                'title' => $data->num.' - '.$data->name,
            ];
        }
        return $result;
    }
}
