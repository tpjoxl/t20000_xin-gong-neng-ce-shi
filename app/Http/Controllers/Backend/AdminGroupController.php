<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\AdminGroup;
use App\Models\Power;
use App\Helpers\ArrHelper as Arr;

class AdminGroupController extends BackendController
{

    public function __construct(AdminGroup $AdminGroup, Power $Power)
    {
        parent::__construct($AdminGroup);
        $this->power = $Power;
        $this->prefix = 'admin.group';
        $this->template = 'category';
        $this->valid_attrs = [
            'title' => __('validation.attributes.group_name', [], env('BACKEND_LOCALE')),
            'powers' => __('validation.attributes.power_option', [], env('BACKEND_LOCALE')),
        ];
    }
    protected function getValidRules()
    {
        $validRules = array_merge(
            $this->getTemplateValidRules(['title']),
            [
                'powers' => 'required',
            ]
        );
        $admin_group = auth()->guard('admin')->user()->group;
        if (!is_null($admin_group->parent_id)) {
            $validRules = Arr::insertAfter($validRules, [
                'parent_id' => 'required',
            ]);
        }
        return $validRules;
    }
    protected function getFormFields()
    {
        $admin_group = auth()->guard('admin')->user()->group;
        if (is_null($this->data->parent_id)) {
            if (is_null($admin_group->parent_id)) {
                $powers = $this->power->tree()->get();
            } else {
                $powers = $admin_group->parent->powers()->with([
                    'children' => function($q) {
                        $q->where('active', 1)->ordered();
                    }
                ])->where('active', 1)->tree()->get();
            }
        } else {
            $powers = $this->data->parent->powers()->with([
                'children' => function($q) {
                    $q->where('active', 1)->ordered();
                }
            ])->where('active', 1)->tree()->get();
        }
        // dd($powers);
        $keys = ['title'];
        if ($this->model->getLimitLevel() > 1) {
            $keys = Arr::prepend($keys, 'parent_id');
        }
        $fields = $this->getTemplateFormFields($keys);
        $fields = array_insert_after($fields, [
            'powers' => [
                'type' => 'include',
                'label' => $this->getValidAttrs('powers'),
                'name' => 'powers',
                'required' => true,
                'options' => $powers,
                'view' => "backend.$this->prefix._powers",
            ],
        ], 'title');
        $fields['parent_id']['key'] = 'title';

        if (!is_null($admin_group->parent_id)) {
            $parentOptions = $this->model
                ->tree($this->data->parent_id?$this->data->parent->parent_id:$admin_group->parent->parent_id)
                ->get()
                ->transform(function ($item, $key) {
                    if ($key==0) {
                        $item->title = __('backend.top_level', [], env('BACKEND_LOCALE'));
                    }
                    return $item;
                });

            $fields['parent_id']['placeholder'] = null;
            $fields['parent_id']['empty_hide'] = true;
            $fields['parent_id']['options'] = $parentOptions;
            $fields['parent_id']['default'] = $admin_group->parent_id;
        }
        if ($admin_group->level == $this->model->getLimitLevel()) {
            $fields['parent_id']['type'] = 'hidden';
        }
        return $fields;
    }
    public function index(Request $request)
    {
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $model = $this->model;
        $template = $this->template;
        $view = 'backend.' . $this->prefix . '.index';

        $admin_group = auth()->guard('admin')->user()->group;
        if (is_null($admin_group->parent_id)) {
            $datas = $this->model->tree()->get();
        } else {
            $datas = $this->model
                ->whereIn('id', $admin_group->parent->getFullChildren()->pluck('id')->toArray())
                ->tree($admin_group->parent_id)
                ->get();
        }
        $dropdowns = $this->getDropdowns();

        if (!view()->exists($view)) {
            $view = 'backend.common.category.index';
        }
        $viewDatas = compact('prefix', 'template', 'datas', 'model', 'dropdowns');

        return view($view, $viewDatas);
    }
    public function edit(Request $request)
    {
        $id = $request->id;
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $model = $this->model;
        $template = $this->template;

        $this->data = $data = $this->model->find($id);

        $admin_group = auth()->guard('admin')->user()->group;
        // dd($admin_group, $admin_group->getFullChildren()->where('id', $data->id)->count());

        if (!is_null($admin_group->parent_id) && $admin_group->parent->getFullChildren()->where('id', $data->id)->count() == 0) {
            return redirect()->route('backend.'.$this->prefix.'.index')->with('error', __('backend.no_power', [], env('BACKEND_LOCALE')));
        }
        if (is_null($data)) {
            return redirect()->route('backend.' . $this->prefix . '.index')->with('error', __('backend.data_not_found', [], env('BACKEND_LOCALE')));
        }
        $form_fields = $this->getEditFormFields();
        $view = 'backend.' . $this->prefix . '.edit';
        if (!view()->exists($view)) {
            if (!view()->exists($view)) {
                $view = 'backend.common.edit';
            }
        }
        return view($view, compact('model', 'template', 'data', 'prefix', 'rank_all', 'form_fields'));
    }
}
