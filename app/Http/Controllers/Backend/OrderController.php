<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Helpers\ArrHelper as Arr;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;

class OrderController extends BackendController
{
    public function __construct(Order $Order)
    {
        parent::__construct($Order);
        $this->prefix = 'order';
        $this->valid_attrs = [];
    }
    protected function getValidRules()
    {
        return [];
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields([
            'status', 'admin_note'
        ]);
        $fields = Arr::insertAfter($fields, [
            'payment_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('payment_status'),
                'name' => 'payment_status',
                'required' => true,
                'options' => $this->model->present()->payment_status(),
            ],
            'shipping_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('shipping_status'),
                'name' => 'shipping_status',
                'required' => true,
                'options' => $this->model->present()->shipping_status(),
            ],
            'num' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('num'),
                'name' => 'num',
                'required' => false,
            ],
            'created_at' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('created_at'),
                'name' => 'created_at',
                'required' => false,
            ],
            'user_id' => [
                'type' => 'text',
                'link' => $this->data->user_id?route('backend.user.edit', ['id'=>$this->data->user_id]):'',
                'label' => $this->getValidAttrs('user_id'),
                'name' => 'user_id',
                'required' => false,
                'value' => $this->data->user_id?'<a href="'.route('backend.user.edit', $this->data->user_id).'" target="_blank">'.$this->data->user->email.' <i class="fa fa-external-link" aria-hidden="true"></i></a>':null,
            ],
            'items_total_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('items_total_price'),
                'name' => 'items_total_price',
                'required' => false,
            ],
            'shipping_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('shipping_price'),
                'name' => 'shipping_price',
                'required' => false,
            ],
            'payment_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('payment_price'),
                'name' => 'payment_price',
                'required' => false,
            ],
            'coupon_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('coupon_price'),
                'name' => 'coupon_price',
                'required' => false,
            ],
            'bonus_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('bonus_price'),
                'name' => 'bonus_price',
                'required' => false,
            ],
            'sum_price' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('sum_price'),
                'name' => 'sum_price',
                'required' => false,
            ],
            'note' => [
                'type' => 'text',
                'show' => 'nl2br',
                'label' => $this->getValidAttrs('note'),
                'name' => 'note',
                'required' => false,
            ],
            'shipping_id' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('shipping_id'),
                'name' => 'shipping_id',
                'required' => false,
                'value' => $this->data->shipping_info?$this->data->shipping_info['title']:'',
            ],
            'payment_id' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('payment_id'),
                'name' => 'payment_id',
                'required' => false,
                'value' => $this->data->payment_info?$this->data->payment_info['title']:'',
            ],
            'coupons' => [
                'type' => 'text',
                'link' => $this->data->coupon_info?route('backend.coupon.edit', ['id'=>$this->data->coupon_info['id']]):'',
                'label' => $this->getValidAttrs('coupons'),
                'name' => 'coupons',
                'required' => false,
                'value' => $this->data->coupon_info?'<a href="'.route('backend.coupon.edit', $this->data->coupon_info['id']).'" target="_blank">'.$this->data->coupon_info['title'].'('.$this->data->coupon_info['code'].') <i class="fa fa-external-link" aria-hidden="true"></i></a>':'',
            ],
            'shippingnumber' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('shippingnumber'),
                'name' => 'shippingnumber',
                'required' => false,
            ],
        ], 'status');
        // dd($fields);
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = Arr::insertAfter($this->getTemplateSearchFields(['status', 'created_range']), [
            'payment_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('payment_status'),
                'name' => 'payment_status',
                'options' => $this->model->present()->payment_status(),
                'has_all' => true,
                'search' => 'equal',
            ],
            'shipping_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('shipping_status'),
                'name' => 'shipping_status',
                'options' => $this->model->present()->shipping_status(),
                'has_all' => true,
                'search' => 'equal',
            ],
            'num' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('num'),
                'name' => 'num',
                'search' => 'like',
            ],
            'user_id' => [
                'type' => 'select_ajax',
                'label' => $this->getValidAttrs('user_id'),
                'name' => 'user_id',
                'options' => method_exists($this->model, 'user') ? $this->model->user()->getRelated()->where('id', request()->user_id)->get() : collect(),
                'key' => ['account', 'name', 'phone'],
                'ajax_attrs' => [
                    'url' => route('backend.api.user'),
                    'type' => 'GET',
                ],
                'search' => 'equal',
                'hint' => '可輸入會員帳號、姓名、電話當作搜尋關鍵字',
            ],
            'shippingnumber' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('shippingnumber'),
                'name' => 'shippingnumber',
                'search' => 'like',
            ],
        ], 'status');
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = array_merge(
            [
                'num' => [
                    'label' => $this->getValidAttrs('num'),
                    'align' => 'left',
                    'type' => 'link',
                    'data' => $data->num,
                    'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
                ],
                'sum_price' => [
                    'label' => $this->getValidAttrs('sum_price'),
                    'align' => 'left',
                    'type' => 'text',
                    'data' => $data->sum_price,
                ],
            ],
            $this->getTemplateList($data, ['created_at', 'status', 'admin_note'])
        );
        $fields = Arr::insertAfter($fields, [
            'payment_status' => [
                'label' => $this->getValidAttrs('payment_status'),
                'align' => 'center',
                'type' => 'html',
                'options' => $this->model->present()->payment_status(),
                'data' => !is_null($data->payment_status)?$this->model->present()->payment_status($data->payment_status):'',
            ],
            'shipping_status' => [
                'label' => $this->getValidAttrs('shipping_status'),
                'align' => 'center',
                'type' => 'html',
                'options' => $this->model->present()->shipping_status(),
                'data' => !is_null($data->shipping_status)?$this->model->present()->shipping_status($data->shipping_status):'',
            ],
        ], 'status');
        return $fields;
    }
    public function export(Request $request)
    {
        // 標題列
        $attrs = $this->getValidAttrs(
            'num',
            'created_at',
            'status',
            'payment_status',
            'shipping_status',
            'items_total_price',
            'shipping_price',
            'payment_price',
            'coupon_price',
            'bonus_price',
            'sum_price',
            'name',
            'sex',
            'email',
            'phone',
            'receiver_name',
            'receiver_sex',
            'receiver_email',
            'receiver_phone',
            'receiver_address',
            'shipping_type',
            'shipping_id',
            'hope_time',
            'shippingnumber',
            'payment_id',
            'coupon_id',
            'coupon_code',
            'admin_note',
            'product_title',
            'product_num',
            'product_options',
            'product_original_price',
            'product_price',
            'product_discount',
            'product_qty',
            'product_total'
        );
        $headings = [
            array_values($attrs),
            array_keys($attrs),
        ];

        // 資料列
        $datas = $this->search()->get();
        $result = collect();
        foreach ($datas as $data) {
            foreach ($data->items as $item) {
                $result->push([
                    $data->num,
                    $data->created_at,
                    strip_tags($data->present()->status($data->status)),
                    strip_tags($data->present()->payment_status($data->payment_status)),
                    strip_tags($data->present()->shipping_status($data->shipping_status)),
                    $data->items_total_price,
                    $data->shipping_price,
                    $data->payment_price,
                    $data->coupon_price,
                    $data->bonus_price,
                    $data->sum_price,
                    $data->name,
                    $data->sex?strip_tags($data->present()->sex($data->sex)):'',
                    $data->email,
                    $data->phone,
                    $data->receiver_name,
                    $data->receiver_sex?strip_tags($data->present()->sex($data->receiver_sex)):'',
                    $data->receiver_email,
                    $data->receiver_phone,
                    $data->receiver_full_address,
                    $data->shipping_id&&$data->shipping_type?$data->shipping->present()->type($data->shipping_type):'',
                    $data->shipping_info?$data->shipping_info['title']:'',
                    $data->hope_time,
                    $data->shippingnumber,
                    $data->payment_info?$data->payment_info['title']:'',
                    $data->coupon_info?$data->coupon_info['title']:'',
                    $data->coupon_info?$data->coupon_info['code']:'',
                    $data->admin_note,
                    $item->title,
                    $item->num,
                    $item->options,
                    $item->original_price,
                    $item->price,
                    $item->discount,
                    $item->qty,
                    $item->total,
                ]);
            }
        }
        // dd($headings, $result);
        // 匯出
        return Excel::download(new Export($headings, $result), '訂單資料匯出_' . Carbon::now()->format('YmdHis') . '.xlsx');
    }
}
