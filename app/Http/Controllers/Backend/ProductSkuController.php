<?php

namespace App\Http\Controllers\Backend;

use App\Exports\Export;
use App\Models\Sku;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Imports\ProductSkuImport;

class ProductSkuController extends BackendController
{

    public function __construct(Sku $Sku)
    {
        parent::__construct($Sku);
        $this->prefix = 'product.sku';
    }
    protected function getFormFields()
    {
        $fields = [
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'required' => true,
            ],
            'num' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('num'),
                'name' => 'num',
                'required' => true,
            ],
            'description' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('description'),
                'name' => 'description',
                'required' => true,
            ],
            'price' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('price'),
                'name' => 'price',
                'required' => true,
            ],
            'price2' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('price2'),
                'name' => 'price2',
                'required' => false,
            ],
            'qty' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('qty'),
                'name' => 'qty',
                'required' => true,
            ],
            'sales' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('sales'),
                'name' => 'sales',
                'required' => false,
            ],
        ];
        return $fields;
    }
    protected function getValidRules()
    {
        return [
            'title' => ['required', Rule::unique($this->model->getTable(), 'title'), 'string', 'max:191'],
            'num' => ['required', Rule::unique($this->model->getTable()), 'string', 'max:100'],
            'description' => 'required',
            'price' => 'required|integer',
            'price2' => 'nullable|integer',
            'qty' => 'required|integer',
        ];
    }
    protected function getSearchFields()
    {
        $fields = [
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'options' => $this->model->present()->status(),
                'search' => 'equal',
            ],
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'search' => 'like',
            ],
            'num' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('num'),
                'name' => 'num',
                'search' => 'like',
            ],
        ];
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = array_merge([
            'title' => [
                'label' => $this->getValidAttrs('title'),
                'align' => 'left',
                'type' => 'link',
                'data' => $data->title,
                'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
            ],
            'num' => [
                'label' => $this->getValidAttrs('num'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->num,
            ],
            'price' => [
                'label' => $this->getValidAttrs('price'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->price,
            ],
            'price2' => [
                'label' => $this->getValidAttrs('price2'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->price2,
            ],
            'qty' => [
                'label' => $this->getValidAttrs('qty'),
                'align' => 'left',
                'type' => 'text',
                'data' => $data->qty,
            ],
        ], $this->getTemplateList($data, ['status']));
        return $fields;
    }
    public function import(Request $request)
    {
        $validator = Validator::make(
            [
                'import_file' => $request->import_file,
            ],
            [
                'import_file' => 'required|file|mimes:xlsx,xls,csv',
            ]
        );
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // dd($request->all());
        $import = new ProductSkuImport;
        Excel::import($import, $request->file('import_file'));

        return redirect()->back()->with('success', __('backend.import_success', [], env('BACKEND_LOCALE')));
    }
    public function export(Request $request)
    {
        // 標題列
        $attrs = $this->getValidAttrs(
            'title',
            'num',
            'description',
            'price',
            'price2',
            'qty',
            'sales',
            'status'
        );
        $headings = [
            array_values($attrs),
            array_keys($attrs),
        ];

        // 資料列
        $datas = $this->search()->get();
        $result = collect();
        foreach ($datas as $data) {
            $result->push([
                $data->title,
                $data->num,
                $data->description,
                $data->price,
                $data->price2,
                $data->qty,
                $data->qty2,
                $data->sales,
                strip_tags($data->present()->status($data->status)),
            ]);
        }
        // dd($headings, $result);
        $columnFormats = [
            'B' => NumberFormat::FORMAT_TEXT,
        ];
        // 匯出
        return Excel::download(new Export($headings, $result, $columnFormats), $this->model->getTable() . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
    }
}
