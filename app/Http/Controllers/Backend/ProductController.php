<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Route;
use App\Models\Product;
use App\Helpers\ArrHelper as Arr;

class ProductController extends BackendController
{

    public function __construct(Product $Product)
    {
        parent::__construct($Product);
        $this->prefix = 'product';
    }
    protected function getFormFields()
    {
        $fields = Arr::except($this->getTemplateFormFields(), ['is_top', 'url', 'pic']);
        $fields['home_status']['default'] = 0;
        $fields['home_status']['options'] = $this->model->present()->home_status();
        $fields = Arr::insertAfter($fields, [
            'skus' => [
                'type' => 'multi_select_ajax',
                'label' => $this->getValidAttrs('skus'),
                'name' => 'skus',
                'required' => true,
                'options' => old('skus') ? $this->data->skus()->getRelated()->whereIn('id', old('skus'))->get() : $this->data->skus,
                'key' => ['title', 'num'],
                'ajax_attrs' => [
                    'url' => route('backend.api.product.sku', ['ignore'=>$this->data->id]),
                    'type' => 'GET',
                ],
                'hint' => '可輸入品項名稱、編號當作搜尋關鍵字',
            ],
        ], 'slug');
        return $fields;
    }
    protected function checkListPower($data)
    {
        $fields = [];
        $auth_admin = auth()->guard('admin')->user();
        if ($auth_admin->group->checkPowerByRouteName("backend.product.image.index") && Route::has("backend.product.image.index")) {
            $fields = Arr::insertAfter($fields, [
                'image' => [
                    'label' => __('backend.img_manage', [], env('BACKEND_LOCALE')),
                    'align' => 'center',
                    'type' => 'btn',
                    'data' => '<span class="fa fa-picture-o"></span>',
                    'url' => Route::has("backend.product.image.index")?route('backend.product.image.index', ['product_id' => $data->id]):'',
                ],
            ], 'status');
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.copy") && Route::has("backend.$this->prefix.copy")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['copy']));
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.edit") && Route::has("backend.$this->prefix.edit")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['edit']));
        }
        if ($auth_admin->group->checkPowerByRouteName("backend.$this->prefix.destroy") && Route::has("backend.$this->prefix.destroy")) {
            $fields = array_merge($fields, $this->getTemplateList($data, ['delete']));
        }
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => $this->getTemplateFormFields([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::get($validRules, $page) ?: Arr::get($validRules, "backend.$this->prefix.page");
    }
}
