<?php

namespace App\Http\Controllers\Backend;

use App\Models\Contact;
use App\Helpers\ArrHelper as Arr;

class ContactController extends BackendController
{

    public function __construct(Contact $Contact)
    {
        parent::__construct($Contact);
        $this->prefix = 'contact';
        $this->valid_attrs = [
            'created_at' => __('validation.attributes.filled_at'),
        ];
    }
    protected function getValidRules()
    {
        return [];
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields([
            'status', 'created_at', 'name', 'sex', 'email', 'phone', 'message', 'admin_note'
        ]);
        $fields['created_at']['type'] = $fields['name']['type'] = $fields['sex']['type'] = $fields['phone']['type'] = $fields['email']['type'] = 'text';
        $fields['created_at']['hint'] = null;
        $fields['name']['required'] = false;
        // dd($fields);
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = $this->getTemplateSearchFields(['status', 'name', 'email', 'phone', 'created_range']);
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = array_insert_after(
                $this->getTemplateList($data, ['name', 'sex', 'email', 'phone', 'status', 'admin_note']),
                [
                    'phone2' => [
                        'label' => $this->getValidAttrs('phone2'),
                        'align' => 'left',
                        'type' => 'text',
                        'data' => $data->phone2,
                    ],
                ],
                'phone'
            );
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => $this->getTemplateFormFields([
                'text', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
}
