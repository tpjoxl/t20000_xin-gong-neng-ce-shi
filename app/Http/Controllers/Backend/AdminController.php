<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Helpers\ArrHelper as Arr;
use App\Models\Admin;

class AdminController extends BackendController
{

    public function __construct(Admin $Admin)
    {
        parent::__construct($Admin);
        $this->prefix = 'admin';
        $this->valid_attrs = [
            'group_id' => __('validation.attributes.power_group', [], env('BACKEND_LOCALE')),
            'name' => __('validation.attributes.username', [], env('BACKEND_LOCALE')),
        ];
    }
    protected function getValidRules()
    {
        return [
            'group_id' => 'required',
            'account' => 'required|max:150|unique:' . $this->model->getTable(),
            'password' => 'required|min:6|confirmed',
            'name' => 'required|max:255',
        ];
    }
    protected function getEditValidRules()
    {
        return [
            'group_id' => 'required',
            'name' => 'required|max:255',
        ];
    }
    protected function getFormFields()
    {
        $admin_group = auth()->guard('admin')->user()->group;
        $fields = $this->getTemplateFormFields([
            'status', 'group_id', 'account', 'password', 'password_confirmation', 'name'
        ]);
        if (is_null($admin_group->parent_id)) {
            $fields = Arr::insertAfter($fields, [
                'passable' => [
                    'type' => 'radio',
                    'label' => $this->getValidAttrs('passable'),
                    'name' => 'passable',
                    'required' => false,
                    'options' => $this->model->present()->yes_or_no(),
                    'default' => 0,
                ],
            ], 'name');
        }
        return $fields;
    }
    protected function getEditFormFields()
    {
        $fields = Arr::except($this->getFormFields(), ['password', 'password_confirmation']);
        $fields['account']['type'] = 'text';
        $fields['account']['required'] = false;
        return $fields;
    }
    protected function getIndexList($data)
    {
        return $this->getTemplateList($data, ['account', 'group_id', 'name', 'login_at', 'status']);
    }
    protected function getSearchFields()
    {
        $admin_group = auth()->guard('admin')->user()->group;
        $fields = $this->getTemplateSearchFields([
            'status', 'group_id', 'account', 'name', 'login_range'
        ]);
        if (!is_null($admin_group->parent_id)) {
            $parentOptions = $this->model->group()->getRelated()
                ->whereIn('id', $admin_group->parent->getFullChildren()->pluck('id')->toArray())
                ->tree($admin_group->parent_id)
                ->get();

            $fields['group_id']['placeholder'] = null;
            $fields['group_id']['options'] = $parentOptions;
            $fields['group_id']['default'] = $admin_group->parent_id;
        }
        return $fields;
    }
    public function index(Request $request)
    {
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $model = $this->model;
        $template = $this->template;
        $view = 'backend.' . $this->prefix . '.index';

        $admin_group = auth()->guard('admin')->user()->group;
        if (is_null($admin_group->parent_id)) {
            $datas = $this->search()->paginate($this->perPage);
        } else {
            $datas = $this->search()->whereIn('group_id', $admin_group->parent->getFullChildren()->pluck('id')->toArray())->paginate($this->perPage);
        }
        $search_fields = $this->getSearchFields();
        $list_datas = $this->generateListData($datas);
        $list_pager = [
            'first' => $datas->firstItem(),
            'last' => $datas->lastItem(),
            'total' => $datas->total(),
            'render' => $datas->appends(request()->input())->render()
        ];
        $dropdowns = $this->getDropdowns();
        $rank_dropdowns = $this->getRankDropdowns();

        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        $viewDatas = compact('prefix', 'template', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'dropdowns', 'rank_dropdowns');

        return view($view, $viewDatas);
    }
    public function password(Request $request)
    {
        $id = $request->id;
        $admin = auth()->guard('admin')->user();
        $this->data = $data = $this->model->find($id);
        // 表單驗證規則
        $validator = Validator::make($request->all(), [
            'old_password' => $admin->passable==0 || $admin->id == $data->id ? 'required' : 'nullable',
            'password' => 'required|min:6|confirmed',
        ], [], [
            'password' => __('validation.attributes.new_password', [], env('BACKEND_LOCALE')),
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        if (!Hash::check($request->old_password, $data->password) && auth()->guard('admin')->user()->passable==0) {
            return redirect()->back()->with('error', __('backend.old_password_error', [], env('BACKEND_LOCALE')));
        }
        $data->password = $request->password;

        if ($data->save()) {
            return redirect()->route('backend.'.$this->prefix.'.index')->with('success', __('backend.password_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->route('backend.'.$this->prefix.'.index')->with('error', __('backend.password_error', [], env('BACKEND_LOCALE')));
        }
    }
}
