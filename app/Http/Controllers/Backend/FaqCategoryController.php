<?php

namespace App\Http\Controllers\Backend;

use App\Models\FaqCategory;

class FaqCategoryController extends BackendController
{

    public function __construct(FaqCategory $FaqCategory)
    {
        parent::__construct($FaqCategory);
        $this->prefix = 'faq.category';
        $this->template = 'category';
    }
}
