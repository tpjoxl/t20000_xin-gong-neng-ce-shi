<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use App\Helpers\ArrHelper as Arr;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;

class ProductImageController extends BackendController
{
    public function __construct(ProductImage $ProductImage, Product $Product)
    {
        parent::__construct($ProductImage);
        $this->template = 'image';
        $this->prefix = 'product.image';
        $this->useParentFolder = true;
        $this->parent_prefix = 'product';
        $this->parent = $Product->find(request($Product->getForeignKey()));
        $this->parent_relation = 'images';
        $this->valid_attrs = [];
    }
}
