<?php

namespace App\Http\Controllers\Backend;

use App\Models\Shipping;
use App\Models\User;
use App\Models\TwRegion;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Validation\Rule;

class ShippingController extends BackendController
{
    public function __construct(Shipping $Shipping, User $User, TwRegion $TwRegion)
    {
        parent::__construct($Shipping);
        $this->prefix = 'shipping';
        $this->region = $TwRegion;
        $this->user = $User;
        $this->valid_msgs = [
          'required_if' => __('validation.required'),
        ];
    }
    protected function getFormFields()
    {
        $fields = array_merge(
          $this->getTemplateFormFields(['status', 'title', 'text']),
          [
            'type' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('type'),
                'name' => 'type',
                'required' => true,
                'options' => $this->model->present()->type(),
                'default' => 1,
            ],
            'user_type' => [
                'type' => 'checkbox',
                'label' => $this->getValidAttrs('user_type'),
                'name' => 'user_type',
                'required' => true,
                'options' => $this->model->present()->user_type(),
                'default' => array_keys($this->model->present()->user_type()),
            ],
            'price' => [
                'type' => 'shipping_price',
                'label' => $this->getValidAttrs('price'),
                'name' => ['price', 'limit_order_price', 'limit_price'],
                'required' => true,
            ],
            'payments' => [
                'type' => 'multi_select',
                'label' => $this->getValidAttrs('payments'),
                'name' => 'payments',
                'required' => true,
                'actions' => true,
                'options' => method_exists($this->model, 'payments') ? $this->model->payments()->getRelated()->ordered()->get() : [],
            ],
            'url' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('url'),
                'name' => 'url',
                'required' => false,
            ],
          ]
        );
        $fields['text']['type'] = 'text_input';
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = array_merge(
          $this->getTemplateSearchFields(['status', 'title']),
          [
            'user_type' => [
                'type' => 'checkbox',
                'label' => $this->getValidAttrs('user_type'),
                'name' => 'user_type',
                'required' => false,
                'options' => $this->model->present()->user_type(),
                'search' => 'like',
            ],
          ]
        );
        return $fields;
    }
    protected function getValidRules()
    {
        return [
            'title' => ['required', Rule::unique($this->model->getTable(), 'title'), 'string', 'max:191'],
            'price' => 'required|integer',
            'limit_order_price' => 'nullable|integer',
            'limit_price' => 'nullable|integer',
            'payments' => 'required',
        ];
    }
    protected function getIndexList($data)
    {
        $fields = Arr::insertAfter($this->getTemplateList($data, ['title', 'status']), [
            'price' => [
              'label' => $this->getValidAttrs('price'),
              'align' => 'left',
              'type' => 'text',
              'data' => "{$data->price}元".($data->limit_order_price?"，滿 {$data->limit_order_price}".($data->limit_price==0?"免運":"運費為 {$data->limit_price} 元"):""),
            ],
            'user_type' => [
                'label' => $this->getValidAttrs('user_type'),
                'align' => 'left',
                'type' => 'html',
                'options' => $this->model->present()->user_type(),
                'data' => !is_null($data->user_type)?implode(', ', Arr::only($this->model->present()->user_type(), $data->user_type)):'',
            ],
            'payments' => [
              'label' => $this->getValidAttrs('payments'),
              'align' => 'left',
              'type' => 'text',
              'data' => $data->payments->implode('title', ', '),
            ],
            'type' => [
                'label' => $this->getValidAttrs('type'),
                'align' => 'center',
                'type' => 'html',
                'options' => $this->model->present()->type(),
                'data' => !is_null($data->type)?$this->model->present()->type($data->type):'',
            ],
          ], 'title');
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => [
              'remote_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('remote_status'),
                'name' => 'remote_status',
                'required' => true,
                'options' => $this->model->present()->yes_or_no(),
                'hint' => '若選擇「是」請設定以下「外島偏鄉運費」',
              ],
              'remote_price' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('remote_price'),
                'name' => 'remote_price',
                'required' => false,
                'hint' => '外島偏鄉 總運費增加 N 元',
              ],
              'remotes' => [
                'type' => 'multi_select_ajax',
                'label' => $this->getValidAttrs('remotes'),
                'name' => 'remotes',
                'required' => false,
                'options' => old('remotes') ? $this->region->joinCity()->whereIn('tw_regions.id', old('remotes'))->get() : $this->region->joinCity()->where('tw_regions.remoted', 1)->get(),
                'value' => $this->region->joinCity()->where('tw_regions.remoted', 1)->get()->pluck('id')->toArray(),
                'key' => ['city_name', 'name', 'code'],
                'ajax_attrs' => [
                    'url' => route('backend.api.tw_region'),
                    'type' => 'GET',
                ],
                'hint' => '可輸入縣市名稱、鄉鎮名稱、郵遞區號當作搜尋關鍵字',
              ],
              'oversea_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('oversea_status'),
                'name' => 'oversea_status',
                'required' => true,
                'options' => $this->model->present()->yes_or_no(),
              ],
            ],
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => [
                'remote_status' => 'required|integer',
                'remote_price' => 'nullable|required_if:remote_status,1|integer',
                'remotes' => 'nullable|required_if:remote_status,1|array',
                'oversea_status' => 'required|integer',
            ],
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
    protected function handlePageExcept()
    {
      if (request()->filled('remotes')) {
        $this->region->whereIn('id', array_filter(request('remotes')))->update(['remoted' => 1]);
        $this->region->whereNotIn('id', array_filter(request('remotes')))->update(['remoted' => 0]);
      }
    }
}
