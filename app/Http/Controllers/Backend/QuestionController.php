<?php

namespace App\Http\Controllers\Backend;

use App\Models\Question;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;

class QuestionController extends BackendController
{

    public function __construct(Question $Question)
    {
        parent::__construct($Question);
        $this->prefix = 'question';
        
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields(['is_top', 'title', 'text']);
        $fields['title']['label']='問題';
        $fields['text']['label']='回答';

        if (method_exists($this->model, 'categories')) {
            $fields = Arr::insertAfter($fields, $this->getTemplateFormFields(['categories']));
        }
        return $fields;
    }
    protected function getEditFormFields()
    {
        $fields = $this->getTemplateFormFields(['admin_account', 'updated_at','is_top', 'title', 'text']);
        $fields['admin_account']['label']='發佈帳號';
        $fields['updated_at']['label']='修改時間';
        if (method_exists($this->model, 'categories')) {
            $fields = Arr::insertAfter($fields, $this->getTemplateFormFields(['categories']));
        }
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = $this->getTemplateList($data, ['title','categories','admin_account','created_at','updated_at','is_top']);
        $fields['admin_account']['label']='發佈帳號';
        $fields['updated_at']['label']='修改時間';
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = $this->getTemplateSearchFields(['categories','title','admin_account','created_range','updated_range']);
        $fields['admin_account']['label']='發佈帳號';
        $fields['updated_range']['label']='修改時間區間';
        return $fields;
    }
    public function update(Request $request)
    {
        $id = $request->id;
        $model = $this->model;
        $udata = $model->where('id',$id)->first();
        $user=auth()->guard('admin')->user()->account;
        $requestData = $request->all();
        if (!is_null($this->parent)) {
            $this->data = $data = $this->parent->{$this->parent_relation}()->find($id);
        } else {
            $this->data = $data = $this->model->find($id);
        }

        $this->data->updated_at = Carbon::now();

        if($udata->admin_account == '37dadmin'){
            if($user == '37dadmin'){
                $validator = $this->valid($requestData, $this->getEditValidRules(), $this->valid_msgs, $id);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }
                $result1 = $data->fillAndSave($requestData);

                if ($result1) {
                    if ($this->rank_all) {
                        return redirect()->back()->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                    } else {
                        $rank_dropdowns = $this->getRankDropdowns();
                        $rank_key = head(array_keys($rank_dropdowns));
                        $rank_val = is_array(request($rank_key))?head(request($rank_key)):request($rank_key);
                        return redirect()->route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, ['id' => $id, $rank_key => $rank_val]))->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                    }
                } else {
                    return redirect()->back()->with('error', __('backend.save_error', [], env('BACKEND_LOCALE')));
                }
            }
            else{
                return redirect()->back()->with('error', __('backend.account_have_no_permission', [], env('BACKEND_LOCALE')));
            }
        }
        else{
            $validator = $this->valid($requestData, $this->getEditValidRules(), $this->valid_msgs, $id);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }
            $result1 = $data->fillAndSave($requestData);

            if ($result1) {
                if ($this->rank_all) {
                    return redirect()->back()->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                } else {
                    $rank_dropdowns = $this->getRankDropdowns();
                    $rank_key = head(array_keys($rank_dropdowns));
                    $rank_val = is_array(request($rank_key))?head(request($rank_key)):request($rank_key);
                    return redirect()->route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, ['id' => $id, $rank_key => $rank_val]))->with('success', __('backend.save_success', [], env('BACKEND_LOCALE')));
                }
            } else {
                return redirect()->back()->with('error', __('backend.save_error', [], env('BACKEND_LOCALE')));
            }
        }
    }
     public function destroy(Request $request)
    {
        $model = $this->model;
        $ids = explode(",", $request->id);
        $udata = $model->where('id',$ids)->first();
        $user=auth()->guard('admin')->user()->account;

        if($udata->admin_account == '37dadmin'){
            if($user == '37dadmin'){
                if (!is_null($this->parent)) {
                    $result = $this->parent->{$this->parent_relation}()->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
                } else {
                    $result = $this->model->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
                }
            }
            else{
               return redirect()->back()->with('error', __('backend.account_have_no_permission', [], env('BACKEND_LOCALE')));
            }
        }
        else{
           if (!is_null($this->parent)) {
                $result = $this->parent->{$this->parent_relation}()->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
            } else {
                $result = $this->model->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
            } 
        }

        if ($result) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('success', __('backend.delete_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->back()->with('error', __('backend.delete_error', [], env('BACKEND_LOCALE')));
        }
    }
}
