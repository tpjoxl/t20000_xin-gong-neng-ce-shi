<?php

namespace App\Http\Controllers\Backend;

use App\Exports\Export;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Sku;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Lang;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin', 'auth.admin.power']);

        $this->prefix  = 'report';
        $this->perPage = 10;
    }
    /**
     * 產品 指定日期區間報表
     */
    public function productDate(Request $request, Order $orders)
    {
        $prefix        = $this->prefix;
        $rank_all      = true;
        $search_fields = [
            'status'        => [
                'label'   => '訂單狀態',
                'type'    => 'radio',
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'created_range' => [
                'label'          => '訂單成立時間區間',
                'type'           => 'date_range',
                'name'           => ['created_at1', 'created_at2'],
                'placeholder'    => [Lang::get('validation.attributes.date_on', [], env('BACKEND_LOCALE')), Lang::get('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search'         => 'range',
                'compare_fields' => ['created_at', 'created_at']
            ],
        ];
        $query = OrderItem::leftJoin('orders', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('skus', 'skus.id', '=', 'order_items.sku_id')
            ->selectRaw('order_items.num, order_items.type, order_items.title, order_items.options, skus.belong, sum(order_items.qty) sum_qty, sum(order_items.total) sum_total')
            ->groupBy('order_items.num', 'order_items.type', 'order_items.title', 'order_items.options', 'skus.belong', 'order_items.options');
        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('created_at1')) {
            $query->where('orders.created_at', '>=', $request->input('created_at1'));
        }
        if ($request->filled('created_at2')) {
            $query->where('orders.created_at', '<=', (new Carbon($request->input('created_at2')))->addDay());
        }
        $datas = $query->paginate($this->perPage);
        // dd($datas);
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'num'       => [
                    'align' => 'left',
                    'label' => __('validation.order.num', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->num,
                ],
                'title'     => [
                    'align' => 'left',
                    'label' => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->title,
                ],
                'options'   => [
                    'align' => 'left',
                    'label' => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                    'type'  => 'html',
                    'data'  => $data->options,
                ],
                'sum_qty'   => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_qty,
                ],
                'sum_total' => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_total,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];

        if (Route::currentRouteName() == 'backend.report.product.date.export') {
            // 標題列
            $attrs = [
                'num'       => __('validation.order.num', [], env('BACKEND_LOCALE')),
                'title'     => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                'options'   => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                'sum_qty'   => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                'sum_total' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->num,
                    $data->title,
                    $data->options,
                    $data->sum_qty,
                    $data->sum_total,
                ]);
            }
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), '產品_指定日期區間報表_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 產品 日報表
     */
    public function productDay(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;
        $query    = OrderItem::leftJoin('orders', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('skus', 'skus.id', '=', 'order_items.sku_id')
            ->selectRaw('order_items.num, order_items.type, order_items.title, order_items.options, skus.belong, sum(order_items.qty) sum_qty, sum(order_items.total) sum_total, day(orders.created_at) day, month(orders.created_at) month, year(orders.created_at) year')
            ->groupBy(DB::raw('order_items.num, order_items.type, order_items.title, order_items.options, skus.belong, day(orders.created_at), month(orders.created_at), year(orders.created_at)'))
            ->orderByRaw('min(orders.created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(orders.created_at) = ' . $request->year);
        }
        if ($request->filled('month')) {
            $query->whereRaw('month(orders.created_at) = ' . $request->month);
        }
        $datas = $query->paginate($this->perPage);
        // dd($datas);
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'year'      => [
                    'align' => 'left',
                    'label' => __('validation.report.year', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->year,
                ],
                'month'     => [
                    'align' => 'left',
                    'label' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->month,
                ],
                'day'       => [
                    'align' => 'left',
                    'label' => __('validation.report.day', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->day,
                ],
                'num'       => [
                    'align' => 'left',
                    'label' => __('validation.order.num', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->num,
                ],
                'title'     => [
                    'align' => 'left',
                    'label' => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->title,
                ],
                'options'   => [
                    'align' => 'left',
                    'label' => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                    'type'  => 'html',
                    'data'  => $data->options,
                ],
                'sum_qty'   => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_qty,
                ],
                'sum_total' => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_total,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];

        $search_monthes = [];
        for ($i = 1; $i <= 12; $i++) {
            $search_monthes[$i] = $i;
        }
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
            ],
            'month'  => [
                'type'    => 'radio',
                'label'   => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'name'    => 'month',
                'options' => $search_monthes,
                'search'  => 'equal',
            ],
        ];

        if (Route::currentRouteName() == 'backend.report.product.day.export') {
            // 標題列
            $attrs = [
                'year'      => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'month'     => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'day'       => __('validation.report.day', [], env('BACKEND_LOCALE')),
                'num'       => __('validation.order.num', [], env('BACKEND_LOCALE')),
                'title'     => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                'options'   => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                'sum_qty'   => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                'sum_total' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->year,
                    $data->month,
                    $data->day,
                    $data->num,
                    $data->title,
                    $data->options,
                    $data->sum_qty,
                    $data->sum_total,
                ]);
            }
            $fileName = '產品_日報表';
            $fileName .= $request->filled('year') ? '_' . $request->year . '年' : '';
            $fileName .= $request->filled('month') ? '_' . str_pad($request->month, 2, '0', STR_PAD_LEFT) . '月' : '';
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), $fileName . '_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 產品 月報表
     */
    public function productMonth(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;
        $query    = OrderItem::leftJoin('orders', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('skus', 'skus.id', '=', 'order_items.sku_id')
            ->selectRaw('order_items.num, order_items.type, order_items.title, order_items.options, skus.belong, sum(order_items.qty) sum_qty, sum(order_items.total) sum_total, month(orders.created_at) month, year(orders.created_at) year')
            ->groupBy(DB::raw('order_items.num, order_items.type, order_items.title, order_items.options, skus.belong, month(orders.created_at), year(orders.created_at)'))
            ->orderByRaw('min(orders.created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(orders.created_at) = ' . $request->year);
        }
        $datas = $query->paginate($this->perPage);
        // dd($datas);
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'year'      => [
                    'align' => 'left',
                    'label' => __('validation.report.year', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->year,
                ],
                'month'     => [
                    'align' => 'left',
                    'label' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->month,
                ],
                'num'       => [
                    'align' => 'left',
                    'label' => __('validation.order.num', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->num,
                ],
                'title'     => [
                    'align' => 'left',
                    'label' => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->title,
                ],
                'options'   => [
                    'align' => 'left',
                    'label' => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                    'type'  => 'html',
                    'data'  => $data->options,
                ],
                'sum_qty'   => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_qty,
                ],
                'sum_total' => [
                    'align' => 'left',
                    'label' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum_total,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];
        $search_monthes = [];
        for ($i = 1; $i <= 12; $i++) {
            $search_monthes[$i] = $i;
        }
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
            ],
        ];

        if (Route::currentRouteName() == 'backend.report.product.month.export') {
            // 標題列
            $attrs = [
                'year'      => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'month'     => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'num'       => __('validation.order.num', [], env('BACKEND_LOCALE')),
                'title'     => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
                'options'   => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
                'sum_qty'   => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
                'sum_total' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->year,
                    $data->month,
                    $data->num,
                    $data->title,
                    $data->options,
                    $data->sum_qty,
                    $data->sum_total,
                ]);
            }
            $fileName = '產品_月報表';
            $fileName .= $request->filled('year') ? '_' . $request->year . '年' : '';
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), $fileName . '_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 產品 銷售圖表
     */
    public function productChart(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;

        $query = OrderItem::leftJoin('orders', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('skus', 'skus.id', '=', 'order_items.sku_id')
            ->selectRaw('order_items.num, order_items.type, order_items.title, sum(order_items.qty) sum_qty, sum(order_items.total) sum_total, day(orders.created_at) day, month(orders.created_at) month, year(orders.created_at) year')
            ->groupBy(DB::raw('order_items.num, order_items.type, order_items.title, day(orders.created_at), month(orders.created_at), year(orders.created_at)'))
            ->orderByRaw('min(orders.created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('sku_id')) {
            $query->where('skus.id', $request->sku_id);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(orders.created_at) = ' . $request->year);
        } else {
            $query->whereRaw('year(orders.created_at) = ' . Carbon::now()->year);
        }
        if ($request->filled('month')) {
            $query->whereRaw('month(orders.created_at) = ' . $request->month);
        } else {
            $query->whereRaw('month(orders.created_at) = ' . Carbon::now()->month);
        }

        $datas = $query->get();
        // dd($datas);
        $monthLastDay = Carbon::now()
            ->year($request->filled('year') ? $request->year : Carbon::now()->year)
            ->month($request->filled('month') ? $request->month : Carbon::now()->month)
            ->lastOfMonth()
            ->day;

        for ($i = 1; $i <= $monthLastDay; $i++) {
            $days[]      = ($request->filled('month') ? $request->month : Carbon::now()->month) . '/' . $i;
            $daysQty[]   = $datas->keyBy('day')->has($i) ? (int) $datas->keyBy('day')[$i]['sum_qty'] : 0;
            $daysTotal[] = $datas->keyBy('day')->has($i) ? (int) $datas->keyBy('day')[$i]['sum_total'] : 0;
        }
        // dd($days, $daysQty, $daysTotal);

        $valid_attrs = [
            'year'      => __('validation.report.year', [], env('BACKEND_LOCALE')),
            'month'     => __('validation.report.month', [], env('BACKEND_LOCALE')),
            'day'       => __('validation.report.day', [], env('BACKEND_LOCALE')),
            'num'       => __('validation.order.num', [], env('BACKEND_LOCALE')),
            'title'     => __('validation.order.product_title', [], env('BACKEND_LOCALE')),
            'options'   => __('validation.order.product_options', [], env('BACKEND_LOCALE')),
            'sum_qty'   => __('validation.report.sum_qty', [], env('BACKEND_LOCALE')),
            'sum_total' => __('validation.report.sum_total', [], env('BACKEND_LOCALE')),
        ];
        $search_monthes = [];
        for ($i = 1; $i <= 12; $i++) {
            $search_monthes[$i] = $i;
        }
        $skus = Sku::leftJoin('products', 'products.id', '=', 'skus.product_id')
            ->leftJoin('product_translations', 'products.id', '=', 'product_translations.product_id')
            ->where('product_translations.locale', app()->getLocale())
            ->selectRaw('skus.id, CONCAT(skus.num, " ", product_translations.title) as title')
            ->get();
        // dd($skus->pluck('title', 'id')->toArray());
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
                'has_all' => false,
                'default' => request()->filled('year') ? request()->year : Carbon::now()->year,
            ],
            'month'  => [
                'type'    => 'radio',
                'label'   => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'name'    => 'month',
                'options' => $search_monthes,
                'search'  => 'equal',
                'has_all' => false,
                'default' => request()->filled('month') ? request()->month : Carbon::now()->month,
            ],
            'sku_id' => [
                'type'       => 'select_ajax',
                'label'      => $this->getValidAttrs('sku_id'),
                'name'       => 'sku_id',
                'options'    => request()->filled('sku_id') ? $this->sku->where('id', request()->sku_id)->get() : collect(),
                'key'        => ['title', 'num'],
                'ajax_attrs' => [
                    'url'  => route('backend.api.sku'),
                    'type' => 'GET',
                ],
                'search'     => 'equal',
                'hint'       => '可輸入品項名稱、編號當作搜尋關鍵字',
            ],
        ];

        $view = 'backend.' . $this->prefix . '.chart';
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'days', 'daysQty', 'daysTotal', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 訂單 指定日期區間報表
     */
    public function ordersDate(Request $request, Order $orders)
    {
        $prefix        = $this->prefix;
        $rank_all      = true;
        $search_fields = [
            'status'        => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'created_range' => [
                'type'           => 'date_range',
                'label'          => __('validation.report.order_created_range', [], env('BACKEND_LOCALE')),
                'name'           => ['created_at1', 'created_at2'],
                'placeholder'    => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'search'         => 'range',
                'compare_fields' => ['created_at', 'created_at']
            ],
        ];
        $query = Order::selectRaw('count(orders.id) count, sum(orders.sum_price) sum');
        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('created_at1')) {
            $query->where('orders.created_at', '>=', $request->input('created_at1'));
        }
        if ($request->filled('created_at2')) {
            $query->where('orders.created_at', '<=', (new Carbon($request->input('created_at2')))->addDay());
        }
        $datas = $query->paginate($this->perPage);
        // dd($datas);
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'count' => [
                    'align' => 'left',
                    'label' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->count,
                ],
                'sum'   => [
                    'align' => 'left',
                    'label' => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];

        if (Route::currentRouteName() == 'backend.report.orders.date.export') {
            // 標題列
            $attrs = [
                'count' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                'sum'   => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->count,
                    $data->sum,
                ]);
            }
            $fileName = '訂單_指定日期區間報表';
            $fileName .= $request->filled('year') ? '_' . $request->year . '年' : '';
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), $fileName . '_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 訂單 日報表
     */
    public function ordersDay(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;
        $query    = Order::selectRaw('day(created_at) day, month(created_at) month, year(created_at) year, count(*) count, sum(sum_price) sum')
            ->groupBy(DB::raw('day(created_at), month(created_at), year(created_at)'))
            ->orderByRaw('min(created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(created_at) = ' . $request->year);
        }
        if ($request->filled('month')) {
            $query->whereRaw('month(created_at) = ' . $request->month);
        }
        $datas = $query->paginate($this->perPage);
        // $datas = $query->get();
        // dd($query->get()->groupBy('year')->keys()->toArray());
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'year'  => [
                    'align' => 'left',
                    'label' => __('validation.report.year', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->year,
                ],
                'month' => [
                    'align' => 'left',
                    'label' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->month,
                ],
                'day'   => [
                    'align' => 'left',
                    'label' => __('validation.report.day', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->day,
                ],
                'count' => [
                    'align' => 'left',
                    'label' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->count,
                ],
                'sum'   => [
                    'align' => 'left',
                    'label' => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];
        $search_monthes = [];
        for ($i = 1; $i <= 12; $i++) {
            $search_monthes[$i] = $i;
        }
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
            ],
            'month'  => [
                'type'    => 'radio',
                'label'   => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'name'    => 'month',
                'options' => $search_monthes,
                'search'  => 'equal',
            ],
        ];

        if (Route::currentRouteName() == 'backend.report.orders.day.export') {
            // 標題列
            $attrs = [
                'year'  => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'month' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'day'   => __('validation.report.day', [], env('BACKEND_LOCALE')),
                'count' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                'sum'   => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->year,
                    $data->month,
                    $data->day,
                    $data->count,
                    $data->sum,
                ]);
            }
            $fileName = '訂單_日報表';
            $fileName .= $request->filled('year') ? '_' . $request->year . '年' : '';
            $fileName .= $request->filled('month') ? '_' . str_pad($request->month, 2, '0', STR_PAD_LEFT) . '月' : '';
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), $fileName . '_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 訂單 月報表
     */
    public function ordersMonth(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;
        $query    = Order::selectRaw('month(created_at) month, year(created_at) year, count(*) count, sum(sum_price) sum')
            ->groupBy(DB::raw('month(created_at), year(created_at)'))
            ->orderByRaw('min(created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(created_at) = ' . $request->year);
        }
        $datas = $query->paginate($this->perPage);
        // $datas = $query->get();
        // dd($query->get()->groupBy('year')->keys()->toArray());
        $list_datas = [];
        foreach ($datas as $key => $data) {
            $list_datas[$key] = [
                'year'  => [
                    'align' => 'left',
                    'label' => __('validation.report.year', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->year,
                ],
                'month' => [
                    'align' => 'left',
                    'label' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->month,
                ],
                'count' => [
                    'align' => 'left',
                    'label' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->count,
                ],
                'sum'   => [
                    'align' => 'left',
                    'label' => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
                    'type'  => 'text',
                    'data'  => $data->sum,
                ],
            ];
        }
        $list_pager = [
            'first'  => $datas->firstItem(),
            'last'   => $datas->lastItem(),
            'total'  => $datas->total(),
            'render' => $datas->appends(request()->input())->render(),
        ];
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
            ],
        ];

        if (Route::currentRouteName() == 'backend.report.orders.month.export') {
            // 標題列
            $attrs = [
                'year'  => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'month' => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'count' => __('validation.report.order_count', [], env('BACKEND_LOCALE')),
                'sum'   => __('validation.report.order_sum', [], env('BACKEND_LOCALE')),
            ];
            $headings = [
                array_values($attrs),
                array_keys($attrs),
            ];

            // 資料列
            $type   = $request->type;
            $datas  = $query->get();
            $result = collect();
            foreach ($datas as $data) {
                $result->push([
                    $data->year,
                    $data->month,
                    $data->count,
                    $data->sum,
                ]);
            }
            $fileName = '訂單_月報表';
            $fileName .= $request->filled('year') ? '_' . $request->year . '年' : '';
            $sheetName = '全部';
            if ($request->filled('status')) {
                $sheetName = strip_tags($orders->present()->status($request->status));
            }
            // dd($headings, $result);
            return Excel::download(new Export($headings, $result), $fileName . '_' . $sheetName . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
        }
        $view = 'backend.' . $this->prefix . '.index';
        if (!view()->exists($view)) {
            $view = 'backend.common.index';
        }
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
    /**
     * 訂單 銷售圖表
     */
    public function ordersChart(Request $request, Order $orders)
    {
        $prefix   = $this->prefix;
        $rank_all = true;

        $query = Order::selectRaw('day(created_at) day, month(created_at) month, year(created_at) year, count(*) sum_qty, sum(sum_price) sum_total')
            ->groupBy(DB::raw('day(created_at), month(created_at), year(created_at)'))
            ->orderByRaw('min(created_at) desc');

        $search_years = $query->get()->groupBy('year')->keys()->toArray();
        $search_years = array_combine($search_years, $search_years);

        if ($request->filled('status')) {
            $query->where('orders.status', $request->status);
        }
        if ($request->filled('year')) {
            $query->whereRaw('year(orders.created_at) = ' . $request->year);
        } else {
            $query->whereRaw('year(orders.created_at) = ' . Carbon::now()->year);
        }
        if ($request->filled('month')) {
            $query->whereRaw('month(orders.created_at) = ' . $request->month);
        } else {
            $query->whereRaw('month(orders.created_at) = ' . Carbon::now()->month);
        }

        $datas = $query->get();
        // dd($datas);
        $monthLastDay = Carbon::now()
            ->year($request->filled('year') ? $request->year : Carbon::now()->year)
            ->month($request->filled('month') ? $request->month : Carbon::now()->month)
            ->lastOfMonth()
            ->day;

        for ($i = 1; $i <= $monthLastDay; $i++) {
            $days[]      = ($request->filled('month') ? $request->month : Carbon::now()->month) . '/' . $i;
            $daysQty[]   = $datas->keyBy('day')->has($i) ? (int) $datas->keyBy('day')[$i]['sum_qty'] : 0;
            $daysTotal[] = $datas->keyBy('day')->has($i) ? (int) $datas->keyBy('day')[$i]['sum_total'] : 0;
        }
        // dd($days, $daysQty, $daysTotal);

        $valid_attrs = [
            'year'      => __('validation.report.year', [], env('BACKEND_LOCALE')),
            'month'     => __('validation.report.month', [], env('BACKEND_LOCALE')),
            'day'       => __('validation.report.day', [], env('BACKEND_LOCALE')),
            'sum_qty'   => __('validation.report.order_sum_qty', [], env('BACKEND_LOCALE')),
            'sum_total' => __('validation.report.order_sum_total', [], env('BACKEND_LOCALE')),
        ];
        $search_monthes = [];
        for ($i = 1; $i <= 12; $i++) {
            $search_monthes[$i] = $i;
        }
        // dd($skus->pluck('title', 'id')->toArray());
        $search_fields = [
            'status' => [
                'type'    => 'radio',
                'label'   => __('validation.report.order_status', [], env('BACKEND_LOCALE')),
                'name'    => 'status',
                'options' => $orders->present()->status(),
                'search'  => 'equal',
            ],
            'year'   => [
                'type'    => 'radio',
                'label'   => __('validation.report.year', [], env('BACKEND_LOCALE')),
                'name'    => 'year',
                'options' => $search_years,
                'search'  => 'equal',
                'has_all' => false,
                'default' => request()->filled('year') ? request()->year : Carbon::now()->year,
            ],
            'month'  => [
                'type'    => 'radio',
                'label'   => __('validation.report.month', [], env('BACKEND_LOCALE')),
                'name'    => 'month',
                'options' => $search_monthes,
                'search'  => 'equal',
                'has_all' => false,
                'default' => request()->filled('month') ? request()->month : Carbon::now()->month,
            ],
        ];

        $view = 'backend.' . $this->prefix . '.chart';
        return view($view, compact('prefix', 'rank_all', 'model', 'categories', 'days', 'daysQty', 'daysTotal', 'search_fields', 'list_fields', 'list_pager', 'list_datas', 'valid_attrs'));
    }
}
