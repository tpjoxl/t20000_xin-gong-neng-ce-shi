<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Helpers\ArrHelper as Arr;
use App\Models\Setting;

class MailController extends BackendController
{
    public function __construct(Setting $Setting)
    {
        parent::__construct($Setting);
        $this->prefix = 'mail';
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.user.register" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.register" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.forgot" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.forgot" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.coupon" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.ordercreate" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.ordercreate" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.orderreject" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.orderaccept" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.orderpaid" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.reminder" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.reminder" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.user.ban" => [
                'title' => 'required',
            ],
            "backend.$this->prefix.senior.ban" => [
                'title' => 'required',
            ],
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
    protected function getTemplateFormFields($keys = [])
    {
        $templateFormFields = [
            'title' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('title'),
                'name' => 'title',
                'required' => true,
                'hint' => 'Ex: ##site_name## - XXX通知信<br>→##site_name## 將替換為網站名稱',
            ],
            'text' => [
                'type' => 'text_editor_simple',
                'label' => $this->getValidAttrs('text'),
                'name' => 'text',
                'required' => false,
                'hint' => '##user_name## 將替換為使用者名稱',
            ],
        ];

        if (count($keys)) {
            $fields = array_merge(
                array_flip((array) $keys),
                Arr::only($templateFormFields, $keys)
            );
            return $fields;
        } else {
            return $templateFormFields;
        }
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.user.register" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.register" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.forgot" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.forgot" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.coupon" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.ordercreate" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.ordercreate" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.orderreject" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.orderaccept" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.orderpaid" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.reminder" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.reminder" => $this->getTemplateFormFields(),
            "backend.$this->prefix.user.ban" => $this->getTemplateFormFields(),
            "backend.$this->prefix.senior.ban" => $this->getTemplateFormFields(),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
}
