<?php

namespace App\Http\Controllers\Backend;

use App\Models\QuestionCategory;
use Illuminate\Http\Request;

class QuestionCategoryController extends BackendController
{

    public function __construct(QuestionCategory $QuestionCategory)
    {
        parent::__construct($QuestionCategory);
        $this->prefix = 'question.category';
        $this->template = 'category';
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields(['title']);
        $fields['title']['label']='分類名稱';
        return $fields;
    }
     public function edit(Request $request)
    {
        $id = $request->id;
        $prefix = $this->prefix;
        $rank_all = $this->rank_all;
        $parent = $this->parent;
        $parent_prefix = $this->parent_prefix;
        $model = $this->model;
        $template = $this->template;
        $udata = $model->where('id',$id)->first();
        $user=auth()->guard('admin')->user()->account;

        if (!is_null($this->parent)) {
            $this->data = $data = $this->parent->{$this->parent_relation}()->find($id);
        } else {
            $this->data = $data = $this->model->find($id);
        }
        if (is_null($data)) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('error', __('backend.data_not_found', [], env('BACKEND_LOCALE')));
        }

        if($udata->admin_account == '37dadmin'){
            if($user == '37dadmin'){
                $form_fields = $this->getEditFormFields();
                $view = 'backend.' . $this->prefix . '.edit';
                if (!view()->exists($view)) {
                    if (!view()->exists($view)) {
                        $view = 'backend.common.edit';
                    }
                }
            }
            else{
                return redirect()->back()->with('error', __('backend.account_have_no_permission', [], env('BACKEND_LOCALE')));
            }
        }
        else{
            $form_fields = $this->getEditFormFields();
            $view = 'backend.' . $this->prefix . '.edit';
            if (!view()->exists($view)) {
                if (!view()->exists($view)) {
                    $view = 'backend.common.edit';
                }
            }
        }
        return view($view, compact('model', 'template', 'data', 'prefix', 'rank_all', 'form_fields', 'parent', 'parent_prefix'));
    }
     public function destroy(Request $request)
    {
        $model = $this->model;
        $ids = explode(",", $request->id);
        $udata = $model->where('id',$ids)->first();
        $user=auth()->guard('admin')->user()->account;

        if($udata->admin_account == '37dadmin'){
            if($user == '37dadmin'){
                if (!is_null($this->parent)) {
                    $result = $this->parent->{$this->parent_relation}()->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
                } else {
                    $result = $this->model->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
                }
            }
            else{
               return redirect()->back()->with('error', __('backend.account_have_no_permission', [], env('BACKEND_LOCALE')));
            }
        }
        else{
           if (!is_null($this->parent)) {
                $result = $this->parent->{$this->parent_relation}()->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
            } else {
                $result = $this->model->where('deletable', 1)->whereIn('id', $ids)->get()->each->delete();
            } 
        }
        if ($result) {
            return redirect()->route('backend.' . $this->prefix . '.index', request()->route()->parameters)->with('success', __('backend.delete_success', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->back()->with('error', __('backend.delete_error', [], env('BACKEND_LOCALE')));
        }
    }
}
