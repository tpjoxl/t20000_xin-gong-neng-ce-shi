<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ArrHelper as Arr;
use Illuminate\Validation\Rule;
use App\Models\Power;

class PowerController extends BackendController
{

    public function __construct(Power $Power)
    {
        parent::__construct($Power);
        $this->prefix = 'power';
        $this->template = 'category';
    }
    protected function getDropdowns()
    {
        $dropdowns = [
            'active' => [
                'label' => $this->getValidAttrs('active'),
                'name' => 'active',
                'options' => $this->model->present()->active(),
            ],
        ];
        return $dropdowns;
    }
    protected function getValidRules()
    {
        return [
            'title' => ['required', Rule::unique($this->model->getTable(), 'title'), 'max:191'],
        ];
    }
    protected function getFormFields()
    {
        $keys = ['active', 'title'];
        if ($this->model->getLimitLevel() > 1) {
            $keys = Arr::prepend($keys, 'parent_id');
        }
        $fields = $this->getTemplateFormFields($keys);
        if (is_null($this->data->parent_id)) {
            $fields['icon'] = [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('icon'),
                'name' => 'icon',
                'required' => false,
            ];
            $fields['divider_after'] = [
                'type' => 'radio',
                'label' => $this->getValidAttrs('divider_after'),
                'name' => 'divider_after',
                'required' => false,
                'options' => $this->model->present()->yes_or_no(),
                'default' => 0,
            ];
        }
        return $fields;
    }
}
