<?php

namespace App\Http\Controllers\Backend;

use App\Models\Subscriber;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SubscriberController extends BackendController
{

    public function __construct(Subscriber $Subscriber)
    {
        parent::__construct($Subscriber);
        $this->prefix = 'subscriber';
        $this->valid_attrs = [
            'created_at' => __('validation.attributes.filled_at'),
        ];
    }
    protected function getValidRules()
    {
        return [];
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields([
            'status', 'created_at', 'name', 'email', 'admin_note'
        ]);
        $fields['created_at']['type'] = $fields['name']['type'] = $fields['email']['type'] = 'text';
        $fields['created_at']['hint'] = null;
        $fields['name']['required'] = false;
        // dd($fields);
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = $this->getTemplateSearchFields(['status', 'name', 'email', 'created_range']);
        return $fields;
    }
    protected function getTemplateListKeys()
    {
        $keys = ['name', 'email', 'created_at', 'status', 'admin_note'];
        $keys = $this->checkTemplateListKeysPower($keys);
        return $keys;
    }
    public function generateListData($datas)
    {
        $list_datas = [];
        foreach ($datas as $data) {
            $fields = $this->getTemplateList($data);
            $list_datas[$data->id] = $fields;
        }
        return $list_datas;
    }
    public function export(Request $request)
    {
        // 標題列
        $attrs = $this->getValidAttrs(
            'email',
            'name',
            'locale',
            'created_at',
            'status'
        );
        $headings = [
            array_keys($attrs),
            array_values($attrs),
        ];

        // 資料列
        $datas = $this->search()->get();
        $result = collect();
        foreach ($datas as $data) {
            $result->push([
                $data->email,
                $data->name,
                $data->locale,
                $data->created_at,
                strip_tags($data->present()->status($data->status)),
            ]);
        }
        // dd($headings, $result);
        $columnFormats = [];
        // 匯出
        return Excel::download(new Export($headings, $result, $columnFormats), '電子報訂閱_' . Carbon::now()->format('YmdHis') . '.xlsx');
    }
}
