<?php

namespace App\Http\Controllers\Backend;

use App\Models\BlogTag;

class BlogTagController extends BackendController
{

    public function __construct(BlogTag $BlogTag)
    {
        parent::__construct($BlogTag);
        $this->prefix = 'blog.tag';
    }
    protected function getFormFields()
    {
        return $this->getTemplateFormFields(['status', 'title']);
    }
    protected function getSearchFields()
    {
        return $this->getTemplateSearchFields(['status', 'title']);
    }
    protected function getValidRules()
    {
        return $this->getTemplateValidRules(['title']);
    }
    protected function getIndexList($data)
    {
        return $this->getTemplateList($data, ['title', 'status']);
    }
}
