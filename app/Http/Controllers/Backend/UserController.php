<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UserController extends BackendController
{
    public function __construct(User $User)
    {
        parent::__construct($User);
        $this->prefix = 'user';
        $this->valid_attrs = [];
        $this->valid_msgs = [
            'required_if' => __('validation.required'),
        ];
    }
    protected function getValidRules()
    {
        return [
            'account' => ['required', Rule::unique($this->model->getTable(), 'email'), 'email', 'max:191'],
            'email' => 'required|email|max:191',
            'password' => 'required|user_password|confirmed',
            'name' => 'required',
            'sex' => 'required',
            'birthday' => 'required',
            'phone' => 'required|user_phone',
            'address' => 'required',
        ];
    }
    protected function getEditValidRules()
    {
        $fields = Arr::except($this->getValidRules(), ['password', 'email']);
        return $fields;
    }
    protected function getEditFormFields()
    {
        $fields = Arr::except($this->getFormFields(), ['password', 'password_confirmation']);
        $fields['email']['type'] = 'text';
        $fields['email']['required'] = false;
        $fields = Arr::insertAfter($fields, [
            'created_at' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('created_at'),
                'name' => 'created_at',
                'required' => false,
            ],
        ], 'email_verified');
        $fields = Arr::insertAfter($fields, [
            'login_at' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('login_at'),
                'name' => 'login_at',
                'required' => false,
            ],
            'ip' => [
                'type' => 'text',
                'label' => $this->getValidAttrs('ip'),
                'name' => 'ip',
                'required' => false,
            ],
        ], 'subscribe');
        return $fields;
    }
    protected function getFormFields()
    {
        $fields = [
            'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
            'email_verified' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('email_verified'),
                'name' => 'email_verified',
                'required' => true,
                'options' => $this->model->present()->email_verified(),
                'default' => 1,
            ],
            'account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('account'),
                'name' => 'account',
                'required' => true,
            ],
            'email' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('email'),
                'name' => 'email',
                'required' => true,
            ],
            'password' => [
                'type' => 'password_input',
                'label' => $this->getValidAttrs('password'),
                'name' => 'password',
                'required' => true,
                'hint' => __('text.password_hint', [], env('BACKEND_LOCALE')),
            ],
            'password_confirmation' => [
                'type' => 'password_input',
                'label' => $this->getValidAttrs('password_confirmation'),
                'name' => 'password_confirmation',
                'required' => true,
                'error_name' => 'password',
            ],
            'name' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('name'),
                'name' => 'name',
                'required' => true,
            ],
            'sex' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('sex_title'),
                'name' => 'sex',
                'show' => 'option',
                'required' => true,
                'options' => $this->model->present()->sex_title(),
            ],
            'birthday' => [
                'type' => 'date_input',
                'label' => $this->getValidAttrs('birthday'),
                'name' => 'birthday',
                'required' => true,
            ],
            'phone' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('phone'),
                'name' => 'phone',
                'required' => true,
            ],
            'city_region' => [
                'type' => 'city_region',
                'label' => $this->getValidAttrs('city_region'),
                'name' => ['city_id', 'region_id'],
                'required' => false,
            ],
            'address' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('address'),
                'name' => 'address',
                'required' => false,
            ],
        ];
        // dd($fields);
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = Arr::insertAfter($this->getTemplateSearchFields(['status', 'name', 'phone', 'created_range']), [
            'account' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('account'),
                'name' => 'account',
                'search' => 'like',
            ],
        ], 'status');
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = $this->getTemplateList($data, ['name', 'sex', 'phone', 'created_at', 'status', 'admin_note']);
        $fields = Arr::insertAfter($fields, [
            'account' => [
                'label' => $this->getValidAttrs('account'),
                'align' => 'left',
                'type' => 'link',
                'url' => route('backend.' . $this->prefix . '.edit', array_merge(request()->all(), ['id' => $data->id])),
                'data' => $data->account,
            ],
        ]);
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => array_merge(
                [
                    'login_bg' => [
                        'type' => 'img',
                        'label' => $this->getValidAttrs('login_bg'),
                        'name' => 'login_bg',
                        'required' => false,
                        'w' => null,
                        'h' => null,
                        'folder' => 'login',
                    ],
                    'goodnews_url' => [
                        'type' => 'text_input',
                        'label' => $this->getValidAttrs('goodnews_url'),
                        'name' => 'goodnews_url',
                        'required' => false,
                    ],
                    'hr1' => [
                        'type' => 'hr',
                        'label' => '',
                        'name' => 'hr1',
                        'required' => false,
                    ],
                    'heading' => [
                        'type' => 'heading',
                        'label' => '↓ '.__('text.terms').' ↓',
                        'name' => 'heading',
                        'required' => false,
                    ],
                    'hr2' => [
                        'type' => 'hr',
                        'label' => '',
                        'name' => 'hr2',
                        'required' => false,
                    ],
                ],
                $this->getTemplateFormFields([
                    'banner', 'text', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
                ])
            ),
        ];
        $fields["backend.$this->prefix.page"]['banner']['folder'] = $fields["backend.$this->prefix.page"]['og_image']['folder'] = 'terms';
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => array_merge(
                [
                    'goodnews_url' => 'nullable|url',
                ],
                $this->getTemplateValidRules([
                    'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
                ])
            ),
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
    public function export(Request $request)
    {
        // 標題列
        $attrs = $this->getValidAttrs(
            'account',
            'email',
            'name',
            'sex_title',
            'phone',
            'address',
            'birthday',
            'created_at',
            'status',
            'email_verified',
            'login_at',
            'ip'
        );
        $headings = [
            array_keys($attrs),
            array_values($attrs),
        ];

        // 資料列
        $datas = $this->search()->get();
        $result = collect();
        foreach ($datas as $data) {
            $result->push([
                $data->account,
                $data->email,
                $data->name,
                strip_tags($data->present()->sex_title($data->sex)),
                $data->phone,
                $data->address,
                $data->birthday,
                $data->created_at,
                strip_tags($data->present()->status($data->status)),
                strip_tags($data->present()->yes_or_no($data->email_verified)),
                $data->login_at,
                $data->ip,
            ]);
        }
        // dd($headings, $result);
        $columnFormats = [
            'E' => NumberFormat::FORMAT_TEXT,
        ];
        // 匯出
        return Excel::download(new Export($headings, $result, $columnFormats), $this->model->getTable() . '_' . Carbon::now()->format('YmdHis') . '.xlsx');
    }
}
