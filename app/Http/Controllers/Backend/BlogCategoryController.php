<?php

namespace App\Http\Controllers\Backend;

use App\Models\BlogCategory;

class BlogCategoryController extends BackendController
{

    public function __construct(BlogCategory $BlogCategory)
    {
        parent::__construct($BlogCategory);
        $this->prefix = 'blog.category';
        $this->template = 'category';
    }
}
