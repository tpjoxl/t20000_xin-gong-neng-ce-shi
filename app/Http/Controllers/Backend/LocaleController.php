<?php

namespace App\Http\Controllers\Backend;

use App\Models\Locale;
use App\Helpers\ArrHelper as Arr;
use Illuminate\Validation\Rule;

class LocaleController extends BackendController
{

    public function __construct(Locale $Locale)
    {
        parent::__construct($Locale);
        $this->prefix = 'locale';
    }
    protected function getValidRules()
    {
        return [
            'title' => ['required', Rule::unique($this->model->getTable(), 'title'), 'string', 'max:191'],
            'code' => ['required', Rule::unique($this->model->getTable(), 'title'), 'string', 'max:50'],
        ];
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields(['status', 'title']);
        $fields = Arr::insertAfter($fields, [
            'frontend_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('frontend_status'),
                'name' => 'frontend_status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 0,
            ],
        ], 'status');
        $fields = Arr::insertAfter($fields, [
            'code' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('code'),
                'name' => 'code',
                'required' => true,
            ],
            'abbr' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('abbr'),
                'name' => 'abbr',
                'required' => false,
            ],
        ], 'title');
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = Arr::insertAfter($this->getTemplateSearchFields(['status', 'title']), [
            'frontend_status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('frontend_status'),
                'name' => 'frontend_status',
                'options' => $this->model->present()->status(),
                'has_all' => true,
                'search' => 'equal',
            ],
        ], 'status');
        $fields = Arr::insertAfter($fields, [
            'code' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('code'),
                'name' => 'code',
                'search' => 'like',
            ],
            'abbr' => [
                'type' => 'text_input',
                'label' => $this->getValidAttrs('abbr'),
                'name' => 'abbr',
                'search' => 'like',
            ],
        ], 'title');
        return $fields;
    }
    protected function getDropdowns()
    {
        $dropdowns = [
            'status' => [
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'options' => $this->model->present()->status(),
            ],
            'frontend_status' => [
                'label' => $this->getValidAttrs('frontend_status'),
                'name' => 'frontend_status',
                'options' => $this->model->present()->status(),
            ],
        ];
        return $dropdowns;
    }
    protected function getIndexList($data)
    {
        $fields = Arr::insertAfter($this->getTemplateList($data, ['title', 'status']),
            [
                'code' => [
                    'label' => $this->getValidAttrs('code'),
                    'align' => 'left',
                    'type' => 'text',
                    'data' => $data->code,
                ],
                'abbr' => [
                    'label' => $this->getValidAttrs('abbr'),
                    'align' => 'left',
                    'type' => 'text',
                    'data' => $data->abbr,
                ],
            ],
            'title'
        );
        $fields = Arr::insertAfter($fields, [
            'frontend_status' => [
                'label' => $this->getValidAttrs('frontend_status'),
                'align' => 'center',
                'type' => 'html',
                'options' => $this->model->present()->status(),
                'data' => !is_null($data->frontend_status)?$this->model->present()->status($data->frontend_status):'',
            ],
        ], 'status');
        return $fields;
    }
}
