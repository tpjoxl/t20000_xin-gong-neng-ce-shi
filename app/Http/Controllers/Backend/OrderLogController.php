<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\OrderLog;
use App\Helpers\ArrHelper as Arr;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Export;

class OrderLogController extends BackendController
{
    public function __construct(OrderLog $OrderLog)
    {
        parent::__construct($OrderLog);
        $this->prefix = 'order.log';
        $this->valid_attrs = [];
    }
    protected function getSearchFields()
    {
        $fields = array_merge([
            'order_id' => [
                'type' => 'select_ajax',
                'label' => $this->getValidAttrs('order_id'),
                'name' => 'order_id',
                'options' => method_exists($this->model, 'order') ? $this->model->order()->getRelated()->where('id', request()->order_id)->get() : collect(),
                'key' => 'num',
                'ajax_attrs' => [
                    'url' => route('backend.api.order'),
                    'type' => 'GET',
                ],
                'search' => 'equal',
            ],
            'type' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('type'),
                'name' => 'type',
                'options' => $this->model->present()->type(),
                'has_all' => true,
                'search' => 'equal',
            ],
        ], $this->getTemplateSearchFields(['created_range']));
        return $fields;
    }
    protected function getIndexList($data)
    {
        $fields = array_merge(
            [
                'order_id' => [
                    'label' => $this->getValidAttrs('order_id'),
                    'align' => 'left',
                    'type' => 'link',
                    'data' => $data->order_id?$data->order->num:'',
                    'url' => route('backend.order.edit', ['id' => $data->order_id]),
                ],
                'type' => [
                    'label' => $this->getValidAttrs('type'),
                    'align' => 'center',
                    'type' => 'html',
                    'options' => $this->model->present()->type(),
                    'data' => !is_null($data->type)?$this->model->present()->type($data->type):'',
                ],
                'status' => [
                    'label' => $this->getValidAttrs('status'),
                    'align' => 'center',
                    'type' => 'html',
                    'options' => $this->model->order()->getRelated()->present()->{$data->type_status}(),
                    'data' => !is_null($data->status)?$this->model->order()->getRelated()->present()->{$data->type_status}($data->status):'',
                ],
                'who' => [
                    'label' => $this->getValidAttrs('who'),
                    'align' => 'left',
                    'type' => 'text',
                    'data' => $data->who,
                ],
            ],
            $this->getTemplateList($data, ['created_at'])
        );
        return $fields;
    }
}
