<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Route;
use App\Helpers\ArrHelper as Arr;
use App\Models\Bonus;
use App\Models\User;
use Illuminate\Validation\Rule;

class BonusController extends BackendController
{
    public function __construct(Bonus $Bonus, User $User)
    {
        parent::__construct($Bonus);
        $this->prefix = 'bonus';
        $this->user = $User;
        $this->valid_msgs = [
            'required_if' => __('validation.required'),
        ];
    }
    protected function getFormFields()
    {
        $fields = [
			'status' => [
                'type' => 'radio',
                'label' => $this->getValidAttrs('status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->model->present()->status(),
                'default' => 1,
            ],
			'user_id' => [
                'type' => 'select_ajax',
                'label' => $this->getValidAttrs('user_id'),
                'name' => 'user_id',
                'required' => true,
                'options' => method_exists($this->model, 'user')&&$this->data->user_id ? $this->model->user()->getRelated()->where('id', $this->data->user_id)->get() : collect(),
                'key' => ['account', 'name', 'phone'],
                'ajax_attrs' => [
                    'url' => route('backend.api.user'),
                    'type' => 'GET',
                ],
                'hint' => '可輸入會員帳號、姓名、電話當作搜尋關鍵字',
            ],
			'order_id' => [
                'type' => 'select_ajax',
                'label' => $this->getValidAttrs('order_id'),
                'name' => 'order_id',
                'required' => false,
                'options' => method_exists($this->model, 'order') ? $this->model->order()->getRelated()->where('id', request()->order_id)->get() : collect(),
                'key' => ['num', 'name'],
                'ajax_attrs' => [
                    'url' => route('backend.api.order'),
                    'type' => 'GET',
                ],
                'hint' => '可輸入訂單編號當作搜尋關鍵字<br>※若點數增減原因與訂單相關，請務必填寫此欄位',
			],
			'point' => [
				'type' => 'text_input',
                'label' => $this->getValidAttrs('point'),
				'name' => 'point',
				'required' => true,
				'hint' => '若要扣點數請輸入負整數',
			],
			'date_range' => [
                'type' => 'date_range',
                'label' => $this->getValidAttrs('date_range'),
                'name' => ['date_on', 'date_off'],
                'placeholder' => [__('validation.attributes.date_on', [], env('BACKEND_LOCALE')), __('validation.attributes.date_off', [], env('BACKEND_LOCALE'))],
                'required' => false,
			],
			'note' => [
                'type' => 'text_area',
                'label' => $this->getValidAttrs('note'),
                'name' => 'note',
                'required' => false,
            ],
		];
        return $fields;
    }
    protected function getSearchFields()
    {
        $fields = Arr::insertAfter(
            $this->getTemplateSearchFields(['status', 'date_range', 'created_range']),
            [
                'user_id' => [
					'type' => 'select_ajax',
					'label' => $this->getValidAttrs('user_id'),
					'name' => 'user_id',
					'options' => method_exists($this->model, 'user') ? $this->model->user()->getRelated()->where('id', request()->user_id)->get() : collect(),
					'key' => ['account', 'name', 'phone'],
					'ajax_attrs' => [
						'url' => route('backend.api.user'),
						'type' => 'GET',
					],
					'search' => 'equal',
					'hint' => '可輸入會員帳號、姓名、電話當作搜尋關鍵字',
				],
                'order_id' => [
					'type' => 'select_ajax',
					'label' => $this->getValidAttrs('order_id'),
					'name' => 'order_id',
					'options' => method_exists($this->model, 'order') ? $this->model->order()->getRelated()->where('id', request()->order_id)->get() : collect(),
					'key' => ['num'],
					'ajax_attrs' => [
						'url' => route('backend.api.order'),
						'type' => 'GET',
					],
					'search' => 'equal',
					'hint' => '可輸入訂單編號當作搜尋關鍵字',
				],
            ],
        'status');
        return $fields;
    }
    protected function getValidRules()
    {
        return [
            'user_id' => 'required',
            'point' => 'required|integer',
        ];
    }
    protected function getIndexList($data)
    {
        $fields = array_merge(
			[
                'point' => [
                    'label' => $this->getValidAttrs('point'),
                    'align' => 'left',
                    'type' => 'link',
                    'data' => $data->point,
                    'url' => Route::has("backend.$this->prefix.edit")?route('backend.' . $this->prefix . '.edit', array_merge(request()->route()->parameters, request()->all(), ['id' => $data->id])):'',
                ],
				'user_id' => [
					'label' => $this->getValidAttrs('user_id'),
					'align' => 'left',
					'type' => 'text',
					'data' => $data->user_id?$data->user->account:'',
				],
				'order_id' => [
					'label' => $this->getValidAttrs('order_id'),
					'align' => 'left',
					'type' => 'text',
					'data' => $data->order_id?$data->order->num:'',
				],
			],
			$this->getTemplateList($data, ['date_range', 'created_at', 'status']),
			[
				'note' => [
					'label' => $this->getValidAttrs('note'),
					'align' => 'left',
					'type' => 'html',
					'data' => nl2br($data->note),
				],
			]
		);
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => [
                'price_per_point' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('price_per_point'),
                    'name' => 'price_per_point',
                    'required' => true,
                    'hint' => '訂單消費 X 元，累計 1 點紅利',
                ],
                'point_per_price' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('point_per_price'),
                    'name' => 'point_per_price',
                    'required' => true,
                    'hint' => '累積 Y 點紅利，可兌換消費金額 1 元',
                ],
                'limit_percentage' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('limit_percentage'),
                    'name' => 'limit_percentage',
                    'required' => true,
                    'hint' => '紅利點數兌換上限為訂單金額 Z %',
                ],
                'point_after_day' => [
                    'type' => 'text_input',
                    'label' => $this->getValidAttrs('point_after_day'),
                    'name' => 'point_after_day',
                    'required' => true,
                    'hint' => '訂單成立 N 天後點數才生效',
                ],
                'text' => [
                    'type' => 'text_editor',
                    'label' => $this->getValidAttrs('text'),
                    'name' => 'text',
                    'required' => false,
                ],
            ],
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => [
                'price_per_point' => 'required|integer',
                'point_per_price' => 'required|integer',
                'limit_percentage' => 'required|integer',
                'point_after_day' => 'required|integer',
            ],
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
}
