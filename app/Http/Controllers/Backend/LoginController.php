<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Rq;
use Illuminate\Support\Facades\Validator;
use App\Traits\ReCaptchaTrait;
use Carbon\Carbon;

class LoginController extends Controller
{
    use ReCaptchaTrait;

    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('backend.login');
    }

    public function login(Request $request)
    {
        // 驗證表單資料
        $validator = Validator::make($request->all(), [
            'account' => 'required',
            'password' => 'required|min:6',
            'g-recaptcha-response' => 'required'
        ], [], [
            'account' => __('validation.attributes.admin_account', [], env('BACKEND_LOCALE')),
            'password' => __('validation.attributes.admin_password', [], env('BACKEND_LOCALE')),
            'g-recaptcha-response' => __('validation.attributes.captcha', [], env('BACKEND_LOCALE'))
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->only('account'));
        }

        $recaptchaCheck = $this->ReCaptchaCheck();
        // dd($recaptchaCheck);
        if ($recaptchaCheck['status']) {
            // 嘗試登入使用者
            if (auth()->guard('admin')->attempt(['account'=>$request->account, 'password'=>$request->password, 'status'=>1], $request->remember)) {
                auth()->guard('admin')->user()->update([
                    'login_at' => Carbon::now(),
                    'ip' => Rq::ip()
                ]);
                // 如果成功，就導向到後台首頁
                return redirect()->intended(route('backend.home'));
            }
            // 如果失敗，就回到登入頁面
            return redirect()->back()->withInput($request->only('account'))->withErrors(__('backend.login_error', [], env('BACKEND_LOCALE')));
        } else {
            return redirect()->back()->withInput($request->only('account'))->withErrors($recaptchaCheck['errors']);
        }
    }
    public function logout()
    {
        auth()->guard('admin')->logout();

        return redirect()->route('backend.login');
    }
}
