<?php

namespace App\Http\Controllers\Backend;

use App\Models\Setting;
use App\Helpers\ArrHelper as Arr;

class CartController extends BackendController
{

    public function __construct(Setting $Setting)
    {
        parent::__construct($Setting);
        $this->prefix = 'cart';
        $this->valid_attrs = [];
    }
    protected function getPageFormFields($page)
    {
        $pageFields = $this->getTemplateFormFields([
            'banner', 'text', 'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
        ]);
        $pageFields = Arr::insertAfter($pageFields, [
            'text2' => [
                'type' => 'text_editor',
                'label' => $this->getValidAttrs('text2'),
                'name' => 'text2',
                'required' => false,
            ],
        ], 'text');
        $fields = [
            "backend.$this->prefix.page" => $pageFields,
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::exists($validRules, $page) ? Arr::get($validRules, $page) : Arr::get($validRules, "backend.$this->prefix.page");
    }
}
