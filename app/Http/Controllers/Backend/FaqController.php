<?php

namespace App\Http\Controllers\Backend;

use App\Models\Faq;
use App\Helpers\ArrHelper as Arr;

class FaqController extends BackendController
{

    public function __construct(Faq $Faq)
    {
        parent::__construct($Faq);
        $this->prefix = 'faq';
    }
    protected function getFormFields()
    {
        $fields = $this->getTemplateFormFields(['status', 'title', 'text', 'date_range']);
        if (method_exists($this->model, 'categories')) {
            $fields = Arr::insertAfter($fields, $this->getTemplateFormFields(['categories']));
        }
        return $fields;
    }
    protected function getPageFormFields($page)
    {
        $fields = [
            "backend.$this->prefix.page" => $this->getTemplateFormFields([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description', 'og_image', 'meta_robots'
            ]),
        ];
        return Arr::get($fields, $page) ?: Arr::get($fields, "backend.$this->prefix.page");
    }
    protected function getPageValidRules($page)
    {
        $validRules = [
            "backend.$this->prefix.page" => $this->getTemplateValidRules([
                'seo_title', 'seo_description', 'seo_keyword', 'og_title', 'og_description'
            ]),
        ];
        return Arr::get($validRules, $page) ?: Arr::get($validRules, "backend.$this->prefix.page");
    }
}
