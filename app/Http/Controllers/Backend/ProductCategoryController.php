<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductCategory;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProductCategoryController extends BackendController
{

    public function __construct(ProductCategory $ProductCategory)
    {
        parent::__construct($ProductCategory);
        $this->prefix = 'product.category';
        $this->template = 'category';
    }
    public function valid($requestData, $rules, $msgs, $id = null)
    {
        if (method_exists($this->model, 'translations')) {
            $locales = $this->locales;
        } else {
            $locales = $this->locales->where('code', env('BACKEND_LOCALE'));
        }
        $valid_rules = [];
        $valid_msgs = $msgs;
        $valid_attrs = [];
        foreach ($rules as $name => $rule) {
            if ((count($locales)>1||$this->model->getTable()=='settings') && method_exists($this->model, 'translations') && in_array($name, $this->data->translatedAttributes)) {
                foreach ($locales as $locale) {
                    $locale_code = $locale->code;
                    $myRule[$locale->code][$name] = $rule;
                    if (is_array($rule)) {
                        foreach ($rule as $k => $r) {
                            if (is_a($r, 'Illuminate\Validation\Rules\Unique')) {
                                $myRule[$locale->code][$name][$k] = Rule::unique($this->data->translations()->getRelated()->getTable(), $name)->where(function ($query) use ($locale_code) {
                                    return $query->where('locale', $locale_code);
                                })->ignore($id, Str::singular($this->data->getTable()) . '_id');
                                // $myRule[$locale->code][$name][$k] = $myRule[$locale->code][$name][$k]->ignore($id, Str::singular($this->data->getTable()).'_id');
                            } else {
                                $myRule[$locale->code][$name][$k] = $r;
                            }
                        }
                    }
                    $valid_rules[$locale->code . '.' . $name] = $myRule[$locale->code][$name];
                    $valid_attrs[$locale->code . '.' . $name] = $this->getValidAttrs($name) . " [ $locale->title ]";
                }
            } else {
                $myRule[$name] = $rule;
                if (is_array($rule)) {
                    foreach ($rule as $k => $r) {
                        if (is_a($r, 'Illuminate\Validation\Rules\Unique')) {
                            if (method_exists($this->model, 'translations') && in_array($name, $this->data->translatedAttributes)) {
                                $locale_code = $locales->first()->code;
                                if ($name=='title'||$name=='slug') {
                                    $myRule[$name][$k] = Rule::unique($this->data->translations()->getRelated()->getTable(), $name)->where(function ($query) use ($locale_code) {
                                        return $query->where('locale', $locale_code)->whereIn(Str::singular($this->data->getTable()) . '_id', $this->model->all()->pluck('id')->toArray());
                                    })->ignore($id, Str::singular($this->data->getTable()) . '_id');
                                }
                            } else {
                                $myRule[$name][$k] = $myRule[$name][$k]->ignore($id);
                            }
                        } else {
                            $myRule[$name][$k] = $r;
                        }
                    }
                }
                $valid_rules[$name] = $myRule[$name];
                $valid_attrs[$name] = $this->getValidAttrs($name);
            }
        }
        return Validator::make($requestData, $valid_rules, $valid_msgs, $valid_attrs);
    }
}
