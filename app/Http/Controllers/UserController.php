<?php

namespace App\Http\Controllers;

use App\Mail\UserForgot;
use App\Mail\UserRegister;
use App\Models\Setting;
use App\Models\User;
use App\Models\Coupon;
use App\Models\TwCity;
use App\Traits\MailTrait;
use App\Traits\ReCaptchaTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $model, $redirect_after_login, $redirect_after_logout;
    use ReCaptchaTrait, MailTrait;

    public function __construct(User $User)
    {
        $this->middleware('guest:web')->only(['login', 'register', 'registerConfirmed', 'forgot'])->except('logout');
        $this->middleware(['auth:web', 'auth.first_login'])->only(['edit', 'password']);
        $this->model = $User;
        $this->prefix = 'user';
        $this->redirect_after_login = 'user.order.index';
        $this->redirect_after_logout = 'user.login';
    }
    protected function guard()
    {
        return Auth::guard('web');
    }
    public function login(Request $request)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->model->make();
        $custom_page_title = __('text.' . $this->prefix . '_login') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.login'), route($prefix . '.login'), __('text.login')],
        ];

        if ($request->isMethod('POST')) {
            // 驗證表單
            $requestData = $request->all();
            $validator = Validator::make($requestData, [
                'account' => 'required|email|max:150',
                'password' => 'required|user_password',
                'g-recaptcha-response' => 'required',
            ], [], []);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->except('password'));
            }

            $recaptchaCheck = $this->ReCaptchaCheck();
            if ($recaptchaCheck['status']) {
                // 嘗試登入使用者
                if ($this->guard()->attempt(['account' => $request->account, 'password' => $request->password], $request->filled('remember'))) {
                    $auth = $this->guard()->user();
                    $loginErrorMsg = '';
                    if ($auth->email_verified == 0) {
                        $loginErrorMsg = __('text.login_email_verified_error');
                    } elseif ($auth->status == 2) {
                        $loginErrorMsg = __('text.login_status_error');
                    }
                    if (!empty($loginErrorMsg)) {
                        $this->guard()->logout();
                        return redirect()->back()->withInput($request->except('password'))
                            ->with('error', __('text.login_error'))
                            ->with('errorText', $loginErrorMsg);
                    }
                    // $request->session()->regenerate();
                    $auth->update([
                        'login_at' => Carbon::now(),
                        'ip' => $request->ip(),
                    ]);
                    // 如果成功，就導向到指定的頁面
                    if ($request->filled('redirect_url')) {
                        return redirect()->to($request->redirect_url);
                    }
                    return redirect()->intended(route($this->redirect_after_login));
                }
                // 如果失敗，就回到登入頁面
                return redirect()->back()->withInput($request->except('password'))->with('error', __('text.login_error'))->with('errorText', __('text.login_error_text'));
            } else {
                return redirect()->back()->withInput($request->except('password'))->withErrors($recaptchaCheck['errors']);
            }
        }
        return view('frontend.' . $this->prefix . '.login', compact('page', 'data', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        // $request->session()->invalidate();
        // $request->session()->regenerateToken();

        return redirect()->route($this->redirect_after_logout);
    }
    public function register(Request $request)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->model->make();
        $custom_page_title = __('text.' . $prefix . '_register') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.register'), route($this->prefix . '.register'), __('text.register')],
        ];
        $cities = app('twCities');

        if ($request->isMethod('POST')) {
            // 驗證表單
            $requestData = $request->all();
            $validator = Validator::make($requestData, [
                'account' => 'required|email|max:150|unique:' . $this->model->getTable(),
                'email' => 'required|email|max:150',
                'password' => 'required|user_password|confirmed',
                'name' => 'required',
                'sex' => 'nullable',
                'phone' => 'required|user_phone',
                'address' => 'required',
                'birthday' => 'required',
                'agree' => 'required',
                'g-recaptcha-response' => 'required',
            ], [], __('validation.' . $prefix));
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->except('password'));
            }

            $recaptchaCheck = $this->ReCaptchaCheck();
            if ($recaptchaCheck['status']) {
                // 註冊會員
                $requestData['register_confirmed'] = $data->random_register();
                $data->fillAndSave($requestData);

                // 寄註冊信箱驗證信
                if ($this->SendMailTo($data, new UserRegister($data))) {
                    // 寄信成功後導回登入頁
                    return redirect()->route($prefix . '.login')
                        ->with('success', __('text.register_success'))
                        ->with('successText', __('text.user_register_success_text'));
                } else {
                    return redirect()->back()
                        ->with('error', __('text.register_error'))
                        ->with('errorText', __('text.register_error_text'));
                }
            } else {
                return redirect()->back()->withInput($request->except('password'))->withErrors($recaptchaCheck['errors']);
            }
        }
        return view('frontend.' . $this->prefix . '.register', compact('page', 'data', 'cities', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function registerConfirmed(Request $request)
    {
        $register_confirmed = $request->token;
        $created_at = $request->t;

        $datas = $this->model->where('register_confirmed', $register_confirmed)->get();
        if ($datas->isEmpty()) {
            return redirect()->route($this->prefix . '.login')->with('error', 'ERROR!')->with('errorText', '註冊驗證連結已驗證過，或查無註冊的帳號!');
        }

        foreach ($datas as $data) {
            if (preg_replace('/\D/', '', $data->created_at) == $created_at) {
                $data->register_confirmed = null;
                $data->email_verified = 1;
                $data->email_verified_at = Carbon::now();
                $data->save();

                return redirect()->route($this->prefix . '.login')->with('success', 'SUCCEED!')->with('successText', '您的信箱已驗證成功!');
            }
        }
    }
    public function forgot(Request $request)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $custom_page_title = __('text.forgot') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.forgot'), route($this->prefix . '.forgot'), __('text.forgot')],
        ];

        if ($request->isMethod('POST')) {
            // 驗證表單
            $requestData = $request->all();
            $validator = Validator::make($requestData, [
                'account' => 'required|email|exists:' . $this->model->getTable(),
                'g-recaptcha-response' => 'required',
            ], [
                'exists' => '輸入的 :attribute 沒有註冊過',
            ], []);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $recaptchaCheck = $this->ReCaptchaCheck();
            if ($recaptchaCheck['status']) {
                $data = $this->model->where('account', $request->account)->first();
                $data->password = $new_psw = $data->random_password();
                // 寄註冊信箱驗證信
                if ($this->SendMailTo($data->account, new UserForgot($data, $new_psw))) {
                    // 寄信成功
                    $data->save();
                    return redirect()->route($prefix . '.login')
                        ->with('success', '重設密碼成功')
                        ->with('successText', '密碼重設信已送至信箱<br>' . $request->account . '<br>請前往收信。<br><br><small>如未看到信件，請搜尋是否被歸到垃圾信件，如有其他問題，請前往<a href=\"' . route('contact.index') . '\">' . __('text.contact') . '</a>網站管理員將協助您的問題，謝謝</small>');
                } else {
                    return redirect()->back()
                        ->with('error', 'ERROR!')
                        ->with('errorText', '信件送出失敗');
                }
            } else {
                return redirect()->back()->withInput($request->except('password'))->withErrors($recaptchaCheck['errors']);
            }
        }
        return view('frontend.' . $this->prefix . '.forgot', compact('page', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function edit(Request $request)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->guard()->user();
        $custom_page_title = __('text.' . $this->prefix . '_edit') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.' . $prefix . '_edit'), route($prefix . '.edit'), __('text.' . $prefix . '_edit')],
        ];
        $cities = app('twCities');

        if ($request->isMethod('POST')) {
            // 驗證表單
            $requestData = $request->all();
            $validator = Validator::make($requestData, [
                'email' => 'required|email|max:150',
                'name' => 'required',
                'sex' => 'nullable',
                'phone' => 'required|user_phone',
                'address' => 'required',
                'g-recaptcha-response' => 'required',
            ], [], []);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->except('password'));
            }

            $recaptchaCheck = $this->ReCaptchaCheck();
            if ($recaptchaCheck['status']) {
                // 修改會員
                $data->fillAndSave($requestData);
                return redirect()->back()
                    ->with('success', __('text.edit_success'))
                    ->with('successText', __('text.edit_success_text'));
            } else {
                return redirect()->back()->withInput($request->except('password'))->withErrors($recaptchaCheck['errors']);
            }
        }
        return view('frontend.' . $this->prefix . '.edit', compact('page', 'data', 'custom_page_title', 'prefix', 'breadcrumb', 'cities'));
    }
    public function password(Request $request)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->guard()->user();
        $custom_page_title = __('text.password') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.password'), route($prefix . '.password'), __('text.password')],
        ];

        if ($request->isMethod('POST')) {
            // 驗證表單
            $requestData = $request->all();
            $validator = Validator::make($requestData, [
                'old_password' => 'required|user_password|password:web',
                'password' => 'required|user_password|confirmed',
                'g-recaptcha-response' => 'required',
            ], [], [
                'password' => __('validation.attributes.new_password'),
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->except('password'));
            }

            $recaptchaCheck = $this->ReCaptchaCheck();
            if ($recaptchaCheck['status']) {
                // 修改會員
                $data->fillAndSave($requestData);
                $this->guard()->logout();
                return redirect()->route($this->redirect_after_logout)
                    ->with('success', __('text.password_success'))
                    ->with('successText', __('text.password_success_text'));
            } else {
                return redirect()->back()->withInput($request->except('password'))->withErrors($recaptchaCheck['errors']);
            }
        }
        return view('frontend.' . $this->prefix . '.password', compact('page', 'data', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function terms()
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $custom_page_title = ($data->seo_title ?? __('text.user_terms')) . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.user_terms'), route($prefix . '.terms'), __('text.user_terms')],
        ];
        return view('frontend.' . $this->prefix . '.terms', compact('data', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function coupon()
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->guard()->user();

        $items = Coupon::display()
            ->usable($data)
            ->with([
                'orders' => function ($q) {
                    // 排除已取消的訂單
                    $q->where('status', '!=', 2);
                }
            ])
            ->get()
            ->filter(function ($item, $key) use ($data) {
                if ($data) {
                    if ($item->orders->where('user_id', $data->id)->count() == 0) {
                        if ($item->use_type == 1) {
                            return !($item->count) || ($item->count > 0 && $item->count > $item->orders->count());
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                } else {
                    if ($item->use_type == 1) {
                        return !($item->count) || ($item->count > 0 && $item->count > $item->orders->count());
                    } else {
                        return false;
                    }
                }
            });

        $custom_page_title = __('text.user_coupon') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.user_coupon'), route($prefix . '.coupon'), __('text.user_coupon')],
        ];
        return view('frontend.' . $this->prefix . '.coupon', compact('data', 'page', 'items', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function bonus()
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->guard()->user();

        $bonusSetting = Setting::getData('bonus');

        $custom_page_title = __('text.user_bonus') . ' - ' . __('text.' . $prefix . '_centre');
        $breadcrumb = [
            [__('text.' . $prefix . '_centre'), route($this->redirect_after_login), __('text.' . $prefix . '_centre')],
            [__('text.user_bonus'), route($prefix . '.bonus'), __('text.user_bonus')],
        ];
        return view('frontend.' . $this->prefix . '.bonus', compact('data', 'page', 'bonusSetting', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
}
