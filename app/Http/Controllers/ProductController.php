<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $model, $category, $perPage;

    public function __construct(Product $Product, ProductCategory $ProductCategory)
    {
        $this->model = $Product;
        $this->category = $ProductCategory;
        $this->prefix = 'product';
        $this->perPage = 12;
    }
    public function index()
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $items = $this->model
            ->with([
                'categories' => function($q) {
                    $q->display()->ordered();
                },
                'images' => function($q) {
                    $q->where('is_cover', 1);
                },
                'skus'
            ])
            ->has('skus')
            ->display()
            ->ordered()
            ->paginate($this->perPage);

        $custom_page_title = $data->seo_title ? $data->seo_title : __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];

        return view('frontend.product.index', compact('prefix', 'data', 'items', 'breadcrumb', 'custom_page_title'));
    }
    public function category($category, $category2=null)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        if (!is_null($category2)) {
          $data = $this->category
            ->display()
            ->whereTranslation('slug', $category2)
            ->whereHas('parent', function($q) use ($category) {
              $q->display()
              ->whereTranslation('slug', $category);
            })
            ->firstOrFail();
          $custom_page_title = ($data->seo_title ? $data->seo_title : $data->title) . ' - ' .($data->parent->seo_title ? $data->parent->seo_title : $data->parent->title) . ' - ' . __('text.' . $this->prefix);
          $breadcrumb = [
              [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
              [$data->parent->title, route($prefix . '.category', ['category'=>$category]), $data->parent->title],
              [$data->title, route($prefix . '.category', ['category'=>$category, 'category2'=>$category2]), $data->title],
          ];
        } else {
          $data = $this->category
              ->display()
              ->whereTranslation('slug', $category)
              ->firstOrFail();
          $custom_page_title = ($data->seo_title ? $data->seo_title : $data->title) . ' - ' . __('text.' . $this->prefix);
          $breadcrumb = [
              [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
              [$data->title, route($prefix . '.category', $category), $data->title],
          ];
        }
        // dd($data);
        $data->banner = $page->banner;
        $items = $data->{$this->model->getTable()}()
            ->with([
                'categories' => function($q) {
                    $q->display()->ordered();
                },
                'images' => function ($q) {
                    $q->where('is_cover', 1);
                },
                'skus'
            ])
            ->has('skus')
            ->display()
            ->ordered('pivot_rank')
            ->paginate($this->perPage);
        return view('frontend.product.index', compact('prefix', 'data', 'items', 'breadcrumb', 'custom_page_title'));
    }
    public function detail($slug)
    {
        $prefix = $this->prefix;
        $page = Setting::getData($this->prefix);
        $data = $this->model
            ->has('skus')
            ->display()
            ->with([
                'categories' => function ($q) {
                    $q->display()->ordered();
                },
                'images' => function ($q) {
                    $q->display()->ordered();
                },
                'recommends' => function ($q) {
                    $q->with([
                        'images' => function($q) {
                            $q->where('is_cover', 1);
                        },
                    ])->has('skus')->display()->ordered();
                },
                'skus'
            ])
            ->whereTranslation('slug', $slug)
            ->firstOrFail();

        $data->banner = $page->banner;
        // 確認現在的URL KEY的語系是否正確，不是的話要跳到相對語系的URL KEY
        if (is_null($data->translate()->where('slug', $slug)->where('locale', app()->getLocale())->first())) {
            return redirect()->route($this->prefix . '.detail', $data->translate()->slug);
        }
        $all = $this->model
            ->has('skus')
            ->display()
            ->ordered()
            ->get();
        $currPos = array_search($data->id, $all->pluck('id')->toArray());
        $prev = $all->get($currPos - 1);
        $next = $all->get($currPos + 1);
        $custom_page_title = ($data->seo_title ? $data->seo_title : $data->title) . ' - ' . __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
            [$data->title, route($prefix . '.detail', $slug), $data->title],
        ];
        return view('frontend.product.detail', compact('prefix', 'data', 'breadcrumb', 'custom_page_title', 'prev', 'next'));
    }
    /**
     * 搜尋
     */
    public function search(Request $request)
    {
        $prefix = $this->prefix;
        $data = $this->model;
        // 搜尋的字串如果有空格就把它分成兩個搜尋條件
        $searchArr = preg_split("/\s+/", $request->q);
        $data->meta_robots = 'noindex,follow';
        $query = $this->model
            ->where(function ($q) use ($searchArr) {
                $q->whereTranslationLike('title', '%' . $searchArr[0] . '%')
                    ->orWhereTranslationLike('description', '%' . $searchArr[0] . '%')
                    ->orWhereTranslationLike('text', '%' . $searchArr[0] . '%');
                if (count($searchArr) > 1) {
                    for ($i = 1; $i < count($searchArr); $i++) {
                        $q->orWhereTranslationLike('title', '%' . $searchArr[$i] . '%')
                            ->orWhereTranslationLike('description', '%' . $searchArr[$i] . '%')
                            ->orWhereTranslationLike('text', '%' . $searchArr[$i] . '%');
                    }
                }
            })
            ->has('skus')
            ->display()
            ->ordered()
            ->get()
            ->filter(function ($item) use ($searchArr) {
                foreach ($item->translations as $translation) {
                    if (Str::contains(trim($translation->title), $searchArr) || Str::contains(trim($translation->description), $searchArr) || Str::contains(trim(strip_tags($translation->text)), $searchArr)) {
                        return true;
                        break;
                    }
                }
                return false;
            });

        $items = paginate($query, $this->perPage);

        $custom_page_title = __('text.search') . ' - ' . __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
            [__('text.product_search'), '', __('text.product_search')],
        ];

        return view('frontend.product.index', compact('prefix', 'data', 'items', 'custom_page_title', 'breadcrumb'));
    }
}
