<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Setting;
use App\Payment\Ecpay;
use App\Traits\MailTrait;
use App\Traits\EcpayTrait;
use App\Traits\NewebpayTrait;
use App\Mail\AdminOrderPaid;
use App\Mail\OrderPaid;
use App\Mail\OrderPayinfo;

class OrderController extends Controller
{
    use MailTrait, EcpayTrait, NewebpayTrait;
    protected $model;

    public function __construct(Order $Order)
    {
        $this->model = $Order;
        $this->prefix = 'order';
        $this->perPage = 10;
        $this->ecpay = new Ecpay;
        $this->ecpayReturnRoute = 'order.ecpay.return';
        $this->ecpayPaymentInfoRoute = 'order.ecpay.payment_info';
        $this->ecpayOrderResultRoute = 'order.ecpay.order_result';
        $this->ecpayClientRedirectRoute = 'order.ecpay.client_redirect';
        $this->ecpayClientBackRoute = 'order_query.index';
        $this->ecpayClientBackRouteAuth = 'user.order.index';
        $this->ecpayResultRoute = 'cart.result';
    }
    protected function guard()
    {
        return Auth::guard('web');
    }
    public function cancel(Request $request)
    {
        if ($request->filled('id')) {
            $requestData = $request->all();
            $user = $this->guard()->user();
            $order = $this->model->find($request->id);
            if (!is_null($order->user_id) && (is_null($user) || $user->id != $order->user_id)) {
                return back()->with('errorText', '無法取消此訂單，請重新操作');
            }
            if ($order->status != 1) {
                return back()->with('errorText', '訂單狀態為「'.$order->present()->status(1).'」時，才可以取消訂單');
            }
            $order->update($requestData);

            // 加回庫存
            foreach ($order->items as $item) {
                $item->sku->qty += $item->qty;
                $item->sku->save();
            }
            // 紅利無效
            $order->bonuses->each->update(['status'=>0]);

            return back()->with('successText', '已取消訂單')->with('orderid', [$request->id]);
        } else {
            return back()->with('errorText', '查無訂單，請重新操作')->with('orderid', [$request->id]);
        }
    }
    public function pay(Request $request)
    {
        $num = $request->num;
        $prefix = $this->prefix;
        $user = $this->guard()->user();
        $order = $this->model
            ->where('num', $num)
            ->first();
        if (!is_null($order->user_id) && (is_null($user) || $user->id != $order->user_id)) {
            return back()->with('errorText', '無法支付此訂單，請重新操作');
        }
        if ($order->status != 1 || $order->payment_status != 1) {
            return back()->with('errorText', '訂單狀態為「'.$order->present()->status(1).'」時，才需要進行繳費');
        }

        $order->epaynumber = $order->generateEpaynumber();
        $order->save();

        $provider = $order->payment_info['provider']??$order->payment['provider'];
        if ($provider == 'ECPAY') {
            return $this->ecpayCheckout($order);
        } elseif ($provider == 'NEWEBPAY') {
            $this->newebpayCheckout($order);
        } elseif ($provider == 'LINEPAY') {
            $this->linepayCheckout($order);
        } elseif ($provider == 'JKOPAY') {
            $this->jkopayCheckout($order);
        }
    }
    public function result(Request $request)
    {
        $prefix = $this->prefix;

        $custom_page_title = __('text.paid_result');
        $breadcrumb = [
            [__('text.paid_result'), route($prefix . '.result'), __('text.paid_result')],
        ];
        if (Session::has('num')) {
            $num = Session::get('num');
            $order = $this->model
                ->where('num', $num)
                ->first();

            return view('frontend.' . $this->prefix . '.result', compact('data', 'order', 'custom_page_title', 'prefix', 'breadcrumb'));
        } else {
            return redirect()->route('home');
        }
    }
    protected function afterPaid($record)
    {
        $order = $record->order;
        $provider = $order->payment_info['provider']??$order->payment['provider'];
        if ($provider == 'ECPAY') {
            if ($record['RtnCode'] == "1") {
                // 付款成功 狀態變成已付款
                $order->status = 1;
            }
        }
        $order->save();

        $form = $order->forms->first();
        if (!is_null($form->user_id)) {
            $user = $form->user;
        } elseif (!is_null($form->teacher_id)) {
            $user = $form->teacher;
        }

        // 寄通知信
        $this->SendMailToAdmin(new AdminOrderPaid($record));
        $this->SendMailTo($user, new OrderPaid($record));
    }
    protected function afterPayinfo($record)
    {
        $order = $record->order;
        $provider = $order->payment_info['provider']??$order->payment['provider'];
        if ($provider == 'ECPAY') {
            if ($record['RtnCode'] == '2' || $record['RtnCode'] == '10100073') {
                //取號成功 狀態變成待付款
                $order->status = 2;
            }
            $ReturnMsg = "";
            if ($record['vAccount']!="") {
                $ReturnMsg .= "付款方式：".$order->payment->title."<br>";//付款方式
                $ReturnMsg .= "繳費銀行代碼：".$record['BankCode']."<br>";//繳費銀行代碼
                $ReturnMsg .= "繳費虛擬帳號：".$record['vAccount']."<br>";//繳費虛擬帳號
                $ReturnMsg .= "繳費期限：".$record['ExpireDate']."<br>";//繳費期限
                $ReturnMsg .= "繳費金額：".$record['TradeAmt']."<br>";//交易金額
                $ReturnMsg .= "訂單成立時間：".$record['TradeDate']."<br>";//訂單成立時間
            } elseif ($record['PaymentNo']!="") {
                $ReturnMsg .= "付款方式：".$order->payment->title."<br>";//付款方式
                $ReturnMsg .= "繳費代碼：".$record['PaymentNo']."<br>";//繳費代碼
                $ReturnMsg .= "繳費期限：".$record['ExpireDate']."<br>";//繳費期限
                $ReturnMsg .= "繳費金額：".$record['TradeAmt']."<br>";//交易金額
                $ReturnMsg .= "訂單成立時間：".$record['TradeDate']."<br>";//訂單成立時間
            } else {
                $ReturnMsg .= "付款方式：".$order->payment->title."<br>";//付款方式
                $ReturnMsg .= "條碼第一段號碼：".$record['Barcode1']."<br>";//繳費條碼
                $ReturnMsg .= "條碼第二段號碼：".$record['Barcode2']."<br>";//繳費條碼
                $ReturnMsg .= "條碼第三段號碼：".$record['Barcode3']."<br>";//繳費條碼
                $ReturnMsg .= "繳費期限：".$record['ExpireDate']."<br>";//繳費期限
                $ReturnMsg .= "繳費金額：".$record['TradeAmt']."<br>";//交易金額
                $ReturnMsg .= "訂單成立時間：".$record['TradeDate']."<br>";//訂單成立時間
            }
        }
        $order->payment_return = $ReturnMsg?:null;
        $order->save();

        $form = $order->forms->first();
        if (!is_null($form->user_id)) {
            $user = $form->user;
        } elseif (!is_null($form->teacher_id)) {
            $user = $form->teacher;
        }

        // 寄通知信
        $this->SendMailTo($user, new OrderPayinfo($record));
    }
}
