<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Shipping;
use App\Mail\AdminOrderCreate;
use App\Mail\OrderCreate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class CartController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->prefix = 'cart';
    }

    public function index()
    {
        // request()->session()->forget('cart');
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->step = 1;

        $custom_page_title = (isset($data->seo_title) ? $data->seo_title : __('text.' . $this->prefix));
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];
        return view('frontend.' . $this->prefix . '.index', compact('data', 'cart', 'shippings', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function postIndex(Request $request)
    {
        if (!Session::has('cart')) {
            return redirect()->route('cart.index');
        }

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $validator = Validator::make($request->all(), [
            'shipping_type' => 'required',
            'shipping_id' => 'required',
            'hope_time' => 'required',
            'payment_id' => 'required',
            'name' => 'required',
            'sex' => 'nullable',
            'phone' => 'required|user_phone',
            'email' => 'required|email',
            // 'address' => 'required',
            'receiver_name' => 'required',
            'receiver_sex' => 'nullable',
            'receiver_phone' => 'required|user_phone',
            'receiver_email' => 'required|email',
            'receiver_city_id' => 'required_if:shipping_type,1',
            'receiver_region_id' => 'required_if:shipping_type,1',
            'receiver_address' => 'required',
        ], [
            'required_if' => __('validation.required'),
        ], [
            'receiver_name' => __('validation.attributes.name'),
            'receiver_sex' => __('validation.attributes.sex'),
            'receiver_phone' => __('validation.attributes.phone'),
            'receiver_email' => __('validation.attributes.email'),
            'receiver_city_id' => __('validation.attributes.city_id'),
            'receiver_region_id' => __('validation.attributes.region_id'),
            'receiver_address' => __('validation.attributes.address'),
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $cart->updateFormData($request->except(['_token']));
        $request->session()->put('cart', $cart);

        return redirect()->route('cart.checkout');
    }
    public function checkout(Request $request)
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->step = 2;

        if (count($cart->items)==0 || $cart->itemsChecked == 0) {
            return redirect()->route('cart.index')->with('error', '無商品可結帳');
        }

        $custom_page_title = (isset($data->seo_title) ? $data->seo_title : __('text.' . $this->prefix));
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
            [__('text.' . $prefix . '_checkout'), route($prefix . '.checkout'), __('text.' . $prefix . '_checkout')],
        ];
        return view('frontend.' . $this->prefix . '.checkout', compact('data', 'cart', 'shippings', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function postCheckout(Request $request)
    {
        if (!Session::has('cart')) {
            return redirect()->route('cart.index');
        }

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        if (count($cart->items)==0 || $cart->itemsChecked == 0) {
            return redirect()->route('cart.index')->with('error', '無商品可結帳');
        }
        // 檢查庫存
        $itemNotEnough = [];
        foreach ($cart->items as $props => $item) {
            $checkQtyEnough = $cart->checkQtyEnough($item['sku_id'], $item['qty'], $item['type']);
            if (!$checkQtyEnough['status']) {
                $itemNotEnough[$props] = $item;
                $itemNotEnough[$props]['msg'] = __('text.cart_sku_qty', ['qty' => $checkQtyEnough['qty']]);
            }
        }
        if (count($itemNotEnough)) {
            return redirect()->route('cart.index')
                ->with('itemNotEnough', $itemNotEnough)
                ->with('error', __('text.cart_sku_not_enough'));
        }
        $order = new Order($cart->formData->toArray());
        $order->items_total_price = $cart->totalPrice;
        $order->coupon_price = $cart->couponPrice;
        $order->bonus_price = $cart->bonusPrice;
        $order->shipping_price = $cart->shippingPrice;
        $order->payment_price = $cart->paymentPrice;
        $order->sum_price = $cart->sumPrice;
        $order->coupon_info = $cart->coupon?$cart->coupon->toArray():null;
        $order->shipping_info = $cart->shipping?$cart->shipping->toArray():null;
        $order->payment_info = $cart->payment?$cart->payment->toArray():null;
        // $order->tax = $cart->tax;
        $order->save();

        // 訂單品項
        $order->items()->createMany($cart->items);

        // 扣庫存
        foreach ($cart->items as $item) {
            $item['sku']->qty -= $item['qty'];
            $item['sku']->save();
        }

        // 優惠券
        if ($cart->coupon) {
            $order->coupons()->sync([$cart->coupon->id]);
        }
        // 紅利點數
        if (!is_null($order->user_id)) {
            // 消費獲得點數
            if ($cart->orderBonus) {
                $order->bonuses()->create([
                    'user_id' => $order->user_id,
                    'point' => $cart->orderBonus,
                    'date_on' => Carbon::now()->addDays($cart->bonusSetting->point_after_day)->toDateString(),
                    'date_off' => Carbon::createFromDate(Carbon::now()->addYears($cart->bonusSetting->limit_year)->year, 12, 31)->toDateString(),
                    'note' => $order->num.'消費獲得',
                ]);
            }
            // 是否使用紅利折抵
            if ($cart->bonusPrice) {
                $order->bonuses()->create([
                    'user_id' => $order->user_id,
                    'point' => -($cart->bonusPrice * $cart->bonusSetting->point_per_price),
                    'note' => $order->num.'消費折抵',
                ]);
            }
        }

        // 清空購物車
        $cart->finishCheckout();
        // request()->session()->forget('cart');

        //寄訂單成立通知
        $order->SendMailToAdmin(new AdminOrderCreate($order));
        $order->SendMailTo($order, new OrderCreate($order));

        // 進入付款流程
        return redirect()->route('order.pay', ['num'=>$order->num]);
    }
    public function result(Request $request)
    {
        if (Session::has('num')) {
            $num = Session::get('num');
            $prefix = $this->prefix;
            $data = Setting::getData($this->prefix);
            $order = Order::where('num', $num)->firstOrFail();

            // if ($data->user_id) {
            //     auth()->guard('web')->login($data->user);
            // }

            $custom_page_title = __('text.cart_result') . ' - ' . __('text.' . $prefix);
            $breadcrumb = [
                [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
                [__('text.' . $prefix . '_result'), route($prefix . '.result'), __('text.' . $prefix . '_result')],
            ];

            return view('frontend.' . $this->prefix . '.result', compact('data', 'order', 'custom_page_title', 'prefix', 'breadcrumb'));
        } else {
            return redirect()->route('user.order.index');
        }
    }
}
