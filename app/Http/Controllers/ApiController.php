<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use Carbon\Carbon;

class ApiController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->limit = 50;
    }

    public function getOrderPayinfo(Request $request)
    {
        $auth_web = auth()->guard('web')->user();
        $order = Order::find($request->id);
        if (!is_null($order->user_id) && !is_null($auth_web) && $order->user_id != $auth_web->id) {
            return response()->json([
                'message' => '查詢的訂單為會員訂單，請登入會員'
            ], 401);
        }

        if (!is_null($order)) {
            return response()->json([
                'message' => '取得付款資訊',
                'data' => nl2br($order->payment_return),
            ], 200);
        } else {
            return response()->json([
                'message' => '無法取得訂單資料'
            ], 404);
        }
    }
    public function getOrderDetail(Request $request)
    {
        $auth_web = auth()->guard('web')->user();
        $order = Order::find($request->id);
        if (!is_null($order->user_id) && !is_null($auth_web) && $order->user_id != $auth_web->id) {
            return response()->json([
                'message' => '查詢的訂單為會員訂單，請登入會員'
            ], 401);
        }
        if (!is_null($order)) {
            return response()->json([
                'message' => '取得訂單資訊',
                'data' => view('frontend.order._detail', ['data'=>$order])->render(),
            ], 200);
        } else {
            return response()->json([
                'message' => '無法取得訂單資料'
            ], 404);
        }
    }
}
