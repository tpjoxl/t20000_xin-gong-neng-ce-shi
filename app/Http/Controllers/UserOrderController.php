<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Setting;
use App\Payment\Ecpay;
use App\Traits\MailTrait;
use App\Traits\EcpayTrait;
use App\Traits\NewebpayTrait;

class UserOrderController extends Controller
{
    use MailTrait, EcpayTrait, NewebpayTrait;
    protected $model;

    public function __construct(Order $Order, User $User)
    {
        $this->middleware(['auth:web', 'auth.first_login'])->except(['pay', 'ecpayCheckout', 'ecpayReturn', 'ecpayPaymentInfo', 'ecpayOrderResult', 'ecpayClientRedirect']);
        $this->model = $Order;
        $this->user = $User;
        $this->prefix = 'user.order';
        $this->perPage = 10;
        $this->redirect_after_login = 'user.order.index';
        $this->redirect_after_logout = 'user.login';
        $this->ecpay = new Ecpay;
        $this->ecpayReturnRoute = 'user.order.ecpay.return';
        $this->ecpayPaymentInfoRoute = 'user.order.ecpay.payment_info';
        $this->ecpayOrderResultRoute = 'user.order.ecpay.order_result';
        $this->ecpayClientRedirectRoute = 'user.order.ecpay.client_redirect';
        $this->ecpayClientBackRoute = 'user.order.index';
        $this->ecpayResultRoute = 'cart.result';
    }
    protected function guard()
    {
        return Auth::guard('web');
    }

    public function index()
    {
        $prefix = $this->prefix;
        $data = $this->guard()->user();
        $page = Setting::getData('user');
        $years = $data->orders()
            ->selectRaw('year(created_at) as year')
            ->orderBy('year', 'desc')
            ->get()
            ->groupBy('year')
            ->keys();
        $items = $data->orders()
            ->selectRaw('*, year(created_at) as year')
            ->when(request()->filled('status'), function ($q) {
                $q->where('status', request('status'));
            })
            ->when(request()->filled('year'), function ($q) {
                $q->where('year', request('year'));
            })
            ->ordered()
            ->paginate($this->perPage);

        $custom_page_title = __('text.user_order') . ' - ' . __('text.user_centre');
        $breadcrumb = [
            [__('text.user_centre'), route($this->redirect_after_login), __('text.user_centre')],
            [__('text.user_order'), route($prefix . '.index'), __('text.user_order')],
        ];

        return view('frontend.' . $this->prefix . '.index', compact('data', 'page', 'years', 'items', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function detail($num)
    {
        $prefix = $this->prefix;
        $user = $this->guard()->user();
        $page = Setting::getData('user');
        $data = $user->orders()
            ->where('num', $num)
            ->firstOrFail();

        $custom_page_title = __('text.user_order') . ' - ' . __('text.user_centre');
        $breadcrumb = [
            [__('text.user_centre'), route($this->redirect_after_login), __('text.user_centre')],
            [__('text.user_order'), route($prefix . '.index'), __('text.user_order')],
        ];

        return view('frontend.' . $this->prefix . '.detail', compact('data', 'page', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
    public function cancel(Request $request)
    {
        if ($request->filled('id')) {
            $requestData = $request->all();
            $user = $this->guard()->user();
            $order = $user->orders()->find($request->id);
            if ($order->status != 1) {
                return back()->with('errorText', '訂單狀態為「'.$order->present()->status(1).'」時，才可以取消訂單');
            }
            $order->update($requestData);

            // 加回庫存
            foreach ($order->items as $item) {
                $item->sku->qty += $item->qty;
                $item->sku->save();
            }
            // 紅利無效
            $order->bonuses->each->update(['status'=>0]);

            return back()->with('successText', '已取消訂單');
        } else {
            return back()->with('errorText', '查無訂單，請重新操作');
        }
    }
    public function pay(Request $request)
    {
        $prefix = $this->prefix;
        $user = $this->guard()->user();
        if ($user) {
            $order = $user->orders()
                ->where('num', $request->num)
                ->firstOrFail();
        } else {
            $order = $this->model
                ->where('num', $request->num)
                ->firstOrFail();
        }
        if ($order->status != 1 || $order->payment_status != 1) {
            return back()->with('errorText', '訂單狀態為「'.$order->present()->status(1).'」時，才需要進行繳費');
        }

        $order->epaynumber = $order->generateEpaynumber();
        $order->save();

        if ($order->payment_info['provider'] == 'ECPAY') {
            $this->ecpayCheckout($order);
        } elseif ($order->payment_info['provider'] == 'NEWEBPAY') {
            $this->newebpayCheckout($order);
        } elseif ($order->payment_info['provider'] == 'LINEPAY') {
            $this->linepayCheckout($order);
        } elseif ($order->payment_info['provider'] == 'JKOPAY') {
            $this->jkopayCheckout($order);
        }
    }
}
