<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Contact;
use App\Models\Setting;
use App\Mail\AdminContact;
use App\Traits\ReCaptchaTrait;
use App\Traits\MailTrait;

class ContactController extends Controller
{
    protected $model, $category, $perPage;
    use ReCaptchaTrait, MailTrait;

    public function __construct(Contact $Contact)
    {
        $this->model = $Contact;
        $this->prefix = 'contact';
    }

    public function index()
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $model = $this->model;
        $custom_page_title = (isset($data->seo_title) ? $data->seo_title : __('text.' . $this->prefix));
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];
        return view('frontend.' . $this->prefix . '.index', compact('data', 'custom_page_title', 'prefix', 'model', 'breadcrumb'));
    }
    /**
     * 送出表單
     */
    public function formSubmit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'sex' => 'required',
            'email' => 'required|email',
            'phone' => 'required|user_phone',
            'message' => 'required',
            'g-recaptcha-response' => 'required',
        ], [], [
            'sex' => __('validation.attributes.sex_title'),
        ]);
        if ($validator->fails()) {
            // return dd($validator->getMessageBag());
            return back()->withErrors($validator)->withInput();
        }

        $recaptchaCheck = $this->ReCaptchaCheck();
        // dd($recaptchaCheck);
        if ($recaptchaCheck['status']) {
            // 儲存表單內容
            $dataSave = $request->except('g-recaptcha-response');
            $data = $this->model->make();
            $data->fill($dataSave);

            if ($data->save()) {
                if ($this->SendMailToAdmin(new AdminContact($data))) {
                    return back()->with('success', __('text.contact_success'))->with('successText', __('text.contact_success_txt'));
                } else {
                    return back()->with('error', __('text.contact_error'))->with('errorText', __('text.contact_error_txt'))->withInput();
                }
            } else {
                return back()->with('error', __('text.contact_error'))->with('errorText', __('text.contact_error_txt'))->withInput();
            }
        } else {
            return redirect()->back()->withInput()->withErrors($recaptchaCheck['errors']);
        }
    }
}
