<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class GeneratedController extends Controller
{
    protected $tags;

    public function __construct()
    {
    }
    public function siteMap()
    {
        $view = Cache()->remember('generated.sitemap', 0, function () {

            $posts = \App\Models\Product::display()->ordered()->first();
            $this->addTag(route('product.index'), $posts->updated_at, 'weekly', '0.5');

            $posts = \App\Models\ProductCategory::display()->tree()->get();
            foreach ($posts as $post) {
                $this->addTag(route('product.category', $post->slug), '', 'monthly', '0.5', $post);

                if ($post->children->count() > 0) {
                    foreach ($post->children as $child) {
                        $this->addTag(route('product.category', ['category' => $post->slug, 'category2' => $child->slug]), '', 'monthly', '0.6', $child);
                    }
                }
            }
            $posts = \App\Models\Product::display()->ordered()->get();
            if ($posts->count() > 0) {
                foreach ($posts as $post) {
                    if (is_null($post->url)) {
                        $this->addTag(route('product.detail', $post->slug), $post->updated_at, 'never', '0.8', $post);
                    }
                }
            }

            $posts = \App\Models\Blog::display()->ordered()->first();
            $this->addTag(route('blog.index'), $posts->updated_at, 'weekly', '0.5');

            $posts = \App\Models\BlogCategory::display()->tree()->get();
            foreach ($posts as $post) {
                $this->addTag(route('blog.category', $post->slug), '', 'monthly', '0.5', $post);

                if ($post->children->count() > 0) {
                    foreach ($post->children as $child) {
                        $this->addTag(route('blog.category', ['category' => $post->slug, 'category2' => $child->slug]), '', 'monthly', '0.6', $child);
                    }
                }
            }
            $posts = \App\Models\Blog::display()->ordered()->get();
            if ($posts->count() > 0) {
                foreach ($posts as $post) {
                    if (is_null($post->url)) {
                        $this->addTag(route('blog.detail', $post->slug), $post->updated_at, 'never', '0.8', $post);
                    }
                }
            }
            $post = Setting::getData('about');
            $this->addTag(route('about.index'), (!empty($post)) ? $post->updated_at : null, 'never', '0.5', $post);

            $post = Setting::getData('contact');
            $this->addTag(route('contact.index'), (!empty($post)) ? $post->updated_at : null, 'never', '0.5', $post);

            $post = Setting::getData('faq');
            $this->addTag(route('faq.index'), (!empty($post)) ? $post->updated_at : null, 'never', '0.5', $post);

            $post = Setting::getData('terms');
            $this->addTag(route('terms.index'), (!empty($post)) ? $post->updated_at : null, 'never', '0.1', $post);

            // $post = Setting::getData('privacy');
            // $this->addTag(route('privacy.index'), (!empty($post)) ? $post->updated_at : null, 'monthly', '0.1', $post);

            $posts = $this->tags;

            // return generated xml (string) , cache whole file

            return view('generated.sitemap', compact('posts'))->render();
        });

        return response($view)->header('Content-Type', 'text/xml');
    }

    /**
     * 加入標籤
     * @param string $loc
     * @param bool $lastmod
     * @param string $changefreq
     * @param float $priority
     */
    public function addTag($loc, $lastmod = false, $changefreq = 'yearly', $priority = 0.5, $data = null)
    {
        $setting = new Setting();
        $post = new static();
        $post->loc = urldecode($loc);
        if ($lastmod) {
            $post->lastmod = (Carbon::parse($lastmod))->tz('UTC')->toAtomString();
        }
        $post->changefreq = $changefreq;
        $post->priority = $priority;
        $locales = app('frontendLocales');
        if ($locales->count() > 1) {
            foreach ($locales as $locale) {
                $post->alternate[$locale->code] = $setting->present()->langUrl($data, $locale->code, $loc);
            }
        }

        $this->tags[] = $post;
    }

    /**
     * 建立多語Alternate
     * @param string $loc
     * @param object $data
     */
    public function alternate($loc, $data)
    {
        // dd(url('/'));
        $arr = array();
        $lang_datas = app('frontendLocales');
        // $sitesetting = SiteSetting::first();

        //$segments = request()->segments();
        $segments = explode('/', str_replace(url('/').'/', '', urldecode($loc)));
        $output = $segments;

        $lang = $segments[0];
        $langExplode = explode('-', $lang);
        if (count($langExplode) > 1) {
            $langExplode[count($langExplode) - 1] = mb_strtoupper($langExplode[count($langExplode) - 1]);
        }
        $lang = implode('-', $langExplode);
        if ($lang_datas->where('code', $lang)->count() == 0) {
            return $arr;
        }

        foreach ($lang_datas as $locale) {

            foreach ($segments as $key => $segment) {
                if ($key == 0) {
                    $output[$key] = strtolower($locale->code);
                } else {
                    $route_key = array_search($segment, Lang::get('routes'));
                    if ($route_key && Lang::has('routes.' . $route_key, $locale->code)) {
                        $output[$key] = __('routes.' . $route_key, [], $locale->code);
                    } else {
                        if (isset($data) && !is_null($data) && $data->translate($locale->code)) {
                            $output[$key] = $data->translate($locale->code)->slug;
                        } else {
                            $output[$key] = $segment;
                        }
                    }
                }
            }
            $full_url = url('/') . '/' . implode('/', $output);

            $arr[$locale->code] = $full_url;
        }

        return $arr;
    }
}
