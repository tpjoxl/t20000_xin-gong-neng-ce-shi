<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    protected $model, $category, $perPage;

    public function __construct(Product $Product)
    {
        $this->model = $Product;
        $this->prefix = 'search';
        $this->perPage = 12;
    }
    public function index(Request $request)
    {
        $prefix = $this->prefix;
        $data = $this->model;
        // 搜尋的字串如果有空格就把它分成兩個搜尋條件
        $searchArr = preg_split("/\s+/", $request->q);
        $data->meta_robots = 'noindex,follow';
        $query = $this->model
            ->with(['skus', 'categories'])
            ->whereHas('skus', function($q) use ($searchArr) {
                $q->where('num', 'like', '%' . $searchArr[0] . '%');
                if (count($searchArr) > 1) {
                    for ($i = 1; $i < count($searchArr); $i++) {
                        $q->orWhere('num', 'like', '%' . $searchArr[$i] . '%');
                    }
                }
            })
            ->orWhereHas('categories', function($q) use ($searchArr) {
                $q->whereTranslationLike('title', '%' . $searchArr[0] . '%');
                if (count($searchArr) > 1) {
                    for ($i = 1; $i < count($searchArr); $i++) {
                        $q->orWhereTranslationLike('title', '%' . $searchArr[$i] . '%');
                    }
                }
            })
            ->orWhere(function ($q) use ($searchArr) {
                $q->whereTranslationLike('title', '%' . $searchArr[0] . '%')
                    ->orWhereTranslationLike('description', '%' . $searchArr[0] . '%')
                    ->orWhereTranslationLike('text', '%' . $searchArr[0] . '%');
                if (count($searchArr) > 1) {
                    for ($i = 1; $i < count($searchArr); $i++) {
                        $q->orWhereTranslationLike('title', '%' . $searchArr[$i] . '%')
                            ->orWhereTranslationLike('description', '%' . $searchArr[$i] . '%')
                            ->orWhereTranslationLike('text', '%' . $searchArr[$i] . '%');
                    }
                }
            })
            ->has('skus')
            ->display()
            ->ordered()
            ->get()
            ->filter(function ($item) use ($searchArr) {
                foreach ($item->translations as $translation) {
                    if (Str::contains(trim($translation->title), $searchArr) || Str::contains(trim($translation->description), $searchArr) || Str::contains(trim(strip_tags($translation->text)), $searchArr)) {
                        return true;
                        break;
                    }
                }
                foreach ($item->skus as $sku) {
                    if (Str::contains(trim($sku->num), $searchArr)) {
                        return true;
                        break;
                    }
                }
                foreach ($item->categories as $category) {
                    if (Str::contains(trim($category->title), $searchArr)) {
                        return true;
                        break;
                    }
                }
                return false;
            });

        $items = paginate($query, $this->perPage);

        $custom_page_title = __('text.search') . ' - ' . __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.product_search'), '', __('text.product_search')],
        ];

        return view('frontend.' . $this->prefix . '.index', compact('prefix', 'data', 'items', 'custom_page_title', 'breadcrumb'));
    }
}
