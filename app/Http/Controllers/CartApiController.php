<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Sku;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartApiController extends Controller
{
    protected $cart;

    public function __construct(Cart $Cart)
    {
        $this->cart = $Cart;
    }

    /**
     * 加入購物車品項
     */
    public function add(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $props = "sku-$request->sku_id";
        $cartItem = !empty($this->cart->items) && array_key_exists($props, $this->cart->items) ? $this->cart->items[$props] : null;
        $cartItemQty = $cartItem ? $cartItem['qty'] : 0;
        $checkQtyEnough = $this->cart->checkQtyEnough($request->sku_id, $request->qty+$cartItemQty);
        if ($checkQtyEnough['status']) {
            $this->cart->add($request->sku_id, $request->qty);

            $request->session()->put('cart', $this->cart);
            return response()->json([
                'message' => __('text.cart_add_item_success'),
                'cart' => $request->session()->get('cart'),
                'cartLayout' => view('frontend.cart._cart', ['cart'=>$request->session()->get('cart')])->render(),
                'topCartLayout' => view('frontend.layouts.top_cart', ['cart'=>$request->session()->get('cart')])->render(),
            ]);
        } else {
            return response()->json([
                'message' => __('text.cart_sku_not_enough'),
            ], 400);
        }
    }
    /**
     * 刪除購物車品項
     */
    public function delete(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->delete($request->sku_id);

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_delete_item_success'),
            'cart' => $request->session()->get('cart'),
            'cartLayout' => view('frontend.cart._cart', ['cart'=>$request->session()->get('cart')])->render(),
            'topCartLayout' => view('frontend.layouts.top_cart', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新購物車品項選取狀態
     */
    public function updateItemChecked(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updateItemChecked($request->sku_id, $request->checked);

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_item_checked_update'),
            'cart' => $request->session()->get('cart'),
            'cartLayout' => view('frontend.cart._cart', ['cart'=>$request->session()->get('cart')])->render(),
            'topCartLayout' => view('frontend.layouts.top_cart', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新購物車品項數量
     */
    public function updateQty(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $checkQtyEnough = $this->cart->checkQtyEnough($request->sku_id, $request->qty);
        if ($checkQtyEnough['status']) {
            $this->cart->updateQty($request->sku_id, $request->qty);

            $request->session()->put('cart', $this->cart);
            return response()->json([
                'message' => __('text.cart_qty_update'),
                'cart' => $request->session()->get('cart'),
                'cartLayout' => view('frontend.cart._cart', ['cart'=>$request->session()->get('cart')])->render(),
                'topCartLayout' => view('frontend.layouts.top_cart', ['cart'=>$request->session()->get('cart')])->render(),
            ]);
        } else {
            return response()->json([
                'message' => __('text.cart_sku_not_enough') .'<br>'. __('text.cart_sku_qty', ['qty' => $checkQtyEnough['qty']]),
            ], 400);
        }
    }
    /**
     * 更新運送方式
     */
    public function updateShipping(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updateShipping($request->shipping_id);
        $this->cart->updateSum();

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_shipping_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新運送類型
     */
    public function updateShippingType(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updateShippingType($request->type);

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_shipping_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新付款方式
     */
    public function updatePayment(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updatePayment($request->payment_id);
        $this->cart->updateSum();

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_payment_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新使用優惠券
     */
    public function updateCoupon(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);
        $coupon_error = false;
        if ($request->filled('coupon')) {
            $coupon = $this->cart->checkCoupon($request->coupon);
            if (is_null($coupon) || $this->cart->totalPrice < $coupon->min_price) {
                $coupon_error = true;
                $coupon = null;
            }
        } else {
            $coupon = null;
        }

        $this->cart->updateCoupon($coupon);
        $this->cart->updateSum();

        $request->session()->put('cart', $this->cart);

        if ($coupon_error) {
            return response()->json([
                'message' => __('text.cart_coupon_error'),
                'cart' => $request->session()->get('cart'),
                'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
                'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
            ], 400);
        }
        return response()->json([
            'message' => __('text.cart_coupon_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新使用紅利
     */
    public function updateBonus(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updateBonus($request->bonus);
        $this->cart->updateSum();

        $request->session()->put('cart', $this->cart);
        return response()->json([
            'message' => __('text.cart_bonus_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 更新偏鄉運費
     */
    public function updateRemote(Request $request)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $this->cart = new Cart($oldCart);

        $this->cart->updateRemote($request->region_id);
        $this->cart->updateSum();

        $request->session()->put('cart', $this->cart);

        if (!$request->filled('region_id')) {
            return response()->json([
                'message' => __('text.cart_remote_error'),
                'cart' => $request->session()->get('cart'),
                'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
                'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
            ], 400);
        }
        return response()->json([
            'message' => __('text.cart_shipping_update'),
            'cart' => $request->session()->get('cart'),
            'cartTableLayout' => view('frontend.cart._cart_table', ['cart'=>$request->session()->get('cart')])->render(),
            'cartSumLayout' => view('frontend.cart._cart_sum', ['cart'=>$request->session()->get('cart')])->render(),
        ]);
    }
    /**
     * 切換商品規格時，更新商品資訊
     */
    public function changeSku(Request $request)
    {
        $currentSku = Sku::find($request->sku_id);
        $product = $currentSku->product()
            ->with(['skus'])->first();
        if (is_null($currentSku)) {
            return response()->json([
                'message' => __('text.cart_sku_no_data'),
            ], 400);
        }

        if ($request->holder == '#detial-sku-info') {
            $layout = view('frontend.product.detail_sku_info', ['data'=>$product, 'currentSku'=>$currentSku])->render();
        } elseif ($request->holder == '#list-item'.$product->id.'-sku-info') {
            $layout = view('frontend.product.list_sku_info', ['item'=>$product, 'currentSku'=>$currentSku])->render();
        }

        return response()->json([
            'message' => __('text.cart_change_sku_success'),
            'layout' => $layout,
        ]);
    }
    /**
     * 清空購物車
     */
    public function clear(Request $request)
    {
        $request->session()->forget('cart');
        return redirect()->route('cart.index');
    }
}
