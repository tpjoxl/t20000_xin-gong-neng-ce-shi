<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Setting;

class AboutController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->prefix = 'about';
    }

    public function index()
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $custom_page_title = (isset($data->seo_title) ? $data->seo_title : __('text.' . $this->prefix));
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];
        return view('frontend.' . $this->prefix . '.index', compact('data', 'custom_page_title', 'prefix', 'breadcrumb'));
    }
}
