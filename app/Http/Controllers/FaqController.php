<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\Setting;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    protected $model, $category, $perPage;

    public function __construct(Faq $Faq)
    {
        $this->model = $Faq;
        $this->prefix = 'faq';
        $this->perPage = 10;
    }
    public function index()
    {
        $prefix = $this->prefix;
        $data = Setting::getData($this->prefix);
        $items = $this->model
            ->display()
            ->ordered()
            ->paginate($this->perPage);

        $custom_page_title = $data->seo_title ? $data->seo_title : __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];

        return view('frontend.' . $this->prefix . '.index', compact('prefix', 'data', 'items', 'breadcrumb', 'custom_page_title'));
    }
}
