<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class LineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToLine()
    {
        // 紀錄登入前造訪網址
        if (request()->has('redirect_url')) {
            session()->put('redirect_url', request()->redirect_url);
        } else {
            if (!empty($_SERVER['HTTP_REFERER'])) {
              // Log::info('Log', [$_SERVER['HTTP_REFERER']]);
              session()->put('redirect_url', $_SERVER['HTTP_REFERER']);
            }
        }
        return Socialite::driver('line')->scopes(['email'])->redirect();
        // return Socialite::driver('line')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleLineCallback()
    {
        session()->put('state', request()->input('state'));
        $redirect_url = Session::get('redirect_url');

        try {
            $scuser = Socialite::driver('line')->user();
            if (!empty($scuser)) {
              $user = new User();
              $create = [
                'email' => $scuser->getEmail(),
                'name' => $scuser->getName(),
                'account' => $scuser->getId(),
                'line_id' => $scuser->getId(),
                'is_line' => 1,
                'first_login' => 1,
                'status' => 1,
                'email_verified' => 1,
              ];

              // 連結現有Email帳號
              $check = $user::where('email', $create['email'])->whereNull('line_id')->first();
              if(!empty($check)){
                $check->update(['line_id' => $create['line_id']]);
              }
              // 註冊新帳號
              $check = $user::where('line_id', $create['line_id'])->first();
              if(is_null($check)){
                $user->fill($create)->save();
                Auth::loginUsingId($user->id);
              } else {
                Auth::loginUsingId($check->id);
              }
              Auth::guard('web')->user()->update([
                'login_at' => \Carbon\Carbon::now(),
                'ip' => \Request::ip()
              ]);
            }

            // 如果成功，就導向到指定頁面
            if ($redirect_url) {
                return redirect()->to($redirect_url);
            }

            return redirect()->route('home');
        } catch (Exception $e) {
            Log::error('Line login callback error.', ['exception'=>$e]);

            return redirect()->route('user.login')->with('error', 'Line登入失敗');
        }
    }
}
