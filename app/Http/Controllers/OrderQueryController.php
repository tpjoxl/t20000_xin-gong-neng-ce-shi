<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Models\Order;
use App\Models\Setting;
use Illuminate\Support\Str;
use Carbon\Carbon;

class OrderQueryController extends Controller
{
    protected $model, $category, $perPage;

    public function __construct(Order $Order)
    {
        $this->model = $Order;
        $this->prefix = 'order_query';
        $this->perPage = 10;
    }
    public function index()
    {
        $prefix = $this->prefix;
        $data = null;

        $custom_page_title = __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];

        return view('frontend.order.query', compact('prefix', 'data', 'items', 'breadcrumb', 'custom_page_title'));
    }
    public function postIndex(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required|user_phone',
            'g-recaptcha-response' => 'required',
          ], [], []);
          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->except('phone'));
          }
          $data = $this->model
            ->whereNull('user_id')
            ->where('email', $request->email)
            ->where('phone', $request->phone)
            ->get();
      
          if (is_null($data)) {
            return redirect()->route('order_query.index')->with('error', '查無此訂單資料，請確認輸入的資料是否正確。如有任何問題請與我們聯絡，謝謝！')->withInput($request->except('phone'));
          }
          return redirect()->route('order_query.result')->with('orderid', $data->pluck('id')->toArray());
    }
    public function result(Request $request)
    {
        $prefix = $this->prefix;
        $data = null;

        $custom_page_title = __('text.' . $this->prefix);
        $breadcrumb = [
            [__('text.' . $prefix), route($prefix . '.index'), __('text.' . $prefix)],
        ];

        if (Session::has('orderid')) {
            $items = $this->model->whereIn('id', Session::get('orderid'))->where('created_at', '<=', Carbon::today()->addMonths(3))->get();
            if (count($items)==0) {
                return redirect()->route('order_query.index')->with('error', '查無此訂單資料，請確認輸入的資料是否正確。如有任何問題請與我們聯絡，謝謝！')->withInput($request->except('phone'));
            }
            return view('frontend.order.result', compact('data', 'items', 'breadcrumb', 'custom_page_title', 'prefix'));
        } else {
            return redirect()->route('order_query.index')->with('error', '瀏覽時間已逾時，請重新查詢訂單');
        }
    }
}
