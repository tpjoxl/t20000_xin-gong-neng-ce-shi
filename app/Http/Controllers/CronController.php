<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Traits\MailTrait;
use App\Models\User;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Setting;
use App\Mail\UserBonusExpired;
use App\Mail\UserBirthdayCoupon;
use Illuminate\Support\Facades\Log;

class CronController extends Controller
{
    use MailTrait;
    // 每年12/1 紅利到期通知
    // 0 0 1 12 * /usr/bin/curl -l {domain}/cron/bonus_expired > /dev/null 2>&2
    public function bonusExpired()
    {
        $users = User::whereHas('bonuses', function($q) {
            $q->where('date_off', (new Carbon('last day of this year'))->toDateString());
        })->get();
        // dd($users->implode('id', ', '));
        if ($users->count()) {
            $this->SendMailBcc($users, new UserBonusExpired());

            echo '紅利點數到期通知發送成功：'.$users->implode('id', ', ').'<br>';
        } else {
            echo '無會員紅利到期';
        }

        Log::channel('cron')->info('紅利點數到期通知', [
            'users' => $users->implode('id', ', '),
        ]);
    }
    // 每個月1號 建立&發送生日優惠券
    // 0 0 1 * * /usr/bin/curl -l {domain}/cron/birthday_coupon > /dev/null 2>&2
    public function birthdayCoupon()
    {
        $couponSetting = Setting::getData('coupon');
        $today = Carbon::today();
        $first = new Carbon('first day of this month');
        $last = new Carbon('last day of this month');
        $users = User::whereRaw('month(birthday) = month(NOW())')
            ->get();

        // 一般會員
        $code = 'BIRTH' . $today->format('Ym');
        $coupon = Coupon::where('code', $code)->first();
        if (is_null($coupon) && $users->where('type', 1)->count() > 0) {
            $coupon = Coupon::create([
                'title' => $today->format('Ym') . '生日優惠券',
                'code' => $code,
                'type' => 2, // 百分比
                'number' => $couponSetting->birthday_number_1,
                'min_price' => $couponSetting->birthday_min_price_1,
                'date_on' => $first->format('Y-m-d'),
                'date_off' => $last->format('Y-m-d'),
                'use_type' => 2, // 指定會員
            ]);
            $coupon->users()->sync($users->where('type', 1)->pluck('id')->toArray());

            $this->SendMailBcc($users->where('type', 1), new UserBirthdayCoupon($coupon));

            echo '一般會員 生日優惠券發送成功：'.$users->where('type', 1)->implode('id', ', ').'<br>';
        } else {
            echo '一般會員 生日優惠券已存在or無當月壽星的會員<br>';
        }
        Log::channel('cron')->info('生日優惠券發送通知(一般會員)', [
            'users' => $users->where('type', 1)->implode('id', ', '),
        ]);

        // VIP會員
        $code_vip = 'BIRTH' . $today->format('Ym') . 'VIP';
        $coupon_vip = Coupon::where('code', $code_vip)->first();
        if (is_null($coupon_vip) && $users->where('type', 2)->count() > 0) {
            $coupon_vip = Coupon::create([
                'title' => $today->format('Ym') . '生日優惠券(VIP)',
                'code' => $code,
                'type' => 2, // 百分比
                'number' => $couponSetting->birthday_number_2,
                'min_price' => $couponSetting->birthday_min_price_2,
                'date_on' => $first->format('Y-m-d'),
                'date_off' => $last->format('Y-m-d'),
                'use_type' => 2, // 指定會員
            ]);
            $coupon_vip->users()->sync($users->where('type', 2)->pluck('id')->toArray());

            $this->SendMailBcc($users->where('type', 2), new UserBirthdayCoupon($coupon_vip));

            echo 'VIP會員 生日優惠券發送成功：'.$users->where('type', 2)->implode('id', ', ').'<br>';
        } else {
            echo 'VIP會員 生日優惠券已存在or無當月壽星的會員<br>';
        }
        Log::channel('cron')->info('生日優惠券發送通知(VIP會員)', [
            'users' => $users->where('type', 2)->implode('id', ', '),
        ]);
    }
    // 每天0點 訂單出貨後7天自動變已完成
    // 0 0 * * * /usr/bin/curl -l {domain}/cron/order_completed > /dev/null 2>&2
    public function orderCompleted()
    {
        $orders = Order::where('status', 4)
            ->get()
            ->filter(function ($item, $key) {
                $statusLog = $item->logs->where('type', 1)->where('status', 4)->sortByDesc('created_at')->first();
                return $statusLog->created_at->addDays(7)->toDateString() <= Carbon::today()->toDateString();
            });
        $orders->each->update(['status', 5]);
    }
}
