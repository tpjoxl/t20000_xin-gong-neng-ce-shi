<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Product;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::display()
          ->ordered()
          ->get();
        $products = Product::with([
          'images' => function($q) {
            $q->orderBy('is_cover', 'desc')->ordered();
          }
        ])
        ->has('skus')
          ->where('home_status', 1)->display()->ordered()->limit(8)->get();

        return view('frontend.home', compact('banners', 'products'));
    }
}
