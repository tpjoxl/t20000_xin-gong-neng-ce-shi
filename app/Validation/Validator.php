<?php
namespace App\Validation;

use Illuminate\Validation\Validator as BaseValidator;
use App\Helpers\ArrHelper as Arr;

class Validator extends BaseValidator
{
    public function validateEmails($attribute, $value, $parameters, $validator)
    {
        $email_arr = explode(',', preg_replace('/\s(?=)/', '', $value));
        foreach ($email_arr as $email) {
            if (!$this->validateEmail($attribute, $email, $parameters)) {
                return false;
            }
        }
        return true;
    }
    public function validateNoneHtml($attribute, $value, $parameters, $validator)
    {
        return !(preg_match("/<[^<]+>/",$value,$m) != 0);
    }
    public function validateUserPassword($attribute, $value, $parameters, $validator)
    {
        $regex = '/^[A-Za-z0-9]{8,}$/';
        return $this->validateRegex($attribute, $value, [$regex]);
    }
    public function validateUserPhone($attribute, $value, $parameters, $validator)
    {
        $regex = '/\d$/';
        // $regex = '/0\d{1,3}-?\d{3,4}-?\d{3,4}(#\d{3,4})?$/';
        // $regex = '/0\d{1,2}-?\d{6,8}$/';
        // $regex = '/09\d{2}-?\d{3}-?\d{3}$/';

        return $this->validateRegex($attribute, $value, [$regex]);
    }

    public function validateBetweenIf($attribute, $value, $parameters, $validator)
    {
        $this->requireParameterCount(4, $parameters, 'between_if');

        [$values, $other] = $this->prepareValuesAndOther(array_slice($parameters, 2));

        if (in_array($other, $values)) {
            return $this->validateBetween($attribute, $value, array_slice($parameters, 0, 2));
        }

        return true;
    }
    protected function replaceBetweenIf($message, $attribute, $rule, $parameters)
    {
        $parameters[3] = $this->getDisplayableValue($parameters[2], Arr::get($this->data, $parameters[2]));

        $parameters[2] = $this->getDisplayableAttribute($parameters[2]);

        return str_replace([':min', ':max', ':other', ':value'], $parameters, $message);
    }
}
