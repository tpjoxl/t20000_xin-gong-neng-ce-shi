<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Setting;
use App\Models\User;

class UserRegister extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $User)
    {
        $this->data = $User;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting = app('siteSettings');
        $subject = $setting->name.' - 會員註冊通知';
        return $this->markdown('emails.user_register')
                    ->subject($subject);
    }
}
