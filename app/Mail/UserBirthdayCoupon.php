<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Setting;
use App\Models\Coupon;

class UserBirthdayCoupon extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Coupon $Coupon)
    {
        $this->data = $Coupon;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting = app('siteSettings');
      $subject = $setting->name.' - 生日優惠券發送通知';
        return $this->markdown('emails.user_birthday_coupon')
                    ->subject($subject);
    }
}
