<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Setting;
use App\Models\Coupon;

class UserCoupon extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Coupon $Coupon)
    {
        $this->data = $Coupon;
        $content = Setting::getData('mail.user.coupon');
        $setting = app('siteSettings');
        $replaces = [
            'site_name' => $setting->name,
            'user_name' => $this->data->user->name,
        ];
        $content->title = replace_mail_content_params($content->title, $replaces);
        $content->text = replace_mail_content_params($content->text, $replaces);
        $this->content = $content->toArray();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = $this->content['title'];
        return $this->markdown('emails.user_coupon')
                    ->subject($subject);
    }
}
