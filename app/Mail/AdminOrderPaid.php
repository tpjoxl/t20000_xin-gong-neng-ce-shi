<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Setting;
use App\Models\EcpayRecord;

class AdminOrderPaid extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EcpayRecord $record)
    {
        $this->data = $record;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting = app('siteSettings');
        $subject = $setting->name.' - 訂單付款通知';
        return $this->markdown('emails.admin_order_paid')
                    ->subject($subject);
    }
}
