<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Setting;

class UserForgot extends Mailable
{
    use Queueable, SerializesModels;
    public $data, $new_psw;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $User, $new_psw)
    {
        $this->data = $User;
        $this->new_psw = $new_psw;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting = app('siteSettings');
        $subject = $setting->name.' - 忘記密碼通知';
        return $this->markdown('emails.user_forgot')
                    ->subject($subject);
    }
}
