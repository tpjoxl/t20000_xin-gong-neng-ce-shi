<?php

namespace App\Presenters;

class AdminPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1 => '<span class="label label-success">' . __("backend.active", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.inactive", [], $this->locale) . '</span>',
        ];
    }
    public function group_name()
    {
        return $this->group->title;
    }
}
