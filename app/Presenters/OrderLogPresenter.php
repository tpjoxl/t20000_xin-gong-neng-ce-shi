<?php

namespace App\Presenters;

class OrderLogPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->type_arr = [
            1 => '<span class="label bg-blue">訂單狀態</span>',
            2 => '<span class="label bg-yellow">付款狀態</span>',
            3 => '<span class="label bg-green">貨品狀態</span>',
        ];
    }
    public function type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->type_arr) ? $this->type_arr[$val] : head($this->type_arr);
        }
        return $this->type_arr;
    }
}
