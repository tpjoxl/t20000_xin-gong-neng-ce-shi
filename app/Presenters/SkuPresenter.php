<?php

namespace App\Presenters;

class SkuPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->usein_arr = [
            1 => '產品',
            2 => '加價購',
        ];
        $this->price_type_arr = [
            1 => '原價',
            2 => '優惠價',
        ];
    }
    public function usein($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->usein_arr) ? $this->usein_arr[$val] : head($this->usein_arr);
        }

        return $this->usein_arr;
    }
    public function price_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->price_type_arr) ? $this->price_type_arr[$val] : head($this->price_type_arr);
        }

        return $this->price_type_arr;
    }
}
