<?php

namespace App\Presenters;

class OrderPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1=>'<span class="text-red">待付款</span>',
            2=>'<span class="text-red">已取消</span>',
            3=>'<span class="text-light-blue">待處理</span>',
            4=>'<span class="text-red">已出貨</span>',
            5=>'<span class="text-green">已完成</span>',
        ];
        $this->payment_status_arr = [
            1=>'未付款',
            2=>'<span class="text-red">付款失敗</span>',
            3=>'<span class="text-yellow">取號成功(待付款)</span>',
            4=>'<span class="text-green">已付款</span>',
            5=>'超過付款時間',
            6=>'已退款',
        ];
        $this->shipping_status_arr = [
            1=>'備貨中',
            2=>'<span class="text-light-blue">已出貨</span>',
            3=>'<span class="text-aqua">已到達</span>',
            4=>'<span class="text-green">已取貨</span>',
            5=>'<span class="text-red">已退貨</span>',
        ];
    }
    public function payment_status($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->payment_status_arr) ? $this->payment_status_arr[$val] : head($this->payment_status_arr);
        }
        return $this->payment_status_arr;
    }
    public function shipping_status($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->shipping_status_arr) ? $this->shipping_status_arr[$val] : head($this->shipping_status_arr);
        }
        return $this->shipping_status_arr;
    }
}
