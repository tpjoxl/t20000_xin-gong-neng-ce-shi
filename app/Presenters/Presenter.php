<?php

namespace App\Presenters;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Arr;

class Presenter extends \Laracasts\Presenter\Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);
        if (Request::route() && explode('.', Request::route()->getName())[0] == 'backend') {
            $this->locale = env('BACKEND_LOCALE');
        } else {
            $this->locale = app()->getLocale();
        }

        $this->status_arr = [
            1 => '<span class="label label-success">' . __("backend.enable", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.disable", [], $this->locale) . '</span>',
        ];
        $this->target_arr = [
            '' => __("backend.target_self", [], $this->locale),
            '_blank' => __("backend.target_blank", [], $this->locale),
        ];
        $this->active_arr = [
            1 => '<span class="label label-success">' . __("backend.active", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.inactive", [], $this->locale) . '</span>',
        ];
        $this->sex_arr = [
            0 => __("backend.sex_m", [], $this->locale),
            1 => __("backend.sex_f", [], $this->locale),
            2 => __("backend.sex_other", [], $this->locale),
        ];
        $this->sex_title_arr = [
            0 => __("backend.sex_title_m", [], $this->locale),
            1 => __("backend.sex_title_f", [], $this->locale),
            2 => __("backend.sex_other", [], $this->locale),
        ];
        $this->yes_or_no_arr = [
            1 => __("backend.yes", [], $this->locale),
            0 => __("backend.no", [], $this->locale),
        ];
        $this->user_type_arr = [
            1 => '一般',
            // 2 => '特約',
            // 3 => '批發',
        ];
        $this->time_slot_arr = [
            '11:00-12:00' => '11:00-12:00',
            '12:00-13:00' => '12:00-13:00',
            '13:00-14:00' => '13:00-14:00',
            '14:00-15:00' => '14:00-15:00',
            '15:00-16:00' => '15:00-16:00',
            '16:00-17:00' => '16:00-17:00',
            '17:00-18:00' => '17:00-18:00',
            '18:00-19:00' => '18:00-19:00',
            '19:00-20:00' => '19:00-20:00',
            '20:00-21:00' => '20:00-21:00',
        ];
        $this->email_verified_arr = [
            0 => '<span class="label label-danger">' . __("backend.email_verified_0", [], $this->locale) . '</span>',
            1 => '<span class="label label-success">' . __("backend.email_verified_1", [], $this->locale) . '</span>',
        ];
    }

    // 顯示狀態
    public function status($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->status_arr) ? $this->status_arr[$val] : head($this->status_arr);
        }
        return $this->status_arr;
    }

    // 連結開啟方式
    public function target($val = '')
    {
        if ($val !== '') {
            return array_key_exists($val, $this->target_arr) ? $this->target_arr[$val] : head($this->target_arr);
        }

        return $this->target_arr;
    }

    // 啟用狀態
    public function active($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->active_arr) ? $this->active_arr[$val] : head($this->active_arr);
        }

        return $this->active_arr;
    }
    // 性別
    public function sex($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->sex_arr) ? $this->sex_arr[$val] : head($this->sex_arr);
        }

        return $this->sex_arr;
    }
    // 稱謂
    public function sex_title($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->sex_title_arr) ? $this->sex_title_arr[$val] : head($this->sex_title_arr);
        }

        return $this->sex_title_arr;
    }
    public function date_range($date_on, $date_off, $divider = '～')
    {
        return ($date_on || $date_off)?$date_on.'～'.$date_off:'';
    }
    public function time_slot($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->time_slot_arr) ? $this->time_slot_arr[$val] : $this->time_slot_arr[0];
        }

        return $this->time_slot_arr;
    }
    public function email_verified($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->email_verified_arr) ? $this->email_verified_arr[$val] : head($this->email_verified_arr);
        }

        return $this->email_verified_arr;
    }
    public function yes_or_no($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->yes_or_no_arr) ? $this->yes_or_no_arr[$val] : head($this->yes_or_no_arr);
        }

        return $this->yes_or_no_arr;
    }
    // 會員層級
    public function user_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->user_type_arr) ? $this->user_type_arr[$val] : head($this->user_type_arr);
        }
        return $this->user_type_arr;
    }
    public function checkSetTop()
    {
        return $this->is_top ? '<i class="fa fa-check-square"></i>' : '';
    }
    public function langUrl($data = null, $lang = null, $requestUrl = null)
    {
        $urlKeys = app('allRoutesKey');
        $langUrl = [];
        $langDatas = config('translatable.locales');
        if (empty($urlKeys)) {
            foreach ($langDatas as $langCode => $langTitle) {
                foreach (__('routes', [], $langCode) as $key => $value) {
                    $urlKeys[$langCode]['routes.'.$key] = $value;
                }
            }
        }
        if (is_null($requestUrl)) {
            $segments = request()->segments();
            $route = request()->route();
        } else {
            $segments = explode('/', str_replace(url('/').'/', '', $requestUrl));
            $route = collect(\Route::getRoutes())->first(function($route) use($requestUrl){
                return $route->matches(request()->create($requestUrl));
            });
        }
        if (is_null($route)) {
            if (!is_null($lang)) {
                return null;
            } else {
                return [];
            }
        }
        $uriArr = array_filter(explode('/', $route->uri()));

        foreach ($langDatas as $langCode) {
            $url = [];
            if (!empty($uriArr)) {
                foreach ($uriArr as $key => $uri) {
                    if ($key == 0) {
                      $url[$key] = strtolower($langCode);
                    } else {
                      if (preg_match_all('/\{(\w*)\??\}/', $uri, $match)) {
                        if(isset($data) && !is_null($data) && count(array_intersect($match[1], ['slug', 'category', 'category2']))) {
                          $url[$key] = $data->translate($langCode)->slug;
                        } else {
                          if (!empty($segments[$key])) {
                            $url[$key] = $segments[$key];
                          }
                        }
                      } else {
                        $route_key = array_search($uri, ($urlKeys->get(app()->getLocale()) ?? Lang::get('routes')));
                        if ($route_key && (Arr::has($urlKeys->get($langCode), $route_key) || Lang::has('routes.' . $route_key, $langCode))) {
                          $url[$key] = Arr::get($urlKeys->get($langCode), $route_key) ?? Lang::get('routes.' . $route_key, [], $langCode);
                        } else {
                          $url[$key] = $segments[$key];
                        }
                      }
                    }
                  }
            }

            $langUrl[$langCode] = url()->to('/') . '/' . implode('/', $url);
        }

        if (!is_null($lang)) {
            return array_key_exists($lang, $langUrl) ? $langUrl[$lang] : head($langUrl);
        } else {
            return $langUrl;
        }
    }
    // 價格顯示
    public function price($price, $tag = null)
    {
        if (!is_null($tag)) {
            $currency = '<' . $tag . '>NT $</' . $tag . '>';
            // $currency = '<'.$tag.'>$</'.$tag.'>';
        } else {
            $currency = 'NT $';
            // $currency = '$';
        }
        // number_format(數字, 顯示到小數點後幾位)
        return $currency . number_format($price, 0);
        // return $currency. $price;
    }
    public function protect_name($name)
    {
        if (mb_strlen($name) < 3) {
            return mb_substr($name, 0, 1) . '*';
        } elseif (mb_strlen($name) > 5) {
            return mb_substr($name, 0, ceil(mb_strlen($name)*0.33)) . '**' . mb_substr($name, -(ceil(mb_strlen($name)*0.33)));
        } else {
            return mb_substr($name, 0, 1) . '*' . mb_substr($name, 2);
        }
    }
    public function protect_email($email)
    {
        $arr = explode('@', $email);
        return mb_substr($arr[0], 0, 4) . '***' . '@' . mb_substr($arr[1], 0, 4) . '***';
    }
    public function protect_phone($phone)
    {
        return mb_substr($phone, 0, ceil(mb_strlen($phone)*0.33)) . '***' . mb_substr($phone, -(ceil(mb_strlen($phone)*0.33)));
    }
    public function protect_address($address)
    {
        return mb_substr($address, 0, ceil(mb_strlen($address)*0.66)) . '***';
    }
}
