<?php

namespace App\Presenters;

class ShippingPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->type_arr = [
            1 => __("text.shipping_type_1", [], $this->locale),
            2 => __("text.shipping_type_2", [], $this->locale),
        ];
        $this->user_type_arr = [
            0 => __("text.user_type_0", [], $this->locale),
            1 => __("text.user_type_1", [], $this->locale),
            2 => __("text.user_type_2", [], $this->locale),
        ];
    }
    public function type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->type_arr) ? $this->type_arr[$val] : head($this->type_arr);
        }

        return $this->type_arr;
    }
    public function user_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->user_type_arr) ? $this->user_type_arr[$val] : head($this->user_type_arr);
        }

        return $this->user_type_arr;
    }
}
