<?php

namespace App\Presenters;

class ContactPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1 => '<span class="label label-success">' . __("backend.processed", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.pending", [], $this->locale) . '</span>',
        ];
        $this->type_arr = [
            1 => __("text.contact_type_1", [], $this->locale),
            2 => __("text.contact_type_2", [], $this->locale),
        ];
    }
    public function type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->type_arr) ? $this->type_arr[$val] : $this->type_arr[0];
        }

        return $this->type_arr;
    }
}
