<?php

namespace App\Presenters;

class CouponPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->use_status_arr = [
            0 => '<span class="label label-danger">' . __("text.coupon_use_status_0", [], $this->locale) . '</span>',
            1 => '<span class="label label-success">' . __("text.coupon_use_status_1", [], $this->locale) . '</span>',
            2 => '<span class="label label-warning">' . __("text.coupon_use_status_2", [], $this->locale) . '</span>',
        ];
        $this->type_arr = [
            1 => __("backend.coupon_type_1", [], $this->locale),
            2 => __("backend.coupon_type_2", [], $this->locale),
        ];
        $this->user_type_arr = [
            0 => __("text.user_type_0", [], $this->locale),
            1 => __("text.user_type_1", [], $this->locale),
            2 => __("text.user_type_2", [], $this->locale),
        ];
        $this->use_type_arr = [
            1 => '會員等級',
            2 => '指定會員',
        ];
    }
    public function use_status($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->use_status_arr) ? $this->use_status_arr[$val] : head($this->use_status_arr);
        }

        return $this->use_status_arr;
    }
    public function type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->type_arr) ? $this->type_arr[$val] : head($this->type_arr);
        }

        return $this->type_arr;
    }
    public function user_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->user_type_arr) ? $this->user_type_arr[$val] : head($this->user_type_arr);
        }

        return $this->user_type_arr;
    }
    public function use_type($val = null)
    {
      if (!is_null($val)) {
        return array_key_exists($val, $this->use_type_arr) ? $this->use_type_arr[$val] : head($this->use_type_arr);
      }

      return $this->use_type_arr;
    }
}
