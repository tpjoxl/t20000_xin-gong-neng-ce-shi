<?php

namespace App\Presenters;

class SubscriberPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1 => '<span class="label label-success">訂閱中</span>',
            0 => '<span class="label label-danger">取消訂閱</span>',
        ];
    }
}
