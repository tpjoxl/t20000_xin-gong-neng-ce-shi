<?php

namespace App\Presenters;

class ProductPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1=>'<span class="label label-success">'.__("backend.product_status_on", [], $this->locale).'</span>',
            0=>'<span class="label label-danger">'.__("backend.product_status_off", [], $this->locale).'</span>',
            2=>'<span class="label label-default">'.__("backend.product_status_soldout", [], $this->locale).'</span>',
        ];
        $this->home_status_arr = [
            1 => '<span class="label label-success">' . __("backend.enable", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.disable", [], $this->locale) . '</span>',
        ];
    }
    public function home_status($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->home_status_arr) ? $this->home_status_arr[$val] : head($this->home_status_arr);
        }
        return $this->home_status_arr;
    }
}
