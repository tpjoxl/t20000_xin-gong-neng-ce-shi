<?php

namespace App\Presenters;

class BonusPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1 => '<span class="label label-success">' . __("backend.bonus_status_1", [], $this->locale) . '</span>',
            0 => '<span class="label label-danger">' . __("backend.bonus_status_0", [], $this->locale) . '</span>',
        ];
    }
}
