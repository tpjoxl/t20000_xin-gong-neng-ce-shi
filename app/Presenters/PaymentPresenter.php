<?php

namespace App\Presenters;

class PaymentPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);
        
        $this->fee_type_arr = [
            1 => __("backend.payment_fee_type_1", [], $this->locale),
            2 => __("backend.payment_fee_type_2", [], $this->locale),
        ];
        $this->user_type_arr = [
            0 => __("text.user_type_0", [], $this->locale),
            1 => __("text.user_type_1", [], $this->locale),
            2 => __("text.user_type_2", [], $this->locale),
        ];
    }
    public function fee_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->fee_type_arr) ? $this->fee_type_arr[$val] : head($this->fee_type_arr);
        }

        return $this->fee_type_arr;
    }
    public function user_type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->user_type_arr) ? $this->user_type_arr[$val] : head($this->user_type_arr);
        }

        return $this->user_type_arr;
    }
}
