<?php

namespace App\Presenters;

class UserPresenter extends Presenter
{
    public function __construct($entity)
    {
        parent::__construct($entity);

        $this->status_arr = [
            1 => '<span class="label label-success">' . __("backend.active", [], $this->locale) . '</span>',
            2 => '<span class="label label-danger">' . __("backend.ban", [], $this->locale) . '</span>',
        ];
        $this->type_arr = [
            1 => __("text.user_type_1", [], $this->locale),
            2 => __("text.user_type_2", [], $this->locale),
        ];
    }
    public function type($val = null)
    {
        if (!is_null($val)) {
            return array_key_exists($val, $this->type_arr) ? $this->type_arr[$val] : head($this->type_arr);
        }
        return $this->type_arr;
    }
}
