<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class Import implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        dd($rows);
    }
    public function headingRow(): int
    {
        return 2;
    }
    protected function parseRowDate($data)
    {
        if (!empty($data)) {
            if (is_string($data)) {
                return Carbon::parse($data)->toDateString();
            } else {
                $excel_date = $data;
                $unix_date = ($excel_date - 25569) * 86400;
                $excel_date = 25569 + ($unix_date / 86400);
                $unix_date = ($excel_date - 25569) * 86400;
                return gmdate("Y-m-d", $unix_date);
            }
        } else {
            return null;
        }
    }
}
