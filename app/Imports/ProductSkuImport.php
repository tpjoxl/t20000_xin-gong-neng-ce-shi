<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use App\Models\Sku;
use App\Models\ProductCategory;
use App\Models\Product;

class ProductSkuImport extends Import
{
    public function collection(Collection $rows)
    {
        // dd($rows);
        foreach ($rows as $row) {
            if (!empty($row['title'])&&!empty($row['num'])&&!empty($row['description'])&&!empty($row['price'])&&!empty($row['qty'])) {
                $data = Sku::firstOrNew(['num' => $row['num']]);
                $data->fillAndSave($row->toArray());
                if (!empty($row['product_title'])) {
                    $product = Product::whereTranslation('title', $row['product_title'])->first();
                    if (is_null($product)) {
                        $product = Product::make();
                        $product->fillAndSave(['title' => $row['product_title'], 'slug' => $row['product_title']]);
                    }
                    if (!empty($row['product_category_title'])) {
                        $productCategoriesId = [];
                        $productCategories = explode(',', preg_replace('/\s(?=)/', '', $row['product_category_title']));
                        foreach ($productCategories as $productCategory) {
                            $category = explode('>', $productCategory);
                            $level1Category = ProductCategory::whereTranslation('title', $category[0])->first();
                            if (is_null($level1Category)) {
                                $level1Category = ProductCategory::make();
                                $level1Category->fillAndSave(['title' => $category[0], 'slug' => $category[0]]);
                            }
                            $productCategoriesId[] = $level1Category->id;
                            if (count($category) > 1) {
                                $level2Category = ProductCategory::whereTranslation('title', $category[1])->where('parent_id', $level1Category->id)->first();
                                if (is_null($level2Category)) {
                                    $level2Category = ProductCategory::make();
                                    $level2Category->fillAndSave(['title' => $category[1], 'slug' => $category[1], 'parent_id' => $level1Category->id]);
                                }
                                $productCategoriesId[] = $level2Category->id;
                            }
                        }
                    }
                    $product->fillAndSave(['categories' => $productCategoriesId]);
                }
                $data->fillAndSave(['product_id' => $product->id]);
            }
        }
    }
}
