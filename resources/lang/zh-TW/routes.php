<?php

return [
  'about' => 'about',
  'product' => 'product',
  'blog' => 'blog',
  'contact' => 'contact',
  'cart' => 'cart',
  'user' => 'user',
  'login' => 'login',
  'logout' => 'logout',
  'register' => 'register',
  'forgot' => 'forgot-password',
  'edit' => 'profile-edit',
  'password' => 'change-password',
  'order' => 'orders',
  'order_query' => 'order-query',
  'coupon' => 'coupon',
  'bonus' => 'bonus',
  'privacy' => 'privacy-policy',
  'terms' => 'terms-of-use',
  'detail' => 'detail',
  'paid_inform' => 'paid-inform',
  'list' => 'list',
  'category' => 'category',
  'search' => 'search',
  'subscriber' => 'subscriber',
];

