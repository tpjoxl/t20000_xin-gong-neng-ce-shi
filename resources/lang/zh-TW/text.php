<?php

return [
  'toggle_nav' => '切換選單',
  'home' => '首頁',
  'about' => '關於我們',
  'blog' => '部落格',
  'faq' => '常見問答',
  'product' => '商品介紹',
  'contact' => '聯絡我們',
  'cart' => '購物車',
  'privacy' => '隱私政策',
  'terms' => '使用條款',
  'user_centre' => '會員專區',
  'user_login' => '登入會員',
  'user_logout' => '會員登出',
  'user_register' => '會員註冊',
  'user_forgot' => '忘記密碼',
  'user_edit' => '會員資料',
  'user_password' => '密碼變更',
  'user_coupon' => '優惠券查詢',
  'user_order' => '訂單查詢',
  'user_bonus' => '紅利點數',
  'user_signup' => '報名紀錄',
  'user_terms' => '使用條款',
  'user_privacy' => '隱私權政策',
  'login' => '登入',
  'logout' => '登出',
  'register' => '註冊',
  'forgot' => '忘記密碼',
  'edit' => '資料編輯',
  'password' => '密碼變更',
  'orders' => '訂單查詢',
  'order_query' => '訂單查詢',
  'paid_inform' => '匯款通知',
  'no_data' => '尚無資料',
  'back_home' => '回首頁',
  'back_list' => '回列表頁',
  'reset' => '清除重填',
  'submit' => '確定送出',
  'ok' => '確定',
  'close' => '關閉',
  'view_more' => '觀看更多',
  'search' => '搜尋',
  'search_no_data' => '查無資料',
  'search_keyword' => '搜尋關鍵字',
  'search_placeholder' => '請輸入關鍵字',
  'sitemap' => '網站地圖',
  'please_input_sth' => '請輸入:sth',
  'please_select' => '請選擇',
  'please_select_sth' => '請選擇:sth',
  'login_error' => '登入失敗',
  'login_error_text' => '帳號密碼輸入錯誤或帳號尚未啟用',
  'login_email_verified_error' => '請先至您的註冊信箱完成信箱驗證手續，謝謝您！',
  'login_status_error' => '您的帳號已被停用，請與我們聯繫，謝謝您！',
  'edit_success' => '送出成功！',
  'edit_success_text' => '您的資料已成功更新',
  'password_success' => '送出成功！',
  'password_success_text' => '新的密碼變更成功，請重新登入！',
  'register_success' => '感謝您的註冊！',
  'register_success_text' => '請至您的信箱收取驗證信。',
  'user_register_success_text' => '請至您的信箱收取信箱驗證信，並完成信箱驗證。',
  'register_error' => 'Error!',
  'register_error_text' => '註冊驗證信送出失敗',
  'contact_success' => '送出成功',
  'contact_success_txt' => '感謝您的填寫，我們會盡速與您聯絡。',
  'contact_error' => '送出失敗',
  'contact_error_txt' => '如有任何問題請撥打電話與我們聯絡，謝謝。',
  'agree' => '我已閱讀並同意「<a href=":privacy_url" target="_blank">隱私政策</a>」及「<a href=":terms_url" target="_blank">使用條款</a>」',
  'agree_txt' => '我同意網站的 <a href=":url_terms" target="_blank">使用條款暨隱私權條款</a>',
  'all_blog' => '所有文章',
  'user_email_hint' => '系統信將寄送至此信箱',
  'account_hint' => '請輸入Email',
  'password_hint' => '請輸入8個以上字元密碼(英文及數字組成)',
  'password_confirmation_hint' => '請再次輸入密碼',
  'name_hint' => '您的姓名不會被公開',
  'birthday_hint' => '請輸入您的生日，或直接輸入格式如(2020-01-01)',
  'shipping_type_1' => '國內',
  'shipping_type_2' => '國外',
  'coupon_use_status_0' => '未使用',
  'coupon_use_status_1' => '已使用',
  'coupon_use_status_2' => '已過期',
  'user_type_0' => '非會員',
  'user_type_1' => '一般會員',
  'user_type_2' => 'VIP會員',
  'cart' => '購物車',
  'cart_info' => '購物清單<br>資料填寫結帳',
  'cart_checkout' => '確認購物明細',
  'cart_result' => '訂單完成',
  'please_input_qty' => '請輸入數量',
  'cart_add_item_success' => '商品成功加入購物車',
  'cart_change_sku_success' => '商品成功切換規格',
  'cart_delete_item_success' => '商品已刪除',
  'cart_qty_not_enough' => '商品數量不足',
  'cart_qty_update' => '商品數量已更新',
  'cart_item_checked_update' => '商品選取狀態已更新',
  'cart_coupon_error' => '優惠代碼無法使用！<br>請確認(1)代碼無誤 (2)活動期限 (3)訂單金額是否達到門檻',
  'cart_coupon_min_price' => '商品金額未達優惠代碼最低使用金額NT$:min_price，無法使用！',
  'cart_coupon_update' => '已更新優惠代碼',
  'cart_bonus_update' => '已更新紅利點數使用狀態',
  'cart_formdata_update' => '已更新購物車資訊',
  'cart_shipping_update' => '已更新配送方式',
  'cart_payment_update' => '已更新付款方式',
  'cart_tax_update' => '已變更稅金',
  'cart_sku_no_data' => '查無此規格的庫存資料！',
  'cart_sku_not_enough' => '商品庫存數量不足',
  'cart_sku_qty' => '庫存數量=:qty',
  'cart_no_item' => '尚未選購商品',
  'cart_remote_error' => '選擇的區域無法配送',
  'subscription' => '訂閱電子報',
  'unsubscription' => '取消訂閱電子報',
  'subscribe' => '訂閱',
  'unsubscribe' => '取消訂閱',
  'subscribe_success' => '訂閱成功',
  'subscribe_error' => '訂閱失敗',
  'unsubscribe_success' => '取消訂閱成功',
  'unsubscribe_error' => '取消訂閱失敗',
];

