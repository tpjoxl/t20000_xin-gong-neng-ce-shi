<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'string' => 'The :attribute must be between :min and :max characters.',
        'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file' => 'The :attribute must be greater than or equal :value kilobytes.',
        'string' => 'The :attribute must be greater than or equal :value characters.',
        'array' => 'The :attribute must have :value items or more.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => 'The password is incorrect.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'The :attribute format is invalid.',
    'uuid' => 'The :attribute must be a valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'title'                 => 'Title',
        'description'           => 'Description',
        'name'                  => 'Name',
        'first_name'            => 'First name',
        'last_name'             => 'Last name',
        'gender'                => 'Gender',
        'sex'                   => 'Sex',
        'sex_title'             => 'Gender title',
        'email'                 => 'Email',
        'company'               => 'Company',
        'job'                   => 'Job',
        'phone'                 => 'Phone',
        'phone2'                => 'Cell phohe',
        'fax'                   => 'Fax',
        'address'               => 'Address',
        'business_hours'        => 'Business hours',
        'qrcode'                => 'QR CODE',
        'status'                => 'Status',
        'home_status'           => 'Show in home page',
        'banner'                => 'Banner image',
        'pic'                   => 'Image',
        'pic_pc'                => 'Image(PC)',
        'pic_m'                 => 'Image(Mobile)',
        'list_pic'              => 'List image ',
        'date_range'            => 'Available date range',
        'created_range'         => 'Created date range',
        'created_at'            => 'Created Time',
        'filled_range'          => 'Filled date range',
        'filled_at'             => 'Filled Time',
        'tag_title'             => 'Image title text',
        'tag_alt'               => 'Image alt text',
        'url'                   => 'Link url',
        'out_url'               => 'External URL',
        'date_on'               => 'Start date',
        'date_off'              => 'End date',
        'slug'                  => 'URL KEY',
        'power_group'           => 'Group',
        'power_option'          => 'Options',
        'group_name'            => 'Group name',
        'admin_account'         => 'Admin ID',
        'admin_password'        => 'Admin Password',
        'account'               => 'Account Email',
        'password'              => 'Password',
        'password_confirmation' => 'Confirm password',
        'old_password'          => 'Old password',
        'new_password'          => 'New password',
        'passable'              => '直接修改密碼',
        'username'              => 'User Name',
        'birthday'              => 'Birthday',
        'birthyear'             => 'Birthyear',
        'category_title'        => 'Category title',
        'seo_title'             => 'Meta Tag Title',
        'seo_description'       => 'Meta Tag Description',
        'seo_keyword'           => 'Meta Tag Keywords',
        'og_title'              => 'og:title',
        'og_description'        => 'og:description',
        'og_image'              => 'og:image',
        'meta_robots'           => 'META(robots)',
        'site_name'             => 'Website Name',
        'copyright'             => 'Copyright notice display name',
        'site_backend_name'     => 'Management System Name',
        'site_mail'             => 'Customer Service Mailbox',
        'site_url'              => 'Website Address',
        'head_code'             => 'Embedded Code(Header)',
        'body_code'             => 'Embedded Code(Body)',
        'smtp'                  => 'SMTP',
        'smtp_port'             => 'SMTP PORT',
        'smtp_account'          => 'SMTP帳號',
        'smtp_password'         => 'SMTP密碼',
        'smtp_ssl'              => 'SMTP SSL',
        'smtp_encryption'       => 'SMTP 加密方式',
        'backupmail'            => '備份資料庫收件信箱',
        'parent_id'             => 'Parent',
        'category_id'           => 'Category',
        'categories'            => 'Category',
        'categories.*'          => 'Category',
        'category'              => 'Category',
        'tag'                   => 'Tags',
        'tags'                  => 'Tags',
        'login_at'              => 'Last login time',
        'login_range'           => 'Login time range',
        'ip'                    => 'Login IP',
        'captcha'               => 'Security code',
        'email_verified'        => 'Mailbox verification status',
        'text'                  => 'Content',
        'note'                  => 'Note',
        'admin_note'            => 'Admin note',
        'is_top'                => 'Pin to top',
        'file'                  => 'File',
        'file_path'             => 'File path',
        'file_title'            => 'File name',
        'file_name'             => 'File display name',
        'import_file'           => 'Import file',
        'google_map'            => 'Google map URL',
        'message'               => 'Message',
        'h1'                    => 'H1 heading',
        'locale'                => 'Locale',
        'product_num'           => 'Product number',
        'year'                  => 'Year',
        'author'                => 'Author',
        'lineid'                => 'LINE ID',
        'question'              => 'Question',
        'answer'                => 'Answer',
        'age'                   => 'Age',
        'idcard'                => 'ID number',
        'remark'                => 'Remark',
        'payment_id'            => 'Payment method',
        'tw_city_region'        => 'City & Region in Taiwan',
        'city_region'           => 'City & Region',
        'city_id'               => 'City',
        'region_id'             => 'Region',
        'user_type'             => 'Membership level',
    ],

];
