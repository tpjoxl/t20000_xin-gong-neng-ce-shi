<?php

return [
  'blog' => 'blog',
  'contact' => 'contact',
  'privacy' => 'privacy-policy',
  'terms' => 'terms-of-use',
  'member' => 'member',
  'member_login' => 'login',
  'member_register' => 'register',
  'member_forgot' => 'forgot-password',
  'member_edit' => 'edit',
  'member_password' => 'change-password',
  'member_coupon' => 'coupon',
  'member_signup' => 'course-list',
  'order_query' => 'order-query',
  'paid_inform' => 'paid-inform',
  'list' => 'list',
  'category' => 'category',
  'search' => 'search',
  'subscriber' => 'subscriber',
];

