@extends('frontend.layouts.master')

@section('content')
<div class="breadcrumbs-area bg-overlay-dark bg-6">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="breadcrumbs-text text-left">
          <h1 class="heading">404 找不到此頁面</h1>
          @php
            $breadcrumb = [
              ['404', '', '404'],
            ];
          @endphp
          @include('frontend.layouts.breadcrumb')
        </div>
      </div>
    </div>
  </div>
</div>
<div class="error-sect pt-120 pb-120">
  <div class="container">
    <div class="section-title text-center mb-55">
      <h2>404 Not Found</h2>
      <p>找不到此頁面</p>
    </div>
    <div class="text-center">
      <a href="{{route('home')}}" class="default-button submit-btn">回首頁</a>
    </div>
  </div>
</div>
@endsection
