@if ($paginator->hasPages())
    <nav>
        <form class="page-jump" method="get" action="">
            @foreach (request()->except('page') as $name => $value)
                <input type="hidden" name="{{$name}}" value="{{$value}}">
            @endforeach
            @if (env('BACKEND_LOCALE') == 'ja')
                <div class="input-group">
                    <span class="input-group-addon">第</span>
                    <input type="text" name="page" id="page" class="form-control">
                    <span class="input-group-addon">ページへ</span>
                </div>
            @elseif (env('BACKEND_LOCALE') == 'en')
                <div class="input-group">
                    <span class="input-group-addon">Jump to</span>
                    <input type="text" name="page" id="page" class="form-control">
                    <span class="input-group-addon">page</span>
                </div>
            @else
                <div class="input-group">
                    <span class="input-group-addon">跳到第</span>
                    <input type="text" name="page" id="page" class="form-control">
                    <span class="input-group-addon">頁</span>
                </div>
            @endif
        </form>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active" aria-current="page"><span>{{ $page }}</span></li>
                        @else
                            <li><a href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
