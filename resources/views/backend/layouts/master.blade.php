<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{$page_power->parent->title}} - {{ $setting->name }} @lang('backend.cms', [], env('BACKEND_LOCALE'))</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  @include('_app_icon')

  <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('AdminLTE/plugins/air-datepicker/dist/css/datepicker.min.css')}}">
  {{-- <link rel="stylesheet" href="{{asset('AdminLTE/plugins/select2/select2.min.css')}}"> --}}
  <link rel="stylesheet" href="{{asset('AdminLTE/plugins/lou-multi-select/css/multi-select.dist.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/plugins/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/plugins/Ajax-Bootstrap-Select/dist/css/ajax-bootstrap-select.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/skins/_all-skins.min.css')}}">

  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/custom.css')}}">
  @yield('style')

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
  <script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/fastclick/fastclick.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/moment/moment.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/ckeditor/ckeditor.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/air-datepicker/dist/js/datepicker.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/air-datepicker/dist/js/i18n/datepicker.'.explode('-', env('BACKEND_LOCALE'))[0].'.js')}}"></script>
  {{-- <script src="{{asset('AdminLTE/plugins/select2/select2.full.min.js')}}"></script> --}}
  <script src="{{asset('AdminLTE/plugins/lou-multi-select/js/jquery.quicksearch.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/lou-multi-select/js/jquery.multi-select.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/bootstrap-select/dist/js/i18n/defaults-'.str_replace('-', '_', env('BACKEND_LOCALE')).'.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/Ajax-Bootstrap-Select/dist/js/ajax-bootstrap-select.min.js')}}"></script>
  <script src="{{asset('AdminLTE/plugins/Ajax-Bootstrap-Select/dist/js/locale/ajax-bootstrap-select.'.env('BACKEND_LOCALE').'.js')}}"></script>

  <script src="{{asset('vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
  <script src="{{asset('AdminLTE/dist/js/app.min.js')}}"></script>
  <script type="text/javascript">
    window.App = {!! json_encode([
      'csrfToken' => csrf_token(),
      'siteUrl' => url('/'),
      'lfmPrefix' => '/'.config('lfm.url_prefix'),
      'locale' => env('BACKEND_LOCALE'),
      'locale_short' => explode('-', env('BACKEND_LOCALE'))[0],
      'text' => [
        'select_all' => __('backend.select_all', [], env('BACKEND_LOCALE')),
        'deselect_all' => __('backend.deselect_all', [], env('BACKEND_LOCALE')),
        'close' => __('text.close', [], env('BACKEND_LOCALE')),
      ],
    ]) !!};
    var alertTxt = {
      'no_data_selected': '@lang('backend.no_data_selected', [], env('BACKEND_LOCALE'))',
      'delete_all_confirm':  '@lang('backend.delete_all_confirm', [], env('BACKEND_LOCALE'))',
      'delete_item_confirm':  '@lang('backend.delete_item_confirm', [], env('BACKEND_LOCALE'))',
      'delete_selected_item_confirm':  '@lang('backend.delete_selected_item_confirm', [], env('BACKEND_LOCALE'))',
      'delete_category_confirm':  '@lang('backend.delete_category_confirm', [], env('BACKEND_LOCALE'))',
      'sure_to_send':  '@lang('backend.sure_to_send', [], env('BACKEND_LOCALE'))',
    };
  </script>
  <script src="{{asset('AdminLTE/dist/js/custom.js')}}"></script>
  @php
    try {
        $editor_css = mix('css/editor.css');
    } catch(Exception $e) {
        $editor_css = asset('css/editor.css');
    }
  @endphp
  <script type="text/javascript">
      CKEDITOR.config.filebrowserImageBrowseUrl= "{{asset(config('lfm.url_prefix').'?type=Images')}}";
      CKEDITOR.config.filebrowserImageUploadUrl= "{{asset(config('lfm.url_prefix').'/upload')}}?type=Images&_token={{csrf_token()}}";
      CKEDITOR.config.filebrowserBrowseUrl= "{{asset(config('lfm.url_prefix').'?type=Files')}}";
      CKEDITOR.config.filebrowserUploadUrl= "{{asset(config('lfm.url_prefix').'/upload')}}?type=Files&_token={{csrf_token()}}";
      CKEDITOR.config.contentsCss = [ "{{asset('assets/bootstrap/css/bootstrap.min.css')}}", "{{asset('assets/css/all.min.css')}}", "{{asset('assets/css/ionicons.min.css')}}", "{{asset('assets/css/themify-icons.css')}}", "{{asset('assets/css/linearicons.css')}}", "{{asset('assets/css/flaticon.css')}}", "{{asset('assets/css/style.css')}}", "{{asset('assets/css/responsive.css')}}", "{{asset('assets/color/theme-yellow.css')}}", "{{$editor_css}}" ];
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <a href="{{route('backend.home')}}" class="logo">
      <span class="logo-mini">@lang('backend.logo_mini', [], env('BACKEND_LOCALE'))</span>
      <span class="logo-lg">@lang('backend.logo_lg', ['name'=>$setting->backend_name], env('BACKEND_LOCALE'))</span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          {{-- <li><a href="{{asset('uploads/manual.pdf')}}" download="參柒設計網站管理後台操作手冊.pdf"><i class="fa fa-download" aria-hidden="true"></i> @lang('backend.manual', [], env('BACKEND_LOCALE'))</a></li> --}}
          <li class="user-menu hidden-xs"><a href="{{ route('backend.admin.edit', ['id' => $auth_admin->id], env('BACKEND_LOCALE')) }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> {{ $auth_admin->name }}</a></li>
          <li><a href="{{route('home')}}" target="_blank"><i class="fa fa-home" aria-hidden="true"></i> @lang('backend.front_page', [], env('BACKEND_LOCALE'))</a></li>
          <li><a href="{{route('backend.logout')}}"><i class="fa fa-power-off"></i> @lang('backend.logout', [], env('BACKEND_LOCALE'))</a></li>
        </ul>
      </div>

    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      @include('backend.layouts.sidebar')
    </section>
  </aside>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        {{$page_power->parent->title}}
          @yield('content-header')
      </h1>
      {{-- @include('backend.layouts.breadcrumb') --}}
    </section>

    <section class="content">
      @include('backend.layouts.message')
      @yield('content-top')
      <div class="box box-primary @yield('content-box-class')">
        <div class="box-header with-border">
          <h3 class="box-title">
            @yield('box-title-before')
            {{$page_power->title}}
          </h3>
          @yield('box-header-right')
        </div>
        @yield('content')
      </div>
      @yield('content-bottom')
      {{-- @yield('content') --}}
    </section>
  </div>

  {{-- <footer class="main-footer hidden-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.12
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer> --}}
</div>
@if (!empty($cities))
<script type="text/javascript">
    $(document).ready(function() {
      var cities = {!!json_encode($cities)!!}
      // console.log(cities)
      // 縣市切換就更新區域下拉
      $('select.city').on('change', function(){
        if ($(this).val() != '') {
            var cityId = $(this).val();
            var regions = '';
            $.each(cities[cityId]['regions'], function(key, region) {
              regions += '<option value="'+region.id+'">'+region.name+'</option>';
            });
            $(this).parents('.city-selector').find('select.region').html(regions);
            $(this).parents('.city-selector').find('select.region').selectpicker('refresh');
        } else {
            $(this).parents('.city-selector').find('select.region').html('<option value="">----</option>');
            $(this).parents('.city-selector').find('select.region').selectpicker('refresh');
        }
      });
    });
</script>
@endif
@yield('script')
</body>
</html>
