<ul class="sidebar-menu">
  @if (count($powers))
    @foreach ($powers as $navroot)
      @php
          $currentRouteChildren = $navroot->children
          ->filter(function ($child, $key) {
              return in_array(Route::currentRouteName(), explode(',', preg_replace('/\s(?=)/', '', $child->route_name)));
          });
      @endphp
      <li class="treeview {{$currentRouteChildren->count()?'active':''}}">
        <a href="#">
          <i class="{{$navroot->icon}}"></i> <span>{{$navroot->title}}</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        @if ($navroot->children->count())
          <ul class="treeview-menu">
              @foreach ($navroot->children->where('status', 1) as $child)
                  @if (Route::has(explode(',', $child->route_name)[0]))
                      <li class="{{ in_array(Route::currentRouteName(), explode(',', preg_replace('/\s(?=)/', '', $child->route_name))) ? 'active' : '' }}">
                      <a href="{{Route::has(explode(',', $child->route_name)[0])? route(explode(',', $child->route_name)[0]) : ''}}{{$child->route_param}}">
                          <i class="fa fa-circle-o"></i> {{$child->title}}
                      </a>
                      </li>
                  @endif
              @endforeach
          </ul>
        @endif
      </li>
      @if ($navroot->divider_after == 1)
      <li class="treeview_line"></li>
      @endif
    @endforeach
  @endif
</ul>
