@php
    if (!empty($parent)) {
        $id = $parent->id;
    } elseif (!empty($data)) {
        $id = $data->id;
    }
    $header_right_menus = [
        [
            'route' => 'backend.product.edit',
            'route_params' => [ 'id' => $id ],
            'text' => __('backend.data_edit', [], env('BACKEND_LOCALE')),
        ],
        [
            'route' => 'backend.product.image.index',
            'route_params' => [ 'product_id' => $id ],
            'text' => __('backend.img_manage', [], env('BACKEND_LOCALE')),
        ],
    ];
@endphp
@if (!empty($header_right_menus))
    <div class="box-tools pull-right">
        <div class="btn-group">
            @foreach ($header_right_menus as $header_right_menu)
                <a class="btn btn-{{Route::currentRouteName()==$header_right_menu['route']?'primary':'default'}} btn-sm btn-flat" href="{{route($header_right_menu['route'], $header_right_menu['route_params'])}}">{{$header_right_menu['text']}}</a>
            @endforeach
        </div>
    </div>
@endif
