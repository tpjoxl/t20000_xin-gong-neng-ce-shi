<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@lang('backend.login_page_title', ['name' => $setting->backend_name], env('BACKEND_LOCALE'))</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @include('_app_icon')

  <link rel="stylesheet" href="{{asset('AdminLTE/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('AdminLTE/dist/css/custom.css')}}">

  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>{{ $setting->backend_name }}</b><br>@lang('backend.cms', [], env('BACKEND_LOCALE'))</a>
  </div>
  <div class="login-box-body">
    @include('backend.layouts.message')
    <form action="{{ route('backend.login.submit') }}" method="post">
      @csrf
      <div class="form-group has-feedback{{ ($errors->first('account')) ? ' has-error' : '' }}">
        <label for="account">@lang('validation.attributes.admin_account', [], env('BACKEND_LOCALE'))</label>
        <input type="text" id="account" name="account" class="form-control" placeholder="" value="{{old('account')}}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback{{ ($errors->first('password')) ? ' has-error' : '' }}">
        <label for="password">@lang('validation.attributes.admin_password', [], env('BACKEND_LOCALE'))</label>
        <input type="password" id="password" name="password"  class="form-control" placeholder="">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group captcha">
        <div class="captcha-box">
            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">@lang('backend.login', [], env('BACKEND_LOCALE'))</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="{{asset('AdminLTE/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<script src="{{asset('AdminLTE/bootstrap/js/bootstrap.min.js')}}"></script>
</body>
</html>
