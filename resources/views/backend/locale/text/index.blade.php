@extends('backend.layouts.master')

@section('content')<form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
    <div class="box-header with-border text-center">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> @lang('backend.save')</button>
    </div>
    <div class="box-body">
        <div class="alert alert-primary" role="alert">前方開頭為 #routes. 表示為網址使用</div>
    </div>
    {{ csrf_field() }}
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs nowrap">
            @foreach ($locales as $locale)
                <li class="{{($locale->code==config('app.locale'))?'active':''}}"><a href="#{{$locale->code}}" data-toggle="tab">{{$locale->title}}({{$locale->code}})</a></li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach ($locales as $locale)
                <div class="tab-pane {{($locale->code==config('app.locale'))?'active':''}}" id="{{$locale->code}}">
                    <div class="table-responsive">
                        <table class="table no-margin locale-text-table table-hover">
                            <tbody>
                                @foreach ($localeTextKeys as $ind => $text_key)
                                    <tr>
                                        <th class="text-right text-primary" style="width: 3%">{{$ind+1}}</th>
                                        <th class="text-right" style="width: 15%">#{{$text_key}}</th>
                                        @php
                                            $localeText = $locale->texts->where('text_key', $text_key)->first();
                                            $value = $localeText&&$localeText->text_value?$localeText->text_value:(request()->has('default')?__($text_key, [], $locale->code):null);
                                        @endphp
                                        <td class="text-left">
                                            <input type="text" name="text[{{$locale->code}}][{{$text_key}}]" class="form-control" value="{{$value}}">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="box-footer text-center">
        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> @lang('backend.save')</button>
    </div>
</form>
@endsection
