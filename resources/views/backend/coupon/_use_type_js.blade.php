<script>
    (function($) {
        $('[name="use_type"]').on('change', function(e) {
            var use_type = $('[name="use_type"]:checked').val();
            console.log(use_type)
            if (use_type == 1) {
                $('#user_type-form-group').show();
                $('#count-form-group').show();
                $('#users-form-group').hide();
            } else if (use_type == 2) {
                $('#user_type-form-group').hide();
                $('#count-form-group').hide();
                $('#users-form-group').show();
            }
        }).change();
    })($)
</script>
