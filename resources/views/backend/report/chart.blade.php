@extends('backend.layouts.master')

@section('content-top')
  @include('backend.'.$prefix.'._search')
@endsection

@section('content')
<div class="box-body">
  <div class="row">
    <div class="col-md-6">
      <h4>銷售數量</h4>
      <canvas id="qtyChart"></canvas>
    </div>
    <div class="col-md-6">
      <h4>銷售總額</h4>
      <canvas id="totalChart"></canvas>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script src="{{asset('AdminLTE/plugins/chartjs/Chart.min.js')}}"></script>
  <script>
    var qtyChartCanvas = $("#qtyChart").get(0).getContext("2d");
    var qtyChart = new Chart(qtyChartCanvas);
    var qtyChartData = {
      labels: {!!json_encode($days)!!},
      datasets: [
        {
          fillColor: "#00C0EF",
          strokeColor: "#00C0EF",
          pointColor: "#00C0EF",
          pointStrokeColor: "#00C0EF",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#00C0EF",
          data: {!!json_encode($daysQty)!!}
        }
      ]
    };
    var qtyChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    qtyChartOptions.datasetFill = false;
    qtyChart.Bar(qtyChartData, qtyChartOptions);


    var totalChartCanvas = $("#totalChart").get(0).getContext("2d");
    var totalChart = new Chart(totalChartCanvas);
    var totalChartData = {
      labels: {!!json_encode($days)!!},
      datasets: [
        {
          fillColor: "#00a65a",
          strokeColor: "#00a65a",
          pointColor: "#00a65a",
          pointStrokeColor: "#00a65a",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#00a65a",
          data: {!!json_encode($daysTotal)!!}
        }
      ]
    };
    var totalChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    totalChartOptions.datasetFill = false;
    totalChart.Bar(totalChartData, totalChartOptions);
  </script>
@endsection
