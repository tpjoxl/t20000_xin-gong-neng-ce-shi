@extends('backend.layouts.master')

@section('content-top')
  @include('backend.common._search')
@endsection

@section('content')
  @include('backend.'.$prefix.'.index_btns', ['wrap_class'=>'box-header with-border'])
  @include('backend.common.pager', ['wrap_class'=>'box-header with-border'])
  <div class="box-body no-padding">
    @include('backend.common.index_table', ['noCheckbox' => true])
  </div>
  @include('backend.common.pager', ['wrap_class'=>'box-footer'])
  @include('backend.'.$prefix.'.index_btns', ['wrap_class'=>'box-footer'])
@endsection
