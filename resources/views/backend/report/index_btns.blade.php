<div class="{{$wrap_class}} text-center hidden-print">
	@if (count($list_datas)>0)
		@if (Route::has(Route::currentRouteName().'.export'))
			<a href="{{route(Route::currentRouteName().'.export', array_merge(request()->all()))}}" class="btn btn-default"><span class="fa fa-file-excel-o"></span> @lang('backend.export', ['title' => 'EXCEL'], env('BACKEND_LOCALE'))</a>
			{{-- <a href="{{route(Route::currentRouteName().'.export', array_merge(request()->all(), ['type' => 'csv']))}}" class="btn btn-default"><span class="fa fa-file-o"></span> @lang('backend.export_csv', [], env('BACKEND_LOCALE'))</a> --}}
		@endif
	@endif
</div>
