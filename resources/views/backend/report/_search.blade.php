<div class="panel box box-default search-panel-box hidden-print">
  <div class="box-header with-border">
    <h4 class="box-title search-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#searchPanel">
        <i class="fa fa-search"></i> 條件篩選
      </a>
    </h4>
  </div>
  <div id="searchPanel" class="collapse in">
    <div class="box-body">
        <form id="search-form" class="form-horizontal" method="get" action="" accept-charset="UTF-8" enctype="multipart/form-data">
          {{-- {{ csrf_field() }} --}}
          @foreach ($search_fields as $search_label => $search_field)
            @php
              if (array_key_exists('label', $search_field)) {
                $label = $search_field['label'];
              } elseif (array_key_exists($search_label, $valid_attrs)) {
                $label = $valid_attrs[$search_label];
              } else {
                $label = __('validation.attributes.'.$search_label, [], env('BACKEND_LOCALE'));
              }
              if (array_key_exists('default', $search_field)) {
                $value = $search_field['default'];
              } elseif (request()->has($search_field['name'])) {
                $value = request()->input($search_field['name']);
              } else {
                $value = '';
              }
            @endphp
            @if ($search_field['type'] == 'text_input')
              <div class="form-group">
                <label for="{{$search_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$label}}</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="{{$search_field['name']}}" value="{{$value}}">
                </div>
              </div>
            @elseif ($search_field['type'] == 'radio')
              <div class="form-group">
                <label for="{{$search_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$label}}</label>
                <div class="col-sm-9">
                  @if (!array_key_exists('no_all', $search_field) || $search_field['no_all'] = false)
                    <label class="radio-inline">
                        <input type="radio" name="{{$search_field['name']}}" value="" checked> @lang('backend.all', [], env('BACKEND_LOCALE'))
                    </label>
                  @endif

                  @foreach ($search_field['options'] as $key => $val)
                    <label class="radio-inline">
                        <input type="radio" name="{{$search_field['name']}}" value="{{ $key }}" {{ $value == $key ? 'checked' : '' }}> {!! $val !!}
                    </label>
                  @endforeach
                </div>
              </div>
            @elseif ($search_field['type'] == 'date_range')
              <div class="form-group">
                <label for="{{$search_field['name'][0]}}" class="col-md-2 col-sm-3 control-label">{{$label}}</label>
                <div class="col-md-3 col-sm-4">
                    <input type="text" class="form-control datepicker" id="{{$search_field['name'][0]}}" name="{{$search_field['name'][0]}}" placeholder="{{$search_field['placeholder'][0]}}" value="{{request()->input($search_field['name'][0])}}" autocomplete="off">
                </div>
                <div class="col-md-3 col-sm-4 date-off-holder">
                    <input type="text" class="form-control datepicker" id="{{$search_field['name'][1]}}" name="{{$search_field['name'][1]}}" placeholder="{{$search_field['placeholder'][1]}}" value="{{request()->input($search_field['name'][1])}}" autocomplete="off">
                </div>
              </div>
            @elseif ($search_field['type'] == 'select')
              <div class="form-group">
                <label for="{{$search_field['name']}}" class="col-md-2 col-sm-3 control-label">{{$label}}</label>
            		<div class="col-sm-9">
                  <select class="form-control show-tick" name="{{$search_field['name']}}"
                  @if (array_key_exists('live_search', $search_field))
                    data-live-search="{{$search_field['live_search']}}"
                  @endif>
                    <option value="">{{array_key_exists('placeholder', $search_field)?$search_field['placeholder']:__('backend.please_select', [], env('BACKEND_LOCALE'))}}</option>
                    @foreach ($search_field['options'] as $key => $value)
                      <option value="{{$key}}"
                      @if (request()->has($search_field['name']))
                        {{request()->input($search_field['name'])==$key?'selected' : ''}}
                      @else
                        {{(old($search_field['name'], (isset($data)?$data[$search_field['name']]:''))==$key) ? 'selected' : ''}}
                      @endif>{{$value}}</option>
                    @endforeach
                    {{-- @include('backend.common._select', ['parents'=>$search_field['options'], 'field'=>$search_field]) --}}
                  </select>
            		</div>
              </div>
            @elseif ($search_field['type'] == 'hidden')
              <input type="hidden" name="{{$search_field['name']}}" value="{{$value}}">
            @elseif ($search_field['type'] == 'include')
              @include($search_field['view'])
            @endif
          @endforeach
          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-2">
              <button type="button" class="btn btn-default btn-search-reset">@lang('backend.clear', [], env('BACKEND_LOCALE'))</button>
              <button type="submit" class="btn btn-primary">@lang('backend.search', [], env('BACKEND_LOCALE'))</button>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>
