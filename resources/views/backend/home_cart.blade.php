@extends('backend.layouts.master')

{{-- @section('content-header')
  Dashboard
  <small>Version 2.0</small>
@endsection --}}
@section('content-top')
  <div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-person-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">@lang('backend.account_info')</span>
          <span class="info-box-text">
              @lang('backend.login_id')：{{ $auth_admin->account }}<br>
              @lang('backend.login_time')：{{ $auth_admin->login_at }}
          </span>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-paper-outline"></i></span>

        <div class="info-box-content">
        <span class="info-box-number">@lang('backend.order_info', [], env('BACKEND_LOCALE'))</span>
          <span class="info-box-text">@lang('backend.order_pending', [], env('BACKEND_LOCALE'))：{{ $orders_count_status1 }}</span>
          <span class="info-box-text">@lang('backend.order_processing', [], env('BACKEND_LOCALE'))：{{ $orders_count_status3 }}</span>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

        <div class="info-box-content">
        <span class="info-box-number">@lang('backend.product_info', [], env('BACKEND_LOCALE'))</span>
          <span class="info-box-text">@lang('backend.product_total', [], env('BACKEND_LOCALE'))：{{ $products_count }}</span>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content-box-class', 'hidden')

@section('content-bottom')
  @php
    $orderPower = Auth::guard('admin')->user()->group->checkPowerByName('order.index');
    $contactPower = Auth::guard('admin')->user()->group->checkPowerByName('contact.index');
    $userPower = Auth::guard('admin')->user()->group->checkPowerByName('user.index');
  @endphp
  <div class="row">
    <div class="col-sm-8">
      @if ($orderPower)
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('backend.home_inform', ['title'=>'待處理訂單'])</h3>
          </div>
          <div class="box-body">
              <div class="alert alert-primary text-center" role="alert">
                  @lang('backend.home_inform_count', [
                      'total' => $orders_count_status1,
                      'title' => '待處理訂單',
                      'pending' => '待處理',
                      'count' => $orders_count_status1<=$listLimit?$orders_count_status1:$listLimit
                      ], env('BACKEND_LOCALE'))
              </div>
            <div class="table-responsive">
                @if (!$orders_count_status1)
                    <p class="text-center">@lang('backend.no_data')</p>
                @else
                    <table class="table no-margin">
                        <thead>
                        <tr>
                          <th></th>
                          <th>@lang('validation.attributes.name')</th>
                          <th>@lang('validation.attributes.filled_at')</th>
                          <th class="text-center">@lang('backend.edit')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $key => $order)
                                <tr>
                                    <td><i class="fa fa-star text-yellow"></i></td>
                                    <td><a href="{{ route('backend.order.edit', ['id' => $order->id]) }}">{{ $order->name }}</a></td>
                                    <td>{{ $order->created_at }}</td>
                                    <td class="text-center"><a href="{{route('backend.order.edit', array_merge(request()->all(), ['id' => $order->id]))}}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
          </div>
          <div class="box-footer clearfix">
              <div class="text-center hidden-print">
                  <a href="{{route('backend.order.index')}}" class="btn btn-default"><span class="fa fa-search"></span> @lang('backend.view_all_sth', ['sth'=>'訂單'], env('BACKEND_LOCALE'))</a>
              </div>
          </div>
        </div>
      @endif
      @if ($contactPower)
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('backend.home_inform', ['title'=>'聯絡我們表單'])</h3>
          </div>
          <div class="box-body">
              <div class="alert alert-primary text-center" role="alert">
                  @lang('backend.home_inform_count', [
                      'total' => $contacts_count_status0,
                      'title' => '聯絡我們表單',
                      'pending' => '未處理',
                      'count' => $contacts_count_status0<=$listLimit?$contacts_count_status0:$listLimit
                      ], env('BACKEND_LOCALE'))
              </div>
            <div class="table-responsive">
                @if (!$contacts_count_status0)
                    <p class="text-center">@lang('backend.no_data')</p>
                @else
                    <table class="table no-margin">
                        <thead>
                        <tr>
                          <th></th>
                          <th>@lang('validation.attributes.name')</th>
                          <th>@lang('validation.attributes.filled_at')</th>
                          <th class="text-center">@lang('backend.edit')</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($contacts as $key => $contact)
                                <tr>
                                    <td><i class="fa fa-star text-yellow"></i></td>
                                    <td><a href="{{ route('backend.contact.edit', ['id' => $contact->id]) }}">{{ $contact->name }}</a></td>
                                    <td>{{ $contact->created_at }}</td>
                                    <td class="text-center"><a href="{{route('backend.contact.edit', array_merge(request()->all(), ['id' => $contact->id]))}}" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
          </div>
          <div class="box-footer clearfix">
              <div class="text-center hidden-print">
                  <a href="{{route('backend.contact.index')}}" class="btn btn-default"><span class="fa fa-search"></span> @lang('backend.view_all_sth', ['sth'=>'表單'], env('BACKEND_LOCALE'))</a>
              </div>
          </div>
        </div>
      @endif
    </div>
    <div class="col-sm-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">訂單筆數變化</h3>
        </div>
        <div class="box-body">
          <canvas id="barChart" style="height:230px"></canvas>
        </div>
      </div>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">會員男女比例</h3>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-sm-9">
              <canvas id="pieChart"></canvas>
            </div>
            <div class="col-sm-3">
              <ul class="chart-legend clearfix">
                <li><i class="fa fa-circle-o text-red"></i> 女</li>
                <li><i class="fa fa-circle-o text-blue"></i> 男</li>
                <li><i class="fa fa-circle-o text-green"></i> 其他</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('backend.overview')</h3>
          </div>
          <div class="box-body">
            <ul class="func-list">
              @if (count($powers))
                @foreach ($powers as $navroot)
                  <li>
                    <h4>{{$navroot->title}}</h4>
                    @if ($navroot->children->count())
                      <ul>
                          @foreach ($navroot->children->where('status', 1) as $child)
                            @if (Route::has(explode(',', $child->route_name)[0]))
                                <li>
                                <a href="{{Route::has(explode(',', $child->route_name)[0])? route(explode(',', $child->route_name)[0]) : ''}}{{$child->route_param}}">{{$child->title}}</a>
                                </li>
                            @endif
                          @endforeach
                      </ul>
                    @endif
                  </li>
                @endforeach
              @endif
            </ul>
          </div>
        </div>
    </div>
  </div>
@endsection
@section('script')
  <script src="{{asset('AdminLTE/plugins/chartjs/Chart.min.js')}}"></script>
  <script>
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: {{$users_count_sex2}},
        color: "#00a65a",
        highlight: "#00a65a",
        label: "其他"
      },
      {
        value: {{$users_count_sex1}},
        color: "#f56954",
        highlight: "#f56954",
        label: "女"
      },
      {
        value: {{$users_count_sex0}},
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "男"
      }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

    //-------------
    //- BAR CHART -
    //-------------

    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = {
      labels: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
      datasets: [
        // {
        //   label: "Electronics",
        //   fillColor: "rgba(210, 214, 222, 1)",
        //   strokeColor: "rgba(210, 214, 222, 1)",
        //   pointColor: "rgba(210, 214, 222, 1)",
        //   pointStrokeColor: "#c1c7d1",
        //   pointHighlightFill: "#fff",
        //   pointHighlightStroke: "rgba(220,220,220,1)",
        //   data: [65, 59, 80, 81, 56, 55, 40]
        // },
        {
          label: "Digital Goods",
          fillColor: "#00C0EF",
          strokeColor: "#00C0EF",
          pointColor: "#00C0EF",
          pointStrokeColor: "#00C0EF",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#00C0EF",
          data: {!!json_encode($orderMonthCount)!!}
        }
      ]
    };
    // barChartData.datasets[1].fillColor = "#3b8bba";
    // barChartData.datasets[1].strokeColor = "#3b8bba";
    // barChartData.datasets[1].pointColor = "#3b8bba";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  </script>
@endsection
