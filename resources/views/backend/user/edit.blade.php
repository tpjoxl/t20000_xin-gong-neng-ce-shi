@extends('backend.layouts.master')

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @includeFirst(['backend.'.$prefix.'._form', 'backend.common._form'], ['btns_view' => ['backend.'.$prefix.'._form_btns', 'backend.common._form_btns']])
  </form>
@endsection
@section('content-bottom')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">@lang('backend.password_change_title', ['title'=>__('backend.'.$prefix, [], env('BACKEND_LOCALE'))], env('BACKEND_LOCALE'))</h3>
    </div>
    <form class="form-horizontal" method="post" action="{{route('backend.'.$prefix.'.password', ['id'=> $data->id])}}" accept-charset="UTF-8" enctype="multipart/form-data">
        @csrf
        <div class="box-body">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-sm-2 control-label">@lang('validation.attributes.new_password', [], env('BACKEND_LOCALE'))</label>
                <div class="col-sm-9">
                    <input id="password" type="password" class="form-control" name="password">
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password-confirm" class="col-sm-2 control-label">@lang('validation.attributes.password_confirmation', [], env('BACKEND_LOCALE'))</label>
                <div class="col-sm-9">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                </div>
            </div>
        </div>
        <div class="box-footer text-center">
            <button type="submit" class="btn btn-primary">@lang('backend.save', [], env('BACKEND_LOCALE'))</button>
            <button type="reset" class="btn btn-default">@lang('backend.clear', [], env('BACKEND_LOCALE'))</button>
        </div>
    </form>
  </div>
@endsection
