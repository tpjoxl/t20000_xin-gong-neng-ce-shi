@extends('backend.layouts.master')

{{-- @section('content-header')
  Dashboard
  <small>Version 2.0</small>
@endsection --}}
@section('content-top')
  <div class="row">
    <div class="col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-person-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">@lang('backend.account_info')</span>
          <span class="info-box-text">
              @lang('backend.login_id')：{{ $auth_admin->account }}<br>
              @lang('backend.login_time')：{{ $auth_admin->login_at }}
          </span>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content-box-class', 'hidden')

@section('content-bottom')
  <div class="row">
    <div class="col-sm-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">@lang('backend.overview')</h3>
          </div>
          <div class="box-body">
            <ul class="func-list">
              @if (count($powers))
                @foreach ($powers as $navroot)
                  <li>
                    <h4>{{$navroot->title}}</h4>
                    @if ($navroot->children->count())
                      <ul>
                          @foreach ($navroot->children->where('status', 1) as $child)
                            @if (Route::has(explode(',', $child->route_name)[0]))
                                <li>
                                <a href="{{Route::has(explode(',', $child->route_name)[0])? route(explode(',', $child->route_name)[0]) : ''}}{{$child->route_param}}">{{$child->title}}</a>
                                </li>
                            @endif
                          @endforeach
                      </ul>
                    @endif
                  </li>
                @endforeach
              @endif
            </ul>
          </div>
        </div>
    </div>
  </div>
@endsection
