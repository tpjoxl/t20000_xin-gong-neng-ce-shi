@php
    if (is_array($form_field['name'])) {
        foreach ($form_field['name'] as $k => $n) {
            $form_field['id'][$k] = $n;
            $form_field['error_name'][$k] = $n;
            $form_field['name'][$k] = $n;
            if (in_array($form_field['type'], ['multi_select', 'checkbox'])) {
                $form_field['value'][$k] = $form_field['value'][$k] ?? $data->{$n}?(is_array($data->{$n})?$data->{$n}:$data->{$n}->pluck('id')->toArray()):(explode(',', request()->input($n)) ?? $form_field['default'] ?? []);
            } else {
                $form_field['value'][$k] = $form_field['value'][$k] ?? $data->{$n} ?? request()->input($n) ?? $form_field['default'] ?? null;
            }
        }
    } else {
        $n = $form_field['name'];
        $form_field['id'] = $n;
        $form_field['name'] = $n;
        $form_field['error_name'] = $n;
        if (in_array($form_field['type'], ['multi_select', 'multi_select_ajax', 'checkbox'])) {
            $form_field['value'] = $form_field['value'] ?? ($data->{$n}?(is_array($data->{$n})?$data->{$n}:$data->{$n}->pluck('id')->toArray()):explode(',', request()->input($n))) ?? ($form_field['default'] ?? []);
        } else {
            $form_field['value'] =  $form_field['value'] ?? $data->{$n} ?? request()->input($n) ?? $form_field['default'] ?? null;
        }
    }
@endphp
@if ($form_field['type'] == 'include')
    @include($form_field['view'], $form_field)
@else
    @include('backend.common.form_fields.'.$form_field['type'], $form_field)
@endif
