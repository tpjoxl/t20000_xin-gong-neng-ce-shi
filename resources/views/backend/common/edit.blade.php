@extends('backend.layouts.master')

@section('box-header-right')
    @includeIf('backend.'.$prefix.'._box_header_right')
@endsection

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off">
    {{ method_field('PATCH') }}
    @includeFirst(['backend.'.$prefix.'._form', 'backend.common._form'], ['btns_view' => ['backend.'.$prefix.'._form_btns', 'backend.common._form_btns']])
  </form>
@endsection
