<div class="{{$wrap_class}} text-center hidden-print">
    @if (Route::has('backend.'.$prefix.'.index'))
        <a class="btn btn-default" href="{{route('backend.'.$prefix.'.index', array_merge(request()->route()->parameters, request()->only('categories')))}}"><i class="fa fa-list-ul"></i> @lang('backend.back', [], env('BACKEND_LOCALE'))</a>
    @endif
    @if (count($dropdowns))
        @foreach ($dropdowns as $dropdown)
            <div class="btn-menu category-menu dropdown">
                <button class="btn btn-info" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$dropdown['label']}}： <span class="name"></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @if ($rank_all)
                        <li class="{{is_null(request($dropdown['name']))?'active':''}}">
                            <a href="{{route('backend.'.$prefix.'.rank.index', request()->route()->parameters)}}">@lang('backend.all_item', [], env('BACKEND_LOCALE'))</a>
                        </li>
                    @endif
                    @includeFirst(['backend.'.$prefix.'.rank_menu', 'backend.common.rank_menu'], ['parents'=>$dropdown['options']])
                </ul>
            </div>
        @endforeach
	@endif
	@if (count($datas)>1)
        <button type="submit" class="btn btn-primary btn-save-rank" data-form="{{$submitForm??'#rankUpdate'}}"><i class="fa fa-save"></i> @lang('backend.save_sort', [], env('BACKEND_LOCALE'))</button>
        @if (Route::has('backend.'.$prefix.'.rank.reset'))
            <a href="{{route("backend.$prefix.rank.reset", array_merge(request()->route()->parameters, request()->only('categories')))}}" class="btn bg-teal">@lang('backend.rank_reset', [], env('BACKEND_LOCALE'))</a>
        @endif
	@endif
</div>
