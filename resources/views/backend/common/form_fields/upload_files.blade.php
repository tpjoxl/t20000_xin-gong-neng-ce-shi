<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
    <label for="{{ $id??$name }}"
        class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
    <div class="col-sm-9">
        @for ($i = 0; $i < $count; $i++)
            @if (!empty($subname))
                {{$subname[$i]}}
            @endif
            <div class="row">
                <div class="col-sm-4">
                    <input type="file" class="form-control form-control-file" id="{{ $id??$name }}{{$i}}" name="{{ $name }}[{{$i}}]"
                        value="" data-max-size="{{ $max_size }}" accept="{{ $accept }}">
                </div>
                @if (!empty($value[$i]))
                    <div class="col-sm-8">
                        <p class="form-control-static">
                            @if (!empty($show) && $show=='img')
                                <img class="pic-holder" src="{{$value[$i]}}" alt="">
                            @elseif(!empty($show) && $show=='file')
                                <a href="{{asset($value[$i])}}" target="_blank">{{pathinfo($value[$i])['basename']}}</a>
                            @endif
                            @if (empty($required))
                                &nbsp;&nbsp;&nbsp;
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="delete_files[{{$name}}][{{$i}}]" value="{{$value[$i]}}" {{ old("delete_files.$name.$i")?'checked':'' }}> 刪除
                                </label>
                            @endif
                        </p>
                    </div>
                @endif
            </div>
        @endfor
    </div>
    @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
