<div class="form-group">
  <label for="{{ $id[0]??$name[0] }}" class="col-md-2 col-sm-3 control-label {{ $errors->first($error_name[0]??$name[0]?? $name[0])||$errors->first($error_name[1]??$name[1]?? $name[1])||$errors->first($error_name[2]??$name[2]?? $name[2])?'has-error':'' }}">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
      運費為
      <span class="input-inline{{ $errors->first($error_name[0]??$name[0]?? $name[0]) ? ' has-error' : '' }}">
        <input type="text"
            class="form-control"
            id="{{ $id[0]??$name[0] }}"
            name="{{ $name[0] }}"
            placeholder="{{ $placeholder[0] ?? null }}"
            value="{{ old($error_name[0]??$name[0], $value[0]) }}">
      </span>
      元<br>
      當訂單金額滿
      <span class="input-inline{{ $errors->first($error_name[1]??$name[1]?? $name[1]) ? ' has-error' : '' }}">
        <input type="text"
            class="form-control"
            id="{{ $id[1]??$name[1] }}"
            name="{{ $name[1] }}"
            placeholder="{{ $placeholder[1] ?? null }}"
            value="{{ old($error_name[1]??$name[1], $value[1]) }}">
      </span>
      元，運費為
      <span class="input-inline{{ $errors->first($error_name[2]??$name[2]?? $name[2]) ? ' has-error' : '' }}">
        <input type="text"
            class="form-control"
            id="{{ $id[2]??$name[2] }}"
            name="{{ $name[2] }}"
            placeholder="{{ $placeholder[2] ?? null }}"
            value="{{ old($error_name[2]??$name[2], $value[2]) }}">
      </span>
      元（若要免運請輸入 0 元）
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
