<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    <textarea class="ckeditor" id="{{ $id??$name }}" name="{{ $name }}">{{ old($error_name??$name, $value) }}</textarea>
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
