<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)||$errors->first(!empty($error_name)?$error_name.'.*':$name.'.*')?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    @if (!empty($add_first) && count($options) < 1)
      <a href="{{$add_first['url']}}" class="btn btn-success">
        <i class="fa fa-plus"></i>
        @lang('backend.add_first', ['title'=>$add_first['title']??$label])
      </a>
    @else
      @if (empty($required))
        <input type="hidden" name="{{ $name }}[]" value="">
      @endif
      <select
        class="form-control show-tick select-level"
        id="{{ $id??$name }}"
        name="{{ $name }}[]"
        title="@lang('backend.please_select')"
        multiple
        @if (!empty($max))
          data-max-options="{{ $max }}"
        @endif
        @if (!empty($actions))
          data-actions-box="{{ $actions }}"
        @endif
        @if (!empty($live_search))
          data-live-search="{{ $live_search }}"
        @endif>
        @if (!empty($placeholder))
            <option value="">{{ $placeholder }}</option>
        @endif
        @includeFirst(['backend.'.$prefix.'._select', 'backend.common.form_fields._select'], ['parents'=>$options])
      </select>
    @endif
  </div>
  @unless (!empty($add_first) && count($options) < 1)
    @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
  @endunless
</div>
