<div class="col-sm-9 col-md-offset-2 col-sm-offset-3">
    <p class="help-block">{!! $hint !!}
        @if (in_array($type, ['select_ajax', 'multi_select_ajax']))
            （輸入米字號「*」可顯示全部資料的前100筆）
        @endif
    </p>
</div>
