<div
    class="form-group {{ $errors->first($error_name[0]??$name[0])||$errors->first($error_name[1]??$name[1])?'has-error':'' }}">
    <label for="{{ $id[0]??$name[0] }}"
        class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
    <div class="col-sm-9">
        <div class="city-selector row">
            <div class="col-sm-6 col-md-3{{ $errors->has($error_name[0]??$name[0]) ? ' has-error' : '' }}">
                <select class="form-control city" id="{{ $id[0]??$name[0] }}" name="{{ $name[0] }}">
                    <option value="">請選擇縣市</option>
                    @foreach ($cities as $city)
                        <option value="{{$city->id}}"
                            {{ ($city->id == old($error_name[0]??$name[0], $value[0])) ? 'selected' : '' }}>{{$city->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-6 col-md-3{{ $errors->has($error_name[1]??$name[1]) ? ' has-error' : '' }}">
                <select class="form-control region" id="{{ $id[1]??$name[1] }}" name="{{ $name[1] }}">
                    @if (old($error_name[0]??$name[0], $value[0]) && old($error_name[1]??$name[1], $value[1]))
                        @foreach ($cities[old($error_name[0]??$name[0], $value[0])]->regions as $region)
                            <option value="{{$region->id}}"
                                {{ ($region->id == old($error_name[1]??$name[1], $value[1])) ? 'selected' : '' }}>
                                {{$region->name}}</option>
                        @endforeach
                    @else
                        <option value="">----</option>
                    @endif
                </select>
            </div>
        </div>
    </div>
    @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>

