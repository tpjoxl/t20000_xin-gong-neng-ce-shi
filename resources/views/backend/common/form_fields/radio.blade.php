<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    @if (!empty($has_all))
      <label class="radio-inline">
        <input type="radio" name="{{ $name }}" value="" {{(is_null(old($error_name??$name, $value)))?'checked':''}}> @lang('backend.all', [], env('BACKEND_LOCALE'))
      </label>
    @endif
    @foreach ($options as $key => $val)
      <label class="radio-inline">
          <input type="radio" name="{{ $name }}" value="{{ $key }}" {{ (!is_null(old($error_name??$name, $value))&&$key == old($error_name??$name, $value)) ? 'checked' : '' }}> {!! $val !!}
      </label>
    @endforeach
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
