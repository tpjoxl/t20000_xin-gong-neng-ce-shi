<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    @if (!empty($add_first) && count($options) < 1)
      <a href="{{$add_first['url']}}" class="btn btn-success">
        <i class="fa fa-plus"></i>
        @lang('backend.add_first', ['title'=>$add_first['title']?:$label])
      </a>
    @else
      <select class="form-control show-tick" id="{{ $id??$name }}" name="{{ $name }}"
      @if (!empty($actions))
          data-actions-box="{{ $actions }}"
        @endif
      @if (!empty($live_search))
        data-live-search="{{$live_search}}"
      @endif>
        @unless (isset($empty_hide))
          <option value="">{{ !empty($placeholder)?$placeholder:__('backend.please_select') }}</option>
        @endunless
        @include('backend.common.form_fields._select', ['parents'=>$options])
      </select>
    @endif
  </div>
  @unless (!empty($add_first) && count($options) < 1)
    @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
  @endunless
</div>
