<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    @if (!empty($link))
        <a href="{{$link}}" target="{{!empty($link_target)?$link_target:''}}">
    @endif
    @if (!empty($show) && $show == 'nl2br')
        <p class="form-control-static">{!! nl2br(old($error_name??$name, $value)) !!}</p>
    @elseif (!empty($show) && $show == 'option')
        <p class="form-control-static">{!! array_key_exists(old($error_name??$name, $value), $options)?$options[old($error_name??$name, $value)]:'' !!}</p>
    @else
        <p class="form-control-static">{!! old($error_name??$name, $value) !!}</p>
    @endif
    @if (!empty($link))
        </a>
    @endif
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
