<input type="hidden"
  class="form-control"
  id="{{ $id??$name }}"
  name="{{ $name }}"
  value="{{ old($error_name??$name, $value) }}">
