@foreach ($parents as $parent_key => $parent)
    @if (is_array($parents))
        <option value="{{$parent_key}}"
            @if ($type == 'multi_select' || $type == 'multi_select_ajax')
                {{in_array($parent_key, old($error_name??$name, $value))?'selected':''}}
            @elseif ($type == 'select' || $type == 'select_ajax')
                {{ (old($error_name??$name, $value)==$parent_key) ? 'selected' : '' }}
            @endif
            >{!!$parent!!}</option>
    @else
        @unless (!empty($ignore) && $parent->id == $ignore)
            <option value="{{(!empty($vkey)?$parent->$vkey:null) ?? $parent->id}}"
                @if (!empty($parent->full_parents_id))
                    data-parents="{{$parent->full_parents_id}}"
                @endif
                @if (Arr::get($parent->getRelations(), 'children') && !empty($parent->full_children_id))
                    data-children="{{$parent->full_children_id}}"
                @endif
                @if ($type == 'multi_select' || $type == 'multi_select_ajax')
                    {{in_array($parent->id, old($error_name??$name, $value))?'selected':''}}
                @elseif ($type == 'select' || $type == 'select_ajax')
                    {{ (old($error_name??$name, $value)==$parent->id) ? 'selected' : '' }}
                @endif
                @php
                    if (!empty($key)) {
                        if (is_array($key)) {
                            $keytitleArr = [];
                            foreach ($key as $k) {
                                $keytitleArr[] = $parent->$k;
                            }
                            $keytitle = implode(' - ', $keytitleArr);
                        } else {
                            $keytitle = $parent->$key;
                        }
                    }
                @endphp
                title="{{(!empty($keytitle)?$keytitle:null) ?? $parent->full_title ?? $parent->title}}"
                >{{(!empty($keytitle)?$keytitle:null) ?? $parent->full_title ?? $parent->title}}</option>

            @if (Arr::get($parent->getRelations(), 'children') && count(Arr::get($parent->getRelations(), 'children')))
                @includeFirst(['backend.'.$prefix.'._select', 'backend.common.form_fields._select'], ['parents'=>$parent->children])
            @endif
        @endunless
    @endif
@endforeach
