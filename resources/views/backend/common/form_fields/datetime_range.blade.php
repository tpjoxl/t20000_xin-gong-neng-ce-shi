<div class="form-group {{ $errors->first($error_name[0]??$name[0]?? $name[0])||$errors->first($error_name[1]??$name[1]?? $name[1])?'has-error':'' }}">
  <label for="{{ $id[0]??$name[0] }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-md-3 col-sm-4">
    <input type="text"
      class="form-control dtp"
      id="{{ $id[0]??$name[0] }}"
      name="{{ $name[0] }}"
      placeholder="{{ $placeholder[0] }}"
      value="{{ old($error_name[0]??$name[0], $value[0]) }}">
  </div>
  <div class="col-md-3 col-sm-4 date-off-holder">
    <input type="text"
      class="form-control dtp"
      id="{{ $id[1]??$name[1] }}"
      name="{{ $name[1] }}"
      placeholder="{{ $placeholder[1] }}"
      value="{{ old($error_name[1]??$name[1], $value[1]) }}">
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
