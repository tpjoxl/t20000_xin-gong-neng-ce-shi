<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    <input type="text"
      class="form-control dp"
      id="{{ $id??$name }}"
      name="{{ $name }}"
      value="{{ old($error_name??$name, $value) }}"
      placeholder="{{ __('backend.select_date') }}">
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
