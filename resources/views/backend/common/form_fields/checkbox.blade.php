<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
  <label for="{{ $id??$name }}[]" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    <input type="hidden" name="{{ $name }}" value="">
    @foreach ($options as $key => $val)
      <label class="checkbox-inline">
          <input type="checkbox" name="{{ $name }}[]" value="{{ $key }}" {{ is_array(old($error_name??$name, $value))&&in_array($key, old($error_name??$name, $value))?'checked':'' }}> {!! $val !!}
      </label>
    @endforeach
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
