<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name??$name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    <div class="input-group">
      <span class="input-group-btn">
          <a data-input="{{ $id??$name }}_thumbnail" class="btn btn-primary lfm-file"
          @if ($folder)
            data-folder="{{$folder}}"
          @endif>
            <i class="fa fa-file-o"></i> @lang('backend.select_file')
          </a>
      </span>
      <input id="{{ $id??$name }}_thumbnail" class="form-control" type="text" name="{{ $name }}" value="{{ old($error_name??$name, $value) }}">
      <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear')</a>
    </div>
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
