<div id="{{ $id??$name }}-form-group" class="form-group {{ $errors->first($error_name??$name)?'has-error':'' }}">
  <label for="{{ $id??$name }}" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
  <div class="col-sm-9">
    <div class="input-group">
      <span class="input-group-btn">
          <a data-input="{{ $id??$name }}_thumbnail" data-preview="{{ $id??$name }}_holder" class="btn btn-primary lfm-img"
          @if ($folder)
            data-folder="{{$folder}}"
          @endif>
            <i class="fa fa-picture-o"></i> @lang('backend.select_img')
          </a>
      </span>
      <input id="{{ $id??$name }}_thumbnail" class="form-control" type="text" name="{{ $name }}" value="{{ old($error_name??$name, $value) }}">
      <a href="#" class="btn btn-default btn-deletePic input-group-addon">@lang('backend.clear')</a>
    </div>
    <div id="{{ $id??$name }}_holder">
      @if (old($error_name??$name, $value))
        @foreach (explode(',', old($error_name??$name, $value)) as $src)
          <img class="pic-holder" src="{{$src}}">
        @endforeach
      @endif
    </div>
    {{-- <img id="{{ $id??$name }}_holder" src="{{ $value }}"> --}}
    @if ($w)
      <p class="help-block">@lang('backend.img_size', ['w'=>$w, 'h'=>$h])</p>
    @endif
  </div>
  @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
