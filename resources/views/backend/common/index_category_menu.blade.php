@foreach ($parents as $parent_key => $parent)
    @if (is_array($parents))
        <li
        @if (request()->has($dropdown['name']))
            class="{{($parent_key==request()->input($dropdown['name']))?'active':''}}"
        @endif
        >
            <a href="{{route('backend.'.$prefix.'.index', array_merge(request()->route()->parameters, [$dropdown['name']=>$parent_key]))}}">{!!$parent!!}</a>
        </li>
    @else
        <li
        @if (request()->has($dropdown['name']))
            class="{{($parent->id==request()->input($dropdown['name']))?'active':''}}"
        @endif
        >
            <a href="{{route('backend.'.$prefix.'.index', array_merge(request()->route()->parameters, [$dropdown['name']=>$parent->id]))}}">{{$parent->full_title}}</a>
        </li>
        @if ($parent->children->count())
          @includeFirst(['backend.'.$prefix.'.index_category_menu', 'backend.common.index_category_menu'], ['parents'=>$parent->children])
        @endif
    @endif
@endforeach
