@if (!array_key_exists('hidden', $search_types = array_count_values(array_column($search_fields, 'type'))) || count($search_fields) != $search_types['hidden'])
  <div class="panel box box-default search-panel-box hidden-print">
    <div class="box-header with-border">
      <h4 class="box-title search-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#searchPanel"
        @if (!$rank_all)
         class="{{(count(request()->except('page', '_token', head(array_keys($rank_dropdowns))))>0)? '': 'collapsed'}}"
        @else
          class="{{(request()->except('page', '_token'))? '': 'collapsed'}}"
        @endif
        >
          <i class="fa fa-search"></i> @lang('backend.search_filter', [], env('BACKEND_LOCALE'))
        </a>
      </h4>
    </div>
    <div id="searchPanel"
    @if (!$rank_all)
      class="panel-collapse collapse {{(count(request()->except('page', '_token', head(array_keys($rank_dropdowns))))>0)? 'in': ''}}"
    @else
      class="panel-collapse collapse {{(request()->except('page', '_token'))? 'in': ''}}"
    @endif>
      <div class="box-body">
          <form id="search-form" class="form-horizontal" method="get" action="" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off">
            {{-- {{ csrf_field() }} --}}
            @foreach ($search_fields as $search_label => $search_field)
              @php
                if (is_array($search_field['name'])) {
                    foreach ($search_field['name'] as $n) {
                        $search_field['value'][] = request()->input($n);
                    }
                } else {
                    if (in_array($search_field['type'], ['multi_select', 'checkbox'])) {
                        $search_field['value'] = request()->input($search_field['name']) ? (is_array(request()->input($search_field['name']))?request()->input($search_field['name']):explode(',',request()->input($search_field['name']))) : ($search_field['default'] ?? []);
                    } else {
                        $search_field['value'] = request()->input($search_field['name']);
                    }

                }
              @endphp
              @include('backend.common.form_fields.'.$search_field['type'], $search_field)
            @endforeach
            <div class="form-group">
              <div class="col-sm-9 col-sm-offset-2">
                <button type="button" class="btn btn-default btn-search-reset">@lang('backend.clear', [], env('BACKEND_LOCALE'))</button>
                <button type="submit" class="btn btn-primary">@lang('backend.search', [], env('BACKEND_LOCALE'))</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
@endif
