<div class="{{$wrap_class}} text-center hidden-print">
		@if (count($datas)>0)
				<label class="btn btn-default"><input type="checkbox" name="chkAll" class="chkAll"> @lang('backend.select_all', [], env('BACKEND_LOCALE'))</label>
				@if ($model->getLimitLevel()>1)
					<button class="btn btn-default dd-control" type="button" data-action="expand-all">@lang('backend.expand_all', [], env('BACKEND_LOCALE'))</button>
					<button class="btn btn-default dd-control" type="button" data-action="collapse-all">@lang('backend.collapse_all', [], env('BACKEND_LOCALE'))</button>
				@endif
				@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
					<a href="{{route('backend.'.$prefix.'.destroy')}}" class="btn btn-danger del_select_all"><span class="fa fa-trash-o"></span> @lang('backend.delete_selected', [], env('BACKEND_LOCALE'))</a>
				@endif
				@if (Route::has('backend.'.$prefix.'.modify') && $auth_admin->group->checkPowerByName($prefix.'.edit') && count($dropdowns))
                    @foreach ($dropdowns as $dropdown)
                        <span class="btn-menu dropdown">
                            <button class="btn bg-navy" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @lang('backend.modify_sth', ['sth'=>$dropdown['label']], env('BACKEND_LOCALE')) <span class="name"></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu bg-navy">
                                @foreach ($dropdown['options'] as $key => $value)
                                    <li><a href="{{route('backend.'.$prefix.'.modify', [$dropdown['name'] => $key])}}" class="modify_select_all">{!!$value!!}</a></li>
                                @endforeach
                            </ul>
                        </span>
                    @endforeach
				@endif
                <button type="button" class="btn btn-primary save-rank"><i class="fa fa-save"></i> @lang('backend.save_sort_level', [], env('BACKEND_LOCALE'))</button>
                @if (Route::has("backend.$prefix.rank.reset"))
                    <a href="{{route("backend.$prefix.rank.reset")}}" class="btn bg-teal">@lang('backend.rank_reset', [], env('BACKEND_LOCALE'))</a>
                @endif
		@endif
		@if (Route::has('backend.'.$prefix.'.create') && $auth_admin->group->checkPowerByName($prefix.'.create'))
			<a href="{{route('backend.'.$prefix.'.create')}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add', [], env('BACKEND_LOCALE'))</a>
		@endif
</div>
