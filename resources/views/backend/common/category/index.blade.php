@extends('backend.layouts.master')

@section('content')
  @includeFirst(['backend.'.$prefix.'.index_btns', 'backend.common.category.index_btns'], ['wrap_class'=>'box-header with-border'])
  <div class="box-body">
    @if (count($datas)<1)
      <div class="no-data text-center">@lang('backend.no_data', [], env('BACKEND_LOCALE'))</div>
    @else
      <div class="dd" id="nestable">
        @includeFirst(['backend.'.$prefix.'._menu', 'backend.common.category._menu'], ['parents'=>$datas])
      </div>
    @endif
  </div>
  <form id="rankUpdate" method="POST" action="{{route('backend.'.$prefix.'.rank.update')}}" class="form" style="display:none;" autocomplete="off">
      {{ csrf_field() }}
      <input type="hidden" id="sortNum" name="sortNum" value="">
  </form>
  @includeFirst(['backend.'.$prefix.'.index_btns', 'backend.common.category.index_btns'], ['wrap_class'=>'box-footer'])
@endsection

@section('script')
  <script src="{{asset('AdminLTE/plugins/nestable/jquery.nestable.js')}}"></script>
  <script src="{{asset('AdminLTE/dist/js/category_rank.js')}}"></script>
@endsection
