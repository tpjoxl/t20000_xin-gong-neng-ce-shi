<ol class="dd-list">
@foreach ($parents as $parent)
	<li class="dd-item" data-id="{{$parent->id}}">
			<div class="dd-handle"><i class="ic fa fa-arrows"></i></div>
			<div class="dd-content">
					<label><input class="selectedItem" type="checkbox" name="selectedItem[]" value="{{ $parent->id }}"></label>
						<a href="{{ route('backend.'.$prefix.'.edit', ['id' => $parent->id]) }}">{{ $parent->title }}</a>
					<div class="btn-holder">
							@if (Route::has('backend.'.$prefix.'.create') && $auth_admin->group->checkPowerByName($prefix.'.create') && $model->getLimitLevel() > $parent->level)
								<a href="{{ route('backend.'.$prefix.'.create')}}?parent_id={{$parent->id}}" class="btn btn-circle" data-toggle="tooltip" title="@lang('backend.add_child', [], env('BACKEND_LOCALE'))"><i class="fa fa-plus-circle"></i><i class="fa fa-level-down"></i></a>
							@endif
							@foreach ($dropdowns as $dropdown)
                                <span data-toggle="tooltip" title="{{$dropdown['label']}}">{!! $dropdown['options'][$parent[$dropdown['name']]] !!}</span>
                            @endforeach
                            @if (Route::has('backend.'.$prefix.'.copy') && $auth_admin->group->checkPowerByRouteName("backend.$prefix.copy"))
								<a href="{{ route('backend.'.$prefix.'.copy', ['id' => $parent->id]) }}" class="btn btn-circle" data-toggle="tooltip" title="@lang('backend.copy', [], env('BACKEND_LOCALE'))"><span class="fa fa-copy"></span></a>
							@endif
							@if (Route::has('backend.'.$prefix.'.edit') && $auth_admin->group->checkPowerByName($prefix.'.edit'))
								<a href="{{ route('backend.'.$prefix.'.edit', ['id' => $parent->id]) }}" class="btn btn-circle" data-toggle="tooltip" title="@lang('backend.edit', [], env('BACKEND_LOCALE'))"><span class="glyphicon glyphicon-pencil"></span></a>
							@endif
                            @if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
                                @if ($parent->deletable)
								    <a href="{{ route('backend.'.$prefix.'.destroy')}}?id={{$parent->id}}" class="btn btn-circle btn-delete-row del-cate" data-toggle="tooltip" title="@lang('backend.delete', [], env('BACKEND_LOCALE'))"><span class="glyphicon glyphicon-trash"></span></a>
                                @else
                                    <span class="btn btn-circle delete-ban" data-toggle="tooltip" title="@lang('backend.delete_ban', [], env('BACKEND_LOCALE'))"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                @endif
							@endif
					</div>
            </div>
			@if (Arr::get($parent->getRelations(), 'children') && count(Arr::get($parent->getRelations(), 'children')))
				@include('backend.common.category._menu', ['parents'=>$parent->children])
			@endif
	</li>
@endforeach
</ol>
