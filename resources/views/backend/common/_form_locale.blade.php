@php
    if (is_array($form_field['name'])) {
        foreach ($form_field['name'] as $k => $n) {
            $form_field['id'][$k] = $lang->code ."_$n";
            $form_field['error_name'][$k] = $lang->code .".$n";
            $form_field['name'][$k] = $lang->code ."[$n]";
            if (in_array($form_field['type'], ['multi_select', 'checkbox'])) {
                $form_field['value'][$k] = $form_field['value'][$k] ?? $data->translate($lang->code)&&isset($data->translate($lang->code)->{$n})?(is_array($data->translate($lang->code)->{$n})?$data->translate($lang->code)->{$n}:$data->translate($lang->code)->{$n}->pluck('id')->toArray()):(explode(',', request()->input($n)) ?? $form_field['default'] ?? []);
            } else {
                $form_field['value'][$k] = $form_field['value'][$k] ?? $data->translate($lang->code)&&isset($data->translate($lang->code)->{$n}) ? $data->translate($lang->code)->{$n} : request()->input($n) ?? $form_field['default'] ?? null;
            }
        }
    } else {
        $n = $form_field['name'];
        $form_field['id'] = $lang->code ."_$n";
        $form_field['name'] = $lang->code ."[$n]";
        $form_field['error_name'] = $lang->code .".$n";
        if (in_array($form_field['type'], ['multi_select', 'checkbox'])) {
            $form_field['value'] = $form_field['value'] ?? $data->translate($lang->code)&&isset($data->translate($lang->code)->{$n})?(is_array($data->translate($lang->code)->{$n})?$data->translate($lang->code)->{$n}:$data->translate($lang->code)->{$n}->pluck('id')->toArray()):explode(',', request()->input($n)) ?? ($form_field['default'] ?? []);
        } else {
            $form_field['value'] = $form_field['value'] ?? $data->translate($lang->code)&&isset($data->translate($lang->code)->{$n}) ? $data->translate($lang->code)->{$n} : request()->input($n) ?? $form_field['default'] ?? null;
        }
    }
@endphp
@if ($form_field['type'] == 'include')
    @include($form_field['view'], $form_field)
@else
    @include('backend.common.form_fields.'.$form_field['type'], $form_field)
@endif
