@extends('backend.layouts.master')

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off">
    @includeFirst(['backend.'.$prefix.'._form', 'backend.common._form'], ['btns_view' => ['backend.'.$prefix.'.page_btns', 'backend.common.page_btns']])
  </form>
@endsection
