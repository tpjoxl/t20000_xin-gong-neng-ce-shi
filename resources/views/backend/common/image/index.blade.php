@extends('backend.layouts.master')

@section('box-header-right')
    @includeIf('backend.'.($parent_prefix?$parent_prefix:$prefix).'._box_header_right')
@endsection

@section('box-title-before', $parent?"【{$parent->title}】":"")

@php
    if ($parent_prefix) {
        $back = [
            'route' => route('backend.'.$parent_prefix.'.index'),
            'text' => '回列表'
        ];
    }
@endphp

@section('content')
    @include('backend.common.image.index_btns', ['wrap_class'=>'box-header with-border', 'datas' => $datas])
    {{-- @include('backend.layouts.pager', ['wrap_class'=>'box-header with-border']) --}}
    <div class="box-body">
        @if (count($datas)<1)
            <div class="alert alert-primary text-center" role="alert">@lang('backend.'.(empty($model->maxImgSize['w'])&&empty($model->maxImgSize['h'])?'img_max_size_no':'img_max_size'), ['mb'=>$model->maxUploadSize / 1024, 'w'=>$model->maxImgSize['w'], 'h'=>$model->maxImgSize['h'], 'dpi'=>72], env('BACKEND_LOCALE'))</div>
            <div class="no-data text-center">@lang('backend.no_img', [], env('BACKEND_LOCALE'))</div>
        @else
            <div class="alert alert-primary text-center" role="alert">@lang('backend.'.(empty($model->maxImgSize['w'])&&empty($model->maxImgSize['h'])?'img_max_size_no':'img_max_size'), ['mb'=>$model->maxUploadSize / 1024, 'w'=>$model->maxImgSize['w'], 'h'=>$model->maxImgSize['h'], 'dpi'=>72], env('BACKEND_LOCALE'))<br>@lang('backend.img_rank_hint', [], env('BACKEND_LOCALE'))</div>
            <div id="img-list" class="row">
                @foreach ($datas as $img)
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-id="{{ $img->id }}">
                    <div class="img-item">
                    <img src="{{imgsrc($img->default_path)}}" alt="">
                    @foreach ($lang_datas as $lang)
                        <div class="form-group">
                        <label class="control-label">@lang('backend.img_desc', [], env('BACKEND_LOCALE'))({{$lang->title}})</label>
                        <input class="form-control" type="text" name="{{$lang->code}}[imgdesc][]" data-id="{{$img->id}}" data-lang="{{$lang->code}}" placeholder="@lang('backend.please_input_img_desc', [], env('BACKEND_LOCALE'))" value="{{$img->translate($lang->code)?$img->translate($lang->code)->description:''}}">
                        </div>
                    @endforeach
                    <label class="delete-checkbox"><input class="selectedItem" type="checkbox" name="selectedItem[]" value="{{ $img->id }}"></label>
                    @if ($img->is_cover && Route::has('backend.'.$prefix.'.cover'))
                        <div class="cover-tag">@lang('backend.cover', [], env('BACKEND_LOCALE'))</div>
                    @endif
                    </div>
                </div>
                @endforeach
            </div>
        @endif
    </div>
    <form id="rankUpdate" method="POST" action="{{route('backend.'.$prefix.'.rank.update', request()->route()->parameters)}}" class="form" autocomplete="off">
        @csrf
        <input type="hidden" id="sortNum" name="sortNum" value="">
    </form>
    <form id="imageUpload" class="form-horizontal" method="post" action="{{route('backend.'.$prefix.'.store', request()->route()->parameters)}}" accept-charset="UTF-8" enctype="multipart/form-data" style="display: none" autocomplete="off">
        @csrf
        <input type="hidden" name="parent" value="{{$parent?$parent->id:null}}">
        <input type="file" name="images[]" data-max-size="{{$model->maxUploadSize}}" multiple="multiple">
    </form>
    {{-- @include('backend.layouts.pager', ['wrap_class'=>'box-footer']) --}}
    @include('backend.common.image.index_btns', ['wrap_class'=>'box-footer', 'datas' => $datas])
@endsection

@section('script')
    <script type="text/javascript">
        var imgDescUpdateUrl = "{{route('backend.'.$prefix.'.update', request()->route()->parameters)}}";
    </script>
    <script src="{{asset('AdminLTE/plugins/jquery-ui-interactions/jquery-ui.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/jQueryUI/jquery.ui.touch-punch.min.js')}}"></script>
    <script src="{{asset('AdminLTE/dist/js/image_upload.js')}}"></script>
@endsection
