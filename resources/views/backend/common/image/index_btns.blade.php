<div class="{{$wrap_class}} text-center hidden-print">
    @if (!empty($back))
        <a class="btn btn-default" href="{{$back['route']}}"><i class="fa fa-arrow-circle-left"></i> {{$back['text']}}</a>
    @endif
    @if (count($datas)>0)
        <label class="btn btn-default"><input type="checkbox" name="chkAll" class="chkAll"> @lang('backend.select_all', [], env('BACKEND_LOCALE'))</label>
        @if (Route::has('backend.'.$prefix.'.destroy'))
            <a href="{{route('backend.'.$prefix.'.destroy', request()->route()->parameters)}}" class="btn btn-danger del_select_all"><span class="glyphicon glyphicon-trash"></span> @lang('backend.delete_selected', [], env('BACKEND_LOCALE'))</a>
        @endif
        @if (Route::has('backend.'.$prefix.'.cover'))
            <a href="{{route('backend.'.$prefix.'.cover', request()->route()->parameters)}}" class="btn bg-navy set_cover">@lang('backend.set_cover', [], env('BACKEND_LOCALE'))</a>
        @endif
        <button type="submit" class="btn btn-primary btn-save-rank"><i class="fa fa-save"></i> @lang('backend.save_sort', [], env('BACKEND_LOCALE'))</button>
        @if (Route::has('backend.'.$prefix.'.rank.reset'))
            <a href="{{route("backend.$prefix.rank.reset", array_merge(request()->route()->parameters, request()->only('categories')))}}" class="btn bg-teal">@lang('backend.rank_reset', [], env('BACKEND_LOCALE'))</a>
        @endif
    @endif
    @if (Route::has('backend.'.$prefix.'.store'))
        <div class="btn btn-success btn-file-trigger">
            <i class="fa fa-plus"></i> @lang('backend.add_img', [], env('BACKEND_LOCALE'))
        </div>
    @endif
</div>
