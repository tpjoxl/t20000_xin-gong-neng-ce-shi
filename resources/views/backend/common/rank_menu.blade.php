@if (is_array($parents))
    @foreach ($parents as $key => $val)
        <li class="{{(!is_null(request()->input($dropdown['name']))&&$key==request()->input($dropdown['name']))?'active':''}}">
            <a href="{{route('backend.'.$prefix.'.rank.index', array_merge(request()->route()->parameters, [$dropdown['name'] => $key]))}}">{!!$val!!}</a>
        </li>
    @endforeach
@else
    @foreach ($parents as $parent)
        <li class="{{(!is_null(request()->input($dropdown['name']))&&$parent->id==request()->input($dropdown['name']))?'active':''}}">
            <a href="{{route('backend.'.$prefix.'.rank.index', array_merge(request()->route()->parameters, [$dropdown['name'] => $parent->id]))}}">{{$parent->full_title}}</a>
        </li>

        @if ($parent->children->count())
            @includeFirst(['backend.'.$prefix.'.rank_menu', 'backend.common.rank_menu'], ['parents'=>$parent->children])
        @endif
    @endforeach
@endif
