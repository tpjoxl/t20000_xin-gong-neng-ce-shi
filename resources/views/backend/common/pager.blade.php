@if (count($list_datas)>0)
<div class="{{$wrap_class}} clearfix">
  <div class="pull-left page-info">
    @lang('backend.page', ['first'=>$list_pager['first'], 'last'=>$list_pager['last'], 'total'=>$list_pager['total']], env('BACKEND_LOCALE'))
  </div>
  <div class="pull-right">
      {{ $list_pager['render'] }}
  </div>
</div>
@endif
