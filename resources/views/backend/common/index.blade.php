@extends('backend.layouts.master')

@section('content-top')
  @include('backend.common._search')
@endsection

@if (!empty($parent))
    @section('box-header-right')
        @includeIf('backend.'.($parent_prefix?$parent_prefix:'').'._box_header_right')
    @endsection
    @section('box-title-before', "【{$parent->title}】")
    @php
        $back = [
            'route' => route('backend.'.($parent_prefix?$parent_prefix:'').'.index'),
            'text' => '回列表'
        ];
    @endphp
@endif

@section('content')
  @include('backend.common.index_btns', ['wrap_class'=>'box-header with-border'])
  @include('backend.common.pager', ['wrap_class'=>'box-header with-border'])
  <div class="box-body no-padding">
    @include('backend.common.index_table')
  </div>
  @include('backend.common.pager', ['wrap_class'=>'box-footer'])
  @include('backend.common.index_btns', ['wrap_class'=>'box-footer'])
  @if (Route::has('backend.'.$prefix.'.import'))
    <form id="importForm" class="hidden" method="post" action="{{route('backend.'.$prefix.'.import', request()->route()->parameters)}}" accept-charset="UTF-8" enctype="multipart/form-data" autocomplete="off">
      {{ csrf_field() }}
      <input type="file" name="import_file" />
    </form>
  @endif
@endsection
