@includeFirst($btns_view, ['wrap_class'=>'box-header with-border'])
@csrf
<div class="box-body">
  <div class="alert alert-primary text-center" role="alert">@lang('backend.required_txt', [], env('BACKEND_LOCALE'))</div>
</div>
@php
    if (count($lang_datas)>1 && (method_exists($data, 'translations') || $model->getTable() == 'settings')) {
        $localeFields = Arr::only($form_fields, $model->translatedAttributes);
        $commonFields = Arr::except($form_fields, $model->translatedAttributes);
    } else {
        $localeFields = [];
        $commonFields = $form_fields;
    }
    // dd($localeFields, $commonFields, $lang_datas);
@endphp
<div class="nav-tabs-custom">
  @if (count($lang_datas)>1)
    <ul class="nav nav-tabs">
        @if (count($localeFields))
            @foreach ($lang_datas as $key => $lang)
            <li class="{{$key==0?'active':''}}"><a href="#{{$lang->code}}" data-toggle="tab">{{$lang->title}}({{$lang->code}})</a></li>
            @endforeach
        @endif
        @if (count($commonFields) && count($localeFields))
            <li><a href="#common" data-toggle="tab">@lang('backend.common', [], env('BACKEND_LOCALE'))</a></li>
        @endif
    </ul>
  @endif
  <div class="tab-content">
    @if (!empty($localeFields))
        @foreach ($lang_datas as $key => $lang)
            <div class="tab-pane {{$key==0?'active':''}}" id="{{$lang->code}}">
                @foreach ($localeFields as $form_label => $form_field)
                    @include('backend.common._form_locale')
                @endforeach
            </div>
        @endforeach
    @endif
    @if (!empty($commonFields))
        <div class="tab-pane {{count($localeFields)==0?'active':''}}" id="common">
            @foreach ($commonFields as $form_label => $form_field)
                @include('backend.common._form_common')
            @endforeach
        </div>
    @endif
  </div>
</div>
@includeFirst($btns_view, ['wrap_class'=>'box-footer'])
