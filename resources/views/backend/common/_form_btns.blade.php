<div class="{{$wrap_class}} text-center">
	<a class="btn btn-default" href="{{route('backend.'.$prefix.'.index', array_merge(Arr::except(request()->route()->parameters, ['id']), request()->all()))}}"><i class="fa fa-list-ul"></i> @lang('backend.back', [], env('BACKEND_LOCALE'))</a>
	@if (Route::currentRouteName() == 'backend.'.$prefix.'.edit')
		@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
			<a class="btn btn-default btn-delete-row{{$template=='category'?' del-cate':''}}" href="{{ route('backend.'.$prefix.'.destroy', array_merge(request()->route()->parameters, ['id' => $data->id])) }}"><span class="fa fa-trash-o"></span> @lang('backend.delete', [], env('BACKEND_LOCALE'))</a>
        @endif
        @if (!empty($data->level) && $data->level<$data->getLimitLevel() && $auth_admin->group->checkPowerByName($prefix.'.create'))
            <a href="{{route('backend.'.$prefix.'.create', array_merge(request()->route()->parameters, ['parent_id'=>$data->id]))}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add_child', [], env('BACKEND_LOCALE'))</a>
        @endif
		<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.save', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@elseif (Route::currentRouteName() == 'backend.'.$prefix.'.create')
		<button type="submit" class="btn btn-primary">
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.add', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@else
		<button type="submit" class="btn btn-primary">
			@if (!empty($btn_txt))
				{{$btn_txt}}
			@else
				@lang('backend.submit', [], env('BACKEND_LOCALE'))
			@endif
		</button>
	@endif
</div>
