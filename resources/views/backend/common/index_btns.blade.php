<div class="{{$wrap_class}} text-center hidden-print">
	@if (isset($frontContent))
		{!!$frontContent!!}
    @endif
    @if (!empty($back))
        <a class="btn btn-default" href="{{$back['route']}}"><i class="fa fa-arrow-circle-left"></i> {{$back['text']}}</a>
    @endif
    @if (!$rank_all && !empty($rank_dropdowns))
        @foreach ($rank_dropdowns as $dropdown)
            <div class="btn-menu category-menu dropdown">
                <button class="btn btn-info" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$dropdown['label']}}： <span class="name"></span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    @includeFirst(['backend.'.$prefix.'.index_category_menu', 'backend.common.index_category_menu'], ['parents'=>$dropdown['options']])
                </ul>
            </div>
        @endforeach
	@endif
	@if (count($list_datas)>0)
		@if (Route::has('backend.'.$prefix.'.destroy') && $auth_admin->group->checkPowerByName($prefix.'.destroy'))
			<a href="{{route('backend.'.$prefix.'.destroy', request()->route()->parameters)}}" class="btn btn-danger btn-del" id="del_select_all"><span class="fa fa-trash-o"></span> @lang('backend.delete_selected', [], env('BACKEND_LOCALE'))</a>
		@endif
		@if (Route::has('backend.'.$prefix.'.modify') && $auth_admin->group->checkPowerByName($prefix.'.edit') && count($dropdowns))
			@foreach ($dropdowns as $dropdown)
                <span class="btn-menu dropdown">
                    <button class="btn bg-navy" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('backend.modify_sth', ['sth'=>$dropdown['label']], env('BACKEND_LOCALE')) <span class="name"></span>
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu bg-navy">
                        @foreach ($dropdown['options'] as $key => $value)
                            <li><a href="{{route('backend.'.$prefix.'.modify', array_merge(request()->route()->parameters, [$dropdown['name'] => $key]))}}" class="modify_select_all">{!!$value!!}</a></li>
                        @endforeach
                    </ul>
                </span>
            @endforeach
		@endif
	@endif
	@if ($auth_admin->group->checkPowerByName($prefix.'.rank') && Route::has('backend.'.$prefix.'.rank.index'))
		<a href="{{route('backend.'.$prefix.'.rank.index', array_merge(request()->route()->parameters, request()->only('categories')))}}" class="btn bg-purple"><i class="fa fa-sort-numeric-asc"></i> @lang('backend.rank', [], env('BACKEND_LOCALE'))</a>
	@endif
	@if (count($list_datas)>0)
		@if (Route::has('backend.'.$prefix.'.export'))
			<a href="{{route('backend.'.$prefix.'.export', array_merge(request()->route()->parameters, request()->all()))}}" class="btn btn-primary"><span class="fa fa-file-excel-o"></span> @lang('backend.export', [], env('BACKEND_LOCALE'))</a>
		@endif
	@endif
	@if ($auth_admin->group->checkPowerByName($prefix.'.create'))
        @php
            if (Storage::disk('import_example')->exists($model->getTable().'_import.xls')) {
                $import_file = $model->getTable().'_import.xls';
            } elseif (Storage::disk('import_example')->exists($model->getTable().'_import.xlsx')) {
                $import_file = $model->getTable().'_import.xlsx';
            } else {
                $import_file = null;
            }
        @endphp
        @if (Route::has('backend.'.$prefix.'.import') && $import_file)
            <a href="{{asset('import_example/'.$import_file)}}" download class="btn btn-default"><i class="fa fa-download"></i> @lang('backend.import_example_download')</a>
        @endif
		@if (Route::has('backend.'.$prefix.'.import'))
			<a href="{{route('backend.'.$prefix.'.import', request()->route()->parameters)}}" class="btn bg-teal btn-import-trigger"><i class="fa fa-upload"></i> @lang('backend.import', [], env('BACKEND_LOCALE'))</a>
		@endif
		@if (Route::has('backend.'.$prefix.'.create'))
			<a href="{{route('backend.'.$prefix.'.create', array_merge(request()->route()->parameters, request()->all()))}}" class="btn btn-success"><i class="fa fa-plus"></i> @lang('backend.add', [], env('BACKEND_LOCALE'))</a>
		@endif
	@endif
	@if (isset($endContent))
		{!!$endContent!!}
	@endif
</div>
