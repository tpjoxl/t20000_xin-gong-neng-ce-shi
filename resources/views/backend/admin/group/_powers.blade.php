<div class="form-group {{ $errors->first($error_name?? $name)?'has-error':'' }}">
    <label for="{{ $id??$name }}[]" class="col-md-2 col-sm-3 control-label">{{ !empty($required)?'*':'' }}{{ $label }}</label>
    <div class="col-sm-9">
        {{-- {{dd($options)}} --}}
        @foreach ($options as $option)
            @php
                $value = isset($data->{$n})?$data->{$n}->pluck('id')->toArray():request()->input($n) ?? ($form_field['default'] ?? []);
            @endphp
            <div class="row item-group">
                <div class="col-md-12 item-root">
                    <label><input type="checkbox" name="{{ $name }}[]" value="{{$option->id}}"
                    @if (old($error_name??$name, $value))
                        {{(in_array($option->id, old($error_name??$name, $value)))? 'checked' : ''}}
                    @else
                        {{($data->checkPower($option->id))? 'checked' : ''}}
                    @endif
                    >{{ $option->title }}</label>
                </div>
                @if ($option->children&&$option->children->count())
                    @foreach ($option->children as $child)
                        <div class="col-lg-3 col-md-4 col-xs-6 item-child">
                            <label><input type="checkbox" name="{{ $id??$name }}[]" value="{{$child->id}}"
                            @if (old($error_name??$name, $value))
                                {{(in_array($child->id, old($error_name??$name, $value)))? 'checked' : ''}}
                            @else
                                {{($data->checkPower($child->id))? 'checked' : ''}}
                            @endif
                            >{{ $child->title }}</label>
                        </div>
                    @endforeach
                @endif
            </div>
        @endforeach
    </div>
    @includeWhen(!empty($hint), 'backend.common.form_fields._hint')
</div>
