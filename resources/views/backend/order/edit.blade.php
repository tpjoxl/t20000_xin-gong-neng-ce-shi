@extends('backend.layouts.master')

@section('box-header-right')
    @include('backend.order._box_header_right')
@endsection

@section('content')
  <form class="form-horizontal" method="post" action="" accept-charset="UTF-8" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @includeFirst(['backend.'.$prefix.'._form', 'backend.common._form'], ['btns_view' => ['backend.'.$prefix.'._form_btns', 'backend.common._form_btns']])
  </form>
@endsection
@section('content-bottom')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">訂單 - 聯絡資訊</h3>
    </div>
    <div class="box-body form-horizontal">
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.name', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->name}} {{$data->sex?$data->present()->sex_title($data->sex):''}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.phone', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->phone}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.email', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->email}}</p>
            </div>
          </div>
          {{-- <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">購買人地址</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->address}}</p>
            </div>
          </div> --}}
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.note', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->note}}</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.receiver_name', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->receiver_name}} {{$data->receiver_sex?$data->present()->sex_title($data->receiver_sex):''}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.receiver_phone', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->receiver_phone}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.receiver_email', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">{{$data->receiver_email}}</p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 col-sm-5 control-label">@lang('validation.order.receiver_address', [], env('BACKEND_LOCALE'))</label>
            <div class="col-sm-7">
              <p class="form-control-static">
                {{$data->receiver_region?$data->receiver_region->code:''}}
                {{$data->receiver_city?$data->receiver_city->name:''}}
                {{$data->receiver_region?$data->receiver_region->name:''}}
                {{$data->receiver_address}}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if ($data->items)
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">訂單 - 購物清單</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table no-margin">
            <thead>
              <tr>
                <th>@lang('validation.order.product_belong', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_title', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_num', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_options', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_price', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_discount', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_qty', [], env('BACKEND_LOCALE'))</th>
                <th>@lang('validation.order.product_total', [], env('BACKEND_LOCALE'))</th>
              </tr>
            </thead>
            <tbody class="cart-holder">
              @foreach ($data->items as $key => $item)
                @php
                  $belong = 'product';
                @endphp
                <tr>
                  <td>
                      @lang('text.'.$belong)
                  </td>
                  <td><a href="{{route('backend.'.$belong.'.edit', $item->sku->product_id)}}">{{$item->title}}</a></td>
                  <td><a href="{{route('backend.'.$belong.'.'.$item->type.'.edit', $item->sku_id)}}">{{$item->num}}</a></td>
                  <td>{{$item->options}}</td>
                  <td>{{$item->sku->present()->price_type($item->price_type)}}：{{$item->price}}</td>
                  <td>{{0-$item->discount}}</td>
                  <td>{{$item->qty}}</td>
                  <td>{{$item->total}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  @endif
@endsection