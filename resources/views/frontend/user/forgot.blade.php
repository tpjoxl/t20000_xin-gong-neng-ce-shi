@extends('frontend.layouts.master')

@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
    <div class="container">
      <h1 class="page-title">@lang('text.user_forgot')</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <aside class="col-md-4 col-lg-3">
                    @include('frontend.'.$prefix.'.side')
                </aside>
                <div class="col-md-8 col-lg-9">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="forgot-account">@lang('validation.attributes.account') *</label>
                            <input type="email" class="form-control @error('account') is-invalid @enderror" id="forgot-account" name="account" placeholder="請輸入您註冊時使用的帳號" value="{{old('account')}}">
                            @error('account')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <small class="form-text text-info">送出表單後系統信將寄送至帳號所填寫的信箱，而非聯絡信箱</small>
                        </div>

                        <div class="form-group">
                            <label>@lang('validation.attributes.captcha') *</label>
                            <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                            </div>
                            @error('g-recaptcha-response')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-outline-primary-2">
                            <span>@lang('text.submit')</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection