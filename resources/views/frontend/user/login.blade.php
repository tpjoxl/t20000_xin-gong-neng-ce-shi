@extends('frontend.layouts.master')

@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="section">
	<div class="container">
    	<div class="row">
            <div class="col-xl-3">
                @include('frontend.'.$prefix.'.side')
            </div>
            <div class="col-xl-9">
                <div class="form-choice">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{!empty(env('GOOGLE_CLIENT_ID'))?route('auth.google'):''}}" class="btn btn-login btn-g">
                                <i class="icon-google"></i>
                                使用 Google 登入
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{!empty(env('FACEBOOK_CLIENT_ID'))?route('auth.facebook'):''}}" class="btn btn-login btn-f">
                                <i class="icon-facebook-f"></i>
                                使用 Facebook 登入
                            </a>
                        </div>
                    </div>
                </div>
                <hr>
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="login-account">@lang('validation.attributes.account') *</label>
                        <input type="email" class="form-control @error('account') is-invalid @enderror" id="login-account" name="account" value="{{old('account')}}">
                        @error('account')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="login-password">@lang('validation.attributes.password') *</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="login-password" name="password">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>@lang('validation.attributes.captcha') *</label>
                        <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                        </div>
                        @error('g-recaptcha-response')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-footer pb-0 mb-0 bb-0">
                        <button type="submit" class="btn btn-outline-primary">
                            <span>@lang('text.login')</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="login-remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="login-remember">記住我</label>
                        </div>

                        <a href="{{route($prefix.'.forgot')}}" class="forgot-link">忘記密碼?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
