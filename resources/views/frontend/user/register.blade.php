@extends('frontend.layouts.master')

@section('style')
    <link rel="stylesheet" href="{{asset('assets/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/air-datepicker/dist/css/datepicker.min.css')}}">
@endsection
@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="section">
	<div class="container">
    	<div class="row">
            <div class="col-xl-3">
                @include('frontend.'.$prefix.'.side')
            </div>
            <div class="col-xl-9">
                <div class="form-choice">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{!empty(env('GOOGLE_CLIENT_ID'))?route('auth.google'):''}}" class="btn btn-login btn-g">
                                <i class="icon-google"></i>
                                使用 Google 註冊
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a href="{{!empty(env('FACEBOOK_CLIENT_ID'))?route('auth.facebook'):''}}" class="btn btn-login btn-f">
                                <i class="icon-facebook-f"></i>
                                使用 Facebook 註冊
                            </a>
                        </div>
                    </div>
                </div>
                <hr>
                <form action="" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="regiater-account">@lang('validation.attributes.account') *</label>
                        <input type="email" class="form-control @error('account') is-invalid @enderror" id="regiater-account" name="account" placeholder="@lang('text.account_hint')" value="{{old('account')}}">
                        @error('account')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="regiater-email">@lang('validation.attributes.email') *</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror" id="regiater-email" name="email" placeholder="@lang('text.user_email_hint')" value="{{old('email')}}">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="regiater-password">@lang('validation.attributes.password') *</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="regiater-password" name="password" placeholder="@lang('text.password_hint')">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="regiater-password_confirmation">@lang('validation.attributes.password_confirmation') *</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="regiater-password_confirmation" name="password_confirmation">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="regiater-name">@lang('validation.attributes.name') *</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="regiater-name" name="name" value="{{old('name')}}">
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="regiater-sex">@lang('validation.attributes.sex')</label>
                                <select class="form-control @error('sex') is-invalid @enderror" id="regiater-sex" name="sex">
                                    <option value="">@lang('text.please_select')</option>
                                    @foreach ($data->present()->sex() as $key => $val)
                                    <option value="{{$key}}" {{!is_null(old('sex'))&&old('sex')==$key?'selected':''}}>{!!$val!!}</option>
                                    @endforeach
                                </select>
                                @error('sex')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="regiater-birthday">@lang('validation.attributes.birthday') *</label>
                                <input type="text" class="form-control dp @error('birthday') is-invalid @enderror" id="regiater-birthday" name="birthday" value="{{old('birthday')}}" autocomplete="off">
                                @error('birthday')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="regiater-phone">@lang('validation.attributes.phone') *</label>
                                <input type="text" class="form-control @error('phone') is-invalid @enderror" id="regiater-phone" name="phone" value="{{old('phone')}}">
                                @error('phone')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="regiater-city_id">@lang('validation.attributes.tw_city_region')</label>
                        <div class="row city-selector">
                            <div class="col-sm-6">
                                <select class="form-control city @error('city_id') is-invalid @enderror" id="regiater-city_id" name="city_id">
                                    <option value="">請選擇縣市</option>
                                    @foreach ($cities as $city)
                                    <option value="{{$city->id}}"  {{ ($city->id == old('city_id')) ? 'selected' : '' }}>{{$city->name}}</option>
                                    @endforeach
                                </select>
                                @error('city_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <select class="form-control region @error('region_id') is-invalid @enderror" id="regiater-region_id" name="region_id">
                                    @if (old('city_id') && old('region_id'))
                                    @foreach ($cities[old('city_id')]->regions as $region)
                                    <option value="{{$region->id}}"  {{ ($region->id == old('region_id')) ? 'selected' : '' }}>{{$region->name}}</option>
                                    @endforeach
                                @else
                                    <option value="">請先選擇縣市</option>
                                @endif
                                </select>
                                @error('region_id')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="regiater-address">@lang('validation.attributes.address') *</label>
                        <input type="text" class="form-control @error('address') is-invalid @enderror" id="regiater-address" name="address" value="{{old('address')}}">
                        @error('address')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>@lang('validation.attributes.captcha') *</label>
                        <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                        </div>
                        @error('g-recaptcha-response')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-footer pb-0 mb-0 bb-0">
                        <button type="submit" class="btn btn-outline-primary">
                            <span>@lang('text.register')</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>

                        <div class="custom-control custom-checkbox @error('agree') is-invalid @enderror">
                            <input type="checkbox" class="custom-control-input" id="register-agree" name="agree" value="1" {{!is_null(old('agree'))&&old('agree')==1?'checked':''}}>
                            <label class="custom-control-label" for="register-agree">@lang('text.agree_txt', ['url_terms'=>route('terms.index')]) *</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    (function($) {
        var cities = {!!json_encode($cities)!!}
        // console.log(cities)
        // 縣市切換就更新區域下拉
        $(document).on('change', 'select.city', function(){
            if ($(this).val() != '') {
                var cityId = $(this).val();
                var regions = '';
                $.each(cities[cityId]['regions'], function(key, region) {
                    regions += '<option value="'+region.id+'">'+region.name+'</option>';
                });
                $(this).parents('.city-selector').find('.region').html(regions);
            } else {
                $(this).parents('.city-selector').find('.region').html('<option value="">請先選擇縣市</option>');
            }
        });
    })($)
</script>
@endsection
