@extends('frontend.layouts.master')

@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
    <div class="container">
      <h1 class="page-title">@lang('text.user_edit')</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <aside class="col-md-4 col-lg-3">
                    @include('frontend.'.$prefix.'.side')
                </aside>
                <div class="col-md-8 col-lg-9">
                    <form action="" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="edit-account">@lang('validation.attributes.account')</label>
                                    <input type="email" class="form-control" id="edit-account" placeholder="@lang('text.account_hint')" value="{{$data->account}}" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="edit-birthday">@lang('validation.attributes.birthday')</label>
                                    <input type="text" class="form-control" id="edit-birthday" value="{{$data->birthday}}" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-email">@lang('validation.attributes.email') *</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="edit-email" name="email" placeholder="@lang('text.user_email_hint')" value="{{old('email', $data->email)}}">
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="edit-name">@lang('validation.attributes.name') *</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="edit-name" name="name" value="{{old('name', $data->name)}}">
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="edit-sex">@lang('validation.attributes.sex')</label>
                                    <select class="form-control @error('sex') is-invalid @enderror" id="edit-sex" name="sex">
                                      <option value="">@lang('text.please_select')</option>
                                      @foreach ($data->present()->sex() as $key => $val)
                                        <option value="{{$key}}" {{!is_null(old('sex', $data->sex))&&old('sex', $data->sex)==$key?'selected':''}}>{!!$val!!}</option>
                                      @endforeach
                                    </select>
                                    @error('sex')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-phone">@lang('validation.attributes.phone') *</label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="edit-phone" name="phone" value="{{old('phone', $data->phone)}}">
                            @error('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="regiater-city_id">@lang('validation.attributes.tw_city_region')</label>
                            <div class="row city-selector">
                                <div class="col-6">
                                    <select class="form-control city @error('city_id') is-invalid @enderror" id="regiater-city_id" name="city_id">
                                        <option value="">請選擇縣市</option>
                                        @foreach ($cities as $city)
                                        <option value="{{$city->id}}"  {{ ($city->id == old('city_id', $data->city_id)) ? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('city_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <select class="form-control region @error('region_id') is-invalid @enderror" id="regiater-region_id" name="region_id">
                                        @if (old('city_id', $data->city_id) && old('region_id', $data->region_id))
                                        @foreach ($cities[old('city_id', $data->city_id)]->regions as $region)
                                        <option value="{{$region->id}}"  {{ ($region->id == old('region_id', $data->region_id)) ? 'selected' : '' }}>{{$region->name}}</option>
                                        @endforeach
                                    @else
                                        <option value="">請先選擇縣市</option>
                                    @endif
                                    </select>
                                    @error('region_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-address">@lang('validation.attributes.address') *</label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" id="edit-address" name="address" value="{{old('address', $data->address)}}">
                            @error('address')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>@lang('validation.attributes.captcha') *</label>
                            <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                            </div>
                            @error('g-recaptcha-response')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-outline-primary-2">
                            <span>儲存變更</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        (function($) {
            var cities = {!!json_encode($cities)!!}
            // console.log(cities)
            // 縣市切換就更新區域下拉
            $(document).on('change', 'select.city', function(){
                if ($(this).val() != '') {
                    var cityId = $(this).val();
                    var regions = '';
                    $.each(cities[cityId]['regions'], function(key, region) {
                        regions += '<option value="'+region.id+'">'+region.name+'</option>';
                    });
                    $(this).parents('.city-selector').find('.region').html(regions);
                } else {
                    $(this).parents('.city-selector').find('.region').html('<option value="">請先選擇縣市</option>');
                }
            });
        })($)
    </script>
@endsection
