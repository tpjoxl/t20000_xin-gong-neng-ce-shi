@extends('frontend.layouts.master')

@section('content')
    <div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
        <div class="container">
        <h1 class="page-title">@lang('text.user_bonus')</h1>
        </div>
    </div>
  @include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
  <div class="page-content">
    <div class="dashboard">
      <div class="container">
          <div class="row">
              <aside class="col-md-4 col-lg-3">
                  @include('frontend.'.$prefix.'.side')
              </aside>
              <div class="col-md-8 col-lg-9">
                @if ($auth_web)
                  <div class="member-bonus mb-3">目前紅利點數為 <span class="point">{{$auth_web->bonuses()->display()->get()->sum('point')}}</span> 點</div>
                @else
                  <div class="member-bonus mb-3">目前未登入，請登入會員查看您的點數</div>
                  <a href="{{route('user.login')}}" class="btn btn-outline-primary">@lang('text.user_login')</a>
                  <a href="{{route('user.register')}}" class="btn btn-outline-dark">@lang('text.user_register')</a>
                @endif
                <hr>
                <article class="editor">{!!$bonusSetting->text!!}</article>
              </div>
          </div>
      </div>
    </div>
  </div>
@endsection
