@extends('frontend.layouts.master')

@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
    <div class="container">
      <h1 class="page-title">@lang('text.user_edit')</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <aside class="col-md-4 col-lg-3">
                    @include('frontend.'.$prefix.'.side')
                </aside>
                <div class="col-md-8 col-lg-9">
                    <form action="" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="password-account">@lang('validation.attributes.account')</label>
                            <input type="email" class="form-control" id="password-account" placeholder="@lang('text.account_hint')" value="{{$data->account}}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="password-old_password">@lang('validation.attributes.old_password') *</label>
                            <input type="password" class="form-control @error('old_password') is-invalid @enderror" id="password-old_password" name="old_password">
                            @error('old_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="password-password">@lang('validation.attributes.new_password') *</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password-password" name="password" placeholder="@lang('text.password_hint')">
                            @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password-password_confirmation">@lang('validation.attributes.password_confirmation') *</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password-password_confirmation" name="password_confirmation">
                            @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>@lang('validation.attributes.captcha') *</label>
                            <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                            </div>
                            @error('g-recaptcha-response')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-outline-primary-2">
                            <span>儲存變更</span>
                            <i class="icon-long-arrow-right"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection