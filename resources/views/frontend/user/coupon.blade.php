@extends('frontend.layouts.master')

@section('content')
    <div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
        <div class="container">
        <h1 class="page-title">@lang('text.user_bonus')</h1>
        </div>
    </div>
  @include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
  <div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <aside class="col-md-4 col-lg-3">
                    @include('frontend.'.$prefix.'.side')
                </aside>
                <div class="col-md-8 col-lg-9">
                    @if ($items->count())
                        <table class="table table-mobile">
                            <thead>
                                <tr>
                                    <th scope="col">@lang('validation.coupon.title')</th>
                                    <th scope="col">@lang('validation.coupon.code')</th>
                                    <th scope="col">@lang('validation.coupon.content')</th>
                                    <th scope="col">@lang('validation.coupon.date_range')</th>
                                    <th scope="col">@lang('validation.coupon.min_price')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->code}}</td>
                                        <td>{{$item->content}}</td>
                                        <td>{{$item->date_on}}～{{$item->date_off}}</td>
                                        <td><span class="d-inline d-lg-none">@lang('validation.coupon.min_price')：</span>{{$item->min_price}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center pt-3 pb-3">@lang('text.no_data')</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
