@extends('frontend.layouts.master')

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc($page->banner?:'images/page-header-bg.jpg')}}')">
    <div class="container">
    <h1 class="page-title">@lang('text.user_order')</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <aside class="col-md-4 col-lg-3">
                    @include('frontend.user.side')
                </aside>
                <div class="col-md-8 col-lg-9">
                    @include('frontend.order._detail')

                    <div class="clearfix">
                        <div class="float-left"><a class="btn btn-outline-dark btn-sm" href="{{route('user.order.index')}}"><i class="demo-icon icon-arrow-left"></i> 回列表</a></div>
                        <div class="float-right">
                            @if ($data->status == 1)
                                <button class="btn btn-dark btn-sm btn-cancel" data-toggle="modal" data-target="#cancelModal" data-id="{{$data->id}}">取消訂單</button>
                                @if ($data->payment_info['type'] != 'CREDIT' && !is_null($data->payment_return))
                                    <button class="btn btn-primary btn-sm btn-payinfo" data-toggle="modal" data-target="#payinfoModal" data-id="{{$data->id}}">付款資訊</button>
                                @else
                                    <a class="btn btn-primary btn-sm btn-pay" href="{{route('order.pay', ['num' => $data->num])}}">重新付款</a>
                                @endif
                            @endif
                        </div>
                    </div>
                    @includeWhen($data->status == 1, 'frontend.order._cancel_modal')
                    @includeWhen($data->status == 1, 'frontend.order._payinfo_modal')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    @include('frontend.order._script')
@endsection
