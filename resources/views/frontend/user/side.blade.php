<ul class="nav nav-dashboard flex-column mb-3 mb-md-0" role="tablist">
    @if ($auth_web)
        <li class="nav-item"><a class="nav-link {{ strpos(Route::currentRouteName(), 'user.order') !== false ? 'active' : '' }}" href="{{route('user.order.index')}}">@lang('text.user_order')</a></li>
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.coupon') ? 'active' : '' }}" href="{{route('user.coupon')}}"><span class="categories_name">@lang('text.user_coupon')</span></a></li>
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.bonus') ? 'active' : '' }}" href="{{route('user.bonus')}}"><span class="categories_name">@lang('text.user_bonus')</span></a></li>
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.edit') ? 'active' : '' }}" href="{{route('user.edit')}}"><span class="categories_name">@lang('text.user_edit')</span></a></li>
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.password') ? 'active' : '' }}" href="{{route('user.password')}}"><span class="categories_name">@lang('text.password')</span></a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('user.logout')}}"><span class="categories_na</span>me">@lang('text.user_logout')</a></li>
    @else
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.register') ? 'active' : '' }}" href="{{route('user.register')}}"><span class="categories_name">@lang('text.user_register')</span></a></li>
        <li class="nav-item"><a class="nav-link {{ (Route::currentRouteName() == 'user.login') ? 'active' : '' }}" href="{{route('user.login')}}"><span class="categories_name">@lang('text.user_login')</span></a></li>
    @endif
</ul>