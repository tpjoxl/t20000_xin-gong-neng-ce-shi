@extends('frontend.layouts.master')

@section('head_script')
  <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="main-top">
    <div class="container">
        <div class="main-top-title">@lang('text.'.$prefix)</div>
    </div>
</div>
<div class="main-content">
    <div class="container">
        @include('frontend.layouts.breadcrumb')
        <article class="editor">{!!$data->text!!}</article>
        <hr>
        <form class="contact-form form-box" method="post" action="">
            @csrf
            <input type="hidden" name="locale" value="{{app()->getLocale()}}">

            <div class="field-group @error('name') has-error @enderror">
              <label class="label" for="contact_name"><b>*</b> @lang('validation.attributes.name')</label>
              <input name="name" id="contact_name" class="field" type="text" value="{{old('name')}}">
            </div>
            @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="field-group @error('sex') has-error @enderror">
              <label class="label" for="contact_sex"><b>*</b> @lang('validation.attributes.sex_title')</label>
              @foreach ($model->present()->sex_title() as $key => $val)
                <label for="contact_sex{{$key}}">
                    <input type="radio" name="sex" id="contact_sex{{$key}}" value="{{$key}}" {{old('sex')&&old('sex')==$key?'checked':''}}> {!!$val!!}
                </label>
              @endforeach
            </div>
            @error('sex')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="field-group @error('phone') has-error @enderror">
              <label class="label" for="contact_phone"><b>*</b> @lang('validation.attributes.phone')</label>
              <input name="phone" id="contact_phone" class="field" type="text" value="{{old('phone')}}">
            </div>
            @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="field-group @error('email') has-error @enderror">
              <label class="label" for="contact_email"><b>*</b> @lang('validation.attributes.email')</label>
              <input name="email" id="contact_email" class="field" type="text" value="{{old('email')}}">
            </div>
            @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="field-group @error('message') has-error @enderror">
              <label class="label" for="contact_message"><b>*</b> @lang('validation.attributes.message')</label>
              <textarea name="message" id="contact_message" class="field" rows="5" placeholder="在此輸入訊息">{{old('message')}}</textarea>
            </div>
            @error('message')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="field-group">
              <label class="label"><b>*</b> @lang('validation.attributes.captcha')</label>
              <div class="captcha @error('g-recaptcha-response') has-error @enderror">
                <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
              </div>
            </div>
            @error('g-recaptcha-response')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-btn-box">
              <button type="submit" class="btn btn-primary">@lang('text.submit')</button>
            </div>
        </form>
    </div>
</div>
@endsection
