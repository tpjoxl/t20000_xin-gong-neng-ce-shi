@extends('frontend.layouts.master')

@section('head')
  @include('frontend.layouts.page_link', ['datas' => $items])
@endsection

@section('content')
  <div class="main-top">
    <div class="container">
      @if (Route::currentRouteName()=='blog.index')
        <h1 class="main-top-title">@lang('text.blog')</h1>
      @else
        <div class="main-top-title">@lang('text.blog')</div>
      @endif
    </div>
  </div>
  <div class="main-content has-side">
    <div class="container">
      @include('frontend.layouts.breadcrumb')
      <aside class="side">
        @include('frontend.blog.side')
      </aside>
      <div class="content">
        @if (Route::currentRouteName()=='blog.index')
          <div class="main-content-title style2">@lang('text.all_blog')</div>
        @elseif (Route::currentRouteName()=='blog.search')
          <div class="main-content-title style2">@lang('text.search_keyword')：{{request()->q}}</div>
        @else
          <h1 class="main-content-title style2">{{$data->title}}</h1>
        @endif
        @if (count($items)>0)
          <ul class="blog-list list">
            @foreach ($items as $item)
              <li class="item">
                <div class="box">
                  <div class="pic">
                    <a href="{{$item->url?:route($prefix.'.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}">
                      <img src="{{imgsrc($item->pic_path)}}" alt="{{$item->title}}">
                    </a>
                  </div>
                  <h2 class="title"><a href="{{$item->url?:route($prefix.'.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}">{{$item->title}}</a></h2>
                  <div class="txt">
                    <div class="category">{{$item->categories->first()->title}}</div>
                    <div class="date">{{$item->created_at->format('Y.m.d')}}</div>
                    <p class="desc">{{$item->description}}</p>
                    <div class="more"><a href="{{$item->url?:route($prefix.'.detail', ['slug'=>rawurlencode($item->slug)])}}" target="{{$item->url?'_blank':''}}">...more</a></div>
                  </div>
                </div>
              </li>
            @endforeach
          </ul>
          {{ $items->appends(request()->input())->links('vendor.pagination.default_front') }}
        @else
          @if (Route::currentRouteName()=='blog.search')
            <div class="text-center">@lang('text.search_no_data')</div>
          @else
            <div class="text-center">@lang('text.no_data')</div>
          @endif
        @endif
      </div>
    </div>
  </div>
@endsection
