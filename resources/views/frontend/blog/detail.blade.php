@extends('frontend.layouts.master')

@section('head_script')
    @include('frontend.layouts.blog-json-ld')
@endsection

@section('content')
  <div class="main-top">
    <div class="container">
        <div class="main-top-title">@lang('text.blog')</div>
    </div>
  </div>
  <div class="main-content has-side">
    <div class="container">
      @include('frontend.layouts.breadcrumb')
      <aside class="side">
        @include('frontend.blog.side')
      </aside>
      <div class="content">
        <div class="blog-detail">
          <h1 class="title">{{$data->title}}</h1>
          <div class="meta">
            <div class="category">{{$data->categories->first()->title}}</div>
            <div class="date">{{$data->created_at->format('Y.m.d')}}</div>
            <div class="share">
              <div class="label">分享：</div>
              <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_line"></a>
                <a class="a2a_button_copy_link"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
          </div>
          <article class="editor">{!!$data->text!!}</article>
          @if ($data->tags->count() > 0)
            <div class="tags">
              <i class="ic ic-tag"></i>
              @foreach ($data->tags as $tag)
                <span class="tag">{{$tag->title}}</span>
              @endforeach
            </div>
          @endif
          <div class="meta">
            <div class="share">
              <div class="label">分享：</div>
              <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_line"></a>
                <a class="a2a_button_copy_link"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
            </div>
          </div>
          <div class="pager">
            @if ($prev)
              <a class="btn-prev"
              @if ($prev->url)
                href="{{$prev->url}}" target="_blank"
              @else
                href="{{route($prefix.'.detail', ['slug'=>rawurlencode($prev->slug)])}}"
              @endif>
                <span class="btn"><i class="ic prev"></i> 上一則</span>
                <span class="txt">{{$prev->title}}</span>
              </a>
            @endif
            @if ($next)
              <a class="btn-next"
              @if ($next->url)
                href="{{$next->url}}" target="_blank"
              @else
                href="{{route($prefix.'.detail', ['slug'=>rawurlencode($next->slug)])}}"
              @endif>
                <span class="btn">下一則 <i class="ic next"></i></span>
                <span class="txt">{{$next->title}}</span>
              </a>
            @endif
          </div>
          @if ($data->recommends->count() > 0)
            <div class="recommend">
              <div class="recommend-heading">推薦閱讀</div>
              <ul class="recommend-list list">
                @foreach ($data->recommends as $recommend)
                  <li class="item">
                    <a class="box" href="{{$recommend->url?:route($prefix.'.detail', ['slug'=>rawurlencode($recommend->slug)])}}" target="{{$recommend->url?'_blank':''}}" title="{{$recommend->title}}">
                      <span class="pic"><img src="{{imgsrc($recommend->pic_path)}}" alt="{{$recommend->title}}"></span>
                      <span class="title">{{$recommend->title}}</span>
                    </a>
                  </li>
                @endforeach
              </ul>
            </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection
