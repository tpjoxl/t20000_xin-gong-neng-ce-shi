<div class="side-nav pc style2">
  <div class="side-nav-title"></div>
  <ul class="side-nav-list list">
    <li class="{{Route::currentRouteName()=='blog.index'?'current':''}}">
      <a href="{{route('blog.index')}}">@lang('text.all_blog')</a>
    </li>
    @foreach ($menu_blog_categories as $menu_blog_category)
      <li class="{{Route::currentRouteName()=='blog.category'&&$data->id==$menu_blog_category->id?'current':''}}">
        <a href="{{route('blog.category', ['category' => $menu_blog_category->slug])}}">{{$menu_blog_category->title}}<small>({{$menu_blog_category->blogs_count}})</small></a>
      </li>
    @endforeach
  </ul>
</div>
<div class="side-nav pc style2">
  <div class="side-nav-title">最新文章</div>
  <ul class="side-nav-list list">
    @foreach ($blog_latests as $blog_latest)
      <li class="{{Route::currentRouteName()=='blog.detail'&&$data->id==$blog_latest->id?'current':''}}">
        <a href="{{route('blog.detail', ['slug' => $blog_latest->slug])}}">{{$blog_latest->title}}</a>
      </li>
    @endforeach
  </ul>
</div>
<form class="side-search" action="{{route($prefix.'.search')}}" method="get">
  <input type="text" id="side-q" name="q" class="field" placeholder="@lang('text.search_placeholder')" value="{{request()->q}}" required>
  <button type="submit" class="btn">文章@lang('text.search')</button>
</form>
