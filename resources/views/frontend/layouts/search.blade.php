<form class="search-form" action="{{route('search.index')}}" method="get">
  <div class="input-group">
    <input type="text" name="q" class="field" placeholder="@lang('text.search_placeholder')" value="{{request()->q}}" required>
    <button type="submit" class="btn btn-normal">@lang('text.search')</button>
  </div>
  <div class="search-target">
    <label class="custom-radio"><input type="radio" name="search_target" value="1" {{!request()->has('search_target')||request()->search_target==1?'checked':''}}><span>產品搜尋</span></label>
    <label class="custom-radio"><input type="radio" name="search_target" value="2" {{request()->search_target==2?'checked':''}}><span>文章搜尋</span></label>
  </div>
</form>
