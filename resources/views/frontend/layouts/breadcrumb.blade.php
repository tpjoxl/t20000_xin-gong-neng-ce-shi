<div class="breadcrumbs">
  <a href="{{route('home')}}" title="@lang('text.home')">@lang('text.home')</a>
  @foreach ($breadcrumb as $key => $path)
    @if ($key == count($breadcrumb)-1)
      <span>{{$path[0]}}</span>
    @else
      <a href="{{$path[1]}}" title="{{$path[2]}}">{{$path[0]}}</a>
    @endif
  @endforeach
</div>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement":
  [
    @php
      $last = last($breadcrumb);
    @endphp
    {
      "@type": "ListItem",
      "position": 1,
      "item":
      {
        "@id": "{{route('home')}}",
        "name": "{{__('text.home')}}"
      }
    },
    @foreach ($breadcrumb as $key=>$path)
    {
      "@type": "ListItem",
      "position": {{$key+2}},
      "item":
      {
        "@id": "{{$path[1]}}",
        "name": "{{$path[0]}}"
      }
    }
    {{$last!=$path ? ',' : ''}}
    @endforeach
  ]
}
</script>
