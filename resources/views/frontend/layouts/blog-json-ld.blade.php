@php
  $page_title=$page_description=$page_keyword=$page_pic=$page_og_title=$page_og_description='';
  // SEO Description
  if (isset($data->seo_description) && !empty($data->seo_description)) {
    $page_description .= $data->seo_description;
  } elseif (isset($data->description) && !empty($data->description)) {
    $page_description .= $data->description;
  } elseif (isset($setting->seo_description) && !empty($setting->seo_description)) {
    $page_description .= $setting->seo_description;
  }
  // SEO Keyword
  if (isset($data->seo_keyword) && !empty($data->seo_keyword)) {
    $page_keyword .= $data->seo_keyword;
  } elseif (isset($setting->seo_keyword) && !empty($setting->seo_keyword)) {
    $page_keyword .= $setting->seo_keyword;
  }
@endphp
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BlogPosting",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "{{request()->url()}}"
  },
  "headline": "{{ $data->title }}",
  "image": "{{imgsrc($data->pic_path)}}",
  "datePublished": "{{$data->created_at->format('Y-m-d')}}",
  "dateModified": "{{$data->updated_at->format('Y-m-d')}}",
  "author": {
    "@type": "Organization",
    "name": "{{ $setting->name }}"
  },
   "publisher": {
    "@type": "Organization",
    "name": "{{ $setting->name }}",
    "logo": {
      "@type": "ImageObject",
      "url": "{{asset('images/logo.png')}}"
    }
  },
  "description": "{{$page_description}}"
}
</script>
