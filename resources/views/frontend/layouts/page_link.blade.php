@if ($datas->hasPages())
  @if (!$datas->onFirstPage())
    <link rel="prev" href="{{ $datas->previousPageUrl() }}">
  @endif
  @if ($datas->hasMorePages())
    <link rel="next" href="{{ $datas->nextPageUrl() }}">
  @endif
@endif
