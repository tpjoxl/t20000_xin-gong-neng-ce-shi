<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" class="no-js">
  <head>
    @include('frontend.layouts.head')

    @yield('head')

    @yield('style')

    @yield('head_script')

    {!! $setting->head_code !!}
  </head>
  <body class="@yield('body_class')">
    <div id="app" class="wrapper">
      @include('frontend.layouts.header')
      <main class="main">
        @yield('content')
      </main>
      @include('frontend.layouts.footer')
    </div>

    {{-- @include('frontend.layouts.floating') --}}
    @include('frontend.layouts.foot')
    @include('frontend.layouts.message')
    @yield('script')

    {!! $setting->body_code !!}
  </body>
</html>
