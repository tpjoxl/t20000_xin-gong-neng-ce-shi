<header class="header">
    <div class="container">
        <div class="site-title">
            <a href="{{Route::has('home')? route('home') : ''}}" class="logo">{{ $setting->name }}</a>
        </div>
        <button type="button" class="btn nav-toggle">
            <span class="ic ic-bar"></span>
            <span class="ic ic-bar"></span>
            <span class="ic ic-bar"></span>
            <span class="sr-only">@lang('text.toggle_nav')</span>
        </button>
        <nav class="nav">
            <div class="nav-content">
                <ul class="menu">
                    <li class="{{ (Route::currentRouteName() == 'about.index') ? 'current' : '' }}">
                        <a href="{{Route::has('about.index')? route('about.index') : ''}}" title="@lang('text.about')">@lang('text.about')</a>
                    </li>
                    <li class="{{ (Route::currentRouteName() == 'product.index') ? 'current' : '' }}">
                        <a href="{{Route::has('product.index')? route('product.index') : ''}}" title="@lang('text.product')">@lang('text.product')</a>
                    </li>
                    <li class="{{ (Route::currentRouteName() == 'blog.index') ? 'current' : '' }}">
                        <a href="{{Route::has('blog.index')? route('blog.index') : ''}}" title="@lang('text.blog')">@lang('text.blog')</a>
                    </li>
                    <li class="{{ (Route::currentRouteName() == 'contact.index') ? 'current' : '' }}">
                        <a href="{{Route::has('contact.index')? route('contact.index') : ''}}" title="@lang('text.contact')">@lang('text.contact')</a>
                    </li>
                    <li class="{{ (Route::currentRouteName() == 'cart.index') ? 'current' : '' }}">
                        <a href="{{Route::has('cart.index')? route('cart.index') : ''}}" title="@lang('text.cart')">@lang('text.cart')</a>
                    </li>
                    <li class="{{ (Route::currentRouteName() == 'order_query.index') ? 'current' : '' }}">
                        <a href="{{Route::has('order_query.index')? route('order_query.index') : ''}}" title="@lang('text.order_query')">@lang('text.order_query')</a>
                    </li>
                    <li class="{{ explode('.', Route::currentRouteName())[0] == 'user' ? 'active' : '' }}">
                        <a href="{{route('user.login')}}">@lang('text.user_centre')</a>
                        <ul>
                            @if ($auth_web)
                                <li class="{{ strpos(Route::currentRouteName(), 'user.order') != false ? 'active' : '' }}"><a href="{{route('user.order.index')}}">@lang('text.user_order')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.coupon') ? 'active' : '' }}"><a href="{{route('user.coupon')}}">@lang('text.user_coupon')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.bonus') ? 'active' : '' }}"><a href="{{route('user.bonus')}}">@lang('text.user_bonus')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.edit') ? 'active' : '' }}"><a href="{{route('user.edit')}}">@lang('text.user_edit')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.password') ? 'active' : '' }}"><a href="{{route('user.password')}}">@lang('text.password')</a></li>
                                <li><a href="{{route('user.logout')}}">@lang('text.logout')</a></li>
                            @else
                                <li class="{{ (Route::currentRouteName() == 'user.register') ? 'active' : '' }}"><a href="{{route('user.register')}}">@lang('text.register')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.login') ? 'active' : '' }}"><a href="{{route('user.login')}}">@lang('text.login')</a></li>
                                <li class="{{ (Route::currentRouteName() == 'user.forgot') ? 'active' : '' }}"><a href="{{route('user.forgot')}}">@lang('text.forgot')</a></li>
                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="header-lang">
        <button class="btn lang-toggle"><i class="ic ic-global"></i></button>
        <ul class="lang list">
            @foreach ($lang_datas as $locale)
                <li class="{{app()->getLocale() == $locale->code?'current':''}}">
                    <a href="{{$locale->present()->langUrl(isset($data) && !is_null($data) ? $data : null, $locale->code)}}">{{$locale->title}}</a>
                </li>
            @endforeach
        </ul>
    </div>
    </div>
</header>

<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "SiteNavigationElement",
        "headline": "headline-string",
        "@id": "#nav",
        "@graph": [{
            "@context": "https://schema.org",
            "@type": "SiteNavigationElement",
            "@id": "#nav",
            "name": "@lang('text.blog')",
            "url": "{{Route::has('blog.index')? route('blog.index') : ''}}"
        },{
            "@context": "https://schema.org",
            "@type": "SiteNavigationElement",
            "@id": "#nav",
            "name": "@lang('text.contact')",
            "url": "{{Route::has('contact.index')? route('contact.index') : ''}}"
        }]
    }

</script>
