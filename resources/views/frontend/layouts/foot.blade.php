@php
    try {
        $main_js = mix('js/main.js');
    } catch(Exception $e) {
        $main_js = asset('js/main.js');
    }
@endphp
<script src="{{$main_js}}"></script>
