<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
    <i class="icon-shopping-cart"></i>
    <span class="cart-count">{{$cart->totalQty}}</span>
</a>

<div class="dropdown-menu dropdown-menu-right">
    @if (count($cart->items))
        <div class="dropdown-cart-products">
            @foreach ($cart->items as $item)
                @php
                    $prefix = 'product';
                @endphp
                <div class="product">
                    <div class="product-cart-details">
                        <h4 class="product-title">
                            <a href="{{route($prefix.'.detail', ['slug'=>$item['sku']->product->slug])}}">{{$item['sku']->title}}</a>
                        </h4>

                        <span class="cart-product-info">
                            <span class="cart-product-qty">{{$item['qty']}}</span>
                            x {{$item['price']}}
                        </span>
                    </div>

                    <figure class="product-image-container">
                        <a href="{{route($prefix.'.detail', ['slug'=>$item['sku']->product->slug])}}" class="product-image">
                            <img src="{{imgsrc($item['sku']->product->pic_path)}}" alt="{{$item['sku']->product->title}}">
                        </a>
                    </figure>
                    <a href="#" class="btn-remove js-cart-delete-item" data-sku="{{$item['sku_id']}}" title="刪除商品"><i class="icon-close"></i></a>
                </div>
            @endforeach
        </div>

        <div class="dropdown-cart-total">
            <span>@lang('validation.order.items_total_price')</span>

            <span class="cart-total-price">{{$cart->totalPrice}}</span>
        </div>

        <div class="dropdown-cart-action">
            <a href="{{route('cart.index')}}" class="btn btn-primary btn-block"><span>前往購物車結帳</span><i class="icon-long-arrow-right"></i></a>
        </div>
    @else
        <div class="text-center pt-3 pb-3">
            @lang('text.cart_no_item')
        </div>
    @endif
</div>