<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">
@php
  $page_title = ($custom_page_title??$data->seo_title??$data->title??$setting->seo_title);
  // SEO Title
  $page_title .= ($page_title?' - ':'').$setting->name;
  // SEO Description
  $page_description = ($data->seo_description??$data->description??$setting->seo_description);
  // SEO Keyword
  $page_keyword = ($data->seo_keyword??$setting->seo_keyword);
  // og image
  $page_pic = ($data->og_image??$data->pic??$setting->og_image);
  if ($page_pic && is_array($page_pic)) {
    $page_pic = $page_pic[0];
  }
  // og title
  $page_og_title = ($data->og_title??$custom_page_title??$data->seo_title??$data->title??$setting->og_title??$setting->seo_title);
  $page_og_title .= ($page_og_title?' - ':'').$setting->name;
  // og description
  $page_og_description = ($data->og_description??$data->seo_description??$data->description??$setting->og_description??$setting->seo_description);
@endphp
<title>{{$page_title}}</title>

@if(isset($data->meta_robots))
  <meta name="robots" content="{{$data->meta_robots}}">
@elseif(isset($setting->meta_robots))
  <meta name="robots" content="{{$setting->meta_robots}}">
@endif

<meta name="keywords" content="{{$page_keyword}}">
<meta name="description" content="{{$page_description}}">
<meta name="copyright" content="Copyrights © {{ $setting->name }} All Rights Reserved">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

<meta property="og:title" content="{{$page_og_title}}">
<meta property="og:type" content="website">
@if ($page_pic)
  <meta property="og:image" content="{{asset($page_pic)}}">
@endif
<meta property="og:url" content="{{url()->full()}}">
<meta property="og:description" content="{{$page_og_description}}">

<meta property="og:site_name" content="{{ $setting->name }}">

<link rel="canonical" href="{{request()->url()}}">

@include('_app_icon')

@if (count($lang_datas)>1)
  @foreach ($setting->present()->langUrl(isset($data) && !is_null($data) ? $data : null) as $lang_code => $url)
    <link rel="alternate" href="{{$url}}" hreflang="{{$lang_code}}">
  @endforeach
@endif
@php
    try {
        $main_css = mix('css/main.css');
    } catch(Exception $e) {
        $main_css = asset('css/main.css');
    }
@endphp
<link href="{{ $main_css }}" rel="stylesheet">

{{-- <script src="{{ asset('js/modernizr.js') }}"></script> --}}
{{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
<script>
    window.App = {!! json_encode([
      'csrfToken' => csrf_token(),
      'homeUrl' => route('home'),
      'locale' => explode('-', app()->getLocale())[0],
      'text' => [
          'ok' => __('text.ok'),
          'close' => __('text.close'),
      ]
    ]) !!};
</script>
