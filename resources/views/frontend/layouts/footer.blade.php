<footer class="footer">
    <div class="container">
        <div class="site-info">
            <div class="company-name">{{$setting->name}}</div>
            <ul class="site-info-list">
                <li>TEL: <a href="tel:{{$setting->phone}}">{{$setting->phone}}</a></li>
                <li>FAX: {{$setting->fax}}</li>
                <li>MAIL: <a href="mailto:{{$setting->email}}">{{$setting->email}}</a></li>
                <li>ADD: <a href="{{$setting->google_map}}" target="_blank">{{$setting->address}}</a></li>
                {{-- <li>{!! nl2br($setting->business_hours) !!}</li> --}}
            </ul>
        </div>
        <form action="{{route('subscriber.subscribe')}}" method="POST">
            <div class="modal-body">
                @csrf
                <input type="hidden" name="pos" value="footer.subscribe">
                <input type="hidden" name="locale" value="{{app()->getLocale()}}">
                <div class="form-group">
                    <label>@lang('validation.attributes.name') <span class="required">*</span></label>
                    <input type="text" class="form-control {{(old('pos')&&old('pos')=='footer.subscribe'&&$errors->first('name'))?'is-invalid':''}}" name="name" placeholder="" value="{{old('pos')&&old('pos')=='footer.subscribe'?old('name'):''}}">
                    @if(old('pos')&&old('pos')=='footer.subscribe'&&$errors->first('name'))
                        <div class="invalid-feedback"><i class="fas fa-exclamation-circle"></i> {{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>@lang('validation.attributes.email') <span class="required">*</span></label>
                    <input type="text" class="form-control {{(old('pos')&&old('pos')=='footer.subscribe'&&$errors->first('email'))?'is-invalid':''}}" name="email" placeholder="" value="{{old('pos')&&old('pos')=='footer.subscribe'?old('email'):''}}">
                    @if(old('pos')&&old('pos')=='footer.subscribe'&&$errors->first('email'))
                        <div class="invalid-feedback"><i class="fas fa-exclamation-circle"></i> {{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">@lang('text.subscribe')</button>
            </div>
        </form>
        <form action="{{route('subscriber.unsubscribe')}}" method="POST">
            <div class="modal-body">
                @csrf
                <input type="hidden" name="pos" value="footer.unsubscribe">
                <input type="hidden" name="locale" value="{{app()->getLocale()}}">
                <div class="form-group">
                    <label>@lang('validation.attributes.email') <span class="required">*</span></label>
                    <input type="text" class="form-control {{(old('pos')&&old('pos')=='footer.unsubscribe'&&$errors->first('email'))?'is-invalid':''}}" name="email" placeholder="" value="{{old('pos')&&old('pos')=='footer.unsubscribe'?old('email'):''}}">
                    @if(old('pos')&&old('pos')=='footer.unsubscribe'&&$errors->first('email'))
                        <div class="invalid-feedback"><i class="fas fa-exclamation-circle"></i> {{ $errors->first('email') }}</div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">@lang('text.unsubscribe')</button>
            </div>
        </form>
        <div class="copyright">
                Copyright © {{Carbon::now()->year}} {{$setting->copyright}} All rights reserved.
                <a href="https://www.37design.com.tw/" target="_blank" rel="nofollow">Design by 37design</a>
                <a href="{{Route::has('privacy.index')?route('privacy.index'):''}}">@lang('text.privacy')</a>
                <a href="{{Route::has('terms.index')?route('terms.index'):''}}">@lang('text.terms')</a>
        </div>
    </div>
</footer>
