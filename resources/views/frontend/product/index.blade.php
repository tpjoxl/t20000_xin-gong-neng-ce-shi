@extends('frontend.layouts.master')

@section('head')
@include('frontend.layouts.page_link', ['datas' => $items])
@endsection

@section('content')
<div class="page-header text-center">
    <div class="container">
        @if (Route::currentRouteName()==$prefix.'.index')
        <h1 class="page-title">@lang('text.'.$prefix)</h1>
        @elseif (Route::currentRouteName()==$prefix.'.search')
        <h1 class="page-title">@lang('text.'.$prefix)<span>@lang('text.product_search')</span></h1>
        @else
        <div class="page-title">@lang('text.'.$prefix)<span>{{$data->title}}</span></div>
        @endif
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
              @if ($items->count())
                <div class="products mb-3">
                    <div class="row">
                      @foreach ($items as $item)
                        <div class="col-6 col-md-4 col-lg-4">
                            <div class="product product-7 text-center">
                                <figure class="product-media">
                                    {{-- <span class="product-label label-new">New</span> --}}
                                    <a href="{{route($prefix.'.detail', ['slug'=>$item->slug])}}">
                                        <img src="{{imgsrc($item->pic_path)}}" alt="{{$item->title}}"
                                            class="product-image">
                                    </a>
                                </figure>

                                <div class="product-body">
                                    <div class="product-cat">
                                      @foreach ($item->categories as $key => $item_category)
                                        {{$key!=0?', ':''}}<a href="{{route($prefix.'.category', ['category'=>$item_category->slug])}}" title="{{$item_category->title}}">{{$item_category->title}}</a>
                                      @endforeach
                                    </div>
                                    <h3 class="product-title"><a href="{{route($prefix.'.detail', ['slug'=>$item->slug])}}">{{$item->title}}</a></h3>
                                    <div id="list-item{{$item->id}}-sku-info">
                                        @php
                                            $currentSku = $item->skus->first();
                                        @endphp
                                        @include('frontend.product.list_sku_info')
                                    </div>
                                </div>
                            </div>
                        </div>
                      @endforeach
                    </div>
                </div>
                {{ $items->appends(request()->input())->links('vendor.pagination.default_front', ['class' => 'justify-content-center']) }}
              @else
                <div class="text-center pt-3 pb-3">@lang('text.no_data')</div>
              @endif
            </div>
            <aside class="col-lg-3 order-lg-first">
                <div class="sidebar sidebar-shop">
                  @include('frontend.product.side')
                </div>
            </aside>
        </div>
    </div>
</div>
@endsection
@section('foot_plugin')
    <script src="{{asset('assets/js/bootstrap-input-spinner.js')}}"></script>
@endsection
