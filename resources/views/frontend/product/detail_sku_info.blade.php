<div class="product-price">
    <span class="new-price">{{$data->present()->price($currentSku->current_price)}}</span>
    @if ($currentSku->current_price_type != 1)
      <span class="old-price">{{$data->present()->price($currentSku->price)}}</span>
    @endif
</div>

<div class="product-content">
    <p>{!!nl2br($data->description)!!}</p>
</div>

<div class="details-filter-row details-row-size">
    <label for="size">規格：</label>
    <div class="select-custom">
        <select id="detail-sku" class="form-control js-change-sku" data-holder="#detial-sku-info">
          @foreach ($data->skus as $sku)
            <option value="{{$sku->id}}" {{$currentSku->id==$sku->id?'selected':''}}>{{$sku->description??$sku->title}}</option>
          @endforeach
        </select>
    </div>
</div>

@if ($data->status == 1 && $currentSku->qty > 0)
  <div class="details-filter-row details-row-size">
      <label for="qty">數量：</label>
      <div class="qty-field-group">
          <button class="btn less js-qty-control" data-action="less">-</button>
          <input type="text" name="qty" id="detail-qty" class="field js-qty" value="1" data-max="{{$currentSku->qty}}">
          <button class="btn add js-qty-control" data-action="add">+</button>
      </div>
  </div>
@endif

<div class="product-details-action">
  @if ($data->status == 1 && $currentSku->qty > 0)
      <a href="#" class="btn-product btn-cart js-cart-add-item" data-qty="#detail-qty" data-sku="#detail-sku"><span>加入購物車</span></a>
  @else
      <span class="btn btn-outline-secondary soldout">缺貨中</span>
  @endif
</div>
<div class="product-floating">
    <div class="container">
        <div class="row">
            {{-- <div class="col-5">
                <div class="product-price">
                    <span class="new-price">{{$data->present()->price($currentSku->current_price)}}</span>
                </div>
            </div> --}}
            <div class="col-6">
                <div class="select-custom">
                    <select id="detail-floating-sku" class="form-control js-change-sku" data-holder="#detial-sku-info">
                      @foreach ($data->skus as $sku)
                        <option value="{{$sku->id}}" {{$currentSku->id==$sku->id?'selected':''}}>{{$sku->description??$sku->title}}</option>
                      @endforeach
                    </select>
                </div>
            </div>
            <div class="col-6">
              @if ($data->status == 1 && $currentSku->qty > 0)
                <input type="hidden" id="detail-floating-qty" class="form-control" value="1">
                <button class="btn btn-outline-primary addcart js-cart-add-item" data-qty="#detail-floating-qty" data-sku="#detail-floating-sku"><i class="icon-cart-plus"></i>加入購物車</button>
              @else
                <span class="btn btn-outline-secondary soldout">缺貨中</span>
              @endif
            </div>
        </div>
    </div>
</div>