@if ($currentSku->current_price_type != 1)
    <div class="product-price">
        <del class="out-price">{{$currentSku->present()->price_type(1)}}：{{$item->present()->price($currentSku->price)}}</del>
    </div>
@endif
<div class="product-price">
    {{$currentSku->present()->price_type($currentSku->current_price_type)}}：{{$item->present()->price($currentSku->current_price)}}
</div>
<div class="select-custom">
    <select id="list-item{{$item->id}}-sku" class="form-control js-change-sku" data-holder="#list-item{{$item->id}}-sku-info">
        @foreach ($item->skus as $sku)
            <option value="{{$sku->id}}" {{$currentSku->id==$sku->id?'selected':''}}>{{$sku->description??$sku->title}}</option>
        @endforeach
    </select>
</div>
@if ($item->status == 1 && $currentSku->qty > 0)
    <div class="product-shopping">
        <div class="qty-field-group">
            <button class="btn less js-qty-control" data-action="less">-</button>
            <input type="text" name="qty" id="list-item{{$item->id}}-qty" class="field js-qty" value="1" data-max="{{$currentSku->qty}}">
            <button class="btn add js-qty-control" data-action="add">+</button>
        </div>
        <button class="btn btn-primary addcart js-cart-add-item" data-qty="#list-item{{$item->id}}-qty" data-sku="#list-item{{$item->id}}-sku">加入購物車</button>
    </div>
@else
    <span class="btn btn-outline-secondary soldout btn-block">缺貨中</span>
@endif
