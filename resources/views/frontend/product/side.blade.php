<ul>
    @foreach ($menu_product_categories as $menu_product_category)
        <li>
            <a href="{{route($prefix.'.category', ['category' => $menu_product_category->slug])}}">{{$menu_product_category->title}}({{$menu_product_category->products_count}})</a>
            <ul>
                @foreach ($menu_product_category->children as $child)
                  <li class="{{Route::currentRouteName()==$prefix.'.category'&&$data->id==$menu_product_category->id?'active':''}}">
                    <a href="{{route($prefix.'.category', ['category' => $menu_product_category->slug, 'category2' => $child->slug])}}">{{$child->title}}<span>({{$child->products->count()}})</span></a>
                  </li>
                @endforeach
            </ul>
        </li>
    @endforeach
</ul>
