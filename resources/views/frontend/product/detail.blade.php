@extends('frontend.layouts.master')

@section('head_plugin')
<link rel="stylesheet" href="{{asset('assets/css/plugins/owl-carousel/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/plugins/magnific-popup/magnific-popup.css')}}">
@endsection
@section('style')
<link rel="stylesheet" href="{{asset('assets/css/plugins/nouislider/nouislider.css')}}">
@endsection

@section('content')
<div class="page-header text-center"
    style="background-image: url('{{imgsrc($data->banner?:'images/page-header-bg.jpg')}}')">
    <div class="container">
      <div class="page-title">@lang('text.'.$prefix)</div>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['container_class' => 'container-fluid d-flex align-items-center'])
<div class="page-content">
  <div class="container-fluid">
    <div class="row gutter-20">
        <div class="col-xl-10 gutter-20">
            <div class="product-details-top">
                <div class="row gutter-20">
                    <div class="col-md-5 col-lg-6 gutter-20">
                        <div class="product-gallery">
                          @if ($data->images->count())
                            <figure class="product-main-image">
                                {{-- <span class="product-label label-sale">Sale</span> --}}
                                <img id="product-zoom" src="{{imgsrc($data->images->first()->detail_path)}}" data-zoom-image="{{imgsrc($data->images->first()->zoom_path)}}" alt="{{$data->images->first()->description}}">

                                <a href="#" id="btn-product-gallery" class="btn-product-gallery">
                                    <i class="icon-arrows"></i>
                                </a>
                            </figure>

                            <div id="product-zoom-gallery" class="product-image-gallery max-col-6">
                              @foreach ($data->images as $key => $img)
                                <a class="product-gallery-item {{$key==0?'active':''}}" href="#" data-image="{{imgsrc($img->detail_path)}}" data-zoom-image="{{imgsrc($img->zoom_path)}}">
                                    <img src="{{imgsrc($img->thumb_path)}}" alt="{{$img->description}}">
                                </a>
                              @endforeach
                            </div>
                          @else
                            <figure class="product-main-image">
                              <img src="{{imgsrc($data->pic_path)}}" alt="No image">
                            </figure>
                          @endif
                        </div>
                    </div>

                    <div class="col-md-7 col-lg-6 gutter-20">
                        <div class="product-details">
                            <h1 class="product-title">{{$data->title}}</h1>
                            <div id="detial-sku-info">
                                @php
                                    $currentSku = $data->skus->first();
                                @endphp
                                @include('frontend.product.detail_sku_info')
                            </div>

                            <div class="product-details-footer">
                                <div class="product-cat">
                                    <span>分類：</span>
                                    @foreach ($data->categories as $key => $data_category)
                                      {{$key!=0?', ':''}}<a href="{{route($prefix.'.category', ['category'=>$data_category->slug])}}" title="{{$data_category->title}}">{{$data_category->title}}</a>
                                    @endforeach
                                </div>

                                <div class="social-icons social-icons-sm">
                                    <span class="social-label">分享：</span>
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                      <a class="a2a_button_facebook"></a>
                                      <a class="a2a_button_line"></a>
                                    </div>
                                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                                </div>
                            </div>

                            <div class="accordion accordion-plus product-details-accordion" id="product-accordion">
                                <div class="card card-box card-sm">
                                    <div class="card-header" id="product-desc-heading">
                                        <h2 class="card-title">
                                            <a role="button" data-toggle="collapse" href="#product-accordion-desc" aria-expanded="true" aria-controls="product-accordion-desc">
                                              商品說明
                                            </a>
                                        </h2>
                                    </div>
                                    <div id="product-accordion-desc" class="collapse show" aria-labelledby="product-desc-heading" data-parent="#product-accordion">
                                        <div class="card-body">
                                            <div class="product-desc-content editor">
                                                {!!$data->text!!}
                                                {{-- <p>個飯表銷夫談。刻進？</p>
                                                <p>快爸過學例會過信平路此時的事平業我華水經營有我爭什、這張相怎給景物一約別生研可！不現一美，和活回開雄長許拿走算車興阿點足後；水去加現的！告令案天地出能帶資你現；不系趣在的許日老要？我統去，這子前這商我兒意陽片的；星站聞力才出知定續臺第裡程製一規種給式河千利的始？可國以坐子新銷，面支從真臺二上張年說性他外那居始。業車立情分交員一級輕不文最特；我益舉還突高？死拉來的兒多我可種去車表在特比；大在理我期是市界生的；裡聞。</p>
                                                <ul class="product-desc-ul">
                                                    <li>是變食？要害新；並廠先、直質完委流得望本英入實，可次腦方工人物請望親，和者設樓起考首人自省火期治紅另止手英？成青來過一日學所假演政，善積。</li>
                                                    <li>吸有醫聽院的苦下作式地沒子了間成以！</li>
                                                    <li>加小年年是連，上和對德。</li>
                                                </ul>
                                                <p>後需教強這算起等風言她統有不燈著，會國解女選；懷再入的文易：看動學產，車少病今也你治，是兩像有是，公物先人於好。收沒家教總政也皮山。經第它體離以大們，出十開體利保導到內機。</p>
                                                <p>而去和背因本大近我。跑甚！</p>
                                                <p>球生開離童計出許；主政點書加學議動；大成為類程個多以保人濟設無、新事春：人是德？消像年給各常態陸成、線主整打北。</p>
                                                <p>變晚環，媽中館班好那面減太。</p> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <aside class="col-xl-2 gutter-20 d-md-none d-xl-block">
            <div class="sidebar sidebar-product">
                <div class="widget widget-products">
                    <h4 class="widget-title">推薦商品</h4>

                    <div class="products">
                        @foreach ($data->recommends as $item)
                            <div class="product product-sm">
                                <figure class="product-media">
                                    <a href="{{route($prefix.'.detail', ['slug'=>$item->slug])}}" title="{{$item->title}}">
                                        <img src="{{imgsrc($item->pic_path)}}" alt="{{$item->title}}" class="product-image">
                                    </a>
                                </figure>

                                <div class="product-body">
                                    <h5 class="product-title"><a href="{{route($prefix.'.detail', ['slug'=>$item->slug])}}" title="{{$item->title}}">{{$item->title}}</a></h5>
                                    @php
                                        $currentSku = $item->skus->first();
                                    @endphp
                                    <div class="product-price">
                                        <span class="new-price">{{$data->present()->price($currentSku->current_price)}}</span>
                                        @if ($currentSku->current_price_type != 1)
                                            <span class="old-price">{{$data->present()->price($currentSku->price)}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <a href="{{route($prefix.'.index')}}" class="btn btn-outline-dark-3"><span>觀看更多商品</span><i class="icon-long-arrow-right"></i></a>
                </div>
            </div>
        </aside>
    </div>
</div>
</div>
@endsection
@section('foot_plugin')
  <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/js/bootstrap-input-spinner.js')}}"></script>
  <script src="{{asset('assets/js/jquery.elevateZoom.min.js')}}"></script>
  <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
@endsection
@section('script')
    <script>
        (function($) {
            $('footer').css('margin-bottom', $('.product-floating').outerHeight())
            $(window).on('scroll', function() {
                if ($(window).scrollTop() > $('.details-filter-row').eq(0).offset().top) {
                    $('.product-floating').removeClass('hide');
                } else {
                    $('.product-floating').addClass('hide');
                }
            }).scroll();
        })($)
    </script>
@endsection