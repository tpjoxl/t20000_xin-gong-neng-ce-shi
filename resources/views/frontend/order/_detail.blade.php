<table class="table table-bordered order-detail-table">
    <tbody>
        <tr>
            <th>@lang('validation.order.num')</th>
            <td>{{$data->num}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.created_at')</th>
            <td>{{$data->created_at}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.status')</th>
            <td>{!!$data->present()->status($data->status)!!}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.payment_id')</th>
            <td>{{$data->payment_info['title']}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.payment_status')</th>
            <td>{!!$data->present()->payment_status($data->payment_status)!!}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.shipping_id')</th>
            <td>{{$data->shipping?$data->shipping->present()->type($data->shipping_type):''}} {{$data->shipping_info['title']}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.hope_time')</th>
            <td>{{$data->hope_time}}</td>
        </tr>
        @if ($data->status >= 3)
            <tr>
                <th>@lang('validation.order.shipping_status')</th>
                <td>{!!$data->present()->shipping_status($data->shipping_status)!!}</td>
            </tr>
            @if ($data->shippingnumber)
                <tr>
                    <th>@lang('validation.order.shippingnumber')</th>
                    <td>
                        {{$data->shippingnumber}}
                        @if ($data->shipping && $data->shipping->url)
                            <a href="{{$data->shipping->url}}" target="_blank"><i class="icon-search"></i>查詢</a>
                        @endif
                    </td>
                </tr>
            @endif
        @endif
        @if ($data->coupon_info)
            <tr>
                <th>@lang('validation.order.coupons')</th>
                <td>{{$data->coupon_info['title']}}({{$data->coupon_info['code']}})</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="row">
    <div class="col-sm-6">
        <div class="card card-dashboard">
            <div class="card-body">
                <h3 class="card-title">購買人資訊</h3>

                <p>
                    @lang('validation.attributes.name')：{{$data->present()->protect_name($data->name)}} {{$data->sex?$data->present()->sex_title($data->sex):''}}<br>
                    @lang('validation.attributes.email')：{{$data->present()->protect_email($data->email)}}<br>
                    @lang('validation.attributes.phone')：{{$data->present()->protect_phone($data->phone)}}<br>
                    @lang('validation.order.note')：{{$data->note}}<br>
                </p>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card card-dashboard">
            <div class="card-body">
                <h3 class="card-title">收件人資訊</h3>

                <p>
                    @lang('validation.attributes.name')：{{$data->present()->protect_name($data->receiver_name)}} {{$data->receiver_sex?$data->present()->sex_title($data->receiver_sex):''}}<br>
                    @lang('validation.attributes.email')：{{$data->present()->protect_email($data->receiver_email)}}<br>
                    @lang('validation.attributes.phone')：{{$data->present()->protect_phone($data->receiver_phone)}}<br>
                    @lang('validation.attributes.address')：{{$data->present()->protect_address($data->receiver_full_address)}}<br>
                </p>
            </div>
        </div>
    </div>
</div>

<table class="table table-cart table-mobile">
    <thead>
        <tr>
            <th>@lang('validation.order.product_title')</th>
            <th>@lang('validation.order.product_options')</th>
            <th>@lang('validation.order.product_price')</th>
            <th>@lang('validation.order.product_qty')</th>
            <th>@lang('validation.order.product_total')</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($data->items as $item)
            @php
                $belong = 'shop';
                if ($item->sku->belong == 2) {
                    $belong = 'selectshop';
                }
            @endphp
            <tr>
                <td class="product-col">
                    <div class="product">
                        <figure class="product-media">
                            <a href="{{route($belong.'.detail', ['slug'=>$item->sku->product->slug])}}">
                                <img src="{{imgsrc($item->pic)}}" alt="{{$item->title}}">
                            </a>
                        </figure>

                        <h3 class="product-title">
                            <a href="{{route($belong.'.detail', ['slug'=>$item->sku->product->slug])}}">{{$item->title}}</a>
                        </h3>
                    </div>
                </td>
                <td>{{$item->options}}</td>
                <td class="price-col">{{$item->present()->price($item->price)}}</td>
                <td class="quantity-col">{{$item->qty}}</td>
                <td class="total-col">{{$item->present()->price($item->total)}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-right">
                @lang('validation.order.items_total_price')：{{$data->present()->price($data->items_total_price)}}<br>
                @if ($data->payment_price)
                    @lang('validation.order.payment_price')：{{$data->present()->price($data->payment_price)}}<br>
                @endif
                @lang('validation.order.shipping_price')：{{$data->present()->price($data->shipping_price)}}<br>
                @if ($data->coupon_price)
                    @lang('validation.order.coupon_price')：-{{$data->present()->price($data->coupon_price)}}<br>
                @endif
                @if ($data->bonus_price)
                    @lang('validation.order.bonus_price')：-{{$data->present()->price($data->bonus_price)}}<br>
                @endif
                @lang('validation.order.sum_price')：{{$data->present()->price($data->sum_price)}}<br>
            </td>
        </tr>
    </tfoot>
</table>