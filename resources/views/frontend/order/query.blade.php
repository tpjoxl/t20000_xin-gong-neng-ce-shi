@extends('frontend.layouts.master')

@section('head_script')
<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc('images/page-header-bg.jpg')}}')">
    <div class="container">
        <h1 class="page-title">@lang('text.'.$prefix)</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="container">
        <div class="form-box order-query">
            <p class="mb-3">※此處僅供「非會員」的訂單查詢，會員訂單查詢請前往「<a href="{{route('user.order.index')}}">@lang('text.user_centre')→@lang('text.user_order')</a>」</p>
            <form action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="orderquery-phone">@lang('validation.order.phone') *</label>
                    <input type="text" class="form-control @error('phone') is-invalid @enderror" id="orderquery-phone"
                        name="phone" value="{{old('phone')}}">
                    @error('phone')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="orderquery-email">@lang('validation.order.email') *</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="orderquery-email"
                        name="email" value="{{old('email')}}">
                    @error('email')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>@lang('validation.attributes.captcha') *</label>
                    <div class="captcha @error('g-recaptcha-response') is-invalid @enderror">
                        <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}"></div>
                    </div>
                    @error('g-recaptcha-response')
                    <div class="invalid-feedback">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-footer pb-0 mb-0 bb-0">
                    <button type="submit" class="btn btn-outline-primary">
                        <span>@lang('text.submit')</span>
                        <i class="icon-long-arrow-right"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
