@extends('frontend.layouts.master')

@section('content')
<div class="page-header text-center" style="background-image: url('{{imgsrc('images/page-header-bg.jpg')}}')">
    <div class="container">
    <h1 class="page-title">@lang('text.'.$prefix)</h1>
    </div>
</div>
@include('frontend.layouts.breadcrumb', ['class' => 'mb-3'])
<div class="page-content">
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-12">
                    @if ($items->count())
                        <div class="table-responsive">
                            <table class="table shop_cart_table order-table table-mobile">
                                <thead>
                                <tr>
                                    <th scope="col">@lang('validation.order.num')</th>
                                    <th scope="col">@lang('validation.order.created_at')</th>
                                    <th scope="col">@lang('validation.order.sum_price')</th>
                                    <th class="text-md-center" scope="col">@lang('validation.order.status')</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($items as $item)
                                    <tr>
                                        <td data-title="@lang('validation.order.num')"><a class="num" data-toggle="modal" data-target="#detailModal" data-id="{{$item->id}}">{{$item->num}}</a></td>
                                        <td data-title="@lang('validation.order.created_at')">{{$item->created_at}}</td>
                                        <td data-title="@lang('validation.order.sum_price')">{!!$item->present()->price($item->sum_price)!!}</td>
                                        <td data-title="@lang('validation.order.status')" class="text-md-center">{!!$item->present()->status($item->status)!!}</td>
                                        <td class="text-md-center">
                                            @if ($item->status == 1 && ($item->payment_status == 1 || $item->payment_status == 3))
                                                <button class="btn btn-dark btn-sm btn-cancel" data-toggle="modal" data-target="#cancelModal" data-id="{{$item->id}}">取消訂單</button>
                                                @if ($item->payment_info['type'] != 'CREDIT' && !is_null($item->payment_return))
                                                    <button class="btn btn-primary btn-sm btn-payinfo" data-toggle="modal" data-target="#payinfoModal" data-id="{{$item->id}}">付款資訊</button>
                                                @else
                                                    <a class="btn btn-primary btn-sm btn-pay" href="{{route('order.pay', ['num' => $item->num])}}">重新付款</a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @includeWhen($items->where('status', 1)->count(), 'frontend.order._cancel_modal')
                        @includeWhen($items->where('status', 1)->count(), 'frontend.order._payinfo_modal')
                        @include('frontend.order._detail_modal')
                    @else
                        <div class="no-data text-center pt-5">@lang('text.no_data')</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    @include('frontend.order._script')
@endsection
