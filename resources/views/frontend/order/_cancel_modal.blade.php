<div class="modal fade" id="cancelModal" tabindex="-1" aria-labelledby="cancelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
      <form class="modal-content" method="POST" action="{{route('order.cancel')}}">
        <div class="modal-header">
          <h5 class="modal-title" id="cancelModalLabel">取消訂單</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body p-4">
            @csrf
            @method('patch')
            <input type="hidden" name="id">
            <input type="hidden" name="status" value="2">
            <p>確定取消此訂單?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal">返回</button>
          <button type="submit" class="btn btn-primary btn-sm">確定取消</button>
        </div>
      </form>
    </div>
</div>
