<script>
    (function($) {
        $('#cancelModal').on('show.bs.modal', function (event) {
            var $button = $(event.relatedTarget);
            var orderId = $button.data('id');
            var $modal = $(this);
            $modal.find('[name="id"]').val(orderId);
        });
        $('#payinfoModal').on('show.bs.modal', function (event) {
            var $button = $(event.relatedTarget);
            var orderId = $button.data('id');
            var $modal = $(this);
            if (orderId) {
                $modal.find('[name="id"]').val(orderId);
            }
            $.ajax({
                method: "get",
                url: "/api/order/payinfo",
                data: {
                    id: orderId
                },
                success: function(response, textStatus, jqXHR) {
                    // console.log(response, textStatus, jqXHR);
                    $modal.find('.payinfo').html(response.data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log(jqXHR, textStatus, errorThrown)

                    Swal.fire({
                        title: jqXHR.responseJSON.message,
                        icon: 'error',
                        confirmButtonText: "{{__('text.close')}}"
                    }).then((result) => {
                        $modal.modal('hide');
                    });
                }
            });
        });
        $('#detailModal').on('show.bs.modal', function (event) {
            var $button = $(event.relatedTarget);
            var orderId = $button.data('id');
            var $modal = $(this);
            if (orderId) {
                $modal.find('[name="id"]').val(orderId);
            }
            $.ajax({
                method: "get",
                url: "/api/order/detail",
                data: {
                    id: orderId
                },
                success: function(response, textStatus, jqXHR) {
                    // console.log(response, textStatus, jqXHR);
                    $modal.find('.order-detail').html(response.data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // console.log(jqXHR, textStatus, errorThrown)

                    Swal.fire({
                        title: jqXHR.responseJSON.message,
                        icon: 'error',
                        confirmButtonText: "{{__('text.close')}}"
                    }).then((result) => {
                        $modal.modal('hide');
                    });
                }
            });
        });
    })($)
</script>
@if (Session::has('openModal'))
    <script>
        (function($) {
            var openModal = "#{{Session::get('openModal')}}";
            $(openModal).modal('show');
        })($)
    </script>
@endif
