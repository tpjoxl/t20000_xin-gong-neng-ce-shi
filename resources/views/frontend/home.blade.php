@extends('frontend.layouts.master')

@section('content')
  <div class="home-banner">
    @foreach ($banners as $banner)
        <div class="item">
            <a href="{{$banner->url}}" target="{{$banner->target}}" title="{{$banner->tag_title?:$banner->title}}">
                <img class="pc" src="{{imgsrc($banner->pic)}}" alt="{{$banner->tag_alt?:$banner->title}}">
                <img class="m" src="{{imgsrc($banner->pic2)}}" alt="{{$banner->tag_alt?:$banner->title}}">
                {!!nl2br($banner->text)!!}
            </a>
        </div>
    @endforeach
  </div>
  @if (!empty($setting->home_editor1))
    <section class="home-sect">
      <div class="container">
        <div class="editor">{!!$setting->home_editor1!!}</div>
      </div>
    </section>
  @endif
@endsection
