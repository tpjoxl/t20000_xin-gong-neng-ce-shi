@extends('frontend.layouts.master')

@section('content')
    <div class="page-header text-center">
        <div class="container">
            <h1 class="page-title">@lang('text.cart')</h1>
        </div>
    </div>
    @include('frontend.layouts.breadcrumb', ['class' => ''])
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <ol class="cart-step-list list">
                    <li class="current active"><span>@lang('text.cart_info')</span></li>
                    <li><span>@lang('text.cart_checkout')</span></li>
                    <li><span>@lang('text.cart_result')</span></li>
                </ol>
                <div id="cart-holder">
                    @include('frontend.cart._cart')
                </div>
                @if ($data->text)
                    <div class="alert alert-secondary mt-3" role="alert">
                        <h5 class="alert-heading">重要說明</h5>
                        <div class="editor">{!!$data->text!!}</div>
                        {{-- (1) 如未登入會員購物，則無法累計紅利與相關優惠。<br>(2) 紅利點數相關規則，請見「<a href="/member/bonus" target="_blank">紅利點數說明</a>」。 --}}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('foot_plugin')
    <script src="{{asset('assets/js/bootstrap-input-spinner.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sticky-kit.min.js')}}"></script>
@endsection

@section('script')
<script>
  (function() {
    var cities = {!!json_encode($cart->cities)!!}
    var remoteStatus = {{$cart->shippingSetting->remote_status?1:0}};
    // console.log(cities)
    // 縣市切換就更新區域下拉
    $(document).on('change', 'select.city', function(){
      if ($(this).val() != '') {
          var cityId = $(this).val();
          var regions = '';
          $.each(cities[cityId]['regions'], function(key, region) {
            if (!remoteStatus&&region.remoted==1) {
              regions += '<option value="'+region.id+'" disabled>'+region.name+' (不宅配)</option>';
            } else if (remoteStatus&&region.remoted==1) {
              regions += '<option value="'+region.id+'">'+region.name+' (偏鄉地區)</option>';
            } else {
              regions += '<option value="'+region.id+'">'+region.name+'</option>';
            }
          });
          $(this).parents('.city-selector').find('.region').html(regions)
          .trigger('change');
      } else {
          $(this).parents('.city-selector').find('.region').html('<option value="">請先選擇縣市</option>');
      }
    });

  })($);
</script>
@endsection
