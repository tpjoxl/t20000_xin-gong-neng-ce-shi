<table class="table table-summary">
    <tbody>
        <tr class="summary-subtotal">
            <td>@lang('validation.order.items_total_price'):</td>
            <td>{{$cart->present()->price($cart->totalPrice)}}</td>
        </tr>
        @if ($cart->shipping)
            <tr>
                <td>@lang('validation.order.shipping_price'):</td>
                <td>{{$cart->present()->price($cart->shippingPrice)}}</td>
            </tr>
        @endif
        @if ($cart->shipping && $cart->totalPrice < $cart->shipping->limit_order_price)
            <tr>
                <td colspan="2">購買滿{{$cart->shipping->limit_order_price}}元，{{$cart->shipping->limit_price==0?'可享免運':'運費為'.$cart->shipping->limit_price.'元'}}！<a href="{{route('product.index')}}">再去逛逛<i class="icon-long-arrow-right"></i></a></td>
            </tr>
        @endif
        @if ($cart->payment && $cart->paymentPrice > 0)
            <tr>
                <td>@lang('validation.order.payment_price'):</td>
                <td>{{$cart->present()->price($cart->paymentPrice)}}</td>
            </tr>
        @endif
        @if ($cart->coupon)
            <tr>
                <td>@lang('validation.order.coupon_price'):</td>
                <td>-{{$cart->present()->price($cart->couponPrice)}}</td>
            </tr>
        @endif
        @if ($cart->user)
            @if ($cart->bonus)
                <tr>
                    <td>@lang('validation.order.bonus_price'):</td>
                    <td>-{{$cart->present()->price($cart->bonusPrice)}}</td>
                </tr>
            @endif
            <tr>
                <td>@lang('validation.order.bonus_point'):</td>
                <td>{{$cart->orderBonus}}點</td>
            </tr>
        @endif
        <tr class="summary-total">
            <td>@lang('validation.order.sum_price'):</td>
            <td>{{$cart->present()->price($cart->sumPrice)}}</td>
        </tr>
    </tbody>
</table>