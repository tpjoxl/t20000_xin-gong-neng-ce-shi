<table class="table table-cart table-mobile">
    <thead>
        <tr>
            @if ($cart->step == 1)
                <th><input type="checkbox" data-sku="all" class="js-cart-check" {{$cart->itemsChecked > 0 && $cart->itemsChecked==count($cart->items)?'checked':''}}></th>
            @endif
            <th></th>
            <th>商品名稱</th>
            <th>單價</th>
            <th>數量</th>
            <th>小計</th>
            @if ($cart->step == 1)
                <th></th>
            @endif
        </tr>
    </thead>

    <tbody>
        @foreach ($cart->items as $props => $item)
            @if ($cart->step == 1 || ($cart->step != 1 && $item['checked']))
                @php
                    $belong = 'product';
                @endphp
                <tr>
                    @if ($cart->step == 1)
                        <td><input type="checkbox" data-sku="{{$item['sku_id']}}" class="js-cart-check" {{$item['checked']?'checked':''}} {{$item['sku']->qty<$item['qty']?'disabled':''}}></td>
                    @endif
                    <td class="product-col">
                        <figure class="product-media">
                            <a href="{{route($belong.'.detail', ['slug'=>$item['sku']->product->slug])}}">
                                <img src="{{imgsrc($item['sku']->product->pic_path)}}" alt="{{$item['sku']->product->title}}" style="max-width:80px;">
                            </a>
                        </figure>
                    </td>
                    <td class="product-col">
                        <div class="product">
                            <h3 class="product-title">
                                <a href="{{route($belong.'.detail', ['slug'=>$item['sku']->product->slug])}}">{{$item['sku']->title}}</a>
                            </h3>
                        </div>
                    </td>
                    <td class="price-col">{{$cart->present()->price($item['price'])}}</td>
                    <td class="quantity-col">
                        @if ($cart->step == 1)
                        <div class="qty-field-group {{$item['sku']->qty<$item['qty'] || (Session::has('itemNotEnough') && array_key_exists($props, Session::get('itemNotEnough')))?'is-invalid':''}}">
                            <button class="btn less js-qty-control" data-action="less">-</button>
                            <input type="text" name="qty" class="field js-qty js-cart-update-item-qty" value="{{$item['qty']}}" data-sku="{{$item['sku_id']}}">
                            <button class="btn add js-qty-control" data-action="add">+</button>
                        </div>
                            {{-- <div class="cart-product-quantity {{Session::has('itemNotEnough') && array_key_exists($props, Session::get('itemNotEnough'))?'is-invalid':''}}">
                                <input type="number" class="form-control js-cart-update-item-qty" value="{{$item['qty']}}" min="1" step="1" data-decimals="0" data-sku="{{$item['sku_id']}}">
                            </div> --}}
                            @if ($item['sku']->qty<$item['qty'] || (Session::has('itemNotEnough') && array_key_exists($props, Session::get('itemNotEnough'))))
                                <small class="form-text text-danger">{{$item['sku']->qty<$item['qty']?__('text.cart_sku_qty', ['qty' => $item['sku']->qty]):Session::get('itemNotEnough')[$props]['msg']}}</small>
                            @endif
                        @else
                            {{$item['qty']}}
                        @endif
                    </td>
                    <td class="total-col">{{$cart->present()->price($item['total'])}}</td>
                    @if ($cart->step == 1)
                        <td class="remove-col"><button class="btn-remove js-cart-delete-item" data-sku="{{$item['sku_id']}}" title="刪除商品">刪除</button>
                        </td>
                    @endif
                </tr>
            @endif
        @endforeach
    </tbody>
</table>
