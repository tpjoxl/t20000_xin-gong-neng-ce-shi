@if (count($cart->items))
    @if (is_null($cart->user) && $cart->step == 1)
        <div class="alert alert-primary mb-5" role="alert">
            如未登入會員購物下單，則無法累計紅利與會員相關優惠。
        </div>
    @endif

    <form action="" method="post">
        @csrf
        <div class="row">
            <div class="col-lg-9">
                <div id="cart-table-holder">
                    @include('frontend.cart._cart_table')
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="checkout-title">運送方式</h2>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <select class="js-cart-update-shipping-type form-control @error('shipping_type') is-invalid @enderror" id="cart-shipping_type" name="shipping_type" {{$cart->step!=1?'disabled':''}}>
                                        <option value="">@lang('text.please_select')</option>
                                        @foreach ($cart->allShippingTypes as $key => $val)
                                        <option value="{{$key}}" {{!is_null(old('shipping_type', $cart->shippingType))&&old('shipping_type', $cart->shippingType)==$key?'selected':''}}>{!!$val!!}</option>
                                        @endforeach
                                    </select>
                                    @error('shipping_type')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="form-group">
                                    <select class="js-cart-update-shipping form-control @error('shipping_id') is-invalid @enderror" id="cart-shipping_id" name="shipping_id" {{$cart->step!=1?'disabled':''}}>
                                    <option value="">@lang('text.please_select')</option>
                                    @foreach ($cart->allShippings->where('type', $cart->shippingType) as $shipping)
                                        <option value="{{$shipping->id}}" {{!is_null(old('shipping_id', $cart->shipping->id))&&old('shipping_id', $cart->shipping->id)==$shipping->id?'selected':''}}>{{$shipping->title}}</option>
                                    @endforeach
                                    </select>
                                    @error('shipping_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="checkout-title">希望到貨時段</h2>
                        <div class="form-group">
                            <select class="form-control @error('hope_time') is-invalid @enderror" id="cart-hope_time" name="hope_time" {{$cart->step!=1?'disabled':''}}>
                                <option value="不限" {{!is_null(old('hope_time', $cart->formData->hope_time))&&old('hope_time', $cart->formData->hope_time)=='不限'?'selected':''}}>不限</option>
                                <option value="13時前" {{!is_null(old('hope_time', $cart->formData->hope_time))&&old('hope_time', $cart->formData->hope_time)=='13時前'?'selected':''}}>13時前</option>
                                <option value="14-18時" {{!is_null(old('hope_time', $cart->formData->hope_time))&&old('hope_time', $cart->formData->hope_time)=='14-18時'?'selected':''}}>14-18時</option>
                            </select>
                            @error('hope_time')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                @if ($cart->shipping)
                    <h2 class="checkout-title">付款方式</h2>
                    <div class="row">
                        <div class="col-sm-6">
                            @if ($cart->shipping->payments->count())
                                <div class="form-group">
                                    <select class="js-cart-update-payment form-control @error('payment_id') is-invalid @enderror" id="cart-payment_id" name="payment_id" {{$cart->step!=1?'disabled':''}}>
                                        <option value="">@lang('text.please_select')</option>
                                        @foreach ($cart->shipping->payments as $payment)
                                            <option value="{{$payment->id}}" {{old('payment_id', ($cart->payment?$cart->payment->id:null))==$payment->id?'selected':''}}>{{$payment->title}}</option>
                                        @endforeach
                                    </select>
                                    @error('payment_id')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            @else
                                <div class="form-text">
                                    選擇的運送方式沒有可用的付款方式
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="row">
                    @if ($cart->step==1 || $cart->coupon)
                        <div class="col-sm-6">
                            <h2 class="checkout-title">使用優惠券</h2>
                            <div class="form-group">
                                <input type="text" id="cart-coupon" class="form-control js-cart-update-coupon" placeholder="輸入優惠代碼" value="{{$cart->coupon?$cart->coupon->code:''}}" {{$cart->step!=1?'disabled':''}}>
                            </div>
                        </div>
                    @endif
                    @if ($cart->user && ($cart->step==1 || $cart->usableBonus))
                        <div class="col-sm-6">
                            <h2 class="checkout-title">使用紅利折抵</h2>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="js-cart-update-bonus custom-control-input" id="bonus" value="{{$cart->usableBonus}}" {{$cart->bonus?'checked':''}}>
                                    <label class="custom-control-label" for="bonus">紅利折抵 {{$cart->usableBonus}} 元</label>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <input type="hidden" name="user_id" value="{{$auth_web?$auth_web->id:null}}">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="checkout-title">購買人資訊</h2>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="cart-name">@lang('validation.attributes.name') *</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="cart-name" name="name" value="{{old('name')??$cart->formData->name??($auth_web?$auth_web->name:null)}}" {{$cart->step!=1?'disabled':''}}>
                                    @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="cart-sex">@lang('validation.attributes.sex')</label>
                                    <select class="form-control @error('sex') is-invalid @enderror" id="cart-sex" name="sex" {{$cart->step!=1?'disabled':''}}>
                                    <option value="">@lang('text.please_select')</option>
                                    @foreach ($cart->present()->sex() as $key => $val)
                                        <option value="{{$key}}" {{!is_null(old('sex')??$cart->formData->sex??($auth_web?$auth_web->sex:null))&&(old('sex')??$cart->formData->sex??($auth_web?$auth_web->sex:null))==$key?'selected':''}}>{!!$val!!}</option>
                                    @endforeach
                                    </select>
                                    @error('sex')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cart-phone">@lang('validation.attributes.phone') *</label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="cart-phone" name="phone" value="{{old('phone')??$cart->formData->phone??($auth_web?$auth_web->phone:null)}}" {{$cart->step!=1?'disabled':''}}>
                            @error('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="cart-email">@lang('validation.attributes.email') *</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="cart-email" name="email" placeholder="@lang('text.user_email_hint')" value="{{old('email')??$cart->formData->email??($auth_web?$auth_web->email:null)}}" {{$cart->step!=1?'disabled':''}}>
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <label>@lang('validation.order.note')</label>
                        <textarea class="form-control" cols="30" rows="4" id="cart-note" name="note" {{$cart->step!=1?'disabled':''}}>{{old('note', $cart->formData->note)}}</textarea>
                    </div>
                    <div class="col-md-6">
                        <h2 class="checkout-title">收件人資訊
                            @if ($cart->step == 1)
                                <button class="btn btn-sm btn-outline-primary copy-buyer js-cart-copy-buyer float-right" type="button"><i class="icon-check-circle-o"></i> 複製購買人資訊</button>
                            @endif
                        </h2>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label for="cart-receiver_name">@lang('validation.attributes.name') *</label>
                                    <input type="text" class="form-control @error('receiver_name') is-invalid @enderror" id="cart-receiver_name" name="receiver_name" value="{{old('receiver_name')??$cart->formData->receiver_name??($auth_web?$auth_web->name:null)}}" data-copy-from="#cart-name" {{$cart->step!=1?'disabled':''}}>
                                    @error('receiver_name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="cart-receiver_sex">@lang('validation.attributes.sex')</label>
                                    <select class="form-control @error('receiver_sex') is-invalid @enderror" id="cart-receiver_sex" name="receiver_sex" data-copy-from="#cart-sex" {{$cart->step!=1?'disabled':''}}>
                                    <option value="">@lang('text.please_select')</option>
                                    @foreach ($cart->present()->sex() as $key => $val)
                                        <option value="{{$key}}" {{!is_null(old('receiver_sex')??$cart->formData->receiver_sex??($auth_web?$auth_web->sex:null))&&(old('receiver_sex')??$cart->formData->receiver_sex??($auth_web?$auth_web->sex:null))==$key?'selected':''}}>{!!$val!!}</option>
                                    @endforeach
                                    </select>
                                    @error('receiver_sex')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cart-receiver_phone">@lang('validation.attributes.phone') *</label>
                            <input type="text" class="form-control @error('receiver_phone') is-invalid @enderror" id="cart-receiver_phone" name="receiver_phone" value="{{old('receiver_phone')??$cart->formData->receiver_phone??($auth_web?$auth_web->phone:null)}}" data-copy-from="#cart-phone" {{$cart->step!=1?'disabled':''}}>
                            @error('receiver_phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="cart-receiver_email">@lang('validation.attributes.email') *</label>
                            <input type="email" class="form-control @error('receiver_email') is-invalid @enderror" id="cart-receiver_email" name="receiver_email" placeholder="@lang('text.user_email_hint')" value="{{old('receiver_email')??$cart->formData->receiver_email??($auth_web?$auth_web->email:null)}}" data-copy-from="#cart-email" {{$cart->step!=1?'disabled':''}}>
                            @error('receiver_email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="cart-receiver_address">@lang('validation.attributes.address') *</label>
                            @if ($cart->shippingType==1)
                                <div class="row city-selector">
                                    <div class="col-6">
                                        <select class="form-control city @error('receiver_city_id') is-invalid @enderror" id="cart-receiver_city_id" name="receiver_city_id" {{$cart->step!=1?'disabled':''}}>
                                            <option value="">請選擇縣市</option>
                                            @foreach ($cart->cities as $city)
                                            <option value="{{$city->id}}"  {{ ($city->id == (old('receiver_city_id')??$cart->formData->receiver_city_id??($auth_web?$auth_web->city_id:null))) ? 'selected' : '' }}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('receiver_city_id')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-6">
                                        <select class="js-cart-update-remote form-control region @error('receiver_region_id') is-invalid @enderror" id="cart-receiver_region_id" name="receiver_region_id" {{$cart->step!=1?'disabled':''}}>
                                            @if ((old('receiver_city_id')??$cart->formData->receiver_city_id??($auth_web?$auth_web->city_id:null)) && (old('receiver_region_id')??$cart->formData->receiver_region_id??($auth_web?$auth_web->region_id:null)))
                                                @foreach ($cart->cities[(old('receiver_city_id')??$cart->formData->receiver_city_id??($auth_web?$auth_web->city_id:null))]->regions as $region)
                                                    <option value="{{$region->id}}" {{ ($region->id == (old('receiver_region_id')??$cart->formData->receiver_region_id??($auth_web?$auth_web->region_id:null))) ? 'selected' : '' }} {{$cart->shippingSetting->remote_status==0&&$region->remoted==1?'disabled':''}}>{{$region->name}}
                                                        @if ($cart->shippingSetting->remote_status==0&&$region->remoted==1)
                                                            (不宅配)
                                                        @elseif ($cart->shippingSetting->remote_status==1&&$region->remoted==1)
                                                            (偏鄉地區)
                                                        @endif
                                                    </option>
                                                @endforeach
                                            @else
                                                <option value="">請先選擇縣市</option>
                                            @endif
                                        </select>
                                        @error('receiver_region_id')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="text" class="form-control mt-2 @error('receiver_address') is-invalid @enderror" id="cart-receiver_address" name="receiver_address" value="{{old('receiver_address')??$cart->formData->receiver_address??($auth_web?$auth_web->address:null)}}" {{$cart->step!=1?'disabled':''}}>
                                @error('receiver_address')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                                @if ($cart->shippingSetting->remote_status)
                                    <small class="form-text mt-1">※偏鄉地區運費增收{{$cart->present()->price($cart->shippingSetting->remote_price)}}元。</small>
                                @else
                                    <small class="form-text mt-1">※偏鄉地區不宅配。</small>
                                @endif
                            @else
                                <input type="text" class="form-control @error('receiver_address') is-invalid @enderror" id="cart-receiver_address" name="receiver_address" value="{{old('receiver_address')??$cart->formData->receiver_address??($auth_web?$auth_web->address:null)}}" {{$cart->step!=1?'disabled':''}}>
                                @error('receiver_address')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <aside class="col-lg-3">
                <div class="summary summary-cart sticky-content">
                    <h3 class="summary-title">購物車金額明細</h3>

                    <div id="cart-sum-holder">
                        @include('frontend.cart._cart_sum')
                    </div>
                    @if ($cart->step == 1 && $cart->itemsChecked > 0)
                        <button type="submit" class="btn btn-outline-primary-2 btn-order btn-block">送出訂單</button>
                    @elseif ($cart->step != 1)
                        <a class="btn btn-outline-dark-2 btn-order btn-block" href="{{route('cart.index')}}">返回修改</a>
                        <button type="submit" class="btn btn-outline-primary-2 btn-order btn-block">送出結帳</button>
                    @endif
                </div>
            </aside>
        </div>
    </form>
@else
    <div class="text-center pt-3 pb-3">
        @lang('text.cart_no_item')
    </div>
    <div class="text-center pt-3">
        <a href="{{route('product.index')}}" class="btn btn-outline-dark-2 mb-3"><span>前往@lang('text.product')</span><i class="icon-long-arrow-right"></i></a>
    </div>
@endif
