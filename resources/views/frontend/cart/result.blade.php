@extends('frontend.layouts.master')

@section('content')
    <div class="page-header text-center"
        style="background-image: url('{{imgsrc($data->banner?:'images/page-header-bg.jpg')}}')">
        <div class="container">
            <h1 class="page-title">@lang('text.cart')</h1>
        </div>
    </div>
    @include('frontend.layouts.breadcrumb', ['class' => ''])
    <div class="page-content">
        <div class="cart">
            <div class="container">
                <ol class="cart-step-list list">
                    <li class="active"><span>@lang('text.cart_info')</span></li>
                    <li class="active"><span>@lang('text.cart_checkout')</span></li>
                    <li class="current active"><span>@lang('text.cart_result')</span></li>
                </ol>
                <div class="text-center">
                    <p class="title-desc mb-1">@lang('validation.order.num')：{{$order->num}}</p>
                    <h2 class="title-sm">{{Session::get('result')}}</h2>
                    <div class="editor text-left mb-5">
                        {!!$data->text2!!}
                        <br><br>
                        @if ($order->payment_return)
                            {!!$order->payment_return!!}
                        @elseif ($record = $order->records->first())
                            @if ($order->payment_info['type'] == 'ATM')
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th scope="row">付款方式</th>
                                            <td>{{$order->payment_info['title']}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費銀行代碼</th>
                                            <td>{{$record->BankCode}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費虛擬帳號</th>
                                            <td>{{$record->vAccount}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費期限</th>
                                            <td>{{$record->ExpireDate}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費金額</th>
                                            <td>{{$record->TradeAmt}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">訂單成立時間</th>
                                            <td>{{$record->TradeDate}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @elseif ($order->payment_info['type'] == 'CVS')
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th scope="row">付款方式</th>
                                            <td>{{$order->payment_info['title']}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費代碼</th>
                                            <td>{{$record->PaymentNo}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費期限</th>
                                            <td>{{$record->ExpireDate}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">繳費金額</th>
                                            <td>{{$record->TradeAmt}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">訂單成立時間</th>
                                            <td>{{$record->TradeDate}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        @endif
                        @if ($order->payment && $order->payment->text)
                            <hr>
                            {!!$order->payment->text!!}
                        @endif
                    </div>
                    
                    <a href="{{route('home')}}}" class="btn btn-outline-primary btn-rounded"><span>@lang('text.back_home')</span><i class="icon-long-arrow-right"></i></a>
                    @if ($order->user_id)
                        <a href="{{route('user.order.index')}}" class="btn btn-outline-primary btn-rounded"><span>@lang('text.user_order')</span><i class="icon-long-arrow-right"></i></a>
                    @else
                        <a href="{{route('order_query.index')}}" class="btn btn-outline-primary btn-rounded"><span>@lang('text.order_query')</span><i class="icon-long-arrow-right"></i></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection