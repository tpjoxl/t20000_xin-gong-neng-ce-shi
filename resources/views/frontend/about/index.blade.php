@extends('frontend.layouts.master')

@section('content')
<div class="main-top">
    <div class="container">
        <div class="main-top-title">@lang('text.'.$prefix)</div>
    </div>
</div>
<div class="main-content">
    <div class="container">
        @include('frontend.layouts.breadcrumb')
        <article class="editor">{!!$data->text!!}</article>
    </div>
</div>
@endsection
