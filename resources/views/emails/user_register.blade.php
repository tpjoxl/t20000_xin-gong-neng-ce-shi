@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ $data->name }} 您好：<br>
    您填寫了<b>「 會員註冊 」</b>表單，請點擊下面的連結網址以完成註冊：<br>
    <a href="{{route('user.register.confirmed', ['token' => $data->register_confirmed, 't' => preg_replace('/\D/', '', $data->created_at)])}}">
      {{route('user.register.confirmed', ['token' => $data->register_confirmed, 't' => preg_replace('/\D/', '', $data->created_at)])}}
    </a>
    <div class="filled-date">
      註冊日期：<span>{{$data->created_at}}</span>
    </div>
  </div>

  @slot('footer')

  @endslot
@endcomponent
