@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    本月壽星生日快樂！<br>
    您的生日優惠券已發送至您的帳號，優惠券資訊如下：
    <table>
        <tr>
            <th>@lang('validation.coupon.code')</th>
            <td>{{$data->code}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.date_range')</th>
            <td>{{$data->date_on}}～{{$data->date_off}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.content')</th>
            <td>{{$data->content}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.min_price')</th>
            <td>{{$data->min_price}}</td>
        </tr>
    </table>
    更多優惠券請至「<a href="{{route('user.coupon')}}">{{__('text.user_centre')}}→{{__('text.user_coupon')}}</a>」查詢
  </div>

  @slot('footer')

  @endslot
@endcomponent
