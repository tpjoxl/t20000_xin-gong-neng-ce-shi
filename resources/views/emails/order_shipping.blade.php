@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    親愛的 {{$data->name}} 您好！<br>
    您的訂單已出貨，訂單資訊如下：
    <br>
    <table>
        <tr>
            <th>@lang('validation.order.num')</th>
            <td>{{$data->num}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.created_at')</th>
            <td>{{$data->created_at}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.shipping_id')</th>
            <td>{{$data->shipping->title}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.shippingnumber')</th>
            <td>{{$data->shippingnumber}}</td>
        </tr>
    </table>
    @if (is_null($data->user_id))
        詳細訂單資料請至「<a href="{{route('order_query.index')}}">{{__('text.order_query')}}</a>」查看
    @else
        詳細訂單資料請至「<a href="{{route('user.order.index')}}">{{__('text.user_centre')}}→{{__('text.user_order')}}</a>」查看
    @endif
  </div>

  @slot('footer')

  @endslot
@endcomponent
