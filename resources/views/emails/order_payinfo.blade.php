@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    親愛的 {{$data->order->name}} 您好！<br>
    您的訂單已送出成功<br>
    請依照下方資訊繳費，謝謝您！<br>
    <hr class="hr-dash">
    【付款資訊】
    <table>
        <tr>
          <th>訂單編號</th>
          <td>{{$data->order->num}}</td>
        </tr>
        <tr>
          <th>付款方式</th>
          <td>{{$data->order->payment->title}}</td>
        </tr>
        @if ($data->vAccount)
            <tr>
                <th>繳費銀行代碼</th>
                <td>{{$data->BankCode}}</td>
            </tr>
            <tr>
                <th>繳費虛擬帳號</th>
                <td>{{$data->vAccount}}</td>
            </tr>
        @elseif($data->PaymentNo)
            <tr>
                <th>繳費代碼</th>
                <td>{{$data->PaymentNo}}</td>
            </tr>
        @else
            <tr>
                <th>條碼第一段號碼</th>
                <td>{{$data->Barcode1}}</td>
            </tr>
            <tr>
                <th>條碼第二段號碼</th>
                <td>{{$data->Barcode2}}</td>
            </tr>
            <tr>
                <th>條碼第三段號碼</th>
                <td>{{$data->Barcode3}}</td>
            </tr>
        @endif
        <tr>
          <th>繳費金額</th>
          <td>{{$data->TradeAmt}}</td>
        </tr>
        <tr>
          <th>繳費期限</th>
          <td>{{$data->ExpireDate}}</td>
        </tr>
        <tr>
          <th>訂單成立時間</th>
          <td>{{$data->TradeDate}}</td>
        </tr>
    </table>
    @if (is_null($data->order->user_id))
        詳細訂單資料請至「<a href="{{route('order_query.index')}}">{{__('text.order_query')}}</a>」查看
    @else
        詳細訂單資料請至「<a href="{{route('user.order.index')}}">{{__('text.user_centre')}}→{{__('text.user_order')}}</a>」查看
    @endif
</div>

  @slot('footer')

  @endslot
@endcomponent
