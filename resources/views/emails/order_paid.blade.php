@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    親愛的 {{$data->order->name}} 您好！<br>
    您的訂單已付款完成，付款資資訊如下：
    <hr class="hr-dash">
    【付款資訊】
    <table>
        <tr>
          <th>訂單編號</th>
          <td>{{$data->order->num}}</td>
        </tr>
        <tr>
          <th>訂單金額</th>
          <td>{{$data->order->sum_price}}</td>
        </tr>
        <tr>
          <th>交易日期</th>
          <td>{{$data->PaymentDate}}</td>
        </tr>
        <tr>
          <th>交易金額</th>
          <td>{{$data->TradeAmt}}</td>
        </tr>
    </table>
    @if (is_null($data->order->user_id))
        詳細訂單資料請至「<a href="{{route('order_query.index')}}">{{__('text.order_query')}}</a>」查看
    @else
        詳細訂單資料請至「<a href="{{route('user.order.index')}}">{{__('text.user_centre')}}→{{__('text.user_order')}}</a>」查看
    @endif
</div>

  @slot('footer')

  @endslot
@endcomponent
