@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ config('app.name') }} 網站管理員 您好：<br>
    訂單<b>「{{$data->num}}」</b>已成立，資訊如下，詳細資料請至後台查看
    <hr class="hr-dash">
    【訂單資訊】
    <table>
        <tr>
            <th>@lang('validation.order.num')</th>
            <td>{{$data->num}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.created_at')</th>
            <td>{{$data->created_at}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.sum_price')</th>
            <td>{{$data->present()->price($data->sum_price)}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.payment_id')</th>
            <td>{{$data->payment->title}}</td>
        </tr>
        <tr>
            <th>@lang('validation.order.shipping_id')</th>
            <td>{{$data->shipping->title}}</td>
        </tr>
    </table>
    <hr class="hr-dash">
    【購物清單】
    <table class="table-h">
      <thead>
        <tr>
          <th>商品名稱</th>
          <th>商品編號</th>
          <th>規格</th>
          <th>單價</th>
          <th>數量</th>
          <th>小計</th>
        </tr>
      </thead>
      <tbody class="cart-holder">
        @foreach ($data->items as $key => $item)
          <tr>
            <td>{{$item->title}}</td>
            <td>{{$item->num}}</td>
            <td>{{$item->options}}</td>
            <td>{{$item->price}}</td>
            <td>{{$item->qty}}</td>
            <td>{{$item->total}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <br>
    <div style="text-align:right;">
      <strong>{{__('validation.order.items_total_price')}}:{{$data->present()->price($data->items_total_price)}}</strong>
    </div>
    <div style="text-align:right;">
      <strong>{{__('validation.order.sum_price')}}: {{$data->present()->price($data->sum_price)}}</strong>
    </div>
  </div>

  @slot('footer')

  @endslot
@endcomponent
