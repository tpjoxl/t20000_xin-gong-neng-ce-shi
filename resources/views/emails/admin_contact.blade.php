@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ config('app.name') }} 網站管理員 您好：<br>
    使用者填寫了<b>「聯絡我們」</b>表單，內容如下表：
    <table>
      <tr>
        <th>@lang('validation.attributes.name')</th>
        <td>{{$data->name}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.sex_title')</th>
        <td>{{$data->sex?$data->present()->sex_title($data->sex):''}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.phone')</th>
        <td>{{$data->phone}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.email')</th>
        <td>{{$data->email}}</td>
      </tr>
      <tr>
        <th>@lang('validation.attributes.message')</th>
        <td>{!! nl2br($data->message) !!}</td>
      </tr>
    </table>
    <div class="filled-date">
        填寫日期：<span>{{$data->created_at}}</span>
    </div>
  </div>

  @slot('footer')

  @endslot
@endcomponent
