@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ $data->name }} 您好：<br>
    您填寫了<b>「 忘記密碼 」</b>表單，產生的新密碼如下，因安全性考量，請在登入後立即修改成您自己的密碼：
    <table>
      <tr>
        <th>新密碼</th>
        <td>{{$new_psw}}</td>
      </tr>
    </table>
    <div class="filled-date">
      填寫日期：<span>{{Carbon::now()}}</span>
    </div>
  </div>

  @slot('footer')

  @endslot
@endcomponent
