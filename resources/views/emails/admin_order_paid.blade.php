@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ config('app.name') }} 網站管理員 您好：<br>
    訂單<b>「{{$data->order->num}}」</b>已付款，內容如下表：
    <hr class="hr-dash">
    【付款資訊】
    <table>
      <tr>
        <th>訂單編號</th>
        <td>{{$data->order->num}}</td>
      </tr>
      <tr>
        <th>訂單金額</th>
        <td>{{$data->order->sum_price}}</td>
      </tr>
      <tr>
        <th>交易日期</th>
        <td>{{$data->PaymentDate}}</td>
      </tr>
      <tr>
        <th>交易金額</th>
        <td>{{$data->TradeAmt}}</td>
      </tr>
    </table>
  </div>

  @slot('footer')

  @endslot
@endcomponent
