@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {!! $content['text'] !!}
    <br>
    <table>
        <tr>
            <th>@lang('validation.coupon.title')</th>
            <td>{{$data->title}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.code')</th>
            <td>{{$data->code}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.date_range')</th>
            <td>{{$data->date_on}}～{{$data->date_off}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.content')</th>
            <td>{{$data->content}}</td>
        </tr>
        <tr>
            <th>@lang('validation.coupon.min_price')</th>
            <td>{{$data->min_price}}</td>
        </tr>
    </table>
  </div>

  @slot('footer')

  @endslot
@endcomponent
