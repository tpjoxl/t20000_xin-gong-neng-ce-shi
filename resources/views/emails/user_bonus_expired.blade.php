@component('mail::layout')
  @slot('header')

  @endslot

  <div class="table-inform">
    {{ config('app.name') }}會員您好：<br>
    您於去年本站若累積之紅利點數，今年 12月 31日過後將無法再使用<br>
    <a href="{{route('user.bonus')}}">查看您的紅利點數</a>
  </div>

  @slot('footer')

  @endslot
@endcomponent
