@php
    header("Content-Type: application/xml; charset=utf-8");
    $xmlOutput = '<?xml version="1.0" encoding="UTF-8"?>';
    echo $xmlOutput;
    $locales = app('frontendLocales');
@endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <url>
    <loc>{{ route('home') }}</loc>
@if ($locales->count()>1)
  @foreach ($locales as $locale)
  <xhtml:link rel="alternate" hreflang="{{$locale->code}}" href="{{url('/').'/'.mb_strtolower($locale->code)}}" />
  @endforeach
@endif
  <changefreq>weekly</changefreq>
    <priority>1.0</priority>
  </url>
@foreach ($posts as $post)
  <url>
    <loc>{{ $post->loc }}</loc>
@if (!empty($post->alternate))
  @foreach ($post->alternate as $locale->code => $lang_url)
  <xhtml:link rel="alternate" hreflang="{{$locale->code}}" href="{{$lang_url}}" />
  @endforeach
@endif
@if (!empty($post->lastmod))
  <lastmod>{{ $post->lastmod }}</lastmod>
@endif
@if (!empty($post->changefreq))
    <changefreq>{{ $post->changefreq }}</changefreq>
@endif
@if (!empty($post->priority))
    <priority>{{ $post->priority }}</priority>
@endif
  </url>
@endforeach
</urlset>
