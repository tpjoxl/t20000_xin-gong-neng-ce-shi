window.$ = window.jQuery = require('jquery');
// import Cookies from 'js-cookie'
window.Swal = require('sweetalert2');
(function($, customMsg) {
    require('air-datepicker');
    try {
      require('air-datepicker/dist/js/i18n/datepicker.'+App.locale);
    } catch (error) {
      App.locale = 'en';
      require('air-datepicker/dist/js/i18n/datepicker.'+App.locale);
    }
    // IE關閉smooth scroll
    if(navigator.userAgent.match(/Trident\/7\./)) {
        document.body.addEventListener("mousewheel", function() {
            event.preventDefault();
            let wd = event.wheelDelta;
            let csp = window.pageYOffset;
            window.scrollTo(0, csp - wd);
        });
    }
    $('.dp').datepicker({
        language: App.locale,
        dateFormat: 'yyyy-mm-dd',
        // todayButton: true,
        onHide: function(dp, animationCompleted){
            if (animationCompleted && dp.$el.val()) {
                dp.selectDate(new Date(dp.$el.val()))
            }
        }
    });
    // if (Cookies.get('topbarStatus')) {
    //     topbarStatus = Cookies.get('topbarStatus');
    // }
    // if (topbarStatus < 0) {
    //     $body.addClass('topbar-hide')
    // }
    // $('.topbar-toggle').click(function(event) {
    //     $body.toggleClass('topbar-hide');
    //     topbarStatus *= -1;
    //     Cookies.set('topbarStatus', topbarStatus)
    //     return false;
    // });
    require('./cart');
})($, customMsg);
const customMsg = window.customMsg = function(options) {
    var inner = '';

    if(options.title) inner += `<div class="result-title">${options.title}</div>`;
    if(options.text) inner += `<div class="result-txt">${options.text}</div>`;
    if(options.customButton) {
        inner += `<div class="result-btn">${options.customButton}</div>`;
    } else {
        inner += `<div class="result-btn"><button class="btn btn-default result-close" type="button">${options.confirmButtonText?options.confirmButtonText:'OK'}</button></div>`;
    }
    if ($('.result').length > 0) {


        $('.result-inner').html(inner);
    } else {
        var html =
        `<div class="result">
            <div class="result-content">
                <button class="btn result-close-btn result-close" type="button"><i class="ic ic-close"></i></button>
                <div class="result-inner">${inner}</div>
            </div>
        </div>`;
        $('body').append(html);
    }
    $(document).on('click', function(e){
        if($(e.target).parents('.result').length==0 || $(e.target).parents('.result-close').length>0 || $(e.target).hasClass('result-close')){
        $('body').removeClass('result-open');
        }
    });
    $('body').addClass('result-open');
};
