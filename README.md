# W00000_37site

【主機環境】

- 本機：[http://w00000.test](http://w00000.test)

## 安裝PHP套件
```
composer install
composer update
```
## 產生key
每個新的專案都需要重新產生一組key

先修改 `.env` 內的 `APP_ENV=install`

執行以下命令列
```
php artisan key:generate
```

## 建立資料表

確認資料夾內的檔案後
- database/migrations
- database/seeds

執行以下命令列
```
php artisan migrate:fresh --seed
```
※每次seeds有異動後都要再執行`composer dumpautoload`

`.env` 參數改回 `APP_ENV=local`

## XAMPP VirtualHost 虛擬主機設定方式
使用系統管理員身分開啟以下檔案進行修改
`\Windows\System32\drivers\etc` 修改 `hosts` 檔
※ host檔修改時可能會被防毒軟體安全設定檔下，小紅傘的設定在「一般>資訊安全」保護Windows主機檔案不被變更，要存檔時先取消勾選，改完再勾回去即可。
```
127.0.0.1       w00000.test
```
`\xampp\apache\conf\extra\` 修改 `httpd-vhosts.conf` 檔
```
<VirtualHost w00000.test:80>
    DocumentRoot "C:/xampp/htdocs/37case/w00000/public"
    ServerName w00000.test

    <Directory "C:/xampp/htdocs/37case/w00000">
        Options All
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```
修改完上面兩個檔案後須重啟Apache，接著在網址輸入 `w00000.test` 即可瀏覽網頁

## 前端編譯
安裝前端套件
```
npm install
```

`wabpack.mix.js`設定要編譯的檔案資訊

檔案修改時執行以下命令列，監聽前端的檔案
```
npm run watch
```


## 正式上線執行的動作
清除編譯或除錯時產生的檔案
```
php artisan view:clear
php artisan debugbar:clear
php artisan route:clear
php artisan cache:clear
php artisan config:clear
```
移除開發用的PHP套件
```
composer install --no-dev
```
※ 正式上線不需要上傳 `node_modules` 、 `.git` 等開發用資料夾

新增recaptcha並綁定客戶網域
[https://www.google.com/recaptcha/admin](https://www.google.com/recaptcha/admin)

`.env` 參數修改
```
APP_ENV=prodcution
APP_DEBUG=false

DB_DATABASE=主機資料庫名稱
DB_USERNAME=主機資料庫帳號
DB_PASSWORD=主機資料庫密碼

MAIL_HOST=SMTP主機
MAIL_PORT=25(SMTP PORT)
MAIL_USERNAME=SMTP帳號
MAIL_PASSWORD=SMTP密碼
MAIL_ENCRYPTION=null(SMTP加密方式null/SSL/TLS)
MAIL_FROM_ADDRESS=網域的服務信箱

BACKEND_ROUTE_PREFIX=專案編號

RE_CAP_SITE=綁定網域的recaptcha金鑰
RE_CAP_SECRET=綁定網域的recaptcha密鑰
```
`.env` 參數刪除
```
BACKEND_ADMIN_PASSWORD=
```


## htaccess轉址設定
以下只是相關轉址程式的筆記，請依照這專案使用需求複製適用的程式碼
```
RewriteEngine On

# redirect www to non-www (有分HTTP跟HTTPS)
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]
RewriteRule ^(.*)$ http://%1/$1 [R=301,L]

RewriteCond %{HTTPS} on
RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]
RewriteRule ^(.*)$ https://%1/$1 [R=301,L]

# redirect non-www to www (有分HTTP跟HTTPS)
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} !^www\.(.*)$ [NC]
RewriteRule ^(.*)$ http://www.%{HTTP_HOST}/$1 [R=301,L]

RewriteCond %{HTTPS} on
RewriteCond %{HTTP_HOST} !^www\.(.*)$ [NC]
RewriteRule ^(.*)$ https://www.%{HTTP_HOST}/$1 [R=301,L]

# ensure www.
RewriteCond %{HTTP_HOST} !^www\. [NC]
RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

# ensure https
RewriteCond %{HTTP:X-Forwarded-Proto} !https
RewriteCond %{HTTPS} off
RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

# 避免網址出現index.php
RewriteCond %{THE_REQUEST} ^GET.*index\.php [NC]
RewriteRule (.*?)index\.php/*(.*) /$1$2 [R=301,NE,L]
```
