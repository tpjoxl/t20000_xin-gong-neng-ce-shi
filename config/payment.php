<?php

return [
  // 綠界
  'ecpay' => [
    'ServiceURL' => env('ECPAY_SERVICE_URL', 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5'),
    'MerchantId' => env('ECPAY_MERCHANT_ID', '2000132'),
    'HashKey' => env('ECPAY_HASH_KEY', '5294y06JbISpM5x9'),
    'HashIV' => env('ECPAY_HASH_IV', 'v77hoKGq4kWxNNIS'),
    'InvoiceUrl' => env('ECPAY_INVOICE_URL', 'https://einvoice-stage.ecpay.com.tw'),
    'InvoiceMerchantId' => env('ECPAY_INVOICE_MERCHANT_ID', '2000132'),
    'InvoiceHashKey' => env('ECPAY_INVOICE_HASH_KEY', 'ejCk326UnaZWKisg'),
    'InvoiceHashIV' => env('ECPAY_INVOICE_HASH_IV', 'q9jcZX8Ib9LM8wYk'),
    'LogisticsType' => env('ECPAY_LOGISTICS_TYPE', 'C2C'),
    'LogisticsMerchantId' => env('ECPAY_LOGISTICS_MERCHANT_ID', '2000933'),
    'LogisticsHashKey' => env('ECPAY_LOGISTICS_HASH_KEY', 'XBERn1YOvpM9nfZc'),
    'LogisticsHashIV' => env('ECPAY_LOGISTICS_HASH_IV', 'h1ONHk4P4yqbl5LK'),
  ],
  // 聯合信用
  'ncccpay' => [
    'ServiceURL' => env('NCCCPAY_SERVICE_URL', 'https://nccnet-ectest.nccc.com.tw/merchant/HPPRequest'),
    'MerchantID' => env('NCCCPAY_MERCHANT_ID', '6600800020'),
    'TerminalID' => env('NCCCPAY_TERMINAL_ID', '70502932'),
    'MacKey' => env('NCCCPAY_MAC_KEY', 'ffc5eea6ff6c873a976730b36e225c5a916861a4658c8288f26d1abf3e360494'),
  ],
  // 藍新
  'newebpay' => [
    'ServiceURL' => env('NEWEBPAY_SERVICE_URL', 'https://ccore.newebpay.com/MPG/mpg_gateway'),
    'MerchantID' => env('NEWEBPAY_MERCHANT_ID'),
    'HashKey' => env('NEWEBPAY_HASH_KEY'),
    'HashIV' => env('NEWEBPAY_HASH_IV'),
    'Version' => '1.5',
  ],
  'ezpay' => [
    'InvoiceServiceURL' => env('EZPAY_INVOICE_SERVICE_URL', 'https://cinv.ezpay.com.tw/Api'),
    'InvoiceMerchantID' => env('EZPAY_INVOICE_MERCHANT_ID'),
    'InvoiceHashKey' => env('EZPAY_INVOICE_HASH_KEY'),
    'InvoiceHashIV' => env('EZPAY_INVOICE_HASH_IV'),
  ],
  'linepay' => [
    'ServiceURL' => env('LINE_PAY_SERVICE_URL', 'https://sandbox-api-pay.line.me'),
    'ChannelID' => env('LINE_PAY_CHANNEL_ID'),
    'ChannelSecret' => env('LINE_PAY_CHANNEL_SECRET'),
  ],
];
