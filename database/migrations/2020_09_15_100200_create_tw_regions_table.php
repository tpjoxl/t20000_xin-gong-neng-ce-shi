<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTwRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tw_regions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 150)->comment('區域名稱');
            $table->string('code', 5)->comment('郵遞區號');
            $table->foreignId('city_id')->comment('所屬城市')->constrained('tw_cities')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('remoted')->default(false)->comment('偏鄉');
            $table->bigInteger('rank')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tw_regions');
    }
}
