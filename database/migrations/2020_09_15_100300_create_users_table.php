<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->default('1')->comment('1=一般會員、2=VIP會員');
            $table->string('account')->unique();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('name', 150)->nullable();
            $table->tinyInteger('sex')->nullable();
            $table->date('birthday')->nullable();
            $table->string('phone', 100)->nullable();
            $table->foreignId('city_id')->nullable()->constrained('tw_cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreignId('region_id')->nullable()->constrained('tw_regions')->onDelete('set null')->onUpdate('cascade');
            $table->string('address', 512)->nullable();
            $table->tinyInteger('status')->default('1');
            $table->boolean('email_verified')->default(false);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('register_confirmed', 100)->nullable();
            $table->dateTime('login_at')->nullable();
            $table->ipAddress('ip')->nullable();
            $table->string('admin_note', 3000)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string('facebook_id', 255)->nullable();
            $table->boolean('is_facebook')->default(false);
            $table->string('google_id', 512)->nullable();
            $table->boolean('is_google')->default(false);
            $table->boolean('first_login')->default(false)->comment('社群or匯入初次登入');
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
