<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinepayRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linepay_records', function (Blueprint $table) {
            $table->id();
            $table->string('route_name', 255)->nullable();
            $table->string('action', 50)->nullable()->comment('執行的API');
            $table->string('returnCode', 4)->nullable()->comment('結果代碼');
            $table->string('returnMessage', 300)->nullable()->comment('結果訊息');
            $table->string('orderId', 100)->nullable()->comment('商家唯一訂單編號')->index();
            $table->string('transactionId', 19)->nullable()->comment('交易序號')->index();
            $table->string('transactionDate', 20)->nullable()->comment('交易日期');
            $table->string('transactionType', 50)->nullable()->comment('交易分類');
            $table->string('payStatus', 20)->nullable()->comment('付款狀態');
            $table->string('productName', 4000)->nullable()->comment('商品名稱');
            $table->text('merchantName')->nullable()->comment('商家名稱');
            $table->string('currency', 3)->nullable()->comment('貨幣');
            $table->string('paymentAccessToken', 12)->nullable()->comment('該代碼在LINE Pay可以代替掃描器使用');
            $table->longText('paymentUrl')->nullable()->comment('用來跳轉到付款頁的URL');
            $table->string('authorizationExpireDate', 30)->nullable()->comment('授權過期時間');
            $table->string('regKey', 15)->nullable()->comment('用於自動付款的密鑰');
            $table->longText('payInfo')->nullable()->comment('付款資訊');
            $table->longText('packages')->nullable();
            $table->longText('merchantReference')->nullable();
            $table->longText('shipping')->nullable();
            $table->string('refundTransactionId', 19)->nullable()->comment('退款序號');
            $table->string('refundTransactionDate', 30)->nullable()->comment('退款日期');
            $table->longText('refundList')->nullable();
            $table->longText('events')->nullable()->comment('指定品項組合的資訊');
            $table->longText('errorDetails')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linepay_records');
    }
}
