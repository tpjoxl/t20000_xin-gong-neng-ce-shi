<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('sku_id')->nullable()->constrained('skus')->onDelete('set null')->onUpdate('cascade');
            $table->string('type', 150)->nullable();
            $table->string('pic', 512)->nullable();
            $table->string('title', 191)->nullable();
            $table->string('num', 100)->nullable();
            $table->string('options', 3000)->nullable();
            $table->tinyInteger('price_type')->nullable()->comment('價格類型');
            $table->integer('original_price')->nullable()->comment('原價');
            $table->integer('price')->nullable()->comment('售價');
            $table->integer('discount')->nullable()->comment('折扣');
            $table->integer('qty')->nullable()->comment('數量');
            $table->integer('total')->nullable()->comment('小計');
            $table->timestamps();
            $table->tinyInteger('status')->default(1);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
