<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->string('user_type', 255)->nullable()->comment('可使用的會員種類');
            $table->tinyInteger('use_type')->nullable()->comment('指定方式');
            $table->string('code')->comment('優惠代碼')->unique()->index();
            $table->tinyInteger('type')->nullable()->comment('折扣方式 1固定金額 2百分比');
            $table->integer('number')->comment('折扣金額或百分比');
            $table->integer('min_price')->nullable()->comment('訂單最低使用金額');
            $table->integer('count')->nullable()->comment('限制使用數量');
            $table->string('pic', 512)->nullable();
            $table->date('date_on')->nullable();
            $table->date('date_off')->nullable();
            $table->timestamps();
            $table->boolean('user_visible')->default(true)->comment('會員中心是否顯示');
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
        Schema::create('coupon_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('coupon_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('text', 1000)->nullable();

            $table->unique(['coupon_id', 'locale']);
            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade')->onUpdate('cascade');
        });

        // 優惠券可使用的會員
        Schema::create('coupon_user', function (Blueprint $table) {
            $table->unsignedBigInteger('coupon_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->bigInteger('rank')->default(0);

            $table->primary(['coupon_id', 'user_id']);

            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
        
        // 訂單使用的優惠券
        Schema::create('order_coupon', function (Blueprint $table) {
            $table->unsignedBigInteger('coupon_id')->index();
            $table->unsignedBigInteger('order_id')->index();
            $table->bigInteger('rank')->default(0);

            $table->primary(['coupon_id', 'order_id']);

            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
        Schema::dropIfExists('coupon_translations');
        Schema::dropIfExists('coupon_user');
        Schema::dropIfExists('order_coupon');
    }
}
