<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('num', 100)->index()->comment('訂單編號');
            $table->string('epaynumber', 100)->nullable()->index()->comment('金流訂單編號');
            $table->string('shippingnumber', 100)->nullable()->index()->comment('物流編號');
            $table->integer('items_total_price')->nullable()->comment('商品金額小計');
            $table->integer('coupon_price')->nullable()->comment('優惠券折扣金額');
            $table->integer('bonus_price')->nullable()->comment('紅利折扣金額');
            $table->integer('shipping_price')->nullable()->comment('運費');
            $table->integer('payment_price')->nullable()->comment('金流手續費');
            $table->integer('sum_price')->nullable()->comment('訂單總金額');
            $table->mediumText('payment_info')->nullable();
            $table->foreignId('payment_id')->nullable()->constrained('payments')->onDelete('set null')->onUpdate('cascade');
            $table->string('payment_return',3000)->nullable()->comment('金流回傳自組字串');
            $table->dateTime('payment_deadline')->nullable();
            $table->mediumText('shipping_info')->nullable();
            $table->tinyInteger('shipping_type')->nullable();
            $table->foreignId('shipping_id')->nullable()->constrained('shippings')->onDelete('set null')->onUpdate('cascade');
            $table->mediumText('coupon_info')->nullable();
            $table->string('name')->nullable()->comment('購買人姓名');
            $table->tinyInteger('sex')->nullable()->comment('購買人性別');
            $table->string('phone', 100)->nullable()->comment('購買人電話');
            $table->string('email')->nullable()->comment('購買人信箱');
            $table->foreignId('city_id')->nullable()->comment('購買人縣市')->constrained('tw_cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreignId('region_id')->nullable()->comment('購買人區域')->constrained('tw_regions')->onDelete('set null')->onUpdate('cascade');
            $table->string('address', 512)->nullable()->comment('購買人地址');
            $table->string('receiver_name')->nullable()->comment('收件人姓名');
            $table->tinyInteger('receiver_sex')->nullable()->comment('收件人性別');
            $table->string('receiver_phone', 100)->nullable()->comment('收件人電話');
            $table->string('receiver_email')->nullable()->comment('收件人信箱');
            $table->foreignId('receiver_city_id')->nullable()->comment('收件人縣市')->constrained('tw_cities')->onDelete('set null')->onUpdate('cascade');
            $table->foreignId('receiver_region_id')->nullable()->comment('收件人區域')->constrained('tw_regions')->onDelete('set null')->onUpdate('cascade');
            $table->string('receiver_address', 512)->nullable()->comment('收件人地址');
            $table->string('note',1000)->nullable()->comment('訂單備註');
            $table->date('hope_date')->nullable()->comment('指定收件日期');
            $table->string('hope_time',100)->nullable()->comment('指定收件時段');
            $table->tinyInteger('invoice_type')->nullable()->comment('發票形式');
            $table->string('invoice_mobile', 100)->nullable()->comment('手機載具號碼');
            $table->string('invoice_certificate', 100)->nullable()->comment('自然人憑證載具號碼');
            $table->string('ein_title', 200)->nullable()->comment('公司抬頭');
            $table->string('ein_num', 100)->nullable()->comment('統編');
            $table->string('donate_code', 20)->nullable()->comment('受捐單位捐贈碼');
            $table->string('invoice_address', 100)->nullable()->comment('索取紙本地址');
            $table->string('admin_note',3000)->nullable();
            $table->timestamps();
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('payment_status')->default(1);
            $table->tinyInteger('shipping_status')->default(1);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
