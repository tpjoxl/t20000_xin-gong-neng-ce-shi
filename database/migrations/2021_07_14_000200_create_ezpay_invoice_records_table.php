<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEzpayInvoiceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ezpay_invoice_records', function (Blueprint $table) {
            $table->id();
            $table->string('Status', 10)->nullable()->comment('回傳狀態');
            $table->string('Message', 30)->nullable()->comment('回傳訊息');
            $table->string('MerchantID', 15)->nullable()->comment('商店代號');
            $table->string('InvoiceTransNo', 20)->nullable()->comment('ezPay 電子發票開立序號');
            $table->string('MerchantOrderNo', 20)->nullable()->comment('商店訂單編號')->index();
            $table->integer('TotalAmt')->nullable()->comment('發票金額');
            $table->string('InvoiceNumber', 10)->nullable()->comment('發票號碼');
            $table->string('RandomNum', 4)->nullable()->comment('發票防偽隨機碼');
            $table->dateTime('CreateTime')->nullable()->comment('開立發票時間');
            $table->string('CheckCode', 64)->nullable()->comment('檢查碼');
            $table->string('BarCode', 19)->nullable()->comment('發票條碼');
            $table->string('QRcodeL', 140)->nullable()->comment('發票 QRCode(左)');
            $table->string('QRcodeR', 140)->nullable()->comment('發票 QRCode(右)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ezpay_invoice_records');
    }
}
