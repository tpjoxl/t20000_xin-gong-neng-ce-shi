<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippings', function (Blueprint $table) {
            $table->id();
            $table->string('user_type', 255)->nullable()->default('0,1,2')->comment('可使用的會員種類');
            $table->tinyInteger('type')->nullable()->default(1)->comment('配送類型 1國內 2國外');
            $table->integer('price')->nullable()->comment('一般運費');
            $table->integer('limit_order_price')->nullable()->comment('訂單金額大於等於多少');
            $table->integer('limit_price')->nullable()->comment('超過訂單金額的運費');
            $table->string('url', 512)->nullable();
            $table->string('pic', 512)->nullable();
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('active')->default(false)->comment('啟用狀態');
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
        Schema::create('shipping_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('shipping_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('text', 1000)->nullable();

            $table->unique(['shipping_id', 'locale']);
            $table->foreign('shipping_id')->references('id')->on('shippings')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('shipping_payment', function (Blueprint $table) {
            $table->unsignedBigInteger('shipping_id')->index();
            $table->unsignedBigInteger('payment_id')->index();
            $table->bigInteger('rank')->default(0);

            $table->primary(['shipping_id', 'payment_id']);

            $table->foreign('shipping_id')->references('id')->on('shippings')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippings');
        Schema::dropIfExists('shipping_translations');
        Schema::dropIfExists('shipping_payment');
    }
}
