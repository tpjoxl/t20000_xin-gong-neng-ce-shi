<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skus', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('belong')->nullable()->comment('所屬單元');
            $table->foreignId('product_id')->nullable()->constrained('products')->onDelete('set null')->onUpdate('cascade');
            $table->string('usein', 255)->nullable()->default('1')->comment('適用類型 1產品 2加價購');
            $table->string('title', 191)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('props', 150)->nullable()->index();
            $table->string('num', 100)->nullable()->index();
            $table->integer('price')->nullable()->comment('定價');
            $table->integer('price2')->nullable()->comment('優惠價');
            $table->integer('price3')->nullable();
            $table->integer('price4')->nullable();
            $table->integer('price5')->nullable();
            $table->integer('qty')->nullable()->comment('公司庫存');
            $table->integer('qty2')->nullable()->comment('門市庫存');
            $table->integer('sales')->nullable()->comment('公司銷量');
            $table->integer('sales2')->nullable()->comment('門市銷量');
            $table->string('pic', 512)->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skus');
    }
}
