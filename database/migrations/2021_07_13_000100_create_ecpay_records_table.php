<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcpayRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecpay_records', function (Blueprint $table) {
            $table->id();
            $table->string('route_name', 255)->nullable();
            $table->string('MerchantID', 10)->nullable()->comment('特店編號');
            $table->string('MerchantTradeNo', 20)->nullable()->comment('特店交易編號')->index();
            $table->string('StoreID', 20)->nullable()->comment('特店旗下店舖代號');
            $table->integer('RtnCode')->nullable()->comment('交易狀態');
            $table->string('RtnMsg', 200)->nullable()->comment('交易訊息');
            $table->string('TradeNo', 20)->nullable()->comment('綠界的交易編號');
            $table->integer('TradeAmt')->nullable()->comment('交易金額');
            $table->string('PaymentDate', 20)->nullable()->comment('付款時間');
            $table->string('PaymentType', 20)->nullable()->comment('特店選擇的付款方式');
            $table->integer('PaymentTypeChargeFee')->nullable()->comment('通路費');
            $table->string('TradeDate', 20)->nullable()->comment('訂單成立時間');
            $table->string('CustomField1', 50)->nullable()->comment('自訂名稱欄位1');
            $table->string('CustomField2', 50)->nullable()->comment('自訂名稱欄位2');
            $table->string('CustomField3', 50)->nullable()->comment('自訂名稱欄位3');
            $table->string('CustomField4', 50)->nullable()->comment('自訂名稱欄位4');
            $table->string('WebATMAccBank', 3)->nullable()->comment('付款人銀行代碼');
            $table->string('WebATMAccNo', 5)->nullable()->comment('付款人銀行帳號後五碼');
            $table->string('WebATMBankName', 10)->nullable()->comment('銀行名稱');
            $table->string('ATMAccBank', 3)->nullable()->comment('付款人銀行代碼');
            $table->string('ATMAccNo', 5)->nullable()->comment('付款人銀行帳號後五碼');
            $table->string('BankCode', 3)->nullable()->comment('繳費銀行代碼');
            $table->string('vAccount', 16)->nullable()->comment('繳費虛擬帳號');
            $table->string('ExpireDate', 10)->nullable()->comment('繳費期限');
            $table->string('PaymentNo', 14)->nullable()->comment('繳費代碼');
            $table->string('PayFrom', 10)->nullable()->comment('繳費超商');
            $table->string('Barcode1', 20)->nullable()->comment('條碼第一段號碼');
            $table->string('Barcode2', 20)->nullable()->comment('條碼第二段號碼');
            $table->string('Barcode3', 20)->nullable()->comment('條碼第三段號碼');
            $table->string('PeriodType', 1)->nullable()->comment('週期種類');
            $table->integer('Frequency')->nullable()->comment('執行頻率');
            $table->integer('ExecTimes')->nullable()->comment('執行次數');
            $table->integer('Amount')->nullable()->comment('本次授權金額');
            $table->integer('Gwsr')->nullable()->comment('授權交易單號');
            $table->string('ProcessDate', 20)->nullable()->comment('處理時間');
            $table->string('AuthCode', 6)->nullable()->comment('授權碼');
            $table->integer('FirstAuthAmount')->nullable()->comment('初次授權金額');
            $table->integer('PeriodAmount')->nullable()->comment('訂單建立時的每次要授權金額');
            $table->integer('TotalSuccessTimes')->nullable()->comment('已執行成功次數');
            $table->integer('TotalSuccessAmount')->nullable()->comment('目前已成功授權的金額合計');
            $table->integer('stage')->nullable()->comment('分期期數');
            $table->integer('stast')->nullable()->comment('頭期金額');
            $table->integer('staed')->nullable()->comment('各期金額');
            $table->integer('eci')->nullable()->comment('3D(VBV) 回傳值(eci=5,6,2,1 代表該筆交易3D交易)');
            $table->string('card4no', 4)->nullable()->comment('卡片的末 4 碼');
            $table->string('card6no', 6)->nullable()->comment('卡片的前 6 碼');
            $table->integer('red_dan')->nullable()->comment('紅利扣點');
            $table->integer('red_de_amt')->nullable()->comment('紅利折抵金額');
            $table->integer('red_ok_amt')->nullable()->comment('實際扣款金額');
            $table->integer('red_yet')->nullable()->comment('紅利剩餘點數');
            $table->tinyInteger('SimulatePaid')->nullable()->comment('是否為模擬付款');
            $table->text('CheckMacValue')->nullable()->comment('檢查碼');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecpay_records');
    }
}
