<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_tags', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });
        Schema::create('blog_tag_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('blog_tag_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');

            $table->unique(['blog_tag_id', 'locale']);
            $table->foreign('blog_tag_id')->references('id')->on('blog_tags')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('blog_tag', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_id');
            $table->unsignedBigInteger('blog_tag_id');
            $table->string('note')->nullable();

            $table->primary(['blog_id', 'blog_tag_id']);

            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('blog_tag_id')->references('id')->on('blog_tags')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_tags');
        Schema::dropIfExists('blog_tag_translations');
        Schema::dropIfExists('blog_tag');
    }
}
