<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->default(true);
            $table->boolean('home_status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->string('pic', 512)->nullable();
            $table->string('pic2', 512)->nullable();
            $table->date('date_on')->nullable();
            $table->date('date_off')->nullable();
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });

        Schema::create('banner_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('banner_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('text', 1000)->nullable();
            $table->string('tag_title')->nullable();
            $table->string('tag_alt')->nullable();
            $table->string('url', 512)->nullable();
            $table->string('btn_txt')->nullable();
            $table->string('target', 50)->nullable();

            $table->unique(['banner_id', 'locale']);
            $table->foreign('banner_id')->references('id')->on('banners')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_translations');
        Schema::dropIfExists('banners');
    }
}
