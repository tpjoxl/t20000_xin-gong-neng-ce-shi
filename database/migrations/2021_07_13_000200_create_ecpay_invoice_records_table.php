<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcpayInvoiceRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecpay_invoice_records', function (Blueprint $table) {
            $table->id();
            $table->integer('RtnCode')->nullable()->comment('回應代碼');
            $table->string('RtnMsg', 200)->nullable()->comment('回應訊息');
            $table->string('InvoiceNumber', 10)->nullable()->comment('發票號碼');
            $table->string('InvoiceDate', 20)->nullable()->comment('發票開立時間');
            $table->string('RandomNumber', 4)->nullable()->comment('發票防偽隨機碼');
            $table->text('CheckMacValue')->nullable()->comment('檢查碼');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecpay_invoice_records');
    }
}
