<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePowersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('powers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('name', 255)->nullable();
            $table->foreignId('parent_id')->nullable()->constrained('powers')->onDelete('set null')->onUpdate('cascade');
            $table->string('icon', 100)->nullable();
            $table->string('route_prefix')->nullable();
            $table->text('route_name')->nullable();
            $table->string('url_param', 255)->nullable();
            $table->boolean('status')->default(true)->comment('側選單顯示');
            $table->boolean('active')->default(true)->comment('是否給客戶使用');
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('divider_after')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('powers');
    }
}
