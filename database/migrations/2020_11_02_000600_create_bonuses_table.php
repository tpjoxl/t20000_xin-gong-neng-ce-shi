<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonuses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreignId('order_id')->nullable()->constrained('orders')->onDelete('set null')->onUpdate('cascade');
            $table->integer('point')->default(0)->comment('點數');
            $table->date('date_on')->nullable();
            $table->date('date_off')->nullable();
            $table->timestamps();
            $table->string('note',3000)->nullable();
            $table->string('admin_note',3000)->nullable();
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonuses');
    }
}
