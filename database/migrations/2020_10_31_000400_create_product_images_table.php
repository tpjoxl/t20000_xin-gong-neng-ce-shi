<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')->index();
            $table->string('path', 1000)->nullable();
            $table->string('filename', 1000)->nullable();
            $table->string('ext', 20)->nullable();
            $table->boolean('is_cover')->default(false);
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });

        Schema::create('product_image_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('product_image_id')->index();
            $table->string('locale', 20)->index();
            $table->string('description',255);

            $table->unique(['product_image_id', 'locale']);
            $table->foreign('product_image_id')->references('id')->on('product_images')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_image_translations');
        Schema::dropIfExists('product_images');
    }
}
