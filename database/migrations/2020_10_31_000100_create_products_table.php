<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('belong')->nullable()->comment('所屬單元');
            $table->string('num', 100)->nullable()->index();
            $table->boolean('status')->default(true);
            $table->boolean('home_status')->default(false);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->string('pic', 512)->nullable();
            $table->string('og_image', 512)->nullable();
            $table->string('meta_robots', 100)->nullable()->default('index, follow');
            $table->date('date_on')->nullable();
            $table->date('date_off')->nullable();
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });

        Schema::create('product_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('description', 255)->nullable();
            $table->longText('text')->nullable();
            $table->string('url', 512)->nullable();
            $table->string('target')->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->string('seo_keyword', 255)->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_description', 255)->nullable();

            $table->unique(['product_id', 'locale']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_translations');
        Schema::dropIfExists('products');
    }
}
