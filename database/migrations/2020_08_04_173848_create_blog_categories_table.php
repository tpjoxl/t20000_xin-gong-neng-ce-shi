<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_categories', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->default(true);
            $table->boolean('home_status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->string('pic', 512)->nullable();
            $table->string('og_image', 512)->nullable();
            $table->string('meta_robots', 100)->nullable()->default('index, follow');
            $table->date('date_on')->nullable();
            $table->date('date_off')->nullable();
            $table->timestamps();
            $table->foreignId('parent_id')->nullable()->constrained('blog_categories')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('deletable')->default(true);
        });
        Schema::create('blog_category_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('blog_category_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('slug')->nullable();
            $table->string('description', 255)->nullable();
            $table->longText('text')->nullable();
            $table->string('url', 512)->nullable();
            $table->string('target')->nullable();
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->string('seo_keyword', 255)->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_description', 255)->nullable();

            $table->unique(['blog_category_id', 'locale']);
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('blog_category', function (Blueprint $table) {
            $table->unsignedBigInteger('blog_id')->index();
            $table->unsignedBigInteger('blog_category_id')->index();
            $table->bigInteger('rank')->default(0);

            $table->primary(['blog_id', 'blog_category_id']);

            $table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('blog_category_id')->references('id')->on('blog_categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_categories');
        Schema::dropIfExists('blog_category_translations');
        Schema::dropIfExists('blog_category');
    }
}
