<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('provider');
            $table->string('type')->comment('金流類別');
            $table->string('user_type', 255)->nullable()->default('0,1,2')->comment('可使用的會員種類');
            $table->tinyInteger('fee_type')->nullable()->comment('手續費類型 1=百分比 2=固定金額');
            $table->integer('fee')->nullable()->default(0)->comment('手續費');
            $table->string('pic', 512)->nullable();
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('active')->default(false)->comment('啟用狀態');
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->boolean('deletable')->default(false);
        });
        Schema::create('payment_translations', function(Blueprint $table) {
            $table->id();
            $table->foreignId('payment_id')->index();
            $table->string('locale', 20)->index();
            $table->string('title');
            $table->string('text', 1000)->nullable();

            $table->unique(['payment_id', 'locale']);
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
        Schema::dropIfExists('payment_translations');
    }
}
