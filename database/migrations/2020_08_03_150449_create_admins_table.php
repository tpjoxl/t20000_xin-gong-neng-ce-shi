<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_groups', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('parent_id')->nullable()->constrained('admin_groups')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('level')->default(1);
            $table->boolean('status')->default(true);
            $table->boolean('is_top')->default(false);
            $table->bigInteger('rank')->default(0);
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('account')->unique();
            $table->string('password');
            $table->string('name');
            $table->boolean('status')->default(true)->comment('啟用狀態');
            $table->timestamp('login_at')->nullable();
            $table->ipAddress('ip')->nullable()->comment('登入IP');
            $table->foreignId('group_id')->comment('權限群組編號')->constrained('admin_groups')->onDelete('cascade')->onUpdate('cascade');
            $table->rememberToken();
            $table->boolean('passable')->default(false)->comment('直接修改密碼');
            $table->timestamps();
            $table->boolean('deletable')->default(true);
        });
        Schema::create('admin_group_power', function (Blueprint $table) {
            $table->unsignedBigInteger('admin_group_id');
            $table->unsignedBigInteger('power_id');
            $table->bigInteger('rank')->default(0);

            $table->primary(['admin_group_id', 'power_id']);

            $table->foreign('admin_group_id')->references('id')->on('admin_groups')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('power_id')->references('id')->on('powers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_group_power');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('admin_groups');
    }
}
