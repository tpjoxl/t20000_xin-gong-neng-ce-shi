<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewebpayRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newebpay_records', function (Blueprint $table) {
            $table->id();
            $table->string('route_name', 255)->nullable();
            $table->string('Status', 10)->nullable()->comment('回傳狀態');
            $table->string('Message', 50)->nullable()->comment('敘述此次交易狀態');
            $table->string('MerchantID', 20)->nullable()->comment('商店代號');
            $table->integer('Amt')->nullable()->comment('交易金額');
            $table->string('TradeNo', 20)->nullable()->comment('藍新金流交易序號');
            $table->string('MerchantOrderNo', 30)->nullable()->comment('商店訂單編號')->index();
            $table->string('PaymentType', 10)->nullable()->comment('支付方式');
            $table->string('RespondType', 10)->nullable()->comment('回傳格式');
            $table->dateTime('PayTime')->nullable()->comment('支付完成時間');
            $table->dateTime('ExpireDate')->nullable()->comment('繳費截止日期');
            $table->time('ExpireTime')->nullable()->comment('繳費截止日期');
            $table->string('IP', 15)->nullable()->comment('付款人取號或交易時的IP');
            $table->string('EscrowBank', 10)->nullable()->comment('款項保管銀行');
            $table->string('AuthBank', 10)->nullable()->comment('收單金融機構');
            $table->string('RespondCode', 5)->nullable()->comment('金融機構回應碼');
            $table->string('Auth', 6)->nullable()->comment('由收單機構所回應的授權碼');
            $table->string('Card6No', 6)->nullable()->comment('卡號前六碼');
            $table->string('Card4No', 4)->nullable()->comment('卡號末四碼');
            $table->integer('Inst')->nullable()->comment('信用卡分期交易期別');
            $table->integer('InstFirst')->nullable()->comment('信用卡分期交易首期金額');
            $table->integer('InstEach')->nullable()->comment('信用卡分期交易每期金額');
            $table->string('ECI', 2)->nullable()->comment('ECI值');
            $table->tinyInteger('TokenUseStatus')->nullable()->comment('信用卡快速結帳使用狀態');
            $table->smallInteger('RedAmt')->nullable()->comment('紅利折抵後實際金額');
            $table->string('PaymentMethod', 15)->nullable()->comment('交易類別');
            $table->float('DCC_Amt')->nullable()->comment('外幣金額');
            $table->float('DCC_Rate')->nullable()->comment('匯率');
            $table->float('DCC_Markup')->nullable()->comment('風險匯率');
            $table->string('DCC_Currency', 4)->nullable()->comment('幣別');
            $table->tinyInteger('DCC_Currency_Code')->nullable()->comment('幣別代碼');
            $table->string('BankCode', 10)->nullable()->comment('金融機構代碼');
            $table->string('PayBankCode', 10)->nullable()->comment('付款人金融機構代碼');
            $table->string('PayerAccount5Code', 5)->nullable()->comment('付款人金融機構帳號末五碼');
            $table->string('CodeNo', 30)->nullable()->comment('繳費代碼');
            $table->string('StoreID', 10)->nullable()->comment('繳費門市代號');
            $table->string('Barcode_1', 20)->nullable()->comment('第㇐段條碼');
            $table->string('Barcode_2', 20)->nullable()->comment('第二段條碼');
            $table->string('Barcode_3', 20)->nullable()->comment('第三段條碼');
            $table->string('PayStore', 8)->nullable()->comment('繳費超商');
            $table->string('P2GTradeNo', 25)->nullable()->comment('ezPay 交易序號');
            $table->string('P2GPaymentType', 10)->nullable()->comment('ezPay 支付方式');
            $table->integer('P2GAmt')->nullable()->comment('信用卡分期交易期別');
            $table->string('StoreCode', 10)->nullable()->comment('超商門市編號');
            $table->string('StoreName', 15)->nullable()->comment('繳費門市類別/超商門市名稱');
            $table->string('StoreType', 10)->nullable()->comment('超商類別名稱');
            $table->string('StoreAddr', 100)->nullable()->comment('超商門市地址');
            $table->tinyInteger('TradeType')->nullable()->comment('取件交易方式');
            $table->string('CVSCOMName', 20)->nullable()->comment('取貨人');
            $table->string('CVSCOMPhone', 10)->nullable()->comment('取貨人手機號碼');
            $table->string('ChannelID', 10)->nullable()->comment('跨境通路類型');
            $table->string('ChannelNo', 32)->nullable()->comment('跨境通路交易序號');
            $table->longText('TradeInfo')->nullable()->comment('交易資料 AES 加密');
            $table->longText('TradeSha')->nullable()->comment('交易資料 SHA256 加密');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newebpay_records');
    }
}
