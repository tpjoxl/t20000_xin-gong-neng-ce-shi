<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(15),
        'description' => $faker->realText(100),
        'text' => $faker->realText(400),
        'pic' => $faker->imageUrl(400,300),
    ];
});
