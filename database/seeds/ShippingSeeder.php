<?php

use Illuminate\Database\Seeder;
use App\Models\Power;
use App\Models\Shipping;

class ShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '運送方式管理',
            'icon' => 'fa fa-truck',
            'route_prefix' => 'shipping',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '基本參數設定',
                'name' => 'shipping.page',
                'route_name' => 'backend.shipping.page',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '運送方式 - 列表',
                'name' => 'shipping.index',
                'route_name' => 'backend.shipping.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '運送方式 - 新增',
                'name' => 'shipping.create',
                'route_name' => 'backend.shipping.create,backend.shipping.store,backend.shipping.copy,backend.shipping.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '運送方式 - 編輯',
                'name' => 'shipping.edit',
                'route_name' => 'backend.shipping.edit,backend.shipping.update,backend.shipping.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '運送方式 - 刪除',
                'name' => 'shipping.destroy',
                'route_name' => 'backend.shipping.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '運送方式 - 排序',
                'name' => 'shipping.rank',
                'route_name' => 'backend.shipping.rank.index,backend.shipping.rank.update,backend.shipping.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
        Shipping::make()->fillAndSave([
            'active' => 1,
            'type' => 1,
            'title' => '宅配',
            'price' => 100,
            'limit_order_price' => 1000,
            'limit_price' => 0,
        ]);
        Shipping::make()->fillAndSave([
            'active' => 1,
            'title' => '超商取貨付款',
            'price' => 60,
            'limit_order_price' => 1000,
            'limit_price' => 0,
        ]);
    }
}
