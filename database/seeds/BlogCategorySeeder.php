<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.blog').'管理',
            'icon' => 'fa fa-book',
            'route_prefix' => 'blog',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '分類 - 列表',
                'name' => 'blog.category.index',
                'route_name' => 'backend.blog.category.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '分類 - 新增',
                'name' => 'blog.category.create',
                'route_name' => 'backend.blog.category.create,backend.blog.category.store,backend.blog.category.copy,backend.blog.category.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 編輯',
                'name' => 'blog.category.edit',
                'route_name' => 'backend.blog.category.edit,backend.blog.category.update,backend.blog.category.modify,backend.blog.category.rank.update,backend.blog.category.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 刪除',
                'name' => 'blog.category.destroy',
                'route_name' => 'backend.blog.category.destroy',
                'status' => 0,
                'active' => 1,
            ],
        ]);


        // factory(use App\Models\BlogCategory::class, 3)->create()->each(function ($blogCategory) {
        //     $blogCategory->blogs()->createMany(factory(use App\Models\Blog::class, rand(2,5))->make()->toArray());
        // });
    }
}
