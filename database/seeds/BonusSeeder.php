<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class BonusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '紅利點數管理',
            'icon' => 'fa fa-gift',
            'route_prefix' => 'bonus',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '基本參數設定',
                'name' => 'bonus.page',
                'route_name' => 'backend.bonus.page',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '紅利點數 - 列表',
                'name' => 'bonus.index',
                'route_name' => 'backend.bonus.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '紅利點數 - 新增',
                'name' => 'bonus.create',
                'route_name' => 'backend.bonus.create,backend.bonus.store,backend.bonus.copy,backend.bonus.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '紅利點數 - 編輯',
                'name' => 'bonus.edit',
                'route_name' => 'backend.bonus.edit,backend.bonus.update,backend.bonus.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '紅利點數 - 刪除',
                'name' => 'bonus.destroy',
                'route_name' => 'backend.bonus.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '紅利點數 - 排序',
                'name' => 'bonus.rank',
                'route_name' => 'backend.bonus.rank.index,backend.bonus.rank.update,backend.bonus.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);

        // if (!method_exists(App\Bonus::class, 'categories')) {
        //     factory(App\Bonus::class, 10)->create();
        // }
    }
}
