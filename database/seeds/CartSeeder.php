<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class CartSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.cart').'頁面管理',
            'icon' => 'fa fa-shopping-cart',
            'route_prefix' => 'cart',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '頁面內容管理',
                'name' => 'cart.page',
                'route_name' => 'backend.cart.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
