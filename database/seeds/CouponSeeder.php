<?php

use Illuminate\Database\Seeder;
use App\Models\Power;
use App\Models\Coupon;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '優惠券管理',
            'icon' => 'fa fa-barcode',
            'route_prefix' => 'coupon',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '優惠券 - 列表',
                'name' => 'coupon.index',
                'route_name' => 'backend.coupon.index,backend.coupon.export',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '優惠券 - 新增',
                'name' => 'coupon.create',
                'route_name' => 'backend.coupon.create,backend.coupon.store,backend.coupon.copy,backend.coupon.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '優惠券 - 編輯',
                'name' => 'coupon.edit',
                'route_name' => 'backend.coupon.edit,backend.coupon.update,backend.coupon.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '優惠券 - 刪除',
                'name' => 'coupon.destroy',
                'route_name' => 'backend.coupon.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '優惠券 - 排序',
                'name' => 'coupon.rank',
                'route_name' => 'backend.coupon.rank.index,backend.coupon.rank.update,backend.coupon.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '生日優惠設定',
                'name' => 'coupon.page',
                'route_name' => 'backend.coupon.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
        Coupon::make()->fillAndSave([
            'title'  => '所有人適用',
            'use_type' => 1,
            'user_type' => [0,1,2],
            'code' => 'COUPON1',
            'type' => 1,
            'number' => 100,
            'min_price' => 100,
            'date_on' => '2021-01-01',
            'date_off' => '2021-12-31',
            'user_visible' => 0,
        ]);
        Coupon::make()->fillAndSave([
            'title'  => '會員適用',
            'use_type' => 1,
            'user_type' => [1,2],
            'code' => 'COUPON2',
            'type' => 1,
            'number' => 150,
            'min_price' => 100,
            'date_on' => '2021-01-01',
            'date_off' => '2021-12-31',
            'user_visible' => 1,
        ]);
        Coupon::make()->fillAndSave([
            'title'  => '一般會員適用',
            'use_type' => 1,
            'user_type' => [1],
            'code' => 'COUPON3',
            'type' => 1,
            'number' => 120,
            'min_price' => 0,
            'date_on' => '2021-01-01',
            'date_off' => '2021-12-31',
            'user_visible' => 1,
        ]);
        Coupon::make()->fillAndSave([
            'title'  => 'VIP會員適用',
            'use_type' => 1,
            'user_type' => [2],
            'code' => 'COUPON4',
            'type' => 1,
            'number' => 120,
            'min_price' => 0,
            'date_on' => '2021-01-01',
            'date_off' => '2021-12-31',
            'user_visible' => 1,
        ]);
    }
}
