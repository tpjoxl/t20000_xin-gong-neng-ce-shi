<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.terms').'管理',
            'icon' => 'fa fa-legal',
            'route_prefix' => 'terms',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '頁面內容管理',
                'name' => 'terms.page',
                'route_name' => 'backend.terms.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
