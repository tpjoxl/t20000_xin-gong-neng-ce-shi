<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class OrderLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '訂單管理',
            'icon' => 'fa fa-pencil-square-o',
            'route_prefix' => 'order',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '訂單 - 狀態歷史紀錄',
                'name' => 'order.log.index',
                'route_name' => 'backend.order.log.index',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
