<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '操作問題管理',
            'icon' => 'fa fa-question-circle',
            'route_prefix' => 'question',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '操作問題 - 列表',
                'name' => 'question.index',
                'route_name' => 'backend.question.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '操作問題 - 新增',
                'name' => 'question.create',
                'route_name' => 'backend.question.create,backend.question.store,backend.question.copy,backend.question.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '操作問題 - 編輯',
                'name' => 'question.edit',
                'route_name' => 'backend.question.edit,backend.question.update,backend.question.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '操作問題 - 刪除',
                'name' => 'question.destroy',
                'route_name' => 'backend.question.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '操作問題 - 排序',
                'name' => 'question.rank',
                'route_name' => 'backend.question.rank.index,backend.question.rank.update,backend.question.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
