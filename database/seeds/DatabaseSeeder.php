<?php

use Illuminate\Database\Seeder;

use App\Models\AdminGroup;
use App\Models\Power;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocaleSeeder::class);
        $this->call(PowerSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(LocaleTextSeeder::class);
        $this->call(TwCitySeeder::class);
        $this->call(TwRegionSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(QuestionCategorySeeder::class);
        $this->call(QuestionSeeder::class);
        $this->call(RecordSeeder::class);
        $this->call(AboutSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(ProductImageSeeder::class);
        $this->call(ProductSkuSeeder::class);
        $this->call(BlogCategorySeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(BlogTagSeeder::class);
        $this->call(FaqCategorySeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(ContactSeeder::class);
        $this->call(TermsSeeder::class);
        $this->call(PrivacySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CartSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(OrderLogSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(ShippingSeeder::class);
        $this->call(CouponSeeder::class);
        $this->call(BonusSeeder::class);
        // $this->call(MailSeeder::class);
        $this->call(SubscriberSeeder::class);
        $this->call(ReportSeeder::class);

        $adminGroup = AdminGroup::firstOrCreate([
            'title' => env('MASTER_GROUP'),
        ]);
        $adminGroup->powers()->sync(Power::all()->pluck('id')->toArray());
    }
}
