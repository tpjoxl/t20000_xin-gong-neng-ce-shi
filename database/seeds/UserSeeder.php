<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '會員管理',
            'icon' => 'fa fa-user',
            'route_prefix' => 'user',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '會員 - 列表',
                'name' => 'user.index',
                'route_name' => 'backend.user.index,backend.user.export',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '會員 - 新增',
                'name' => 'user.create',
                'route_name' => 'backend.user.create,backend.user.store',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '會員 - 編輯',
                'name' => 'user.edit',
                'route_name' => 'backend.user.edit,backend.user.update,backend.user.modify,backend.user.password',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '會員 - 刪除',
                'name' => 'user.destroy',
                'route_name' => 'backend.user.destroy',
                'status' => 0,
                'active' => 1,
            ],
            // [
            //     'title' => '列表頁SEO管理',
            //     'name' => 'user.page',
            //     'route_name' => 'backend.user.page',
            //     'status' => 1,
            //     'active' => 1,
            // ],
        ]);
    }
}
