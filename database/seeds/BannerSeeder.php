<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::where('route_prefix', 'site')->first();
        $power->children()->createMany([
            [
                'title' => '首頁Banner輪播圖 - 列表',
                'name' => 'banner.index',
                'route_name' => 'backend.banner.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '首頁Banner輪播圖 - 新增',
                'name' => 'banner.create',
                'route_name' => 'backend.banner.create,backend.banner.store,backend.banner.copy,backend.banner.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '首頁Banner輪播圖 - 編輯',
                'name' => 'banner.edit',
                'route_name' => 'backend.banner.edit,backend.banner.update,backend.banner.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '首頁Banner輪播圖 - 刪除',
                'name' => 'banner.destroy',
                'route_name' => 'backend.banner.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '首頁Banner輪播圖 - 排序',
                'name' => 'banner.rank',
                'route_name' => 'backend.banner.rank.index,backend.banner.rank.update,backend.banner.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
