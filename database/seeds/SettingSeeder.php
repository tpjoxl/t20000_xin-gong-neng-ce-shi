<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insertDatas = [];
        foreach (env('APP_ENV')!='install'?app('frontendLocales')->pluck('code')->toArray():config('translatable.locales') as $locale_code) {
            $insertDatas[] = [
                'setting_name' => 'name',
                'setting_value' => env('APP_NAME'),
                'type' => 'text',
                'locale' => $locale_code,
                'page' => 'backend.site.info',
            ];
        }
        $datas = [
            [
                'setting_name' => 'backend_name',
                'setting_value' => env('APP_NAME'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.setting',
            ],
            [
                'setting_name' => 'site_mail',
                'setting_value' => env('SITE_MAIL'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.setting',
            ],
            [
                'setting_name' => 'site_url',
                'setting_value' => env('APP_URL'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.setting',
            ],
            [
                'setting_name' => 'head_code',
                'setting_value' => null,
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.setting',
            ],
            [
                'setting_name' => 'body_code',
                'setting_value' => null,
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.setting',
            ],
            [
                'setting_name' => 'smtp',
                'setting_value' => env('MAIL_HOST'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.smtp',
            ],
            [
                'setting_name' => 'smtp_port',
                'setting_value' => env('MAIL_PORT'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.smtp',
            ],
            [
                'setting_name' => 'smtp_account',
                'setting_value' => env('MAIL_USERNAME'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.smtp',
            ],
            [
                'setting_name' => 'smtp_password',
                'setting_value' => encrypt(env('MAIL_PASSWORD')),
                'type' => 'password',
                'locale' => 'common',
                'page' => 'backend.site.smtp',
            ],
            [
                'setting_name' => 'smtp_encryption',
                'setting_value' => env('MAIL_ENCRYPTION'),
                'type' => 'text',
                'locale' => 'common',
                'page' => 'backend.site.smtp',
            ],
        ];
        $insertDatas = array_merge($insertDatas, $datas);

        DB::table('settings')->insert($insertDatas);
    }
}
