<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Locale;
use App\Models\Power;

class LocaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locales')->insert([
            [
                'code' => 'zh-TW',
                'title' => '繁體中文',
                'abbr' => 'TW',
                'status' => 1,
                'frontend_status' => 1,
                'deletable' => 0,
            ],
            // [
            //     'code' => 'zh-CN',
            //     'title' => '简体中文',
            //     'abbr' => 'CN',
            //     'status' => 0,
            //     'frontend_status' => 0,
            //     'deletable' => 1,
            // ],
            [
                'code' => 'en',
                'title' => 'English',
                'abbr' => 'EN',
                'status' => 1,
                'frontend_status' => 1,
                'deletable' => 1,
            ],
            // [
            //     'code' => 'ja',
            //     'title' => '日本語',
            //     'abbr' => 'JP',
            //     'status' => 0,
            //     'frontend_status' => 0,
            //     'deletable' => 1,
            // ],
        ]);
        Locale::where('code', env('BACKEND_LOCALE'))->update(['backend_use'=>1]);
    }
}
