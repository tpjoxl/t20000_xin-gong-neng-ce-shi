<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.contact').'管理',
            'icon' => 'fa fa-paper-plane',
            'route_prefix' => 'contact',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '頁面內容管理',
                'name' => 'contact.page',
                'route_name' => 'backend.contact.page',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '表單 - 列表',
                'name' => 'contact.index',
                'route_name' => 'backend.contact.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '表單 - 編輯',
                'name' => 'contact.edit',
                'route_name' => 'backend.contact.edit,backend.contact.update,backend.contact.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '表單 - 刪除',
                'name' => 'contact.destroy',
                'route_name' => 'backend.contact.destroy',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
