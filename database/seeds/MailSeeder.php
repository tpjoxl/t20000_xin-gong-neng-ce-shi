<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Locale;
use App\Models\Power;

class MailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '信件內容管理',
            'icon' => 'fa fa-envelope-o',
            'route_prefix' => 'mail',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '[會員]註冊信箱驗證',
                'name' => 'mail.user.register',
                'route_name' => 'backend.mail.user.register',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]註冊信箱驗證',
                'name' => 'mail.senior.register',
                'route_name' => 'backend.mail.senior.register',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]忘記密碼',
                'name' => 'mail.user.forgot',
                'route_name' => 'backend.mail.user.forgot',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]忘記密碼',
                'name' => 'mail.senior.forgot',
                'route_name' => 'backend.mail.senior.forgot',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]優惠券發送通知',
                'name' => 'mail.user.coupon',
                'route_name' => 'backend.mail.user.coupon',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]新預約訂單建立通知信',
                'name' => 'mail.user.ordercreate',
                'route_name' => 'backend.mail.user.ordercreate',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]新預約訂單通知信(提醒回覆)',
                'name' => 'mail.senior.ordercreate',
                'route_name' => 'backend.mail.senior.ordercreate',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]前輩回覆信(拒絕預約)',
                'name' => 'mail.user.orderreject',
                'route_name' => 'backend.mail.user.orderreject',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]前輩回覆信(答應預約，附繳費資訊)',
                'name' => 'mail.user.orderaccept',
                'route_name' => 'backend.mail.user.orderaccept',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]訂單成立信(會員已繳費)',
                'name' => 'mail.senior.orderpaid',
                'route_name' => 'backend.mail.senior.orderpaid',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]前輩分享提醒信',
                'name' => 'mail.user.reminder',
                'route_name' => 'backend.mail.user.reminder',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]會員預約提醒信',
                'name' => 'mail.senior.reminder',
                'route_name' => 'backend.mail.senior.reminder',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[會員]會員停權通知信',
                'name' => 'mail.user.ban',
                'route_name' => 'backend.mail.user.ban',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '[前輩]前輩停權通知信',
                'name' => 'mail.senior.ban',
                'route_name' => 'backend.mail.senior.ban',
                'status' => 1,
                'active' => 1,
            ],
        ]);

        $insertDatas = [];
        foreach (Locale::display()->get() as $locale) {
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 註冊驗證信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.register',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您填寫了<b>「 會員註冊 」</b>表單，請點擊下面的連結網址以完成註冊：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.register',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 註冊驗證信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.register',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您填寫了<b>「 前輩註冊 」</b>表單，請點擊下面的連結網址以完成註冊：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.register',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 忘記密碼通知',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.forgot',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您填寫了<b>「 忘記密碼 」</b>表單，產生的新密碼如下，因安全性考量，請在登入後立即修改成您自己的密碼：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.forgot',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 忘記密碼通知',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.forgot',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您填寫了<b>「 忘記密碼 」</b>表單，產生的新密碼如下，因安全性考量，請在登入後立即修改成您自己的密碼：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.forgot',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 優惠券發送通知',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.coupon',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的優惠券已發送',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.coupon',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 新預約訂單建立通知信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.ordercreate',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的新預約訂單已建立',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.ordercreate',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 新預約訂單建立通知信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.ordercreate',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的新預約訂單已建立',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.ordercreate',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 前輩回覆信(拒絕預約)',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.orderreject',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的預約訂單已被拒絕',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.orderreject',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 前輩回覆信(預約成功)',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.orderaccept',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的預約訂單已成功。<br />繳費資訊如下：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.orderaccept',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - [前輩]訂單成立信(會員已繳費)',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.orderpaid',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的訂單已成立(會員已繳費)',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.orderpaid',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 前輩分享提醒信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.reminder',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />前輩分享提醒',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.reminder',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 會員預約提醒信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.reminder',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />會員預約提醒',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.reminder',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 會員停權通知信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.ban',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的會員帳號已停權<br />停權理由如下：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.user.ban',
            ];
            $insertDatas[] = [
                'setting_name' => 'title',
                'setting_value' => '##site_name## - 前輩停權通知信',
                'type' => 'text_input',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.ban',
            ];
            $insertDatas[] = [
                'setting_name' => 'text',
                'setting_value' => '##user_name## 您好：<br />您的前輩帳號已停權<br />停權理由如下：',
                'type' => 'text_editor_simple',
                'locale' => $locale->code,
                'page' => 'backend.mail.senior.ban',
            ];
        }
        DB::table('settings')->insert($insertDatas);
    }
}
