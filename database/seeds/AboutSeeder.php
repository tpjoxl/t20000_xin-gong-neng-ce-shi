<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.about').'管理',
            'icon' => 'fa fa-file-text-o',
            'route_prefix' => 'about',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '頁面內容管理',
                'name' => 'about.page',
                'route_name' => 'backend.about.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
