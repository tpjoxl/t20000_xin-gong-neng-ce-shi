<?php

use Illuminate\Database\Seeder;
use App\Models\AdminGroup;
use App\Models\Power;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminGroup = AdminGroup::firstOrCreate([
            'title' => env('MASTER_GROUP'),
            'deletable' => false,
        ]);
        $adminGroup->admins()->create([
            'account' => '37dadmin',
            'password' => env('BACKEND_ADMIN_PASSWORD'),
            'name' => '37管理員',
            'passable' => 1,
            'deletable' => false,
        ]);

        $adminGroup->children()->create([
            'title' => 'admin_group',
        ]);
    }
}
