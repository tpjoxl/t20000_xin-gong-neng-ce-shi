<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.blog').'管理',
            'icon' => 'fa fa-book',
            'route_prefix' => 'blog',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '文章 - 列表',
                'name' => 'blog.index',
                'route_name' => 'backend.blog.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '文章 - 新增',
                'name' => 'blog.create',
                'route_name' => 'backend.blog.create,backend.blog.store,backend.blog.copy,backend.blog.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '文章 - 編輯',
                'name' => 'blog.edit',
                'route_name' => 'backend.blog.edit,backend.blog.update,backend.blog.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '文章 - 刪除',
                'name' => 'blog.destroy',
                'route_name' => 'backend.blog.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '文章 - 排序',
                'name' => 'blog.rank',
                'route_name' => 'backend.blog.rank.index,backend.blog.rank.update,backend.blog.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '列表頁SEO管理',
                'name' => 'blog.page',
                'route_name' => 'backend.blog.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);

        // if (!method_exists(use App\Models\Blog::class, 'categories')) {
        //     factory(use App\Models\Blog::class, 10)->create();
        // }
    }
}
