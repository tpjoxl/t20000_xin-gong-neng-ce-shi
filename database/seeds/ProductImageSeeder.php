<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.product').'管理',
            'icon' => 'fa fa-shopping-bag',
            'route_prefix' => 'product',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '圖片 - 管理',
                'name' => 'product.image.index',
                'route_name' => 'backend.product.image.index,backend.product.image.store,backend.product.image.update,backend.product.image.destroy,backend.product.image.rank.update,backend.product.image.cover',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
