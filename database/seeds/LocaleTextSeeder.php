<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Power;

class LocaleTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '語系文字管理',
            'icon' => 'fa fa-globe',
            'route_prefix' => 'locale',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '語系 - 列表',
                'name' => 'locale.index',
                'route_name' => 'backend.locale.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '語系 - 新增',
                'name' => 'locale.create',
                'route_name' => 'backend.locale.create,backend.locale.store,backend.locale.copy,backend.locale.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '語系 - 編輯',
                'name' => 'locale.edit',
                'route_name' => 'backend.locale.edit,backend.locale.update,backend.locale.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '語系 - 刪除',
                'name' => 'locale.destroy',
                'route_name' => 'backend.locale.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '語系 - 排序',
                'name' => 'locale.rank',
                'route_name' => 'backend.locale.rank.index,backend.locale.rank.update,backend.locale.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '文字內容管理',
                'name' => 'locale.text.index',
                'route_name' => 'backend.locale.text.index',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
