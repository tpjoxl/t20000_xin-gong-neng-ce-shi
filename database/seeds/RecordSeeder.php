<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class RecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '修改紀錄管理',
            'icon' => 'fa fa-question-circle',
            'route_prefix' => 'record',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '修改紀錄 - 列表',
                'name' => 'record.index',
                'route_name' => 'backend.record.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '修改紀錄 - 新增',
                'name' => 'record.create',
                'route_name' => 'backend.record.create,backend.record.store,backend.record.copy,backend.record.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '修改紀錄 - 編輯',
                'name' => 'record.edit',
                'route_name' => 'backend.record.edit,backend.record.update,backend.record.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '修改紀錄 - 刪除',
                'name' => 'record.destroy',
                'route_name' => 'backend.record.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '修改紀錄 - 排序',
                'name' => 'record.rank',
                'route_name' => 'backend.record.rank.index,backend.record.rank.update,backend.record.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
