<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class FaqCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.faq').'管理',
            'icon' => 'fa fa-question-circle',
            'route_prefix' => 'faq',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '分類 - 列表',
                'name' => 'faq.category.index',
                'route_name' => 'backend.faq.category.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '分類 - 新增',
                'name' => 'faq.category.create',
                'route_name' => 'backend.faq.category.create,backend.faq.category.store,backend.faq.category.copy,backend.faq.category.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 編輯',
                'name' => 'faq.category.edit',
                'route_name' => 'backend.faq.category.edit,backend.faq.category.update,backend.faq.category.modify,backend.faq.category.rank.update,backend.faq.category.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 刪除',
                'name' => 'faq.category.destroy',
                'route_name' => 'backend.faq.category.destroy',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
