<?php

use Illuminate\Database\Seeder;
use App\Models\Power;
use App\Models\ProductCategory;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.product').'管理',
            'icon' => 'fa fa-shopping-bag',
            'route_prefix' => 'product',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '分類 - 列表',
                'name' => 'product.category.index',
                'route_name' => 'backend.product.category.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '分類 - 新增',
                'name' => 'product.category.create',
                'route_name' => 'backend.product.category.create,backend.product.category.store,backend.product.category.copy,backend.product.category.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 編輯',
                'name' => 'product.category.edit',
                'route_name' => 'backend.product.category.edit,backend.product.category.update,backend.product.category.modify,backend.product.category.rank.update,backend.product.category.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '分類 - 刪除',
                'name' => 'product.category.destroy',
                'route_name' => 'backend.product.category.destroy',
                'status' => 0,
                'active' => 1,
            ],
        ]);

        // $datas = [
        //     [
        //         'title' => '分類1',
        //         'slug' => '分類1',
        //     ],
        //     [
        //         'title' => '分類2',
        //         'slug' => '分類2',
        //     ],
        //     [
        //         'title' => '分類3',
        //         'slug' => '分類3',
        //     ],
        //     [
        //         'title' => '分類4',
        //         'slug' => '分類4',
        //     ],
        //     [
        //         'title' => '分類5',
        //         'slug' => '分類5',
        //     ],
        //     [
        //         'title' => '分類6',
        //         'slug' => '分類6',
        //     ],
        // ];

        // foreach ($datas as $data) {
        //     ProductCategory::create($data);
        // }
    }
}
