<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class SubscriberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '電子報訂閱管理',
            'icon' => 'fa fa-envelope-open-o',
            'route_prefix' => 'subscriber',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '訂閱者 - 列表',
                'name' => 'subscriber.index',
                'route_name' => 'backend.subscriber.index,backend.subscriber.export',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '訂閱者 - 新增',
                'name' => 'subscriber.create',
                'route_name' => 'backend.subscriber.create,backend.subscriber.store,backend.subscriber.copy,backend.subscriber.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '訂閱者 - 編輯',
                'name' => 'subscriber.edit',
                'route_name' => 'backend.subscriber.edit,backend.subscriber.update,backend.subscriber.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '訂閱者 - 刪除',
                'name' => 'subscriber.destroy',
                'route_name' => 'backend.subscriber.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '訂閱者 - 排序',
                'name' => 'subscriber.rank',
                'route_name' => 'backend.subscriber.rank.index,backend.subscriber.rank.update,backend.subscriber.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
