<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class ProductSkuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.product').'管理',
            'icon' => 'fa fa-shopping-bag',
            'route_prefix' => 'product',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '品項與價格 - 列表',
                'name' => 'product.sku.index',
                'route_name' => 'backend.product.sku.index,backend.product.sku.export',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '品項與價格 - 新增',
                'name' => 'product.sku.create',
                'route_name' => 'backend.product.sku.create,backend.product.sku.store,backend.product.sku.copy,backend.product.sku.copystore,backend.product.sku.import',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '品項與價格 - 編輯',
                'name' => 'product.sku.edit',
                'route_name' => 'backend.product.sku.edit,backend.product.sku.update,backend.product.sku.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '品項與價格 - 刪除',
                'name' => 'product.sku.destroy',
                'route_name' => 'backend.product.sku.destroy',
                'status' => 0,
                'active' => 1,
            ],
            // [
            //     'title' => '基本參數設定',
            //     'name' => 'product.sku.page',
            //     'route_name' => 'backend.product.sku.page',
            //     'status' => 1,
            //     'active' => 1,
            // ],
        ]);
    }
}
