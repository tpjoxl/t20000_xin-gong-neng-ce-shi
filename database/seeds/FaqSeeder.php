<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.faq').'管理',
            'icon' => 'fa fa-question-circle',
            'route_prefix' => 'faq',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '問答 - 列表',
                'name' => 'faq.index',
                'route_name' => 'backend.faq.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '問答 - 新增',
                'name' => 'faq.create',
                'route_name' => 'backend.faq.create,backend.faq.store,backend.faq.copy,backend.faq.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '問答 - 編輯',
                'name' => 'faq.edit',
                'route_name' => 'backend.faq.edit,backend.faq.update,backend.faq.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '問答 - 刪除',
                'name' => 'faq.destroy',
                'route_name' => 'backend.faq.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '問答 - 排序',
                'name' => 'faq.rank',
                'route_name' => 'backend.faq.rank.index,backend.faq.rank.update,backend.faq.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '列表頁SEO管理',
                'name' => 'faq.page',
                'route_name' => 'backend.faq.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
