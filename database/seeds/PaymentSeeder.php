<?php

use Illuminate\Database\Seeder;
use App\Models\Power;
use App\Models\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '付款方式管理',
            'icon' => 'fa fa-money',
            'route_prefix' => 'payment',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '付款方式 - 列表',
                'name' => 'payment.index',
                'route_name' => 'backend.payment.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '付款方式 - 編輯',
                'name' => 'payment.edit',
                'route_name' => 'backend.payment.edit,backend.payment.update,backend.payment.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '付款方式 - 排序',
                'name' => 'payment.rank',
                'route_name' => 'backend.payment.rank.index,backend.payment.rank.update,backend.payment.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
        $datas = [
            [
                'provider' => 'NCCCPAY',
                'type' => 'CREDIT',
                'active' => 1,
                'title' => '信用卡(聯合信用)',
            ],
            [
                'provider' => 'ECPAY',
                'type' => 'CREDIT',
                'active' => 1,
                'title' => '信用卡(綠界)',
            ],
            [
                'provider' => 'ECPAY',
                'type' => 'ATM',
                'active' => 1,
                'title' => 'ATM虛擬帳號轉帳(綠界)',
            ],
            [
                'provider' => 'ECPAY',
                'type' => 'CVS',
                'active' => 1,
                'title' => '超商代碼(綠界)',
            ],
            [
                'provider' => 'ECPAY',
                'type' => 'BARCODE',
                'active' => 1,
                'title' => '超商條碼(綠界)',
            ],
            [
                'provider' => 'SYSTEM',
                'type' => 'ATM',
                'active' => 1,
                'title' => 'ATM匯款',
            ],
            [
                'provider' => 'SYSTEM',
                'type' => 'COD',
                'active' => 1,
                'title' => '貨到付款',
            ],
            [
                'provider' => 'SYSTEM',
                'type' => 'COP',
                'active' => 1,
                'title' => '取貨付款', // 自取現場付款
            ],
            [
                'provider' => 'LINEPAY',
                'type' => 'LINEPAY',
                'active' => 1,
                'title' => 'LinePay',
            ],
            [
                'provider' => 'JKOPAY',
                'type' => 'JKOPAY',
                'active' => 1,
                'title' => '街口支付',
            ],
            [
                'provider' => 'NEWEBPAY',
                'type' => 'CREDIT',
                'active' => 1,
                'title' => '信用卡(藍新)',
            ],
            [
                'provider' => 'NEWEBPAY',
                'type' => 'ATM',
                'active' => 1,
                'title' => 'ATM虛擬帳戶轉帳(藍新)',
            ],
            [
                'provider' => 'NEWEBPAY',
                'type' => 'CVS',
                'active' => 1,
                'title' => '超商代碼(藍新)',
            ],
            [
                'provider' => 'NEWEBPAY',
                'type' => 'BARCODE',
                'active' => 1,
                'title' => '超商條碼(藍新)',
            ],
        ];
        foreach ($datas as $data) {
            Payment::create($data);
        }
    }
}
