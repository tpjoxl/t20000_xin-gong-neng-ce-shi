<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class PrivacySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.privacy').'管理',
            'icon' => 'fa fa-user-secret',
            'route_prefix' => 'privacy',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '頁面內容管理',
                'name' => 'privacy.page',
                'route_name' => 'backend.privacy.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
