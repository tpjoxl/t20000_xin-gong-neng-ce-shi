<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.product').'管理',
            'icon' => 'fa fa-shopping-bag',
            'route_prefix' => 'product',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '商品 - 列表',
                'name' => 'product.index',
                'route_name' => 'backend.product.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '商品 - 新增',
                'name' => 'product.create',
                'route_name' => 'backend.product.create,backend.product.store,backend.product.copy,backend.product.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '商品 - 編輯',
                'name' => 'product.edit',
                'route_name' => 'backend.product.edit,backend.product.update,backend.product.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '商品 - 刪除',
                'name' => 'product.destroy',
                'route_name' => 'backend.product.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '商品 - 排序',
                'name' => 'product.rank',
                'route_name' => 'backend.product.rank.index,backend.product.rank.update,backend.product.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '列表頁SEO管理',
                'name' => 'product.page',
                'route_name' => 'backend.product.page',
                'status' => 1,
                'active' => 1,
            ],
        ]);

        // if (!method_exists(use App\Models\Product::class, 'categories')) {
        //     factory(use App\Models\Product::class, 10)->create();
        // }
    }
}
