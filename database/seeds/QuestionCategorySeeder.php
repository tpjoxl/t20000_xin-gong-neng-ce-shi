<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class QuestionCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '操作問題管理',
            'icon' => 'fa fa-question-circle',
            'route_prefix' => 'question',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '操作問題分類 - 列表',
                'name' => 'question.category.index',
                'route_name' => 'backend.question.category.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '操作問題分類 - 新增',
                'name' => 'question.category.create',
                'route_name' => 'backend.question.category.create,backend.question.category.store,backend.question.category.copy,backend.question.category.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '操作問題分類 - 編輯',
                'name' => 'question.category.edit',
                'route_name' => 'backend.question.category.edit,backend.question.category.update,backend.question.category.modify,backend.question.category.rank.update,backend.question.category.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '操作問題分類 - 刪除',
                'name' => 'question.category.destroy',
                'route_name' => 'backend.question.category.destroy',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
