<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '統計報表',
            'icon' => 'fa fa-bar-chart',
            'route_prefix' => 'report',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '產品 - 報表',
                'name' => 'report.product.date',
                'route_name' => 'backend.report.product.date,backend.report.product.date.export',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '訂單 - 銷售圖表',
                'name' => 'report.orders.chart',
                'route_name' => 'backend.report.orders.chart,backend.report.orders.chart.export',
                'status' => 1,
                'active' => 1,
            ],
        ]);
    }
}
