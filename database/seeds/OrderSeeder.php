<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => '訂單管理',
            'icon' => 'fa fa-pencil-square-o',
            'route_prefix' => 'order',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '訂單 - 列表',
                'name' => 'order.index',
                'route_name' => 'backend.order.index,backend.order.export',
                'status' => 1,
                'active' => 1,
            ],
            // [
            //     'title' => '訂單 - 新增',
            //     'name' => 'order.create',
            //     'route_name' => 'backend.order.create,backend.order.store,backend.order.copy,backend.order.copystore,backend.order.import',
            //     'status' => 0,
            //     'active' => 1,
            // ],
            [
                'title' => '訂單 - 編輯',
                'name' => 'order.edit',
                'route_name' => 'backend.order.edit,backend.order.update,backend.order.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '訂單 - 刪除',
                'name' => 'order.destroy',
                'route_name' => 'backend.order.destroy',
                'status' => 0,
                'active' => 1,
            ],
            // [
            //     'title' => '訂單 - 排序',
            //     'name' => 'order.rank',
            //     'route_name' => 'backend.order.rank.index,backend.order.rank.update,backend.order.rank.reset',
            //     'status' => 0,
            //     'active' => 1,
            // ],
        ]);
    }
}
