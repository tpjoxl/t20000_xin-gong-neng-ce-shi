<?php

use Illuminate\Database\Seeder;
use App\Models\Power;

class BlogTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $power = Power::firstOrCreate([
            'title' => __('text.blog').'管理',
            'icon' => 'fa fa-book',
            'route_prefix' => 'blog',
            'status' => 1,
            'active' => 1,
        ]);
        $power->children()->createMany([
            [
                'title' => '標籤 - 列表',
                'name' => 'blog.tag.index',
                'route_name' => 'backend.blog.tag.index',
                'status' => 1,
                'active' => 1,
            ],
            [
                'title' => '標籤 - 新增',
                'name' => 'blog.tag.create',
                'route_name' => 'backend.blog.tag.create,backend.blog.tag.store,backend.blog.tag.copy,backend.blog.tag.copystore',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '標籤 - 編輯',
                'name' => 'blog.tag.edit',
                'route_name' => 'backend.blog.tag.edit,backend.blog.tag.update,backend.blog.tag.modify',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '標籤 - 刪除',
                'name' => 'blog.tag.destroy',
                'route_name' => 'backend.blog.tag.destroy',
                'status' => 0,
                'active' => 1,
            ],
            [
                'title' => '標籤 - 排序',
                'name' => 'blog.tag.rank',
                'route_name' => 'backend.blog.tag.rank.index,backend.blog.tag.rank.update,backend.blog.tag.rank.reset',
                'status' => 0,
                'active' => 1,
            ],
        ]);
    }
}
