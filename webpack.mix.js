const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/main.js', 'public/js')
    .sass('resources/sass/main.scss', 'public/css')
    .sass('resources/sass/editor.scss', 'public/css')
    .options({
        postCss: [
          require('autoprefixer')({
              cascade: false
          })
        ]
    }).sourceMaps();

if (mix.inProduction()) {
    mix.version();
}
